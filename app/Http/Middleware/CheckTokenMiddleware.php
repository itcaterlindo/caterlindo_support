<?php

namespace App\Http\Middleware;

use Closure;

class CheckTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorize = $request->header('Authorization');
        if ($authorize === env('APP_API_TOKEN')){
            return $next($request);
        }else{
            return response()->json(['code' => 401, 'status' => 'Unauthorized']);
        }
    }
}
