<?php

namespace App\Http\Controllers;

use App\Stok_Opname;
use Illuminate\Http\Request;

class StokOpnameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function show(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stok_Opname $stok_Opname)
    {
        //
    }
}
