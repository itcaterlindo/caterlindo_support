<?php

namespace App\Http\Controllers\IT\MonitoringServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TempserverModel;
use DB;
use PDF;

use Illuminate\Support\Facades\Auth;

class TempserverController extends Controller
{
    private $class_link = 'it/monitoringserver/tempnhumidity';

    public function index(Request $request)
    {
		$temp = $request->temp;
		$humidity = $request->humidity;

		$arrayData = [
            'tempserver_temp' => $temp,
            'tempserver_humidity' => $humidity,
        ];
        $act = TempserverModel::create($arrayData);
        return $act;
    }

    public function temphumidity_live()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tempserver = TempserverModel::orderBy('tempserver_id', 'desc')
            ->first();
        
        $avgMonthlyPagi = TempserverModel::select(DB::raw('AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
                    ->whereYear('tempserver_created_at', $tahun)
                    ->whereMonth('tempserver_created_at', $bulan)
                    ->whereTime('tempserver_created_at', '>=', '07:00:00')
                    ->whereTime('tempserver_created_at', '<=', '19:00:00')
                    ->first();

        $avgMonthlySiang = TempserverModel::select(DB::raw('AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
                    ->whereYear('tempserver_created_at', $tahun)
                    ->whereMonth('tempserver_created_at', $bulan)
                    ->whereTime('tempserver_created_at', '>=', '12:00:00')
                    ->whereTime('tempserver_created_at', '<=', '14:00:00')
                    ->first();
        
        $data['class_link'] = $this->class_link;
        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
        $data['maxid'] = $tempserver;
        $data['avgMonthlyPagi'] = $avgMonthlyPagi ;
        $data['avgMonthlySiang'] = $avgMonthlySiang;

        return view('page/'.$this->class_link.'/tempnhumidity_live', $data);
    }

    public function temphumidityreport_box()
    {
        $data['opsiBulan'] = [
            ['id' => '01', 'text' => 'Januari'],
            ['id' => '02', 'text' => 'Februari'],
            ['id' => '03', 'text' => 'Maret'],
            ['id' => '04', 'text' => 'April'],
            ['id' => '05', 'text' => 'Mei'],
            ['id' => '06', 'text' => 'Juni'],
            ['id' => '07', 'text' => 'Juli'],
            ['id' => '08', 'text' => 'Agustus'],
            ['id' => '09', 'text' => 'September'],
            ['id' => '10', 'text' => 'Oktober'],
            ['id' => '11', 'text' => 'Nopember'],
            ['id' => '12', 'text' => 'Desember'],
        ];
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/tempnhumidityreport_box', $data);

    }

    public function temphumidityreport_table(Request $request) {
        $tahun = $request->tahun;
        $bulan = $request->bulan;
        $avgMonthlyPagi = TempserverModel::select(DB::raw('DATE(tempserver_created_at) as day, AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
            ->whereYear('tempserver_created_at', $tahun)
            ->whereMonth('tempserver_created_at', $bulan)
            ->whereTime('tempserver_created_at', '>=', '07:00:00')
            ->whereTime('tempserver_created_at', '<=', '09:00:00')
            ->groupBy(DB::raw('DATE(tempserver_created_at)'))
            ->get();
        $avgMonthlySiang = TempserverModel::select(DB::raw('DATE(tempserver_created_at) as day, AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
            ->whereYear('tempserver_created_at', $tahun)
            ->whereMonth('tempserver_created_at', $bulan)
            ->whereTime('tempserver_created_at', '>=', '12:00:00')
            ->whereTime('tempserver_created_at', '<=', '14:00:00')
            ->groupBy(DB::raw('DATE(tempserver_created_at)'))
            ->get();

        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
        $data['avgMonthlyPagi'] = $avgMonthlyPagi;
        $data['avgMonthlySiang'] = $avgMonthlySiang;

        return view('page/'.$this->class_link.'/tempnhumidityreport_table', $data);
    }

    public function temphumidityreport_object(Request $request) {
        $data['tahun'] = $request->tahun;
        $data['bulan'] = $request->bulan;
        $url = url($this->class_link.'/temphumidityreport_pdf/?tahun='.$request->tahun.'&bulan='.$request->bulan);

        $html = '<object type="application/pdf" width="100%" height="500px" data="'.$url.'"></object>';

        return $html;
    }

    public function temphumidityreport_pdf (Request $request) {
        $tahun = $request->tahun;
        $bulan = $request->bulan;
        $avgMonthlyPagi = TempserverModel::select(DB::raw('DATE(tempserver_created_at) as day, AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
            ->whereYear('tempserver_created_at', $tahun)
            ->whereMonth('tempserver_created_at', $bulan)
            ->whereTime('tempserver_created_at', '>=', '07:00:00')
            ->whereTime('tempserver_created_at', '<=', '09:00:00')
            ->groupBy(DB::raw('DATE(tempserver_created_at)'))
            ->get();
        $avgMonthlySiang = TempserverModel::select(DB::raw('DATE(tempserver_created_at) as day, AVG(tempserver_temp) as avg_temp, AVG(tempserver_humidity) as avg_humidity'))
            ->whereYear('tempserver_created_at', $tahun)
            ->whereMonth('tempserver_created_at', $bulan)
            ->whereTime('tempserver_created_at', '>=', '12:00:00')
            ->whereTime('tempserver_created_at', '<=', '14:00:00')
            ->groupBy(DB::raw('DATE(tempserver_created_at)'))
            ->get();

        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
        $data['avgMonthlyPagi'] = $avgMonthlyPagi;
        $data['avgMonthlySiang'] = $avgMonthlySiang;
        $view = \View::make('page/'.$this->class_link.'/tempnhumidityreport_table', $data);
        $html = $view->render();

        PDF::setHeaderCallback(function($pdf) {
            $htmlHeader = 
            '<table id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 70%;" border="0" width="100%"> 
                <tr>
                    <td width="20%" rowspan="4" style="text-align: center"><img src="images/logo_cat.png" width="90" height="35"></td>
                    <td width="80%" colspan="3" style="text-align: right; font-size: 120%; font-weight:bold;">CHECKLIST SUHU DAN KELEMBAPAN RUANGAN SERVER PT CATERLINDO</td>
                </tr>
                <tr>
                    <td width="40%" ></td>
                    <td width="20%">No. Dokumen</td>
                    <td width="20%">: CAT-HRD-009</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tanggal Terbit</td>
                    <td>: 28-08-2020</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Rev</td>
                    <td>: 02</td>
                </tr>
            </table>';
            $pdf->SetFont('dejavusans', 9);
            $pdf->writeHTML($htmlHeader, true, false, true, false, '');
    
        });
        $title = 'Report Suhu '.$tahun.'/'.$bulan;
        PDF::SetTitle($title);
        PDF::setPrintFooter(false);
        PDF::SetAuthor('Author');
        PDF::SetDisplayMode('real', 'default');
        PDF::SetFont('dejavusans', '', 9);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(10);
        PDF::SetMargins(5, 30, 5);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::lastPage();
        PDF::Output($title.'.pdf', 'I');
   }


}