<?php

namespace App\Http\Controllers\API\SUPPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\LokasiModel;
use App\Models\PenawaranModel;

use Illuminate\Support\Facades\DB;
// use App\Models\HRM\Tb_bagianModel;

class LelangApiController extends Controller
{

    public function StorePenawaran(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'jumlah' => 'required',
            'id_lelang' => 'required',
        ], [
            'nama.required' => 'Nama tidak boleh kosong',
            'jumlah.required' => 'Jumlah tidak boleh kosong',
            'id_lelang.required' => 'Id Lelang tidak boleh kosong',
        ]);

        $getMax= db::select('SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang =' . $request->id_lelang);
        $getMax = (int) $getMax[0]->max_items;
        $my_penawaran = (int) $request->jumlah;

        if($getMax >= $my_penawaran || $getMax == $my_penawaran){
            $resp['code'] = 403;
            $resp['messages'] = 'Harga Bit tertinggi telah berubah, mohon refresh halaman!'; 
            
            return response()->json($resp);
        }
        
        // $resp['getMax'] = $getMax;
        // $resp['my_penawaran'] = $my_penawaran;

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                    $arrayData = [
                        'nama' => $request->nama,
                        'jumlah' => $request->jumlah,
                        'id_bagian' => $request->id_bagian,
                        'id_lelang' => $request->id_lelang,
                    ];
                    PenawaranModel::create($arrayData);

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['id_lelang'] = $request->id_lelang;
        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }
}
