<?php

namespace App\Http\Controllers\API\SUPPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\LokasiModel;
use App\Models\BudgetingModel;
use App\Models\BarangBudgetingModel;
use App\Models\Budgetingdetail_has_monthModel;
use App\Models\Category_budgetingModel;
// use App\Models\HRM\Tb_bagianModel;

class BudgetingApiController extends Controller
{

    public function allSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $lokasis = BarangBudgetingModel::where('stts', '=', 'Disetujui')->get();

        $arrayLokasis = [];
        foreach ($lokasis as $lokasi) {
            $lokasi['id'] = $lokasi->lokasi_id;
            $lokasi['text'] = $lokasi->lokasi_nama;
            $arrayLokasis[] = $lokasi;
        }
        return response()->json($arrayLokasis);
    }


    public function allDetailBudgeting(Request $request)
    {
        $paramSearch = $request->paramSearch;

        //$lokasis = BarangBudgetingModel::where('stts', '=', 'Disetujui')->get();

        $lokasis = BarangBudgetingModel::select('td_barang_budgeting.id_barang', 'td_barang_budgeting.id_budgeting', 'td_barang_budgeting.nm_barang', 'td_barang_budgeting.spesifikasi_barang', 'tb_bagian.bagian_nama')
        ->leftJoin('tm_budgeting', 'tm_budgeting.kode_budgeting', '=', 'td_barang_budgeting.id_budgeting')
        ->leftJoin('tb_bagian', 'tb_bagian.bagian_kd','=', 'tm_budgeting.id_bagian')
        ->where('td_barang_budgeting.stts', 'Disetujui')->get();

        $arrayLokasis = [];
        foreach ($lokasis as $lokasi) {
            $p['id'] = $lokasi->id_barang;
            $p['text'] = $lokasi->id_budgeting . ' - ' . $lokasi->bagian_nama . ' -- ' . $lokasi->nm_barang . ' -- ' . $lokasi->spesifikasi_barang;
            $arrayLokasis[] = $p;
        }
        return response()->json($arrayLokasis);
    }
}
