<?php

namespace App\Http\Controllers\API\SUPPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\AsetModel;
use App\Models\Aset_mutasi_jenisModel;
use App\Models\Aset_Stokopname_jenisModel;
use App\Models\Aset_statusModel;
use App\Models\Aset_jenisModel;
use App\Models\TagModel;
use App\Models\Aset_mutasiModel;
use App\Models\HRM\Tb_bagianModel;
use App\Models\Aset_Stokopname_periodeModel;

class AsetApiController extends Controller
{

    public function allSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $bagians = Tb_bagianModel::all();
        $arrayBagians = [];
        foreach ($bagians as $bagian) {
            $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
        }

        $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
            ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
            ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id');
        if (!empty($paramSearch)) {
            $asets = $asets->where('aset.aset_kode', 'like', "%$paramSearch%");
            $asets = $asets->orWhere('aset.aset_nama', 'like', "%$paramSearch%");
        }
        $asets = $asets->get();

        $arrayAssets = [];
        foreach ($asets as $aset) {
            $aset['id'] = $aset->aset_id;
            $aset['text'] = $aset->aset_kode . ' | ' .  $aset->aset_barcode . ' | ' .  $aset->aset_nama;
            $aset['nm_bagian'] = isset($arrayBagians[$aset->bagian_kd]) ? $arrayBagians[$aset->bagian_kd] : '';
            $arrayAssets[] = $aset;
        }
        return response()->json($arrayAssets);
    }

    public function allSelect2ntmutasi(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $bagians = Tb_bagianModel::all();
        $arrayBagians = [];
        foreach ($bagians as $bagian) {
            $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
        }

        $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
            ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
            ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
            ->leftJoin('aset_mutasi', 'aset_mutasi.aset_id', '=', 'aset.aset_id')
            ->where('aset_mutasi.asetmutasi_status', '!=', 'pending');
        if (!empty($paramSearch)) {
            $asets = $asets->Where('aset.aset_kode', 'like', "%$paramSearch%");
            $asets = $asets->orWhere('aset.aset_nama', 'like', "%$paramSearch%");
            $asets = $asets->Where('aset_mutasi.asetmutasi_status', '!=', 'pending');
        }
        $asets = $asets->get();

        $arrayAssets = [];
        foreach ($asets as $aset) {
            $aset['id'] = $aset->aset_id;
            $aset['text'] = $aset->aset_kode . ' | ' .  $aset->aset_barcode . ' | ' .  $aset->aset_nama;
            $aset['nm_bagian'] = isset($arrayBagians[$aset->bagian_kd]) ? $arrayBagians[$aset->bagian_kd] : '';
            $arrayAssets[] = $aset;
        }
        return response()->json($arrayAssets);
    }

    public function allAsetmutasijenisSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetMutasijeniss = Aset_mutasi_jenisModel::where('asetmutasijenis_nama', 'like', "%$paramSearch%")
            ->get();

        $arrayAssetMutasijeniss = [];
        foreach ($asetMutasijeniss as $asetMutasijenis) {
            $arrayAssetMutasijeniss[] = [
                'id' => $asetMutasijenis->asetmutasijenis_id,
                'text' => $asetMutasijenis->asetmutasijenis_nama,
            ];
        }
        return response()->json($arrayAssetMutasijeniss);
    }

    public function allAsetstatusSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetStatuss = Aset_statusModel::where('asetstatus_nama', 'like', "%$paramSearch%")
            ->get();

        $arrayAsetStatuss = [];
        foreach ($asetStatuss as $asetStatus) {
            $arrayAsetStatuss[] = [
                'id' => $asetStatus->asetstatus_id,
                'text' => $asetStatus->asetstatus_nama,
            ];
        }
        return response()->json($arrayAsetStatuss);
    }

    public function allAsetOpnamejenisSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetPeriodee = Aset_Stokopname_jenisModel::where('name', 'like', "%$paramSearch%")
            ->get();

        $arrayAsetPeriode = [];
        foreach ($asetPeriodee as $asetPeriode) {
            $arrayAsetPeriode[] = [
                'id' => $asetPeriode->aset_stokopname_jenis_kd,
                'text' => $asetPeriode->name,
            ];
        }
        return response()->json($arrayAsetPeriode);
    }

    public function allAsetOpnamePeriodeSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetPeriodee = Aset_Stokopname_periodeModel::where('aset_stokopname_jenis_kd', '=', "2")
            ->get();

        $arrayAsetPeriode = [];
        foreach ($asetPeriodee as $asetPeriode) {
            $arrayAsetPeriode[] = [
                'id' => $asetPeriode->aset_stokopname_periode_kd,
                'text' => $asetPeriode->nomor_seri,
            ];
        }
        return response()->json($arrayAsetPeriode);
    }
    
    

    public function allAsetjenisSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetJeniss = Aset_jenisModel::where('asetjenis_nama', 'like', "%$paramSearch%")
            ->get();

        $arrayAsetJeniss = [];
        foreach ($asetJeniss as $asetJenis) {
            $arrayAsetJeniss[] = [
                'id' => $asetJenis->asetjenis_id,
                'text' => $asetJenis->asetjenis_nama,
            ];
        }
        return response()->json($arrayAsetJeniss);
    }

    public function allTag(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $asetJeniss = TagModel::where('name', 'like', "%$paramSearch%")
            ->get();

        $arrayAsetJeniss = [];
        foreach ($asetJeniss as $asetJenis) {
            $arrayAsetJeniss[] = [
                'id' => $asetJenis->id_tag,
                'text' => '#' . $asetJenis->name,
            ];
        }
        return response()->json($arrayAsetJeniss);
    }
}
