<?php

namespace App\Http\Controllers\API\SUPPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\LokasiModel;
use App\Models\Aset_Stokopname_periodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\AsetModel;

class ApkStoktakeApiController extends Controller
{

    public function api_lokasi(Request $request)
    {
        $paramSearch = $request->paramSearch;
        //echo $request->key;
        // echo md5("GetApi@" . date('d-m-Y@h', strtotime('now')) . "@lokasi@Caterlindo");
        if ($request->key == strtoupper(md5("GetApi@" . date('d-m-Y@h', strtotime('now')) . "@lokasi@Caterlindo"))) {
            $lokasis = LokasiModel::where('lokasi_nama', 'like', "%$paramSearch%")->get();

            $lokasis = LokasiModel::get();

            $arrayLokasis = [];
            foreach ($lokasis as $lokasi) {
                $lokasi['id'] = $lokasi->lokasi_id;
                $lokasi['text'] = $lokasi->lokasi_nama;
                $arrayLokasis[] = $lokasi;
            }
            return response()->json($arrayLokasis);
        } else {
            exit('No direct script access allowed');
        }
    }

    public function api_stokopname_periode(Request $request)
    {
        if ($request->key == strtoupper(md5("GetApi@" . date('d-m-Y@h', strtotime('now')) . "@periode@Caterlindo"))) {

            $periodes = Aset_Stokopname_periodeModel::where('status', 'aktif')->get();

            $arrayperiodes = [];
            foreach ($periodes as $periode) {
                $periode['id'] = $periode->lokasi_id;
                $periode['text'] = $periode->lokasi_nama;
                $arrayperiodes[] = $periode;
            }
            return response()->json($arrayperiodes);
        } else {
            exit('No direct script access allowed');
        }
    }

    public function api_stokopname_riwayat(Request $request)
    {
        if ($request->key == strtoupper(md5("GetApi@" . date('d-m-Y@h', strtotime('now')) . "@riwayat_stok_opname@Caterlindo"))) {

            $asetStokopnames = Aset_Stokopname_detailModel::join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                ->join('aset_stokopname_periode', function ($join) {
                    $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.status', '=', 'aktif');
                })
                ->join('lokasi', 'lokasi.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')->orderBy('aset_stokopname_created_at', 'desc')->take(20)->get();

            // $arrayasetStokopnames = [];
            // foreach ($asetStokopnames as $asetStokopname) {
            //     $asetStokopname['id'] = $periode->lokasi_id;
            //     $asetStokopname['text'] = $periode->lokasi_nama;
            //     $arrayperiodes[] = $periode;
            // }
            return response()->json($asetStokopnames);
        } else {
            exit('No direct script access allowed');
        }
    }

    public function api_stokopname_save(Request $request)
    {
        if ($request->key == strtoupper(md5("GetApi@" . date('d-m-Y@h', strtotime('now')) . "@save_stokopname@Caterlindo"))) {

            $periode = Aset_Stokopname_periodeModel::where('nomor_seri', $request->periode)->first();
            $lokasi = LokasiModel::where('lokasi_nama', 'like', "%$request->lokasi%")->first();
            $asets = AsetModel::where('aset_barcode', '=', $request->kode)->orWhere('aset_barcode_batang', '=', $request->kode);

            //cek barang ada atau tidak
            if ($asets->count() > 0) {
                $aset = $asets->first();

                //cek duplikat data
                $stokopnames = Aset_Stokopname_detailModel::where('aset_stokopname_periode_kd', $periode->aset_stokopname_periode_kd)
                    ->where('aset_kd', $aset->aset_id);

                if ($stokopnames->count() == 0) {
                    $arrayData = [
                        'aset_stokopname_detail_kd' => '',
                        'aset_stokopname_periode_kd' => $periode->aset_stokopname_periode_kd,
                        'aset_kd' => $aset->aset_id,
                        'lokasi_kd' => $lokasi->lokasi_id,
                        'barcode' => $request->kode,
                        'note' => '',
                        'kondisi' => $request->status,
                        'checker' => 'IT Caterlindo',
                        'user_id' => 6,
                    ];
                    Aset_Stokopname_detailModel::create($arrayData);
                    $result = "200#Stoktake berhasil disimpan";  //sukses input
                    // $result = "200#Stoktake berhasil disimpan" . $stokopnames->count() . " - " . $periode->aset_stokopname_periode_kd . " - " . $request->periode . " - " . json_encode($periode);  //sukses input
                } elseif ($stokopnames->count() > 0) {
                    $stokopname = $stokopnames->first();
                    $lokasi2 = LokasiModel::where('lokasi_id', $stokopname->lokasi_kd)->first();
                    $result = "402#data sudah diinput di lokasi " . $lokasi2->lokasi_nama; //duplikat data
                    //$result = "402#data sudah diinput di lokasi " . $lokasi2->lokasi_nama . " - " . json_encode($stokopname); //duplikat data
                }
            } else {
                $result = "401#kode barang tidak ditemukan";  //data aset belum di input
            }
            return response()->json($result);   //no json, just text
        } else {
            exit('403#refresh aplikasi');
        }
        //echo $request->periode;
        //echo $request->lokasi;
        //echo $request->key;
        //echo $request->kode;
    }
}
