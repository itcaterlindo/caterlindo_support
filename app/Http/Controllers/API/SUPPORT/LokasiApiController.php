<?php

namespace App\Http\Controllers\API\SUPPORT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\LokasiModel;
// use App\Models\HRM\Tb_bagianModel;

class LokasiApiController extends Controller
{

    public function allSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $lokasis = LokasiModel::where('lokasi_nama', 'like', "%$paramSearch%")->get();

        $arrayLokasis = [];
        foreach ($lokasis as $lokasi) {
            $lokasi['id'] = $lokasi->lokasi_id;
            $lokasi['text'] = $lokasi->lokasi_nama;
            $arrayLokasis[] = $lokasi;
        }
        return response()->json($arrayLokasis);
    }
}
