<?php

namespace App\Http\Controllers\API\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Tb_bagianModel;
use App\Models\Tb_bagianRModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BagianApiController extends Controller
{

    public function asetSelect2(Request $request)
    {
        $paramSearch = $request->paramSearch;

        $bagians = Tb_bagianModel::where('bagian_aset', '1');
        if (!empty($paramSearch)) {
            $bagians = $bagians->where('nm_bagian', 'like', "%$paramSearch%");
        }
        $bagians = $bagians->get();
        $arrayBagians = [];
        foreach ($bagians as $bagian) {
            $arrayBagians[] = [
                'id' => $bagian->kd_bagian,
                'text' => $bagian->nm_bagian,
                'kd_unit' => $bagian->kd_unit,
            ];
        }
        return response()->json($arrayBagians);
    }

    public function asetSelect2real(Request $request)
    {
        $paramSearch = $request->paramSearch;

       
        if (!empty($paramSearch)) {
            $bagians = Tb_bagianRModel::where('bagian_nama', 'like', "%$paramSearch%");
        }else{
            $bagians = Tb_bagianRModel::all();
        }
        // $bagians = $bagians->get();
        $arrayBagians = [];
        foreach ($bagians as $bagian) {
            $arrayBagians[] = [
                'id' => $bagian->bagian_kd,
                'text' => $bagian->bagian_nama
            ];
        }
        return response()->json($arrayBagians);
    }

}
