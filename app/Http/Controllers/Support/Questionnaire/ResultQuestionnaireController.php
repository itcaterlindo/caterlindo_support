<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Question_resultModel;
use App\Models\HRM\Tb_karyawanModel;
use Illuminate\Support\Facades\Auth;

class ResultQuestionnaireController extends Controller
{
    private $class_link = 'support/kuesioner/resultkuesioner';
    
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        if (Auth::user()->can('RESULTKUESIONER_VIEWALL')) {
            $qResults = Question_resultModel::leftJoin('questionnaire', 'question_result.q_id','=','questionnaire.q_id')
                            ->leftJoin('questionnaire_periode', 'questionnaire_periode.periode_id','=','question_result.periode_id')
                            ->get();
        }else{
            $qResults = Question_resultModel::where('questionnaire.q_usercreated', Auth::user()->id)
                            ->leftJoin('questionnaire', 'question_result.q_id','=','questionnaire.q_id')
                            ->leftJoin('questionnaire_periode', 'questionnaire_periode.periode_id','=','question_result.periode_id')
                            ->get();
        }
        $kd_karyawans = [];
        foreach ($qResults as $result0) {
            if (!in_array($result0->kd_karyawan, $kd_karyawans)){
                $kd_karyawans[] = $result0->kd_karyawan;
            }
        }
        if (!empty($kd_karyawans)){
            $karyawans = Tb_karyawanModel::leftJoin('tb_bagian', 'tb_karyawan.kd_bagian', '=', 'tb_bagian.kd_bagian')
                ->whereIn('kd_karyawan', $kd_karyawans)->get();
        }
        $results = [];
        foreach ($qResults as $result) {
            $nm_karyawan = ''; $nm_bagian = '';
            foreach ($karyawans as $karyawan) {
                if ($karyawan->kd_karyawan == $result->kd_karyawan){
                    $nm_karyawan = $karyawan->nm_karyawan;
                    $nm_bagian = $karyawan->nm_bagian;
                }
            }
            $results[] = [
                'quesresult_id' => $result->quesresult_id,
                'q_id' => $result->q_id,
                'q_title' => $result->q_title,
                'kd_karyawan' => $result->kd_karyawan,
                'kd_bagian' => $result->kd_bagian,
                'periode_start' => $result->periode_start,
                'periode_end' => $result->periode_end,
                'periode_note' => $result->periode_note,
                'quesresult_updated' => $result->quesresult_updated->toDateTimeString(),
                'nm_karyawan' => $nm_karyawan,
                'nm_bagian' => $nm_bagian
            ];
        }
        
        return DataTables::of($results)
                ->addIndexColumn()
                ->addColumn('opsi', function ($result){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=window.open("'.url('support/kuesioner/listkuesioner/preview').'?quesresult_id='.$result['quesresult_id'].'")> <i class="fas fa-edit"></i> Preview</a>';
                    if (Auth::user()->can('RESULTKUESIONER_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$result['quesresult_id'].'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->addColumn('periode_range', function ($result){
                    return $result['periode_note'].'('.date('Y-m-d', strtotime($result['periode_start'])).' s/d '.date('Y-m-d', strtotime($result['periode_end'])).')';
                })
                ->rawColumns(['opsi', 'quesresult_updated'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Questionnaire_periodeModel::where('periode_id', $id)->first()->toArray();
        }
        /** Opsi Kuesioner */
        $questionnaires = QuestionnaireModel::where('q_active', 1)->get();
        $opsiKuesioner[null] = '-- Pilih Opsi --';
        foreach ($questionnaires as $questionnaire) {
            $opsiKuesioner[$questionnaire->q_id] = $questionnaire->q_title;
        }

        $data['opsiKuesioner'] = $opsiKuesioner;
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'q_id' => 'required',
            'periode_range' => 'required',
        ], [
            'q_id.required' => 'Kuesioner tidak boleh kosong',
            'periode_range.required' => 'Range tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $periode_id = $request->id;
                $q_id = $request->q_id;
                $periode_range = $request->periode_range;
                $expRange = explode(' - ', $periode_range);
                $periode_start = $expRange[0];
                $periode_end = $expRange[1];
                
                $user_id = Auth::user()->id;
                
                if (empty($periode_id)) {
                    $arrayData = [
                        'q_id' => $q_id,
                        'periode_start' => $periode_start,
                        'periode_end' => $periode_end,
                        'user_id' => $user_id,
                    ];
                    Questionnaire_periodeModel::create($arrayData);
                }else {
                    $periode = Questionnaire_periodeModel::find($periode_id);
                    $periode->q_id = $q_id;
                    $periode->periode_start = $periode_start;
                    $periode->periode_end = $periode_end;
                    $periode->user_id = $user_id;

                    $periode->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Question_resultModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
