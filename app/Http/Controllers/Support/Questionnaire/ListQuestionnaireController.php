<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\QuestionnaireModel;
use App\Models\QuestionModel;
use App\Models\Questionnaire_periodeModel;
use App\Models\Question_answerModel;
use App\Models\Question_resultModel;
use App\Models\Question_result_detailModel;
use App\Models\HRM\Tb_karyawanModel;
use Illuminate\Support\Facades\Auth;

class ListQuestionnaireController extends Controller
{
    // use HasRoles;
    private $class_link = 'support/kuesioner/listkuesioner';
    
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $user_id = Auth::user()->id;

        if (Auth::user()->can('KUESIONER_VIEWALL')){
            $questionnaires = QuestionnaireModel::all();
        }else{
            $questionnaires = QuestionnaireModel::where('q_usercreated', $user_id)->get();
        }

        return DataTables::of($questionnaires)
                ->addIndexColumn()
                ->addColumn('opsi', function ($questionnaire){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=window.open("'.url($this->class_link.'/preview?q_id='.$questionnaire->q_id).'")> <i class="fas fa-paper-plane"></i> Preview</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=window.location.assign("'.url($this->class_link.'/listpertanyaan/'.$questionnaire->q_id).'")> <i class="fas fa-list"></i> List Pertanyaan</a>';
                    if (Auth::user()->can('KUESIONER_UPDATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$questionnaire->q_id.'")> <i class="fas fa-edit"></i> Edit</a>
                                <div class="dropdown-divider"></div>';
                    }
                    if (Auth::user()->can('KUESIONER_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$questionnaire->q_id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->editColumn('q_active', function($questionnaire) {
                    return spanStatus($questionnaire->q_active);
                })
                ->editColumn('q_updated', function($questionnaire) {
                    return $questionnaire->q_updated->toDateTimeString();
                })
                ->rawColumns(['opsi', 'q_active'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = QuestionnaireModel::where('q_id', $id)->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }
    
    public function preview(Request $request) {
        
        $q_id = $request->q_id;
        $quesresult_id = $request->quesresult_id;

        $data['q_id'] = $q_id;
        $data['quesresult_id'] = $quesresult_id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/preview', $data);
    }

    public function preview_main(Request $request) {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $q_id = $request->q_id;
        $quesresult_id = $request->quesresult_id;
        
        if (!empty($quesresult_id)) {
            $quesresult = Question_resultModel::where('question_result.quesresult_id', $quesresult_id)->first();
            $quesresults = Question_resultModel::where('question_result.quesresult_id', $quesresult_id)
                ->leftJoin('question_result_detail', 'question_result.quesresult_id', '=', 'question_result_detail.quesresult_id')
                ->leftJoin('question_type', 'question_type.questype_id', '=', 'question_result_detail.questype_id')
                ->get()->toArray();
            $kd_karyawan = $quesresult->kd_karyawan;
            $q_id = $quesresult->q_id;
            $data['karyawan'] = Tb_karyawanModel::where('kd_karyawan', $kd_karyawan)
                    ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian', '=', 'tb_bagian.kd_bagian')->first();
            $data['quesresults'] = $quesresults;
            $data['kuesioner'] = Question_resultModel::where('question_result.quesresult_id', $quesresult_id)
                    ->leftJoin('questionnaire', 'question_result.q_id', '=', 'questionnaire.q_id')
                    ->leftJoin('questionnaire_periode', 'question_result.periode_id', '=', 'questionnaire_periode.periode_id')
                    ->first();
        }
        $questions = new QuestionModel;
        $resultQuestion = $questions->get_byparam_detail(['question.q_id' => $q_id, 'question.question_active' => 1])->toArray();
        $resultAnswer = Question_answerModel::where(['q_id' => $q_id])->get();

        $data['q_id'] = $q_id;
        $data['quesresult_id'] = $quesresult_id;
        $data['questions'] = $resultQuestion;
        $data['answers'] = $resultAnswer;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/preview_main', $data);
    }
    
    public function preview_tablepoin (Request $request) {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $quesresult_id = $request->quesresult_id;

        $quesresults = Question_result_detailModel::where('question_result_detail.quesresult_id', $quesresult_id)
                ->selectRaw('question.question_squence, SUM(question_result_detail.quesresultdetail_point) AS quesresultdetail_point, question_result_detail.quesresultdetail_questionbobot')
                ->leftJoin('question_result', 'question_result_detail.quesresult_id', '=', 'question_result.quesresult_id')
                ->leftJoin('question', 'question_result_detail.question_id', '=', 'question.question_id')
                ->groupBy('question_result_detail.question_id')
                ->orderBy('question.question_squence')
                ->get();

        $data['quesresults'] = $quesresults;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/preview_tablepoin', $data);
    }
   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'q_title' => 'required',
        ], [
            'q_title.required' => 'Nama tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $q_id = $request->id;
                $q_title = $request->q_title;
                $q_active = $request->q_active;
                $user_id = Auth::user()->id;
                
                if (empty($q_id)) {
                    $arrayData = [
                        'q_title' => $q_title,
                        'q_active' => $q_active,
                        'q_usercreated' => $user_id,
                        'user_id' => $user_id,
                    ];
                    QuestionnaireModel::create($arrayData);
                }else {
                    $kuesioner = QuestionnaireModel::find($q_id);
                    $kuesioner->q_title = $q_title;
                    $kuesioner->q_active = $q_active;
                    $kuesioner->user_id = $user_id;

                    $kuesioner->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                QuestionnaireModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
