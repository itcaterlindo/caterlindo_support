<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Questionnaire_periodeModel;
use App\Models\QuestionnaireModel;
use Illuminate\Support\Facades\Auth;

class PeriodeQuestionnaireController extends Controller
{
    private $class_link = 'support/kuesioner/periodekuesioner';
    
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (Auth::user()->can('PERIODE_VIEWALL')){
            $questionnaire_periodes = Questionnaire_periodeModel::leftJoin('questionnaire', 'questionnaire_periode.q_id','=','questionnaire.q_id')->get();
        }else{
            $questionnaire_periodes = Questionnaire_periodeModel::where('questionnaire.q_usercreated', Auth::user()->id)->leftJoin('questionnaire', 'questionnaire_periode.q_id','=','questionnaire.q_id')->get();
        }

        return DataTables::of($questionnaire_periodes)
                ->addIndexColumn()
                ->addColumn('opsi', function ($questionnaire_periode){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    if (Auth::user()->can('PERIODE_UPDATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$questionnaire_periode->periode_id.'")> <i class="fas fa-edit"></i> Edit</a>';
                    }
                    if (Auth::user()->can('PERIODE_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$questionnaire_periode->periode_id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->editColumn('periode_active', function($questionnaire_periode) {
                    $now = strtotime('now');
                    $periode_start = strtotime($questionnaire_periode->periode_start);
                    $periode_end = strtotime($questionnaire_periode->periode_end);
                    $paramText = 'Aktif';
                    $paramColor = 'success';
                    if ($now < $periode_start || $now > $periode_end) {
                        $paramText = 'Tidak Aktif';
                        $paramColor = 'warning';
                    }
                    return buildSpan ($paramText, $paramColor);
                })
                ->rawColumns(['opsi', 'periode_active'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Questionnaire_periodeModel::where('periode_id', $id)->first()->toArray();
        }
        /** Opsi Kuesioner */
        $questionnaires = QuestionnaireModel::where('q_active', 1)->get();
        $opsiKuesioner[null] = '-- Pilih Opsi --';
        foreach ($questionnaires as $questionnaire) {
            $opsiKuesioner[$questionnaire->q_id] = $questionnaire->q_title;
        }

        $data['opsiKuesioner'] = $opsiKuesioner;
        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'q_id' => 'required',
            'periode_range' => 'required',
        ], [
            'q_id.required' => 'Kuesioner tidak boleh kosong',
            'periode_range.required' => 'Range tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $periode_id = $request->id;
                $q_id = $request->q_id;
                $periode_range = $request->periode_range;
                $periode_note = $request->periode_note;
                $expRange = explode(' - ', $periode_range);
                $periode_start = $expRange[0];
                $periode_end = $expRange[1];
                
                $user_id = Auth::user()->id;
                
                if (empty($periode_id)) {
                    $arrayData = [
                        'q_id' => $q_id,
                        'periode_start' => $periode_start,
                        'periode_end' => $periode_end,
                        'periode_note' => $periode_note,
                        'user_id' => $user_id,
                    ];
                    Questionnaire_periodeModel::create($arrayData);
                }else {
                    $periode = Questionnaire_periodeModel::find($periode_id);
                    $periode->q_id = $q_id;
                    $periode->periode_start = $periode_start;
                    $periode->periode_end = $periode_end;
                    $periode->periode_note = $periode_note;
                    $periode->user_id = $user_id;

                    $periode->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Questionnaire_periodeModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
