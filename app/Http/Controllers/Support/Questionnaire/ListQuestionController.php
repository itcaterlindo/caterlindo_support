<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Questionnaire_periodeModel;
use App\Models\QuestionnaireModel;
use App\Models\QuestionModel;
use App\Models\Question_categoryModel;
use App\Models\Question_typeModel;
use Illuminate\Support\Facades\Auth;

class ListQuestionController extends Controller
{
    private $class_link = 'support/kuesioner/listkuesioner/listpertanyaan';
    
    public function index($q_id)
    {
        $data['row'] = QuestionnaireModel::where('q_id', $q_id)->first();
        $data['q_id'] = $q_id;
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $q_id = $request->q_id;

        $data['class_link'] = $this->class_link;
        $data['q_id'] = $q_id;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $q_id = $request->q_id;
        
        $question = new QuestionModel();
        $questions = $question->get_byparam_detail(['question.q_id' => $q_id]);

        return DataTables::of($questions)
                ->addIndexColumn()
                ->addColumn('opsi', function ($question){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=window.location.assign("'.url('support/kuesioner/listkuesioner/listjawaban/'.$question->question_id).'")> <i class="fas fa-list"></i> List Jawaban</a>';
                    if (Auth::user()->can('KUESIONER_UPDATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$question->question_id.'")> <i class="fas fa-edit"></i> Edit</a>
                        <div class="dropdown-divider"></div>';
                    }
                    if (Auth::user()->can('KUESIONER_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$question->question_id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->editColumn('question_active', function($question) {
                    return spanStatus($question->question_active);
                })
                ->rawColumns(['opsi', 'question_active'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $q_id = $request->q_id;
        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = QuestionModel::where('question_id', $id)->first()->toArray();
        }
        /** Opsi Kategori */
        $categorys = Question_categoryModel::all();
        $opsiCategory[null] = '-- Pilih Opsi --';
        foreach ($categorys as $category) {
            $opsiCategory[$category->quescategory_id] = $category->quescategory_name;
        }
        /** Opsi Type */
        $types = Question_typeModel::all();
        $opsiType[null] = '-- Pilih Opsi --';
        foreach ($types as $type) {
            $opsiType[$type->questype_id] = $type->questype_name;
        }

        $data['opsiCategory'] = $opsiCategory;
        $data['opsiType'] = $opsiType;
        $data['id'] = $id;
        $data['q_id'] = $q_id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'quescategory_id' => 'required',
            'questype_id' => 'required',
            'question_squence' => 'required',
            'question_text' => 'required',
            'question_active' => 'required',
            'question_required' => 'required',
            'question_bobot' => 'required',
        ], [
            'quescategory_id.required' => 'Kategori tidak boleh kosong',
            'questype_id.required' => 'Type tidak boleh kosong',
            'question_squence.required' => 'Squence tidak boleh kosong',
            'question_text.required' => 'Pertanyaan tidak boleh kosong',
            'question_active.required' => 'Aktif tidak boleh kosong',
            'question_required.required' => 'Required tidak boleh kosong',
            'question_bobot.required' => 'Required tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $question_id = $request->id;
                $q_id = $request->q_id;
                $quescategory_id = $request->quescategory_id;
                $questype_id = $request->questype_id;
                $question_squence = $request->question_squence;
                $question_text = $request->question_text;
                $question_active = $request->question_active;
                $question_required = $request->question_required;
                $question_bobot = $request->question_bobot;
                
                $user_id = Auth::user()->id;
                
                if (empty($question_id)) {
                    $arrayData = [
                        'q_id' => $q_id,
                        'quescategory_id' => $quescategory_id,
                        'question_text' => $question_text,
                        'questype_id' => $questype_id,
                        'question_required' => $question_required,
                        'question_squence' => $question_squence,
                        'question_active' => $question_active,
                        'question_bobot' => $question_bobot,
                        'user_id' => $user_id,
                    ];
                    QuestionModel::create($arrayData);
                }else {
                    $question = QuestionModel::find($question_id);
                    $question->q_id = $q_id;
                    $question->quescategory_id = $quescategory_id;
                    $question->question_text = $question_text;
                    $question->questype_id = $questype_id;
                    $question->question_required = $question_required;
                    $question->question_squence = $question_squence;
                    $question->question_active = $question_active;
                    $question->question_bobot = $question_bobot;
                    $question->user_id = $user_id;

                    $question->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                QuestionModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
