<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Question_categoryModel;

use Illuminate\Support\Facades\Auth;

class CategoryQuestionController extends Controller
{
    private $class_link = 'support/kuesioner/kategoripertanyaan';
    
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $questionCategorys = Question_categoryModel::all();

        return DataTables::of($questionCategorys)
                ->addIndexColumn()
                ->addColumn('opsi', function ($questionCategory){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    if (Auth::user()->can('KATEGORI_UDPATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$questionCategory->quescategory_id.'")> <i class="fas fa-edit"></i> Edit</a>';
                    }
                    if (Auth::user()->can('KATEGORI_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$questionCategory->quescategory_id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->editColumn('quescategory_updated', function($questionCategory) {
                    return $questionCategory->quescategory_updated->toDateTimeString();
                })
                ->rawColumns(['opsi', 'quescategory_updated'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Question_categoryModel::where('quescategory_id', $id)->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'quescategory_name' => 'required',
        ], [
            'quescategory_name.required' => 'Kategori tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $quescategory_id = $request->id;
                $quescategory_name = $request->quescategory_name;
                
                $user_id = Auth::user()->id;
                
                if (empty($quescategory_id)) {
                    $arrayData = [
                        'quescategory_name' => $quescategory_name,
                        'user_id' => $user_id,
                    ];
                    Question_categoryModel::create($arrayData);
                }else {
                    $quescategory = Question_categoryModel::find($quescategory_id);
                    $quescategory->quescategory_name = $quescategory_name;
                    $quescategory->user_id = $user_id;

                    $quescategory->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Question_categoryModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
