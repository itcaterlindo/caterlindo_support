<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\QuestionnaireModel;
use App\Models\Questionnaire_periodeModel;
use App\Models\Question_resultModel;
use App\Models\QuestionModel;
use App\Models\Question_answerModel;
use App\Models\HRM\Tb_karyawanModel;

use Illuminate\Support\Facades\Auth;

class ReportQuestionnaireController extends Controller
{
    private $class_link = 'support/kuesioner/report';
    
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function get_kuesioner (Request $request) {
		$paramKuesioner = $request->get('paramKuesioner');
		$paramActive = $request->get('paramActive');
		
		$qKuesioners = QuestionnaireModel::orderBy('q_updated');
        if (!empty($paramActive)) {
            $qKuesioners->where('q_active', $paramActive);
        }
        if ((Auth::user()->can('REPORTKUESIONERKARYAWAN_VIEW') && !Auth::user()->can('REPORTKUESIONERKARYAWAN_VIEWALL') ) || 
            ( Auth::user()->can('REPORTKUESIONERPERIODE_VIEW') && !Auth::user()->can('REPORTKUESIONERPERIODE_VIEWALL') ) || 
            ( Auth::user()->can('REPORTKUESIONERPERKARYAWAN_VIEW') && !Auth::user()->can('REPORTKUESIONERPERKARYAWAN_VIEWALL') ) ){
            $qKuesioners->where('q_usercreated', Auth::user()->id);
        }
        $qKuesioners->where('q_title', 'like', "%$paramKuesioner%");
        $kuesioners = $qKuesioners->get();

        $results = [];
        foreach ($kuesioners as $kuesioner) {
            $results[] = [
                'id' => $kuesioner->q_id, 
                'text' => $kuesioner->q_title, 
            ];
        }
		return response()->json($results);
	}

    public function get_periode (Request $request) {
		$q_id = $request->get('q_id');
		$paramPeriode = $request->get('paramPeriode');
		
		$qPeriodes = Questionnaire_periodeModel::orderBy('periode_start', 'desc');
        if (!empty($q_id)) {
            $qPeriodes->where('q_id', $q_id);
        }
        $qPeriodes->where(function ($query) use($paramPeriode)  {
            $query->where('periode_note', 'like', "%$paramPeriode%")->orWhere('periode_start', 'like', "%$paramPeriode%");
        });
        $periodes = $qPeriodes->get();

        $results = [];
        foreach ($periodes as $periode) {
            $results[] = [
                'id' => $periode->periode_id, 
                'text' => $periode->periode_note.date('Y-m-d', strtotime($periode->periode_start)).' s/d '.date('Y-m-d', strtotime($periode->periode_end)), 
            ];
        }
		
		return response()->json($results);
	}

    public function get_karyawan (Request $request) {
		$paramKaryawan = $request->get('paramKaryawan');
		
		$qKaryawans = Tb_karyawanModel::leftJoin('tb_bagian', 'tb_karyawan.kd_bagian', '=', 'tb_bagian.kd_bagian')
                ->orderBy('nm_karyawan', 'asc');
        $qKaryawans->where(function ($query) use($paramKaryawan)  {
            $query->where('nm_karyawan', 'like', "%$paramKaryawan%")->orWhere('nik_karyawan', 'like', "%$paramKaryawan%");
        });
        $karyawans = $qKaryawans->get();

        $results = [];
        foreach ($karyawans as $karyawan) {
            $results[] = [
                'id' => $karyawan->kd_karyawan, 
                'text' => "$karyawan->nik_karyawan | $karyawan->nm_karyawan / $karyawan->nm_bagian", 
            ];
        }
		
		return response()->json($results);
    }

    public function reportkaryawankuesioner_index () 
    {
        $data['opsiStatus'] = ['INPUT' => 'Sudah Input', 'BELUM' => 'Belum Input', 'ALL' => 'Semua'];
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/reportkaryawankuesioner_index', $data);
    }
    
    public function reportkaryawankuesioner_table (Request $request) 
    {   
        $q_id = $request->q_id;
        $periode_id = $request->periode_id;
        $status = $request->status;

        $results = []; $arrayKaryawanInput= [];
        if (!empty($q_id) || !empty($periode_id)) {
            $karyawans = Tb_karyawanModel::where('kd_status_kerja', '<>' ,'04')
                ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian', '=', 'tb_bagian.kd_bagian')
                ->orderBy('nm_karyawan')
                ->get()->toArray();
            # Hitung skor akhir
            $quesresults = Question_resultModel::where(['q_id' => $q_id, 'periode_id' => $periode_id])
                ->leftJoin('question_result_detail', 'question_result.quesresult_id', '=', 'question_result_detail.quesresult_id')
                ->get()->toArray();
            $resultBefSkors = []; $resulSkors = [];
            foreach ($quesresults as $quesresult) {
                $resultBefSkors[$quesresult['kd_karyawan']][] = $quesresult;
            }
            foreach ($resultBefSkors as $karyawan => $pertanyaans) {
                $skor = 0;
                foreach ($pertanyaans as $pertanyaan) {
                    $skor += $pertanyaan['quesresultdetail_point'] * $pertanyaan['quesresultdetail_questionbobot'] / 100;
                }
                $resulSkors[$karyawan] = $skor;

                # Array cek input / blm
                $arrayKaryawanInput[] = $karyawan;
            }
            # Hitung skor akhir
        
            $arrayInput = []; $arrayBlmInput = []; $arrayAll = [];
            foreach ($karyawans as $karyawan) {
                if (in_array($karyawan['kd_karyawan'], $arrayKaryawanInput)){
                    $arrayInput[] = array_merge($karyawan, ['status' => 'INPUT']);
                }
                else{
                    $arrayBlmInput[] = array_merge($karyawan, ['status' => 'BELUM']);
                }
            }
            if ($status == 'INPUT') {
                $results = $arrayInput;
            }elseif ($status == 'BELUM'){
                $results = $arrayBlmInput;
            }else{
                #ALL
                $results = array_merge($arrayInput, $arrayBlmInput);
            }
        }
        $data['results'] = $results;     
        $data['resulSkors'] = $resulSkors;     
        $data['status'] = $status;     
        
        return view('page/'.$this->class_link.'/reportkaryawankuesioner_table', $data);
    }
    
    public function hasilperiode_index () 
    {
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/hasilperiode_index', $data);
    }

    public function hasilperiode_table (Request $request) 
    {
        $q_id = $request->q_id;
        $periode_id = $request->periode_id;

        $kuesioner = Questionnaire_periodeModel::find($periode_id)->first();

        $opsiResults = Question_resultModel::where('question_result.periode_id', $periode_id)
                ->whereIn('question_result_detail.questype_id', [1,2])
                ->selectRaw('question_result_detail.quesanswer_id, COUNT(question_result_detail.quesresultdetail_id) as countResult')
                ->leftJoin('question_result_detail', 'question_result.quesresult_id', '=', 'question_result_detail.quesresult_id')
                ->groupBy('question_result_detail.quesanswer_id')
                ->get();

        $valResults = Question_resultModel::where('question_result.periode_id', $periode_id)
                ->leftJoin('question_result_detail', 'question_result.quesresult_id', '=', 'question_result_detail.quesresult_id')
                ->whereIn('question_result_detail.questype_id', [3,4])
                ->get();

        $resultOpsi = [];
        $resultVal = [];
        $resultValResult = [];
        foreach ($opsiResults as $opsiResult) {
            $resultOpsi[$opsiResult->quesanswer_id] = $opsiResult->countResult;
        }
        foreach($valResults as $valResult) {
            if ($valResult->questype_id == 3){
                $resultVal[$valResult->quesanswer_id][] = strtoupper($valResult->quesresultdetail_text);
            }elseif($valResult->questype_id == 4){
                $resultVal[$valResult->quesanswer_id][] = $valResult->quesresultdetail_value;
            }
        }
        foreach($resultVal as $eVal => $element){
            $resultValResult[$eVal] = implode('; ', array_unique($element));
        }

        $data['questions'] = QuestionModel::where(['question.q_id' => $q_id, 'question.question_active' => 1])
                ->leftJoin('question_answer', 'question.question_id', '=', 'question_answer.question_id')
                ->orderBy('question.question_squence')
                ->orderBy('question_answer.quesanswer_text', 'desc')
                ->get(); 
        $data['resultOpsis'] = $resultOpsi;
        $data['resultVals'] = $resultValResult;
        
        return view('page/'.$this->class_link.'/hasilperiode_table', $data);
    }

    public function hasilkaryawan_index () 
    {
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/hasilkaryawan_index', $data);
    }

    public function hasilkaryawan_preview (Request $request) 
    {
        $kd_karyawan = $request->kd_karyawan;
        $q_id = $request->q_id;

        $data['periodes'] = Question_resultModel::select('question_result.quesresult_id', 'question_result.periode_id', 'questionnaire_periode.*')
            ->leftJoin('questionnaire_periode', 'question_result.periode_id', '=', 'questionnaire_periode.periode_id')
            ->where(['question_result.kd_karyawan' => $kd_karyawan, 'question_result.q_id' => $q_id])
            ->orderBy('question_result.periode_id', 'desc')
            ->get();
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/hasilkaryawan_preview', $data);
    }

}
