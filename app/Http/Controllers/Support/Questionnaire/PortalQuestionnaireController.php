<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\QuestionnaireModel;
use App\Models\Questionnaire_periodeModel;
use App\Models\HRM\Tb_karyawanModel;

class PortalQuestionnaireController extends Controller
{
    // use HasRoles;
    private $class_link = 'support/kuesioner/portalkuesioner';
   
    public function index($encryptKaryawan)
    {
        $kd_karyawan = Crypt::decrypt($encryptKaryawan);
    
        $qKuesioners = Questionnaire_periodeModel::leftJoin('questionnaire', 'questionnaire_periode.q_id', '=', 'questionnaire.q_id')->get();
        $tmNow = strtotime('now');
        $kuesioners = [];
        foreach ($qKuesioners as $kuesioner) {
            $tmStart = strtotime($kuesioner->periode_start);
            $tmEnd = strtotime($kuesioner->periode_end);
            if ($tmNow < $tmStart || $tmNow > $tmEnd) {
                continue;
            }
            $kuesioners[] = $kuesioner;
        }

        $data['karyawan'] = Tb_karyawanModel::where('kd_karyawan', '=', $kd_karyawan)
                    ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian','=','tb_bagian.kd_bagian')->first();
        $data['class_link'] = $this->class_link;
        $data['kuesioners'] = $kuesioners;
        return view("page/$this->class_link/index", $data);
    }
    
    public function encryptPortalQuestionnaire ($kd_karyawan) 
    {
        $encryptKaryawan = Crypt::encrypt($kd_karyawan);
        
        return redirect("$this->class_link/$encryptKaryawan");
    }

    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $users = User::all();

        return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('opsi', function ($user){
                    return '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$user->id.'")> <i class="fas fa-edit"></i> Edit</a>
                            <a class="dropdown-item" href="javascript:void(0)" data-id="'.$user->id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>
                        </div>
                    </div>
                    ';
                })
                ->rawColumns(['opsi'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $periode_id = $request->periode_id;
        $kd_karyawan = $request->kd_karyawan;
        $sts = $request->sts;

        $questions = new QuestionModel;
        $periode = Questionnaire_periodeModel::find($periode_id);
        
        $karyawan = Tb_karyawanModel::where(['tb_karyawan.kd_karyawan' => $kd_karyawan])
                    ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian', '=', 'tb_bagian.kd_bagian')
                    ->first();
        $resultQuestion = $questions->get_byparam_detail(['question.q_id' => $periode->q_id, 'question.question_active' => 1])->toArray();
        $resultAnswer = Question_answerModel::where(['q_id' => $periode->q_id])->get();

        $data['karyawan'] = $karyawan;
        $data['questions'] = $resultQuestion;
        $data['answers'] = $resultAnswer;
        $data['periode'] = $periode;
        $data['kd_karyawan'] = $kd_karyawan;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $q_id = $request->q_id;
        $periode_id = $request->periode_id;
        $kd_karyawan = $request->kd_karyawan;
        $kd_bagian = $request->kd_bagian;

        $countQresult = Question_resultModel::where(['periode_id' => $periode_id, 'kd_karyawan' => $kd_karyawan])->get()->count();
        if (empty($countQresult)) {
            $questions = QuestionModel::where(['question.q_id' => $q_id])
                        ->leftJoin('question_type', 'question.questype_id', '=', 'question_type.questype_id')
                        ->get();

            $arrayQuestionresult = [
                'q_id' => $q_id,
                'periode_id' => $periode_id,
                'kd_karyawan' => $kd_karyawan,
                'kd_bagian' => $kd_bagian,
            ];

            foreach ($questions as $q) {
                $quesanswer_id = $request->{'txt'.$q->question_id};
                $quesresultdetail_text = null;
                $quesresultdetail_value = null;

                switch ($q->questype_name) {
                    case 'text':
                        $quesresultdetail_text = $quesanswer_id;
                        $quesanswer_id = null;
                        break;
                    case 'number' :
                        $quesresultdetail_value = $quesanswer_id;
                        $quesanswer_id = null;
                        break;
                    case 'select' :
                        foreach ($quesanswer_id as $eAnswer) {
                            $arrayQuestionresultDetail[] = [
                                'question_id' => $q->question_id,
                                'quesanswer_id' => $eAnswer,
                                'questype_id' => $q->questype_id,
                                'questype_name' => $q->questype_name,
                                'quesresultdetail_text' => null,
                                'quesresultdetail_value' => null,
                            ];
                        }
                        break;
                }
                if ($q->questype_name == 'select') {
                    continue;
                }

                $arrayQuestionresultDetail[] = [
                    'question_id' => $q->question_id,
                    'quesanswer_id' => $quesanswer_id,
                    'questype_id' => $q->questype_id,
                    'questype_name' => $q->questype_name,
                    'quesresultdetail_text' => $quesresultdetail_text,
                    'quesresultdetail_value' => $quesresultdetail_value,
                ];
            }
            /** Cek required */


            $act = Question_resultModel::create($arrayQuestionresult);
            if ($act) {
                $answers = Question_answerModel::where('q_id', $q_id)->get();
                foreach ($arrayQuestionresultDetail as $eQuestionresult) {
                    $quesresultdetail_point = 0;
                    foreach ($answers as $answer) {
                        if ($answer->quesanswer_id == $eQuestionresult['quesanswer_id']){
                            $quesresultdetail_point = $answer->quesanswer_point;
                        }
                    }
                    $arraySubmitQuestionresultDetail[] = [
                        'quesresult_id' => $act->quesresult_id,
                        'question_id' => $eQuestionresult['question_id'],
                        'quesanswer_id' => $eQuestionresult['quesanswer_id'],
                        'questype_id' => $eQuestionresult['questype_id'],
                        'quesresultdetail_value' => $eQuestionresult['quesresultdetail_value'],
                        'quesresultdetail_text' => $eQuestionresult['quesresultdetail_text'],
                        'quesresultdetail_point' => $quesresultdetail_point,
                    ];
                }
                $actDetail = Question_result_detailModel::insert($arraySubmitQuestionresultDetail);
            }
            // if ($act) {
            // }
            $resp['data'] = $actDetail;

        }else {
            /** Sudah pernah input */
        }




        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                User::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
