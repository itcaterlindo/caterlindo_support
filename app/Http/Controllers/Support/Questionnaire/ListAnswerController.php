<?php

namespace App\Http\Controllers\Support\Questionnaire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\DB;

use App\Models\QuestionnaireModel;
use App\Models\QuestionModel;
use App\Models\Question_answerModel;
use Illuminate\Support\Facades\Auth;

class ListAnswerController extends Controller
{
    private $class_link = 'support/kuesioner/listkuesioner/listjawaban';
    
    public function index($question_id)
    {
        $data['row'] = QuestionModel::where('question_id', $question_id)->first();
        $data['question_id'] = $question_id;
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }
    
    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $question_id = $request->question_id;

        $data['class_link'] = $this->class_link;
        $data['question_id'] = $question_id;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $question_id = $request->question_id;
        
        $answers = Question_answerModel::where(['question_id' => $question_id])->get();

        return DataTables::of($answers)
                ->addIndexColumn()
                ->addColumn('opsi', function ($answer){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    if (Auth::user()->can('KUESIONER_UPDATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$answer->quesanswer_id.'")> <i class="fas fa-edit"></i> Edit</a>
                        <div class="dropdown-divider"></div>';
                    }
                    if (Auth::user()->can('KUESIONER_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$answer->quesanswer_id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->rawColumns(['opsi'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $question_id = $request->question_id;
        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Question_answerModel::where('quesanswer_id', $id)->first()->toArray();
        }
       
        $data['id'] = $id;
        $data['q_id'] = QuestionModel::where('question_id', $question_id)->first()->q_id;
        $data['question_id'] = $question_id;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'quesanswer_text' => 'required',
            'quesanswer_point' => 'required',
        ], [
            'quesanswer_text.required' => 'Kategori tidak boleh kosong',
            'quesanswer_point.required' => 'Type tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $answer_id = $request->id;
                $q_id = $request->q_id;
                $question_id = $request->question_id;
                $quesanswer_text = $request->quesanswer_text;
                $quesanswer_point = $request->quesanswer_point;
                
                $user_id = Auth::user()->id;
                
                if (empty($answer_id)) {
                    $arrayData = [
                        'q_id' => $q_id,
                        'question_id' => $question_id,
                        'quesanswer_text' => $quesanswer_text,
                        'quesanswer_point' => $quesanswer_point,
                        'user_id' => $user_id,
                    ];
                    Question_answerModel::create($arrayData);
                }else {
                    $answer = Question_answerModel::find($answer_id);
                    $answer->q_id = $q_id;
                    $answer->question_id = $question_id;
                    $answer->quesanswer_text = $quesanswer_text;
                    $answer->quesanswer_point = $quesanswer_point;
                    $answer->user_id = $user_id;

                    $answer->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Question_answerModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
