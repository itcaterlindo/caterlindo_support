<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use DB;
use App\Models\ticketModel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ReportTicketController extends Controller
{

    private $class_link = 'general_affair/asetmanajemen/ticket/reportticket';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getData(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        if($request->asetjenis_id){
            $query = DB::select("SELECT ticket.ticket_id, 
                            ticket.ticket_kode, 
                            aset.aset_barcode, 
                            aset.aset_nama, 
                            ticket.status_ticket, 
                            lokasi.lokasi_nama, 
                            ticket.sparepart, 
                            (SELECT COUNT(ticket.aset_id) FROM ticket WHERE ticket.aset_id = aset.aset_id AND DATE(ticket.created_ticket) >= '$request->start' AND DATE(ticket.created_ticket) <= '$request->end') as jumlah,
                            ticket.created_ticket,
                            ticket.problem
                        FROM ticket
                        INNER JOIN aset on ticket.aset_id = aset.aset_id
                        INNER JOIN lokasi on lokasi.lokasi_id = aset.lokasi_id
                        WHERE DATE(ticket.created_ticket) >= '$request->start' AND DATE(ticket.created_ticket) <= '$request->end' AND aset.asetjenis_id ='$request->asetjenis_id'");
        }else{       
                $query = DB::select("SELECT ticket.ticket_id, 
                            ticket.ticket_kode, 
                            aset.aset_barcode, 
                            aset.aset_nama, 
                            ticket.status_ticket, 
                            lokasi.lokasi_nama, 
                            ticket.sparepart, 
                            (SELECT COUNT(ticket.aset_id) FROM ticket WHERE ticket.aset_id = aset.aset_id AND DATE(ticket.created_ticket) >= '$request->start' AND DATE(ticket.created_ticket) <= '$request->end') as jumlah,
                            ticket.created_ticket,
                            ticket.problem
                        FROM ticket
                        INNER JOIN aset on ticket.aset_id = aset.aset_id
                        INNER JOIN lokasi on lokasi.lokasi_id = aset.lokasi_id
                        WHERE DATE(ticket.created_ticket) >= '$request->start' AND DATE(ticket.created_ticket) <= '$request->end'");
        }
                return DataTables::of($query)
                    ->addIndexColumn()
                    ->toJson();
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['start'] = $request->start;
        $data['end'] = $request->end;
        $data['asetjenis_id'] = $request->asetjenis_id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render(); 
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function show(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stok_Opname $stok_Opname)
    {
        //
    }
}
