<?php
namespace App\Http\Controllers\GeneralAffair\ManajemenAset;
use App\Http\Controllers\Controller;

use App\Models\Aset_Stokopname_periodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\AsetModel;
use App\Models\Aset_mutasiModel;
use App\Models\Category_budgetingModel;
use App\Models\Budgetingdetail_has_monthModel;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class ReportBudgetingCtgController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/reportbudgetingctg';
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['csrf_token'] = csrf_token();
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
       
        global $stts;
        global $periode_b;
        global $query;
     
        $ctg_bgd = $request->ctg_bgd;
        $periode_b = $request->tahun;

            if(empty($periode_b)){
                $query = DB::select('SELECT td_barang_budgeting.*, tb_bagian.bagian_nama, category_budgeting.nm_category_budgeting, tm_budgeting.th_anggaran, td_prd.prdetail_kd as pr_no FROM td_barang_budgeting
                        LEFT JOIN tm_budgeting ON tm_budgeting.kode_budgeting = td_barang_budgeting.id_budgeting
                        LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_budgeting.id_bagian
                        LEFT JOIN db_caterlindo_ppic.td_purchaserequisition_detail as td_prd ON td_prd.budgeting_detail_kd = td_barang_budgeting.id_barang
                        LEFT JOIN category_budgeting ON category_budgeting.id_category_budgeting = td_barang_budgeting.id_category

                        WHERE id_category = "'.$ctg_bgd.'" AND stts != "ditolak" ');
            }else if($ctg_bgd == 'all' && !empty($periode_b)){
                $query = DB::select('SELECT td_barang_budgeting.*, tb_bagian.bagian_nama, category_budgeting.nm_category_budgeting, tm_budgeting.th_anggaran, td_prd.prdetail_kd as pr_no FROM td_barang_budgeting
                        LEFT JOIN tm_budgeting ON tm_budgeting.kode_budgeting = td_barang_budgeting.id_budgeting
                        LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_budgeting.id_bagian
                        LEFT JOIN db_caterlindo_ppic.td_purchaserequisition_detail as td_prd ON td_prd.budgeting_detail_kd = td_barang_budgeting.id_barang
                        LEFT JOIN category_budgeting ON category_budgeting.id_category_budgeting = td_barang_budgeting.id_category

                        WHERE stts != "ditolak" AND th_anggaran ='.$periode_b);
            }else{
                $query = DB::select('SELECT td_barang_budgeting.*, tb_bagian.bagian_nama, category_budgeting.nm_category_budgeting, tm_budgeting.th_anggaran, td_prd.prdetail_kd as pr_no FROM td_barang_budgeting
                        LEFT JOIN tm_budgeting ON tm_budgeting.kode_budgeting = td_barang_budgeting.id_budgeting
                        LEFT JOIN tb_bagian ON tb_bagian.bagian_kd = tm_budgeting.id_bagian
                        LEFT JOIN db_caterlindo_ppic.td_purchaserequisition_detail as td_prd ON td_prd.budgeting_detail_kd = td_barang_budgeting.id_barang
                        LEFT JOIN category_budgeting ON category_budgeting.id_category_budgeting = td_barang_budgeting.id_category

                        WHERE id_category = '.$ctg_bgd.' AND stts != "ditolak" AND th_anggaran ='.$periode_b);

            }

                $resp['datax'] =  $query; 
          
                $resp['xx'] = $periode_b;
                $resp['xxx'] = $stts;
                $resp['ctg'] = Category_budgetingModel::all();
                $resp['bhm'] = Budgetingdetail_has_monthModel::all();
                $resp['_token'] = csrf_token();
                return response()->json($resp);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function show(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function edit(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompareStokopname $compareStokopname)
    {
        //
    }
}
