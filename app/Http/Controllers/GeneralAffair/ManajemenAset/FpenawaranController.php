<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use stdClass;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Encryption\DecryptException;
use Session;

use Illuminate\Support\Facades\Crypt;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use App\Models\HRM\Tb_karyawanModel;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Aset_jenisModel;
use App\Models\PenawaranModel;
use App\Models\Lelang_has_fileModel;
use App\Models\LelangModel;
use App\Models\TagModel;
use Illuminate\Support\Facades\Auth;
use Svg\Tag\Rect;

class FpenawaranController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/fpenawaran';
    private $fclass_link = 'lelang/penawaran';
    
    public function rupiah($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
    public function index($encryptKaryawan)
    {   
        $kd_karyawan = decrypt($encryptKaryawan);

        $close = DB::select("UPDATE lelang
                            SET `status`='close'
                            WHERE tanggal_close <= NOW() AND `status` = 'open'");

        $lelang = DB::select('SELECT * FROM
                        (SELECT 
                                         DATE(DATE(B.tanggal_close) + 3) as range_close,
                                            A.id_lelang as kd_lelang,
                                            B.*,
                                            C.*,
                                            (SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang = A.id_lelang) as penawaran
                                            FROM lelang B
                                            LEFT JOIN 
                                            lelang_has_file A ON B.id_lelang = A.id_lelang
                                            INNER JOIN file C ON A.id_file = C.file_id WHERE B.status != "draft" AND DATE(B.tanggal_close) BETWEEN DATE(B.tanggal_close) AND DATE(B.tanggal_close + 3) GROUP BY b.id_lelang ORDER BY b.id_lelang DESC
                        ) AS VW_lelang WHERE range_close >= CURDATE()');


        $data['close'] = $close;
        $data['alltag'] = TagModel::all();

        $newBookInfo = Array();
        $newBookKey = [];
        $newKey = 0;
        foreach($lelang as $lelangs => $lelangsvalue){

        if(!in_array($lelangsvalue->id_lelang,$newBookKey)){
            ++$newKey;
            $newBookInfo[$newKey]['id_lelang'] = $lelangsvalue->id_lelang;
            $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
            $newBookInfo[$newKey]['kode_lelang'] = $lelangsvalue->kode_lelang;
            $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
            $newBookInfo[$newKey]['jumlah'] = $lelangsvalue->jumlah;
            $newBookInfo[$newKey]['status'] = $lelangsvalue->status;
            $newBookInfo[$newKey]['start_harga'] = $lelangsvalue->start_harga;
            $newBookInfo[$newKey]['kelipatan_bit'] = $lelangsvalue->kelipatan_bit;
            $newBookInfo[$newKey]['note'] = $lelangsvalue->note;
            $newBookInfo[$newKey]['penawaran'] = $lelangsvalue->penawaran;
            $newBookInfo[$newKey]['tanggal_close'] = $lelangsvalue->tanggal_close;
            $newBookInfo[$newKey]['tanggal_start'] = $lelangsvalue->tanggal_start;
            $newBookInfo[$newKey]['created_lelang'] = $lelangsvalue->created_lelang;
            $newBookInfo[$newKey]['updated_lelang'] = $lelangsvalue->updated_lelang;
            }
            $mydata = $newBookInfo[$newKey]['filename'][$lelangs] = $lelangsvalue->name;
            $mydata = array_values($newBookInfo[$newKey]['filename']);
            $newBookInfo[$newKey]['filename'] = $mydata;
            $newBookKey[]  = $lelangsvalue->id_lelang;
        }


        
        $data['items']  = $this->paginate($newBookInfo);
        $data['class_link'] = $this->class_link;
        $data['karyawan'] = Tb_karyawanModel::where('kd_karyawan', '=', $kd_karyawan)
                    ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian','=','tb_bagian.kd_bagian')->first();
        $data['key'] = $encryptKaryawan;

        Session::put('data_karyawan', $data['karyawan']);
        return view("page/$this->class_link/index", $data);
    }

    public function encryptLelang($kd_karyawan)
    {
        $encryptKaryawan = encrypt($kd_karyawan);
        
        return redirect("$this->fclass_link/$encryptKaryawan");
    }

    public function search(Request $request)
    {
        
        if($request->tag){
            $lelang = DB::select( DB::raw("SELECT 
                A.*,
                B.*,
                C.*,
                D.*,
                (SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang = A.id_lelang) as penawaran
            FROM lelang B
            LEFT JOIN 
                lelang_has_file A ON B.id_lelang = A.id_lelang
                INNER JOIN file C ON A.id_file = C.file_id 
                INNER JOIN lelang_has_tag D ON B.id_lelang = D.id_lelang WHERE D.id_tag = :tag AND B.status != 'draft' AND B.status != 'close' ORDER BY b.id_lelang DESC"), ['tag' => $request->tag]);
            $data_tag = TagModel::where('id_tag', $request->tag)->first();
            
            $data['tag'] = $data_tag->name;
            
        }else{
            $lelang = DB::select( DB::raw("SELECT 
                A.*,
                B.*,
                C.*,
                (SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang = A.id_lelang) as penawaran
            FROM lelang B
            LEFT JOIN 
                lelang_has_file A ON B.id_lelang = A.id_lelang
                INNER JOIN file C ON A.id_file = C.file_id  WHERE b.nama LIKE :search ORDER BY b.id_lelang DESC"), ['search' => '%' . $request->search . '%']);
        }
        

        $data['alltag'] =  TagModel::all();

        $newBookInfo = Array();
        $newBookKey = [];
        $newKey = 0;
        foreach($lelang as $lelangs => $lelangsvalue){

        if(!in_array($lelangsvalue->id_lelang,$newBookKey)){
        ++$newKey;
        $newBookInfo[$newKey]['id_lelang'] = $lelangsvalue->id_lelang;
        $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
        $newBookInfo[$newKey]['kode_lelang'] = $lelangsvalue->kode_lelang;
        $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
        $newBookInfo[$newKey]['jumlah'] = $lelangsvalue->jumlah;
        $newBookInfo[$newKey]['status'] = $lelangsvalue->status;
        $newBookInfo[$newKey]['start_harga'] = $lelangsvalue->start_harga;
        $newBookInfo[$newKey]['kelipatan_bit'] = $lelangsvalue->kelipatan_bit;
        $newBookInfo[$newKey]['note'] = $lelangsvalue->note;
        $newBookInfo[$newKey]['penawaran'] = $lelangsvalue->penawaran;
        $newBookInfo[$newKey]['tanggal_close'] = $lelangsvalue->tanggal_close;
        $newBookInfo[$newKey]['tanggal_start'] = $lelangsvalue->tanggal_start;
        $newBookInfo[$newKey]['created_lelang'] = $lelangsvalue->created_lelang;
        $newBookInfo[$newKey]['updated_lelang'] = $lelangsvalue->updated_lelang;
        }

        $mydata = $newBookInfo[$newKey]['filename'][$lelangs] = $lelangsvalue->name;
        $mydata = array_values($newBookInfo[$newKey]['filename']);
        $newBookInfo[$newKey]['filename'] = $mydata;
        $newBookKey[]  = $lelangsvalue->id_lelang;
        }



        $data['items']  = $this->paginate($newBookInfo);
        $data['class_link'] = $this->class_link;
        $data['key'] = $request->search;
        $data['karyawan'] = Session::get('data_karyawan');

        // return response()->json($lelang);
        return view("page/$this->class_link/search", $data);
    }

    public function open_view(Request $request)
    {
        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data_pemenang = db::select('SELECT * FROM penawaran_lelang WHERE id_lelang = '. $id .' ORDER BY id_penawaran DESC LIMIT 1');

            $data['row'] = LelangModel::where('id_lelang', $id)->get()->toArray();
            $data['penawaran'] = PenawaranModel::where('id_lelang', $id)->orderBy('id_penawaran', 'DESC')->get();
            $data['jml_penawaran'] = PenawaranModel::where('id_lelang', $id)->count();
            $data['tag'] = DB::select('SELECT * FROM lelang_has_tag INNER JOIN tag ON lelang_has_tag.id_tag = tag.id_tag WHERE lelang_has_tag.id_lelang =' . $id);
            $data['image'] = DB::select('SELECT * FROM lelang_has_file INNER JOIN file ON lelang_has_file.id_file = file.file_id WHERE lelang_has_file.id_lelang =' . $id);
            $data['penawaran_tertinggi'] = $data_pemenang;
        }
        if($request->kd_k){
            $data['karyawan'] = Tb_karyawanModel::where('kd_karyawan', '=', $request->kd_k)
                    ->leftJoin('tb_bagian', 'tb_karyawan.kd_bagian','=','tb_bagian.kd_bagian')->first();
        }

        $data['id'] = $id;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;

        // return response()->json($request->all());

        // dd($data);
        return view('page/' . $this->class_link . '/view', $data)->render();
    }

    // public function partial_list_main(Request $request)
    // {
    //     // if (!($request->ajax())) {
    //     //     exit('No direct script access allowed');
    //     // }
    //     $lelang = DB::select('SELECT 
    //                             A.*,
    //                             B.*,
    //                             C.*,
	// 	                        (SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang = A.id_lelang) as penawaran
    //                         FROM lelang B
    //                         LEFT JOIN 
    //                             lelang_has_file A ON B.id_lelang = A.id_lelang
    //                         INNER JOIN file C ON A.id_file = C.file_id ORDER BY a.id_lelang DESC');
    //     $data['alltag'] =  TagModel::all();

    //     $newBookInfo = Array();
    //     $newBookKey = [];
    //     $newKey = 0;
    //     foreach($lelang as $lelangs => $lelangsvalue){

    //     if(!in_array($lelangsvalue->id_lelang,$newBookKey)){
    //         ++$newKey;
    //         $newBookInfo[$newKey]['id_lelang'] = $lelangsvalue->id_lelang;
    //         $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
    //         $newBookInfo[$newKey]['kode_lelang'] = $lelangsvalue->kode_lelang;
    //         $newBookInfo[$newKey]['nama'] = $lelangsvalue->nama;
    //         $newBookInfo[$newKey]['jumlah'] = $lelangsvalue->jumlah;
    //         $newBookInfo[$newKey]['status'] = $lelangsvalue->status;
    //         $newBookInfo[$newKey]['start_harga'] = $lelangsvalue->start_harga;
    //         $newBookInfo[$newKey]['kelipatan_bit'] = $lelangsvalue->kelipatan_bit;
    //         $newBookInfo[$newKey]['note'] = $lelangsvalue->note;
    //         $newBookInfo[$newKey]['penawaran'] = $lelangsvalue->penawaran;
    //         $newBookInfo[$newKey]['tanggal_close'] = $lelangsvalue->tanggal_close;
    //         $newBookInfo[$newKey]['tanggal_start'] = $lelangsvalue->tanggal_start;
    //         $newBookInfo[$newKey]['created_lelang'] = $lelangsvalue->created_lelang;
    //         $newBookInfo[$newKey]['updated_lelang'] = $lelangsvalue->updated_lelang;
    //         }

    //         $newBookInfo[$newKey]['filename'][$lelangs] = $lelangsvalue->name;
    //         $newBookKey[]  = $lelangsvalue->id_lelang;
    //     }


        
    //     $data['items']  = $this->paginate($newBookInfo);
    //     // $data['ori_lelang'] = DB::table('lelang')->paginate(5);
    //     $data['class_link'] = $this->class_link;

    //     // dd($data);

    //     return view('page/' . $this->class_link . '/list', $data)->render();
    // }

    public function paginate($items, $perPage = 6, $page = null, $options = [])
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 
    
        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
    
        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetJeniss = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
            ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
            ->get();

        return DataTables::of($asetJeniss)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetJenis) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","' . $asetJenis->asetjenis_id . '")> <i class="fas fa-edit"></i> Edit</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetJenis->asetjenis_id . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('asetjenis_updated', function ($asetJenis) {
                return $asetJenis->asetjenis_updated->toDateTimeString();
            })
            ->rawColumns(['opsi', 'asetjenis_updated'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
                ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
                ->where('aset_jenis.asetjenis_id', $id)
                ->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'asetjenis_nama' => 'required',
            'asetjenis_kode' => 'required',
        ], [
            'asetjenis_nama.required' => 'Kategori tidak boleh kosong',
            'asetjenis_kode.required' => 'Kode tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $asetjenis_id = $request->id;
                $user_id = Auth::user()->id;
                if (empty($asetjenis_id)) {

                    $arrayData = [
                        'asetjenis_nama' => $request->asetjenis_nama,
                        'asetjenis_parent' => $request->asetjenis_parent,
                        'asetjenis_kode' => strtoupper($request->asetjenis_kode),
                        'user_id' => $user_id,
                    ];
                    Aset_jenisModel::create($arrayData);
                } else {
                    $lokasi = Aset_jenisModel::find($asetjenis_id);

                    $lokasi->asetjenis_nama = $request->asetjenis_nama;
                    $lokasi->asetjenis_parent = $request->asetjenis_parent;
                    $lokasi->asetjenis_kode = strtoupper($request->asetjenis_kode);
                    $lokasi->user_id = $user_id;

                    $lokasi->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                Aset_jenisModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
