<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\LokasiModel;
use App\Models\HRM\Tb_bagianModel;

use Illuminate\Support\Facades\Auth;

class AsetLokasiController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/asetlokasi';

    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $lokasis = LokasiModel::all();

        return DataTables::of($lokasis)
            ->addIndexColumn()
            ->addColumn('opsi', function ($lokasi) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                if (Auth::user()->can('KATEGORI_UDPATE')) {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","' . $lokasi->lokasi_id . '")> <i class="fas fa-edit"></i> Edit</a>';
                }
                if (Auth::user()->can('KATEGORI_DELETE')) {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $lokasi->lokasi_id . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('lokasi_updated', function ($lokasi) {
                return $lokasi->lokasi_updated->toDateTimeString();
            })
            ->rawColumns(['opsi', 'lokasi_updated'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = LokasiModel::where('lokasi_id', $id)->first()->toArray();
            $data['row']['nm_bagian'] = Tb_bagianModel::where('kd_bagian', $data['row']['kd_bagian'])->first()->nm_bagian;
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'lokasi_nama' => 'required',
            'lokasi_kode' => 'required',
        ], [
            'lokasi_nama.required' => 'Kategori tidak boleh kosong',
            'lokasi_kode.required' => 'Kode tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $lokasi_id = $request->id;
                $user_id = Auth::user()->id;
                if (empty($lokasi_id)) {

                    $arrayData = [
                        'kd_bagian' => $request->kd_bagian,
                        'kd_unit' => $request->kd_unit,
                        'lokasi_nama' => $request->lokasi_nama,
                        'lokasi_keterangan' => $request->lokasi_keterangan,
                        'lokasi_kode' => strtoupper($request->lokasi_kode),
                        'user_id' => $user_id,
                    ];
                    LokasiModel::create($arrayData);
                } else {
                    $lokasi = LokasiModel::find($lokasi_id);
                    $lokasi->kd_unit = $request->kd_unit;
                    $lokasi->kd_bagian = $request->kd_bagian;
                    $lokasi->lokasi_nama = $request->lokasi_nama;
                    $lokasi->lokasi_keterangan = $request->lokasi_keterangan;
                    $lokasi->lokasi_kode = strtoupper($request->lokasi_kode);
                    $lokasi->user_id = $user_id;

                    $lokasi->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                LokasiModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
