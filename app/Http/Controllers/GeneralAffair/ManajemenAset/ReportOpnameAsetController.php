<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;
use App\Http\Controllers\Controller;

use App\Models\Aset_Stokopname_periodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\AsetModel;
use App\Models\Aset_mutasiModel;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class ReportOpnameAsetController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/reportopnameaset';
    protected $dataReport = array();
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['csrf_token'] = csrf_token();
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    public function partial_form_final_report_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['csrf_token'] = csrf_token();
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_final_report_main', $data)->render();
    }


    public function setNomorseri(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $dataPeriode = Aset_Stokopname_periodeModel::where('aset_stokopname_jenis_kd', $request->jenis)->where('bulan', $request->bulan)->where('tahun', $request->tahun)->get();
        
        return response()->json($dataPeriode);
    }
    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function partial_table_report_final_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_report_final_main', $data)->render();
    }

    public function partial_table_report_main(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_report_main', $data)->render();
    }

    public function getDataReport(Request $request)
    {
        $parameter = Aset_Stokopname_periodeModel::where('nomor_seri', $request->periode_nomor_seri)->first();
        $query = DB::select("select 
        LOK.lokasi_nama As lokasi_nama,
        LOK.pic As pic, 
        ASDM.checker as checker,
        (SELECT COUNT(*) FROM aset_stokopname_detail WHERE lokasi_kd = ASDM.lokasi_kd AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as jumlah_opname,
        (SELECT COUNT(*) FROM aset_stokopname_detail  INNER JOIN aset ON aset_stokopname_detail.aset_kd =  aset.aset_id  WHERE lokasi_kd = ASDM.lokasi_kd AND aset.asetstatus_id = '4' AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as jumlah_rusak_opname,
        (SELECT COUNT(*) FROM aset WHERE lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id != '2' AND aset.asetstatus_id != '4') as jumlah_disystem,
        (SELECT COUNT(*) FROM aset WHERE lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id = '4') as jumlah_rusak_disystem,
        ((SELECT COUNT(*) FROM aset WHERE lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id != '2' AND aset.asetstatus_id != '4') - (SELECT COUNT(*) FROM aset_stokopname_detail INNER JOIN aset ON aset.aset_id = aset_stokopname_detail.aset_kd WHERE aset.lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id != '2' AND aset.asetstatus_id != '4' AND aset_stokopname_detail.aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ")) as tidak_ditemukan,
        ((SELECT COUNT(*) FROM aset WHERE lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id = '4') - (SELECT COUNT(*) FROM aset_stokopname_detail INNER JOIN aset ON aset.aset_id = aset_stokopname_detail.aset_kd WHERE aset.lokasi_id = ASDM.lokasi_kd AND aset.asetstatus_id = '4' AND aset_stokopname_detail.aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ")) as rusak_tidak_ditemukan,
        
        (SELECT COUNT(aset.lokasi_id) FROM aset_stokopname_detail 
        INNER JOIN aset ON aset_stokopname_detail.aset_kd =  aset.aset_id 
            WHERE lokasi_kd != ASDM.lokasi_kd AND aset.lokasi_id = ASDM.lokasi_kd AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as pindah_tpt_lain,


        (SELECT COUNT(aset.lokasi_id) FROM aset_stokopname_detail 
        INNER JOIN aset ON aset_stokopname_detail.aset_kd =  aset.aset_id 
            WHERE aset.lokasi_id != ASDM.lokasi_kd AND lokasi_kd = ASDM.lokasi_kd AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as perpindahan,
        (SELECT COUNT(*) FROM aset_stokopname_detail WHERE kondisi = 'baik' AND lokasi_kd = ASDM.lokasi_kd AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as kondisi_baik,
        (SELECT COUNT(*) FROM aset_stokopname_detail WHERE kondisi = 'rusak' AND lokasi_kd = ASDM.lokasi_kd  AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as kondisi_rusak,
        (SELECT COUNT(*) FROM aset_stokopname_detail WHERE kondisi ='kurang baik' AND lokasi_kd = ASDM.lokasi_kd AND aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . ") as kondisi_kurang_baik
        from aset_stokopname_detail ASDM 
        inner join lokasi LOK on ASDM.lokasi_kd = LOK.lokasi_id 
        inner join users USR on ASDM.user_id = USR.id
        WHERE ASDM.aset_stokopname_periode_kd = " . $parameter->aset_stokopname_periode_kd . " GROUP BY ASDM.lokasi_kd");

        
    
        return DataTables::of($query)
            ->addIndexColumn()
            ->rawColumns(['opsi'])
            ->toJson();
    }
     /*
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        global $data_nomor_seri;
        global $asetjenis_id;

        $data_nomor_seri = $request->periode_nomor_seri;
        $asetjenis_id = $request->asetjenis_id;        
        
        if($asetjenis_id && $request->report_lokasi){
            $asetStokopname = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                global $data_nomor_seri;
                $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.nomor_seri', '=', $data_nomor_seri);
                    })->join('aset', function ($join) {
                        global $asetjenis_id;
                        $join->on('aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')->where('aset.asetjenis_id', '=', $asetjenis_id);
                    })
                ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                
                ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                ->select(
                    'aset_stokopname_periode.*',
                    'aset_stokopname_detail.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset_jenis.asetjenis_nama',
                    'lok_aset.lokasi_nama as lokasi_aset',
                    'aset.aset_keterangan',
                    'lok_det.lokasi_nama as lokasi_opname'
                )->where('aset_stokopname_detail.lokasi_kd', '=', $request->report_lokasi)->get();
        }elseif($asetjenis_id && !$request->report_lokasi){
            $asetStokopname = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                global $data_nomor_seri;
                $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.nomor_seri', '=', $data_nomor_seri);
                    })->join('aset', function ($join) {
                        global $asetjenis_id;
                        $join->on('aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')->where('aset.asetjenis_id', '=', $asetjenis_id);
                    })
                ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                
                ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                ->select(
                    'aset_stokopname_periode.*',
                    'aset_stokopname_detail.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset_jenis.asetjenis_nama',
                    'lok_aset.lokasi_nama as lokasi_aset',
                    'aset.aset_keterangan',
                    'lok_det.lokasi_nama as lokasi_opname'
                )->get();
        }elseif(!$asetjenis_id && $request->report_lokasi){
            $asetStokopname = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                global $data_nomor_seri;
                $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.nomor_seri', '=', $data_nomor_seri);
                    })->join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                ->select(
                    'aset_stokopname_periode.*',
                    'aset_stokopname_detail.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset_jenis.asetjenis_nama',
                    'lok_aset.lokasi_nama as lokasi_aset',
                    'aset.aset_keterangan',
                    'lok_det.lokasi_nama as lokasi_opname'
                )->where('aset_stokopname_detail.lokasi_kd', '=', $request->report_lokasi)->get();
        }else{
            $asetStokopname = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                global $data_nomor_seri;
                $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.nomor_seri', '=', $data_nomor_seri);
                    })->join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                
                ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                ->select(
                    'aset_stokopname_periode.*',
                    'aset_stokopname_detail.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset_jenis.asetjenis_nama',
                    'lok_aset.lokasi_nama as lokasi_aset',
                    'aset.aset_keterangan',
                    'lok_det.lokasi_nama as lokasi_opname'
                )->get();
        }

        return DataTables::of($asetStokopname)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetStokopname) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            }) 
            ->editColumn('aset_stokopname_created_at', function ($asetStokopname) {
                return $asetStokopname->aset_stokopname_created_at->toDateTimeString();
            })
            ->rawColumns(['opsi'])
            ->toJson();  
    }

    public function getLokasiInPeriode(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $param = $request->nomor_seri;
        $parameter = Aset_Stokopname_periodeModel::where('nomor_seri', $param)->first();
        $query = DB::select("SELECT lokasi.lokasi_id, lokasi.lokasi_nama
                                FROM aset_stokopname_detail
                                INNER JOIN lokasi ON aset_stokopname_detail.lokasi_kd = lokasi.lokasi_id
                                WHERE aset_stokopname_detail.aset_stokopname_periode_kd = ? GROUP BY lokasi.lokasi_id", [$parameter->aset_stokopname_periode_kd]);
        return response()->json($query);
    }

    public function finalReport(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $parameter = Aset_Stokopname_periodeModel::where('nomor_seri', $request->periode_nomor_seri)->first();
        $ab = $request->status;

       if($request->status == "not-found"){
            $query = [];
            $data = DB::select('SELECT 
                                            "'. $request->periode_nomor_seri .'" as nomor_seri,
                                            aset.aset_barcode,
                                            aset.aset_nama,
                                            aset_jenis.asetjenis_nama,
                                            lokasi.lokasi_nama,
                                            aset.aset_id,
                                            "" as lokasi_opname,
                                            "" as note,
                                            "" as aset_stokopname_created_at,
                                            "" as kondisi,
                                            (SELECT lokasi.lokasi_nama FROM aset_stokopname_detail 
                                            INNER JOIN lokasi ON lokasi.lokasi_id = aset_stokopname_detail.lokasi_kd WHERE aset_stokopname_detail.aset_stokopname_periode_kd ='. $parameter->aset_stokopname_periode_kd .' AND aset_stokopname_detail.aset_kd = aset.aset_id) as lokasi_ditemukan

                                FROM aset
                                                INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                                    INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                                WHERE aset.aset_id 
                                NOT IN (SELECT aset_stokopname_detail.aset_kd FROM aset_stokopname_detail WHERE aset_stokopname_detail.aset_stokopname_periode_kd = :periode AND aset_stokopname_detail.lokasi_kd ='. $request->report_lokasi .' ) 
                                AND aset.asetstatus_id != "2"  AND aset.asetstatus_id != "4" AND aset.lokasi_id ='. $request->report_lokasi , ['periode' => $parameter->aset_stokopname_periode_kd]);

                                foreach($data as $name=>$data)
                                    {
                                        //If the salary is more than 12,0000.
                                        if($data->lokasi_ditemukan == null)
                                        {
                                            //Push to filtered array
                                            array_push($query,$data);
                                        }
                                    }
       }else if($request->status == "pindah_ke_tempat_lain"){
            $query = [];
            $data = DB::select('SELECT 
                                                "'. $request->periode_nomor_seri .'" as nomor_seri,
                                                aset.aset_barcode,
                                                aset.aset_nama,
                                                aset_jenis.asetjenis_nama,
                                                lokasi.lokasi_nama,
                                                aset.aset_id,
                                                "" as lokasi_opname,
                                                "" as note,
                                                "" as aset_stokopname_created_at,
                                                "" as kondisi,
                                                (SELECT lokasi.lokasi_nama FROM aset_stokopname_detail 
                                                INNER JOIN lokasi ON lokasi.lokasi_id = aset_stokopname_detail.lokasi_kd WHERE aset_stokopname_detail.aset_stokopname_periode_kd ='. $parameter->aset_stokopname_periode_kd .' AND aset_stokopname_detail.aset_kd = aset.aset_id) as lokasi_ditemukan

                                    FROM aset
                                                    INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                                        INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                                    WHERE aset.aset_id 
                                    NOT IN (SELECT aset_stokopname_detail.aset_kd FROM aset_stokopname_detail WHERE aset_stokopname_detail.aset_stokopname_periode_kd = :periode AND aset_stokopname_detail.lokasi_kd ='. $request->report_lokasi .' ) 
                                    AND aset.asetstatus_id != "2" AND aset.asetstatus_id != "4" AND aset.lokasi_id ='. $request->report_lokasi , ['periode' => $parameter->aset_stokopname_periode_kd]);


                                    foreach($data as $name=>$data)
                                    {
                                        //If the salary is more than 12,0000.
                                        if($data->lokasi_ditemukan != null)
                                        {
                                            //Push to filtered array
                                            array_push($query,$data);
                                        }
                                    }

   }else if($request->status == "perpindahan"){
            $query = DB::select('SELECT 
                                aset_stokopname_periode.nomor_seri,
                                aset.aset_barcode,
                                aset.aset_nama,
                                aset.aset_id,
                                aset_jenis.asetjenis_nama,
                                lokasi.lokasi_nama as lokasi_opname,
                                (SELECT lokasi.lokasi_nama FROM aset 
                                    INNER JOIN lokasi ON lokasi.lokasi_id = aset.lokasi_id WHERE aset.aset_id = ASDM.aset_kd) as lokasi_nama,
                                ASDM.note,
                                ASDM.aset_stokopname_created_at,
                                ASDM.kondisi,
                                lokasi.lokasi_nama as lokasi_ditemukan
                                    FROM aset_stokopname_detail AS ASDM
                                    INNER JOIN aset ON aset.aset_id = ASDM.aset_kd  
                                            INNER JOIN aset_stokopname_periode ON ASDM.aset_stokopname_periode_kd = aset_stokopname_periode.aset_stokopname_periode_kd
                                                INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                                                    INNER JOIN lokasi ON ASDM.lokasi_kd = lokasi.lokasi_id
                                                            WHERE aset.lokasi_id != :lokasi AND ASDM.lokasi_kd = '.  $request->report_lokasi .' AND ASDM.aset_stokopname_periode_kd = ' . $parameter->aset_stokopname_periode_kd, ['lokasi' => $request->report_lokasi]);
       }else {
           $query = DB::select('SELECT 
                                aset_stokopname_periode.nomor_seri,
                                aset.aset_barcode,
                                aset.aset_nama,
                                aset.aset_id,
                                aset_jenis.asetjenis_nama,
                                lokasi.lokasi_nama as lokasi_opname,
                                (SELECT lokasi.lokasi_nama FROM aset 
                                    INNER JOIN lokasi ON lokasi.lokasi_id = aset.lokasi_id WHERE aset.aset_id = ASDM.aset_kd) as lokasi_nama,
                                ASDM.note,
                                ASDM.aset_stokopname_created_at,
                                ASDM.kondisi as kondisi,
                                lokasi.lokasi_nama as lokasi_ditemukan
                        FROM aset_stokopname_detail AS ASDM
                        INNER JOIN aset ON aset.aset_id = ASDM.aset_kd  
                            INNER JOIN aset_stokopname_periode ON ASDM.aset_stokopname_periode_kd = aset_stokopname_periode.aset_stokopname_periode_kd
                                INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                                    INNER JOIN lokasi ON ASDM.lokasi_kd = lokasi.lokasi_id
                                            WHERE ASDM.kondisi = "'. $request->status . '" AND ASDM.lokasi_kd = :lokasi AND ASDM.aset_stokopname_periode_kd = ' . $parameter->aset_stokopname_periode_kd, ['lokasi' => $request->report_lokasi]);
       }

       return DataTables::of($query)
       ->addIndexColumn()
       ->addColumn('cetak', function ($query) {
        $va = '
        <input type="checkbox" id="cetak" data-id="' . $query->aset_id . '" name="vehicle2" value="Car">';
        return $va;
        })
        ->addColumn('pindah', function ($query) use ($ab) {
            if($ab == "not-found"){
            $html = '<button type="button" id="pindah" onClick="pindah_ke_ctr_area(' . $query->aset_id . ')" name="vehicle2">Mutasi</button>';
            }else if($ab == "pindah_ke_tempat_lain"){
                $html = '<button type="button" id="pindah" onClick="pindah_sesuai_dtmkn(' . $query->aset_id . ')" name="vehicle2">Mutasi</button>';
            }else{
                $html = "<p>.</p>";
            }
            return $html;
            })
            ->rawColumns(['pindah', 'cetak'])
        ->toJson();
    }

    public function sendMutasi(Request $request)
    {
        $aset_id = $request->id;
        $periode = $request->per;

        $aset = AsetModel::find($aset_id);

        try {
            $query = DB::select('SELECT * FROM aset_mutasi WHERE asetmutasi_status = "pending" AND aset_id = '. $aset->aset_id);
            if(COUNT($query) != 0){
                $resp['code'] = 409;
                $resp['messages'] = 'Gagal Simpan, Mutasi Blm di approve!';
                $resp['data'] = 'Dragon Ball';
            }else{
                $arrayData = [
                    'asetmutasi_kode' => Aset_mutasiModel::generate_asetmutasiKode(date('Y-m-d')),
                    'aset_id' => $aset->aset_id,
                    'asetmutasijenis_id' => '4',
                    'asetstatus_id' => $aset->asetstatus_id,
                    'asetmutasi_tanggal' => (date('Y-m-d')),
                    'asetmutasi_penyusutan' => null,
                    'asetmutasi_keterangan' => 'Barang tidak di temukan di stoktabe periode :' . $periode,
                    'asetmutasi_qty' => 1,
                    'asetmutasi_lokasidari' => $aset->lokasi_id,
                    'asetmutasi_lokasike' => '38',
                    'asetmutasi_bagiandari' => '',
                    'asetmutasi_bagianke' => '',
                    'asetmutasi_status' => 'pending',
                    'user_id' => '',
                ];
                Aset_mutasiModel::create($arrayData);
    
                $resp['status'] = 'add';
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
          
        }catch (Exception $e) {
            $resp['code'] = 400;
            $resp['messages'] = 'Gagal Simpan';
            $resp['data'] = $e->getMessage();
        }
        $resp['_token'] = csrf_token();
        return response()->json($resp);

    }
        /**
     * Store a newly created resource in storage.
     *r
     * @param  \Illuminate\Http\Request  $equest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function show(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stok_Opname $stok_Opname)
    {
        //
    }
}
