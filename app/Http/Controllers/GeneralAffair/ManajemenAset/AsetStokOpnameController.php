<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use App\Models\Aset_Stokopname_detailBarcodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\Aset_Stokopname_jenisModel;
use App\Models\Aset_Stokopname_periodeModel;
use App\Models\AsetModel;
use App\Models\LokasiModel;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class AsetStokOpnameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $class_link = 'general_affair/asetmanajemen/asetstokopname';

    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetStokopname = Aset_Stokopname_detailModel::join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                        ->join('aset_stokopname_periode', function ($join) {
                            $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                                 ->where('aset_stokopname_periode.status', '=', 'aktif');
                        })
                        ->join('lokasi', 'lokasi.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')->get();


                        

        return DataTables::of($asetStokopname)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetStokopname) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetStokopname->aset_stokopname_detail_kd . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('aset_stokopname_created_at', function ($asetStokopname) {
                return $asetStokopname->aset_stokopname_created_at->toDateTimeString();
            })
            ->rawColumns(['opsi'])
            ->toJson();
    }

    
    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            if (!($request->ajax())) {
                exit('No direct script access allowed');
            }
            $kd_aset = '';
            $validator = Validator::make($request->all(), [
                'opname_lokasi' => 'required',
                'opname_barcode' => 'required',
                'periode_kd'    => 'required',
                'opname_kondisi'    => 'required',
                'checker'    => 'required'
            ], [
                'opname_lokasi.required' => 'Lokasi tidak boleh kosong',
                'opname_barcode.required' => 'Barcode tidak boleh kosong',
                'periode_kd.required' => 'Kode Periode tidak boleh kosong',
                'opname_kondisi.required' => 'Kondisi Barang tidak boleh kosong',
                'checker.required' => 'Checker tidak boleh kosong',
            ]);


            if ($validator->fails()) {
                $resp['code'] = 401;
                $resp['messages'] = 'Error Validasi';
                $resp['data'] = $validator->errors()->all();
            } else {
                try {
                        $kd_aset = DB::select("SELECT * FROM aset WHERE SUBSTRING_INDEX(aset_barcode, '-',3) = SUBSTRING_INDEX('". $request->opname_barcode."', '-',3)");

                        if(!$kd_aset){
                            $kd_aset = DB::select("SELECT * FROM aset WHERE aset_barcode_batang ='" .  $request->opname_barcode . "'");
                        }

                        if(!$kd_aset){
                            $resp['code'] = 400;
                            $resp['messages'] = 'Data belum masuk Aset!';
                            $resp['_token'] = csrf_token();

                            return response()->json($resp);
                        }

                        $duplicateData = Aset_Stokopname_detailModel::where('aset_stokopname_periode_kd', $request->periode_kd)
                        ->where('aset_kd', $kd_aset[0]->aset_id)->first();
                        
            
                        if($duplicateData){
                            $lokasi = LokasiModel::where('lokasi_id', $duplicateData->lokasi_kd)->first();
                            $resp['code'] = 410;
                            $resp['messages'] = 'Data Sudah ada, di tembak di lokasi '. $lokasi->lokasi_nama;
                            $resp['_token'] = csrf_token();
            
                            return response()->json($resp);
                        }

                        if($kd_aset[0]->asetstatus_id == '2'){
                            $resp['code'] = 420;
                            $resp['messages'] = 'Data Sudah Di Non-activkan';
                            $resp['_token'] = csrf_token();
            
                            return response()->json($resp);
                        };
                        
                        $arrayData = [
                            'aset_stokopname_periode_kd' => $request->periode_kd,
                            'lokasi_kd' => $request->opname_lokasi,
                            'note' => $request->opname_note,
                            'barcode' => $request->opname_barcode,
                            'kondisi' => $request->opname_kondisi,
                            'aset_kd' => $kd_aset[0]->aset_id,
                            'user_id' => Auth::id(),
                            'checker' => $request->checker,
                        ];
                        Aset_Stokopname_detailModel::create($arrayData);
                            
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil';
                } catch (Exception $e) {
                    $resp['code'] = 400;
                    $resp['messages'] = 'Gagal Simpan';
                    $resp['data'] = $e->getMessage();
                }
            }
    
            $resp['_token'] = csrf_token();
            return response()->json($resp);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function show(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                Aset_Stokopname_detailModel::where('aset_stokopname_detail_kd', $id)->delete();

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

}
