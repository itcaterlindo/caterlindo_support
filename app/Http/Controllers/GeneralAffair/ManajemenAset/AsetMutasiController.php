<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Aset_mutasiModel;
use App\Models\AsetModel;
use App\Models\LokasiModel;
use App\Models\PPIC\Td_rawmaterial_goodsreceiveModel;
use App\Models\HRM\Tb_bagianModel;

use App\Models\Aset_Stokopname_periodeModel;

use Illuminate\Support\Facades\Auth;

class AsetMutasiController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/asetmutasi';

    public function index()
    {
        $periode_active = "";
        $periode = Aset_Stokopname_periodeModel::where('status', 'aktif')->first();

        if($periode){
            $periode_active = 'is_active';
        }
        $data['periode_active'] = $periode_active;
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetMutasis = Aset_mutasiModel::leftJoin('aset_mutasi_jenis', 'aset_mutasi_jenis.asetmutasijenis_id', '=', 'aset_mutasi.asetmutasijenis_id')
            ->leftJoin('aset', 'aset.aset_id', '=', 'aset_mutasi.aset_id')
            ->get();

        return DataTables::of($asetMutasis)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetMutasi) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=view_data("' . $asetMutasi->asetmutasi_id . '")> <i class="fas fa-search"></i> Lihat</a>';
                if ($asetMutasi->asetmutasi_status == 'pending') {
                    if (Auth::user()->can('MUTASI_APPROVE')) {
                         $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetMutasi->asetmutasi_id . '" data-token="' . csrf_token() . '" onclick="action_approve(this)"> <i class="fas fa-check"></i> Approve</a>';
                    }

                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetMutasi->asetmutasi_id . '" data-token="' . csrf_token() . '" onclick="action_cancel(this)"> <i class="fas fa-times"></i> Cancel</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","' . $asetMutasi->asetmutasi_id . '")> <i class="fas fa-edit"></i> Edit</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetMutasi->asetmutasi_id . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                }elseif($asetMutasi->asetmutasi_status == 'approved'){

                }
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('asetmutasi_updated', function ($asetMutasi) {
                return $asetMutasi->asetmutasi_updated->toDateTimeString();
            })
            ->editColumn('asetmutasi_status', function ($asetMutasi) {
                return spanMapStatus($asetMutasi->asetmutasi_status);
            })

            ->rawColumns(['opsi', 'asetmutasi_updated', 'asetmutasi_status'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $asetMutasi = Aset_mutasiModel::leftJoin('aset', 'aset.aset_id', '=', 'aset_mutasi.aset_id')
                ->leftJoin('aset_mutasi_jenis', 'aset_mutasi_jenis.asetmutasijenis_id', '=', 'aset_mutasi.asetmutasijenis_id')
                ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset_mutasi.asetstatus_id')
                ->leftJoin('lokasi as lokasiDari', 'lokasiDari.lokasi_id', '=', 'aset_mutasi.asetmutasi_lokasidari')
                ->leftJoin('lokasi as lokasiKe', 'lokasiKe.lokasi_id', '=', 'aset_mutasi.asetmutasi_lokasike')
                ->select(
                    'aset_mutasi.*',
                    'aset_mutasi_jenis.*',
                    'aset_status.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset.aset_qty',
                    'aset.aset_satuan',
                    'aset.aset_keterangan',
                    'lokasiDari.lokasi_nama as asetmutasi_lokasidari_nama',
                    'lokasiKe.lokasi_nama as asetmutasi_lokasike_nama'
                )
                ->where('aset_mutasi.asetmutasi_id', $id)->first()->toArray();

            /** Get Bagian */
            $kd_bagians = [];
            array_push($kd_bagians, $asetMutasi['asetmutasi_bagiandari']);
            array_push($kd_bagians, $asetMutasi['asetmutasi_bagianke']);
            $kd_bagians = array_unique($kd_bagians);
            $qKdbagians = Tb_bagianModel::whereIn('kd_bagian', $kd_bagians)->get();
            $arrayKdbagians = [];
            foreach ($qKdbagians as $qKdbagian) {
                $arrayKdbagians[$qKdbagian->kd_bagian] = $qKdbagian->nm_bagian;
            }
            $asetMutasi['asetmutasi_bagiandari_nama'] = $arrayKdbagians[$asetMutasi['asetmutasi_bagiandari']];
            $asetMutasi['asetmutasi_bagianke_nama'] = $arrayKdbagians[$asetMutasi['asetmutasi_bagianke']];
            $data['row'] = $asetMutasi;
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'asetmutasi_tanggal' => 'required',
            'asetmutasijenis_id' => 'required',
            'aset_id' => 'required',
        ], [
            'asetmutasi_tanggal.required' => 'Tanggal tidak boleh kosong',
            'asetmutasijenis_id.required' => 'Mutasi tidak boleh kosong',
            'aset_id.required' => 'Aset tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $asetmutasi_id = $request->id;

                $user_id = Auth::user()->id;

                /** Add */
                if (empty($asetmutasi_id)) {
                    $arrayData = [
                        'asetmutasi_kode' => Aset_mutasiModel::generate_asetmutasiKode($request->asetmutasi_tanggal),
                        'aset_id' => $request->aset_id,
                        'asetmutasijenis_id' => $request->asetmutasijenis_id,
                        'asetstatus_id' => $request->asetstatus_id,
                        'asetmutasi_tanggal' => $request->asetmutasi_tanggal,
                        'asetmutasi_penyusutan' => null,
                        'asetmutasi_keterangan' => $request->asetmutasi_keterangan,
                        'asetmutasi_qty' => $request->asetmutasi_qty,
                        'asetmutasi_lokasidari' => $request->asetmutasi_lokasidari,
                        'asetmutasi_lokasike' => $request->asetmutasi_lokasike,
                        'asetmutasi_bagiandari' => $request->asetmutasi_bagiandari,
                        'asetmutasi_bagianke' => $request->asetmutasi_bagianke,
                        'asetmutasi_status' => 'pending',
                        'user_id' => $user_id,
                    ];
                    Aset_mutasiModel::create($arrayData);
                } else {
                    /** Edit */
                    $asetmutasi = Aset_mutasiModel::find($asetmutasi_id);

                    $asetmutasi->aset_id = $request->aset_id;
                    $asetmutasi->asetmutasijenis_id = $request->asetmutasijenis_id;
                    $asetmutasi->asetstatus_id = $request->asetstatus_id;
                    $asetmutasi->asetmutasi_tanggal = $request->asetmutasi_tanggal;
                    $asetmutasi->asetmutasi_keterangan = $request->asetmutasi_keterangan;
                    $asetmutasi->asetmutasi_qty = $request->asetmutasi_qty;
                    $asetmutasi->asetmutasi_lokasidari = $request->asetmutasi_lokasidari;
                    $asetmutasi->asetmutasi_lokasike = $request->asetmutasi_lokasike;
                    $asetmutasi->asetmutasi_bagiandari = $request->asetmutasi_bagiandari;
                    $asetmutasi->asetmutasi_bagianke = $request->asetmutasi_bagianke;
                    $asetmutasi->user_id = $user_id;

                    $asetmutasi->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                Aset_mutasiModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    function action_approve(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $asetmutasi_id = $request->id;

        if (!empty($asetmutasi_id)) {

            try {
                $user_id = Auth::user()->id;
                $asetMutasi = Aset_mutasiModel::find($asetmutasi_id);
                $aset = AsetModel::find($asetMutasi->aset_id);

                $lokasi = LokasiModel::find($asetMutasi->asetmutasi_lokasike ? $asetMutasi->asetmutasi_lokasike : $aset->lokasi_id);

                $aset_barcode = AsetModel::aset_generate_barcode($aset->aset_id, $asetMutasi->asetmutasi_lokasike, $aset->aset_nama);
                $stts_asset = "";

                if($asetMutasi->asetmutasijenis_id == 10){
                    $stts_asset = "4";
                }else if($asetMutasi->asetmutasijenis_id == 1 || $asetMutasi->asetmutasijenis_id == 4){
                    $stts_asset = "1";
                }else {
                    $stts_asset = "2";
                }
                /** Aset Act */
                $custom_lokasi = substr_replace($aset->aset_barcode, '', -3);
                $set_custom_lokasi = $custom_lokasi .  $lokasi->lokasi_kode;
                $aset->aset_barcode = $aset->aset_barcode ? $set_custom_lokasi : $aset_barcode;
                $aset->lokasi_id = $asetMutasi->asetmutasi_lokasike;
                $aset->bagian_kd = $asetMutasi->asetmutasi_bagianke;
                $aset->asetstatus_id = $stts_asset ? $stts_asset : "2";
                $aset->aset_status = 'approved';
                $aset->save();

                /** Aset Muatasi Act */
                $asetMutasi->asetstatus_id = '1';
                $asetMutasi->asetmutasi_status = 'approved';
                $asetMutasi->user_approve_id = $user_id;
                $asetMutasi->save();
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
                $resp['bajol'] =  $stts_asset;
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }
        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    function action_cancel(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $asetmutasi_id = $request->id;

        if (!empty($asetmutasi_id)) {

            try {
                $asetMutasi = Aset_mutasiModel::find($asetmutasi_id);

                $asetMutasi->asetmutasi_status = 'cancel';
                $asetMutasi->save();
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }
        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    function action_regenerate_barcode(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $aset_id = $request->id;

        if (!empty($asetmutasi_id)) {

            try {
                $aset = AsetModel::find($aset_id);

                $aset_barcode = AsetModel::aset_generate_barcode($aset->aset_id, $aset->lokasi_id, $aset->aset_nama);
                $lokasi = LokasiModel::find($aset->lokasi_id);

                $custom_lokasi = substr_replace($aset->aset_barcode, '', -3);
                $aset->aset_barcode = $custom_lokasi .  $lokasi->lokasi_kode;
                $aset->save();
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }
        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

}
