<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\Redirect;
// use DOMPDF;

use App\Models\AsetModel;

use App\Models\ticketModel;
use App\Models\LokasiModel;
use App\Models\Aset_mutasiModel;
use App\Models\HRM\Tb_bagianModel;
use App\Models\PPIC\Td_rawmaterial_goodsreceiveModel;
use App\Models\Aset_Stokopname_periodeModel;

use Illuminate\Support\Facades\Auth;

class AsetDataController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/asetdata';

    public function index()
    {
        $periode_active = "";
        $periode = Aset_Stokopname_periodeModel::where('status', 'aktif')->first();

        if($periode){
            $periode_active = 'is_active';
        }
            $data['periode_active'] = $periode_active;
            $data['class_link'] = $this->class_link;
            return view("page/$this->class_link/index", $data);
        
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['data_id'] = $request->data_id;
        $data['data_stts'] = $request->data_stts;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function partial_report_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/report_aset', $data)->render();
    }
    public function report_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

            $query = DB::select("select 
                                    LOK.lokasi_nama As lokasi_nama,
                                    LOK.pic As pic,
                                    LOK.lokasi_kode As l_k,
                                    (SELECT COUNT(*) FROM aset WHERE lokasi_id = LOK.lokasi_id) as jumlah_disystem,
                                            (SELECT COUNT(*) FROM aset WHERE lokasi_id = LOK.lokasi_id AND asetstatus_id = 1) as jumlah_active,
                                            (SELECT COUNT(*) FROM aset WHERE lokasi_id = LOK.lokasi_id AND asetstatus_id = 3) as jumlah_entered,
                                            (SELECT COUNT(*) FROM aset WHERE lokasi_id = LOK.lokasi_id AND asetstatus_id = 2) as jumlah_disposed
                                    from lokasi AS LOK");
                                    
            return DataTables::of($query)
                ->addIndexColumn()
                ->toJson();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if($request->data_id && $request->data_stts){
            $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
                ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
                ->where([['aset.lokasi_id', '=', $request->data_id], ['aset.asetstatus_id', '=', $request->data_stts]])
                ->get()->toArray();
            $bagian_kds = array_values(array_unique(array_column($asets, 'bagian_kd')));
            $bagians = Tb_bagianModel::whereIn('kd_bagian', $bagian_kds)->where('bagian_aset', '=', '1')->get();
            $arrayBagians = [];
            foreach ($bagians as $bagian) {
                $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
            }
            $resultAsets = [];
            foreach ($asets as $aset) {
                $nm_bagian = isset($arrayBagians[$aset['bagian_kd']]) ? $arrayBagians[$aset['bagian_kd']] : '';
                $aset['nm_bagian'] = $nm_bagian;
                $resultAsets[] = $aset;
            }
        }else if($request->data_id){
            $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
                ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
                ->where('aset.lokasi_id', '=', $request->data_id)
                ->get()->toArray();
            $bagian_kds = array_values(array_unique(array_column($asets, 'bagian_kd')));
            $bagians = Tb_bagianModel::whereIn('kd_bagian', $bagian_kds)->where('bagian_aset', '=', '1')->get();
            $arrayBagians = [];
            foreach ($bagians as $bagian) {
                $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
            }
            $resultAsets = [];
            foreach ($asets as $aset) {
                $nm_bagian = isset($arrayBagians[$aset['bagian_kd']]) ? $arrayBagians[$aset['bagian_kd']] : '';
                $aset['nm_bagian'] = $nm_bagian;
                $resultAsets[] = $aset;
            }
        }else if($request->data_stts){
            $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
                ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
                ->where('aset.asetstatus_id', '=', $request->data_stts)
                ->get()->toArray();
            $bagian_kds = array_values(array_unique(array_column($asets, 'bagian_kd')));
            $bagians = Tb_bagianModel::whereIn('kd_bagian', $bagian_kds)->where('bagian_aset', '=', '1')->get();
            $arrayBagians = [];
            foreach ($bagians as $bagian) {
                $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
            }
            $resultAsets = [];
            foreach ($asets as $aset) {
                $nm_bagian = isset($arrayBagians[$aset['bagian_kd']]) ? $arrayBagians[$aset['bagian_kd']] : '';
                $aset['nm_bagian'] = $nm_bagian;
                $resultAsets[] = $aset;
            }
        }
        else{

            $asets = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
                ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
                ->get()->toArray();
            $bagian_kds = array_values(array_unique(array_column($asets, 'bagian_kd')));
            $bagians = Tb_bagianModel::whereIn('kd_bagian', $bagian_kds)->where('bagian_aset', '=', '1')->get();
            $arrayBagians = [];
            foreach ($bagians as $bagian) {
                $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
            }
            $resultAsets = [];
            foreach ($asets as $aset) {
                $nm_bagian = isset($arrayBagians[$aset['bagian_kd']]) ? $arrayBagians[$aset['bagian_kd']] : '';
                $aset['nm_bagian'] = $nm_bagian;
                $resultAsets[] = $aset;
            }
        }

        return DataTables::of($resultAsets)
            ->addIndexColumn()
            ->addColumn('cetak', function ($resultAset) {
                $p = '<input type="checkbox" id="cetak" data-id="' . $resultAset['aset_id'] . '" data-name="' . $resultAset['aset_nama'] . '"  data-barcode="' . $resultAset['aset_barcode'] . '" value="Car">';
                return $p;
            })
            ->addColumn('opsi', function ($resultAset) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=view_data("' . $resultAset['aset_id'] . '")> <i class="fas fa-search"></i> Lihat</a>';
                // $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=regenerate_barcode("' . $resultAset['aset_id'] . '")> <i class="fas fa-barcode"></i> Re-Generate Barcode Barcode</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                if ($resultAset['aset_status'] == 'pending') {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $resultAset['aset_id'] . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                }
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('aset_updated', function ($resultAset) {
                return Carbon::parse($resultAset['aset_updated']);
            })
            ->editColumn('aset_status', function ($resultAset) {
                return spanMapStatus($resultAset['aset_status']);
            })
            ->rawColumns(['opsi', 'cetak', 'aset_updated', 'aset_status'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Question_categoryModel::where('quescategory_id', $id)->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    public function view_aset(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }
        $aset_id = $request->id;



        $bagians = Tb_bagianModel::allBagianAsets();
        $arrayBagians = [];
        foreach ($bagians as $bagian) {
            $arrayBagians[$bagian->kd_bagian] = $bagian->nm_bagian;
        }

        $asetMutasis = Aset_mutasiModel::leftJoin('aset', 'aset.aset_id', '=', 'aset_mutasi.aset_id')
            ->leftJoin('aset_mutasi_jenis', 'aset_mutasi_jenis.asetmutasijenis_id', '=', 'aset_mutasi.asetmutasijenis_id')
            ->leftJoin('lokasi as lokasiDari', 'lokasiDari.lokasi_id', '=', 'aset_mutasi.asetmutasi_lokasidari')
            ->leftJoin('lokasi as lokasiKe', 'lokasiKe.lokasi_id', '=', 'aset_mutasi.asetmutasi_lokasike')
            ->leftJoin('users', 'users.id', '=', 'aset_mutasi.user_id')
            ->select(
                'aset_mutasi.*',
                'aset.*',
                'aset_mutasi_jenis.*',
                'lokasiDari.lokasi_nama as lokasiDari_nama',
                'lokasiKe.lokasi_nama as lokasiKe_nama',
                'users.name'
            )
            ->orderBy('aset_mutasi.asetmutasi_updated', 'desc')
            ->where('aset.aset_id', $aset_id)->get();
        $arrayAsetMutasis = [];
        foreach ($asetMutasis as $asetMutasi) {
            if($asetMutasi->asetmutasijenis_id != 10){
                $asetMutasi['asetmutasi_bagiandari_nama'] = $arrayBagians[$asetMutasi->asetmutasi_bagiandari];
                $asetMutasi['asetmutasi_bagianke_nama'] = $arrayBagians[$asetMutasi->asetmutasi_bagianke];
               $arrayAsetMutasis[] = $asetMutasi;
            }else{
                $asetMutasi['asetmutasi_bagiandari_nama'] = '-';
                $asetMutasi['asetmutasi_bagianke_nama'] = '-';
                $arrayAsetMutasis[] = $asetMutasi;
            }
           
        }
        $data['aset'] = AsetModel::leftJoin('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
            ->leftJoin('lokasi', 'lokasi.lokasi_id', '=', 'aset.lokasi_id')
            ->leftJoin('aset_status', 'aset_status.asetstatus_id', '=', 'aset.asetstatus_id')
            ->where('aset.aset_id', $aset_id)
            ->get()->first()->toArray();
        $data['aset']['nm_bagian'] = isset($arrayBagians[$data['aset']['bagian_kd']]) ? $arrayBagians[$data['aset']['bagian_kd']] : '';
        $data['asetMutasis'] = $arrayAsetMutasis;
        $data['data_ticket'] = ticketModel::where('aset_id', $aset_id)->get();
       //   return response()->json($data);
        return view('page/' . $this->class_link . '/view_aset', $data);
    }

    // public function view_barcode_pdf(Request $request)
    // {
    //     $aset_id = $request->id;
    //     $asetData = AsetModel::whereIn('aset_id', explode(",", $aset_id))->get();

    //     $title = 'Qr_Code Asset';
    //     $pdf = new TCPDF('L', 'mm', array(400, 1), true, 'UTF-8', false);
    //     $pdf::SetTitle($title);
    //     $pdf::SetAuthor('Caterlindo Support');
    //     $pdf::SetDisplayMode('real', 'default');
        
    //     // set default header data
    //     // $pdf::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 027', PDF_HEADER_STRING);
        
    //     // // set header and footer fonts
    //     // $pdf::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    //     // $pdf::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        
    //     // set default monospaced font
    //     // $pdf::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
    //     // // set margins
    //     // $pdf::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    //     // $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
    //     // $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);
        
    //     // // set auto page breaks
    //     // $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
    //     // // set image scale factor
    //     // $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);

    //     $pdf::SetFont('helvetica', '', 11);
    //     foreach ($asetData as $aset):
           
    //         $txtQrcode = 'f0869-01';
    //         $asetname = $aset->aset_nama;
    //         // add a page
    //         $pdf::AddPage('L', 'A5', false, false);
            
    //         // print a message
    //         // -----------------------------------------------------------------------------
            
    //         $pdf::SetFont('helvetica', '', 10);
            
    //         // define barcode style
    //         $style = array(
    //             'position' => '',
    //             'align' => 'C',
    //             'stretch' => false,
    //             'fitwidth' => true,
    //             'cellfitalign' => '',
    //             'border' => true,
    //             'hpadding' => 'auto',
    //             'vpadding' => 'auto',
    //             'fgcolor' => array(0,0,0),
    //             'bgcolor' => false, //array(255,255,255),
    //             'text' => true,
    //             'font' => 'helvetica',
    //             'fontsize' => 8,
    //             'stretchtext' => 4
    //         );
            
    //         // PRINT VARIOUS 1D BARCODES
            
    //         // CODE 39 - ANSI MH10.8M-1983 - USD-3 - 3 of 9.
    //         $pdf::Cell(0, 0, $asetname, 0, 1);
    //         // $pdf::write1DBarcode($txtQrcode, 'C39', '', '', '', 18, 0.4, $style, 'N');
    //         $pdf::write1DBarcode($txtQrcode, 'UPCE', '', '', '', 18, 0.4, $style, 'N');

    //     endforeach;

    //     $pdf::lastPage();
    //     $pdf::Output($title . '.pdf', 'I');
    // }

    public function view_barcode_pdf(Request $request)
    {
        $aset_id = $request->id;
        $asetData = AsetModel::whereIn('aset_id', explode(",", $aset_id))->get();

        $title = 'Qr_Code Asset';
        $pdf = new TCPDF('L', 'mm', array(50, 70), true, 'UTF-8', false);
        $pdf::SetAutoPageBreak(true);
        $pdf::SetMargins(1, 1, 1, true);
        $pdf::SetTitle($title);
        $pdf::SetAuthor('Caterlindo Support');
        $pdf::SetDisplayMode('real', 'default');

        foreach ($asetData as $aset):
            $pdf::AddPage('L');

            $txtQrcode = $aset->aset_barcode;
            // Number Barcode
            $pdf::SetFont('helveticaB','B',16);
            $pdf::SetFillColor(255, 255, 255);
            $pdf::setCellPaddings(0, 0, 0, 0);
            $pdf::MultiCell(42, 5, $txtQrcode . "\n", 1, 'J', 1, 1, 24, 3, true, 0, false, true, 6, 'M', true);

            // Description
            $itemcode = $aset->aset_nama;
            $itemdesc = $aset->aset_keterangan;
            $aset_tglperolehan = date('d-M-y', strtotime($aset->aset_tglperolehan));
            $pdf::SetFillColor(255, 255, 255);
            $pdf::SetFont('helveticaB','B',16); 
            $pdf::setCellPaddings(0, 0, 0, 0);
            $pdf::MultiCell(42, 2, $itemcode . "\n" . $aset_tglperolehan, 1, 'L', 1, 1, 24, 8, true, 0, false, true, 16, 'M', true);

            $style = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0,0,0),
                'bgcolor' => false
            );
            $style = array(
                'border' => 1,
                'vpadding' => 1,
                'hpadding' => 1,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 //   height of a single module in points
            );  
            // QRCODE,H : QR-CODE Best error correction
            $pdf::write2DBarcode($txtQrcode, 'QRCODE,H', 3, 3, 21, 21, $style, 'N');

        endforeach;

        $pdf::lastPage();
        $pdf::Output($title . '.pdf', 'I');
    }
    

    public function call_barcode_pdf(Request $request) 
    {
        $str = str_replace("[", "", $request->id);
        $str = str_replace("]", "", $str);
        $str = str_replace('"', "", $str);
        $url = url($this->class_link.'/view_barcode_pdf/?id='.$str);

        $html = '<object type="application/pdf" width="100%" height="500px" data="'.$url.'?#zoom=300&scrollbar=1&toolbar=1&navpanes=1"></object>';

        // return response()->json($url);
        // // return Redirect::to($url);
        return $html;
    }

    public function get_rmgr(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }
        $paramSearch = $request->paramSearch;

        $rmgrs = Td_rawmaterial_goodsreceiveModel::leftJoin('tm_purchaseorder', 'tm_purchaseorder.po_kd', '=', 'td_rawmaterial_goodsreceive.po_kd')
            ->leftJoin('td_purchaseorder_detail', 'td_purchaseorder_detail.podetail_kd', '=', 'td_rawmaterial_goodsreceive.podetail_kd')
            ->where(function ($query) use ($paramSearch) {
                $query->where('td_purchaseorder_detail.podetail_deskripsi', 'like', "%$paramSearch%")
                    ->orWhere('tm_purchaseorder.po_no', 'like', "%$paramSearch%");
            })
            ->select('td_rawmaterial_goodsreceive.*', 'td_purchaseorder_detail.*', 'tm_purchaseorder.po_no')
            ->get()->toArray();
        $arrayRmgrs = [];
        foreach ($rmgrs as $rmgr) {
            $rmgr['id'] = $rmgr['rmgr_kd'];
            $rmgr['text'] = $rmgr['po_no'] . ' | ' . $rmgr['podetail_deskripsi'];
            $arrayRmgrs[] = $rmgr;
        }
        return response()->json($arrayRmgrs);
    }

    public function getAsetKode(Request $request)
    {
        $aset_kode = AsetModel::aset_generate_kode($request->asetjenis_id);

        $real = AsetModel::aset_generate_kode($request->asetjenis_id, $aset_kode);

        return response()->json($real);
    }

    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'aset_tglperolehan' => 'required',
            'aset_nilaiperolehan' => 'required',
            'asetjenis_id' => 'required',
            'kd_bagian' => 'required',
            'lokasi_id' => 'required',
            'aset_nama' => 'required',
        ], [
            'aset_tglperolehan.required' => 'Tanggal tidak boleh kosong',
            'aset_nilaiperolehan.required' => 'Nilai tidak boleh kosong',
            'asetjenis_id.required' => 'Jenis tidak boleh kosong',
            'kd_bagian.required' => 'Bagian tidak boleh kosong',
            'lokasi_id.required' => 'Lokasi tidak boleh kosong',
            'aset_nama.required' => 'Nama tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $aset_id = $request->id;
                $rmgr_qty = $request->rmgr_qty;

                $user_id = Auth::user()->id;

                if (empty($aset_id)) {
                    /** Add Batch */
                    $aset_id = AsetModel::aset_last_id();
                    $aset_id++;

                    $rmgr_qty = !empty($rmgr_qty) ? $rmgr_qty : 1;
                    for ($i = 1; $i <= $rmgr_qty; $i++) {
                        $arrayDatas[] = [
                            'aset_id' => $aset_id,
                            'aset_kode' => $request->kode_aset,
                            'aset_parent' => $request->aset_parent,
                            'rmgr_kd' => $request->rmgr_kd,
                            'asetjenis_id' => $request->asetjenis_id,
                            'lokasi_id' => $request->lokasi_id,
                            'asetstatus_id' => $request->asetstatus_id,
                            'bagian_kd' => $request->kd_bagian,
                            'aset_nama' => $request->aset_nama,
                            'aset_tglperolehan' => $request->aset_tglperolehan,
                            'aset_nilaiperolehan' => $request->aset_nilaiperolehan,
                            'aset_qty' => 1,
                            'aset_satuan' => 'unit',
                            'aset_status' => 'pending',
                            'aset_keterangan' => $request->aset_keterangan,
                            'aset_updated' => date('Y-m-d H:i:s'),
                            'user_id' => $user_id,
                        ];

                        /** Mutasi */
                        $asetmutasi_kode = Aset_mutasiModel::generate_asetmutasiKode(date('Y-m-d'), null);
                        $arrayDataMutasis[] = [
                            'asetmutasi_kode' => Aset_mutasiModel::generate_asetmutasiKode(date('Y-m-d'), $asetmutasi_kode),
                            'aset_id' => $aset_id,
                            'asetmutasijenis_id' => 1, // Add
                            'asetstatus_id' => $request->asetstatus_id,
                            'asetmutasi_tanggal' => date('Y-m-d'),
                            'asetmutasi_penyusutan' => null,
                            'asetmutasi_keterangan' => $request->aset_keterangan,
                            'asetmutasi_qty' => 1,
                            'asetmutasi_lokasidari' => $request->lokasi_id,
                            'asetmutasi_lokasike' => $request->lokasi_id,
                            'asetmutasi_bagiandari' => $request->kd_bagian,
                            'asetmutasi_bagianke' => $request->kd_bagian,
                            'asetmutasi_status' => 'pending',
                            'asetmutasi_updated' => date('Y-m-dH :i:s'),
                            'user_id' => $user_id,
                        ];
                        $aset_id++;
                    }
                    AsetModel::insert($arrayDatas);
                    Aset_mutasiModel::insert($arrayDataMutasis);
                } else {
                    // $quescategory = AsetModel::find($quescategory_id);
                    // $quescategory->quescategory_name = $quescategory_name;
                    // $quescategory->user_id = $user_id;

                    // $quescategory->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                AsetModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    function action_regenerate_barcode(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $aset_id = $request->id;

        if (!empty($aset_id)) {

            try {
                $aset = AsetModel::find($aset_id);

                $aset_barcode = AsetModel::aset_generate_barcode($aset->aset_id, $aset->lokasi_id, $aset->aset_nama);
                $lokasi = LokasiModel::find($aset->lokasi_id);

                $custom_lokasi = substr_replace($aset->aset_barcode, '', -3);
                $aset->aset_barcode = $aset_barcode;
                $aset->save();
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }
        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }
}
