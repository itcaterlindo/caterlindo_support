<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Aset_jenisModel;
use App\Models\FileModel;
use App\Models\LelangModel;
use App\Models\Lelang_has_tagModel;
use App\Models\Lelang_has_fileModel;
use App\Models\PenawaranModel;
use App\Models\TagModel;
use Illuminate\Support\Facades\Auth;

class LelangController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/lelang';

    public function index()
    {

        if(!auth()->user()->can('IS_ADMIN_LELANG')){
            return redirect()->back();
        }

        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $query = DB::select('SELECT 
                                    lelang.id_lelang,
                                    lelang.kode_lelang,
                                    lelang.nama,
                                    lelang.jumlah,
                                    (SELECT MAX(jumlah) AS max_items FROM penawaran_lelang WHERE id_lelang = lelang.id_lelang) as penawaran_tertinggi,
                                    (SELECT nama FROM penawaran_lelang WHERE id_lelang = lelang.id_lelang AND jumlah = Penawaran_tertinggi) as penawar,
                                    lelang.start_harga,
                                    lelang.status,
                                    lelang.note,
                                    lelang.created_lelang,
                                    lelang.tanggal_close FROM lelang');

        return DataTables::of($query)
            ->addIndexColumn()
            ->addColumn('opsi', function ($query ) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=view_data("View","' . $query->id_lelang . '")> <i class="fas fa-eye"></i> View</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $query->id_lelang . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            // ->editColumn('created_lelang', function ($query) {
            //     return $query->created_lelang->toDateTimeString();
            // })
            ->rawColumns(['opsi', 'created_lelang'])
            ->toJson();
    }

    public function view_data(Request $request){
        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data_pemenang = db::select('SELECT * FROM penawaran_lelang WHERE id_lelang = '. $id .' ORDER BY id_penawaran DESC LIMIT 1');
            $data_image = DB::select('SELECT * FROM lelang_has_file INNER JOIN file ON lelang_has_file.id_file = file.file_id WHERE lelang_has_file.id_lelang =' . $id);

            $data['penawaran'] = PenawaranModel::where('id_lelang', $id)->orderBy('created_penawaran', 'DESC')->get();
            $data['penawaran_tertinggi'] = ($data_pemenang) ? $data_pemenang[0]->nama : 'belum ada bid!';
            $data['row'] = LelangModel::where('id_lelang', $id)->get()->toArray();
            $data['jml_penawaran'] = PenawaranModel::where('id_lelang', $id)->count();
            $data['tag'] = DB::select('SELECT * FROM lelang_has_tag INNER JOIN tag ON lelang_has_tag.id_tag = tag.id_tag WHERE lelang_has_tag.id_lelang =' . $id);
            $data['image'] = ($data_image) ? $data_image : 0;
        }

        $data['id'] = $id;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
        

        // return response()->json($data);
        return view('page/' . $this->class_link . '/view', $data)->render();
    }

    public function partial_form_main(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }

        $id = $request->id;
        $sts = $request->sts;

        $tahun = date("y");
        $total = LelangModel::whereYear('created_lelang', '=', date('Y'))
              ->count() + 1;

        if(strlen($total) == 1){
            $kode_lelang = $tahun . '000' . $total; 
        }elseif(strlen($total) == 2){
            $kode_lelang = $tahun . '00' . $total; 
        }elseif(strlen($total) == 3){
            $kode_lelang = $tahun . '0' . $total; 
        }else{
            $kode_lelang = $tahun . $total; 
        }

        if (!empty($id)) {
            $data['row'] = LelangModel::where('id_lelang', $id)->get()->toArray();
        }

        $data['id'] = $id;
        $data['sts'] = $sts;
        $data['kode_lelang'] = $kode_lelang;
        $data['class_link'] = $this->class_link;

        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'jumlah' => 'required',
            'start_harga' => 'required',
            'kode' => 'required',
            'nama_lelang' => 'required',
            'images1' => 'image|mimes:jpeg,png,jpg|max:2048',
            'images2' => 'image|mimes:jpeg,png,jpg|max:2048',
            'images3' => 'image|mimes:jpeg,png,jpg|max:2048',
        ], [
            'nama_lelang.required' => 'Nama tidak boleh kosong',
            'kode.required' => 'Kode tidak boleh kosong',
            'start_harga.required' => 'Start Harga tidak boleh kosong',
            'jumlah.required' => 'jumlah tidak boleh kosong',
        ]);

        if($request->sts == 'Edit'){

            $inp_file1 = '';
            $inp_file2 = '';
            $inp_file3 = '';
            $lelang = LelangModel::where('id_lelang', $request->id)->first(); 

            if($request->file('images1')){

                $nama_file1 = time()."_".$request->file('images1')->getClientOriginalName();
                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload1 = 'data_lelang';
                $request->file('images1')->move($tujuan_upload1, $nama_file1);


                $inp_file1 = FileModel::create([
                    'name' => $nama_file1,
                ]);
                
            }
            if($request->file('images2')){
                $nama_file2 = time()."_".$request->file('images2')->getClientOriginalName();
                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload2 = 'data_lelang';
                $request->file('images2')->move($tujuan_upload2, $nama_file2);


                $inp_file2 = FileModel::create([
                    'name' => $nama_file2,
                ]);
                
            }
            if($request->file('images3')){
                $nama_file3 = time()."_".$request->file('images3')->getClientOriginalName();
                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload3 = 'data_lelang';
                $request->file('images3')->move($tujuan_upload3, $nama_file3);

                $inp_file3 = FileModel::create([
                    'name' => $nama_file3,
                ]);

               
            }

            if($inp_file1 || $inp_file2 || $inp_file3){
                Lelang_has_fileModel::where('id_lelang', $request->id)->get();
                DB::select('DELETE FROM file WHERE file_id IN (SELECT id_file FROM lelang_has_file WHERE id_lelang = ' . $request->id . ') ');
                Lelang_has_fileModel::where('id_lelang', $request->id)->delete();

                if($inp_file1){
                    Lelang_has_fileModel::create([
                        'id_lelang' => $lelang->id_lelang,
                        'id_file' => $inp_file1->file_id,
                    ]);
                }

                if($inp_file2){
                    Lelang_has_fileModel::create([
                        'id_lelang' => $lelang->id_lelang,
                        'id_file' => $inp_file2->file_id,
                    ]);
                }
                if($inp_file3){
                    Lelang_has_fileModel::create([
                        'id_lelang' => $lelang->id_lelang,
                        'id_file' => $inp_file3->file_id,
                    ]);
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            }
            // return response()->json('Hmmmm');
            if($request->id_tag){
                Lelang_has_tagModel::where('id_lelang', $request->id)->delete();
                $id_tag = $request->id_tag;
                foreach ($id_tag as $value) {
                    Lelang_has_tagModel::create([
                        'id_lelang' => $request->id,
                        'id_tag' => $value
                    ]);
                }
            }

            $lelang->kode_lelang = $request->kode;
            $lelang->id_aset = $request->id_aset;
            $lelang->nama = $request->nama_lelang;
            $lelang->jumlah = $request->jumlah;
            $lelang->status = $request->status;
            $lelang->start_harga = $request->start_harga;
            $lelang->tanggal_close = $request->tanggal_close;
            $lelang->tanggal_start = $request->tanggal_open;
            $lelang->kelipatan_bit = $request->bit;
            $lelang->open_coment = $request->open_coment;
            $lelang->note = $request->keterangan;

            $lelang->save();

            $resp['status'] = 'edit';
            $resp['code'] = 200;
            $resp['messages'] = 'Berhasil Update.';



        }else{
            if ($validator->fails()) {
                $resp['code'] = 401;
                $resp['messages'] = 'Error Validasi';
                $resp['data'] = $validator->errors()->all();
            }else{
                try {

                    $arrayData = [      'kode_lelang' => $request->kode,
                                        'id_aset' => $request->id_aset,
                                        'nama' => $request->nama_lelang,
                                        'jumlah' => $request->jumlah,
                                        'status' => $request->status,
                                        'start_harga' => $request->start_harga,
                                        'open_coment' => $request->open_coment,
                                        'note' => $request->keterangan,
                                        'tanggal_start' => $request->tanggal_open,
                                        'tanggal_close' => $request->tanggal_close,
                                        'kelipatan_bit' => $request->bit,
                                    ];
                    $lelang = LelangModel::create($arrayData);


                    if($request->file('images1')){

                        $nama_file1 = time()."_".$request->file('images1')->getClientOriginalName();
                        // isi dengan nama folder tempat kemana file diupload
                        $tujuan_upload1 = 'data_lelang';
                        $request->file('images1')->move($tujuan_upload1, $nama_file1);


                        $inp_file = FileModel::create([
                            'name' => $nama_file1,
                        ]);

                        if($inp_file->file_id){
                            Lelang_has_fileModel::create([
                                'id_lelang' => $lelang->id_lelang,
                                'id_file' => $inp_file->file_id,
                            ]);
                        }
                        
                    }
                    if($request->file('images2')){
                        $nama_file2 = time()."_".$request->file('images2')->getClientOriginalName();
                        // isi dengan nama folder tempat kemana file diupload
                        $tujuan_upload2 = 'data_lelang';
                        $request->file('images2')->move($tujuan_upload2, $nama_file2);


                        $inp_file = FileModel::create([
                            'name' => $nama_file2,
                        ]);
                        
                        if($inp_file->file_id){
                            Lelang_has_fileModel::create([
                                'id_lelang' => $lelang->id_lelang,
                                'id_file' => $inp_file->file_id,
                            ]);
                        }
                    }
                    if($request->file('images3')){
                        $nama_file3 = time()."_".$request->file('images3')->getClientOriginalName();
                        // isi dengan nama folder tempat kemana file diupload
                        $tujuan_upload3 = 'data_lelang';
                        $request->file('images3')->move($tujuan_upload3, $nama_file3);


                        $inp_file = FileModel::create([
                            'name' => $nama_file3,
                        ]);

                        if($inp_file->file_id){
                            Lelang_has_fileModel::create([
                                'id_lelang' => $lelang->id_lelang,
                                'id_file' => $inp_file->file_id,
                            ]);
                        }
                    }
                    if($request->id_tag){
                        $id_tag = $request->id_tag;
                        foreach ($id_tag as $value) {
                            Lelang_has_tagModel::create([
                                'id_lelang' => $lelang->id_lelang,
                                'id_tag' => $value
                            ]);
                        }
                    }
                    $resp['status'] = 'add';
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil';
                }catch (Exception $e) {
                    $resp['code'] = 400;
                    $resp['messages'] = 'Gagal Simpan';
                    $resp['data'] = $e->getMessage();
                }
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                LelangModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
