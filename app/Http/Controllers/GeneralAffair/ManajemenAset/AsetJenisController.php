<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Aset_jenisModel;

use Illuminate\Support\Facades\Auth;

class AsetJenisController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/asetjenis';

    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetJeniss = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
            ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
            ->get();

        return DataTables::of($asetJeniss)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetJenis) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","' . $asetJenis->asetjenis_id . '")> <i class="fas fa-edit"></i> Edit</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetJenis->asetjenis_id . '" data-token="' . csrf_token() . '" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->editColumn('asetjenis_updated', function ($asetJenis) {
                return $asetJenis->asetjenis_updated->toDateTimeString();
            })
            ->rawColumns(['opsi', 'asetjenis_updated'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
                ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
                ->where('aset_jenis.asetjenis_id', $id)
                ->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'asetjenis_nama' => 'required',
            'asetjenis_kode' => 'required',
        ], [
            'asetjenis_nama.required' => 'Kategori tidak boleh kosong',
            'asetjenis_kode.required' => 'Kode tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $asetjenis_id = $request->id;
                $user_id = Auth::user()->id;
                if (empty($asetjenis_id)) {

                    $arrayData = [
                        'asetjenis_nama' => $request->asetjenis_nama,
                        'asetjenis_parent' => $request->asetjenis_parent,
                        'asetjenis_kode' => strtoupper($request->asetjenis_kode),
                        'user_id' => $user_id,
                    ];
                    Aset_jenisModel::create($arrayData);
                } else {
                    $lokasi = Aset_jenisModel::find($asetjenis_id);

                    $lokasi->asetjenis_nama = $request->asetjenis_nama;
                    $lokasi->asetjenis_parent = $request->asetjenis_parent;
                    $lokasi->asetjenis_kode = strtoupper($request->asetjenis_kode);
                    $lokasi->user_id = $user_id;

                    $lokasi->save();
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                Aset_jenisModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
