<?php
namespace App\Http\Controllers\GeneralAffair\ManajemenAset;
use App\Http\Controllers\Controller;

use App\Models\Aset_Stokopname_periodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\AsetModel;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class CompareStokopnameController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/comparestokopname';
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['csrf_token'] = csrf_token();
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
       
        global $periode_a;
        global $periode_b;
     
        $periode_a = $request->periode_a;
        $periode_b = $request->periode_b;

        $p_a = Aset_Stokopname_periodeModel::where('aset_stokopname_periode_kd', $request->periode_a)->first();
        $p_b = Aset_Stokopname_periodeModel::where('aset_stokopname_periode_kd', $request->periode_b)->first();

        $data_prd_a = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                        global $periode_a;
                        $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                                ->where('aset_stokopname_periode.aset_stokopname_periode_kd', '=', $periode_a);
                            })->join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                        ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                        ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                        
                        ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                        ->select(
                            'aset_stokopname_periode.*',
                            'aset_stokopname_detail.*',
                            'aset.aset_kode',
                            'aset.aset_barcode',
                            'aset.aset_nama',
                            'aset_jenis.asetjenis_nama',
                            'lok_aset.lokasi_nama as lokasi_aset',
                            'aset.aset_keterangan',
                            'lok_det.lokasi_nama as lokasi_opname'
                        )->get();
       

        $data_prd_b = Aset_Stokopname_detailModel::join('aset_stokopname_periode', function ($join) {
                global $periode_b;
                $join->on('aset_stokopname_periode.aset_stokopname_periode_kd', '=', 'aset_stokopname_detail.aset_stokopname_periode_kd')
                        ->where('aset_stokopname_periode.aset_stokopname_periode_kd', '=', $periode_b);
                    })->join('aset', 'aset.aset_id', '=', 'aset_stokopname_detail.aset_kd')
                ->join('lokasi as lok_aset', 'lok_aset.lokasi_id', '=', 'aset.lokasi_id')
                ->join('aset_jenis', 'aset_jenis.asetjenis_id', '=', 'aset.asetjenis_id')
                
                ->join('lokasi as lok_det', 'lok_det.lokasi_id', '=', 'aset_stokopname_detail.lokasi_kd')
                ->select(
                    'aset_stokopname_periode.*',
                    'aset_stokopname_detail.*',
                    'aset.aset_kode',
                    'aset.aset_barcode',
                    'aset.aset_nama',
                    'aset_jenis.asetjenis_nama',
                    'lok_aset.lokasi_nama as lokasi_aset',
                    'aset.aset_keterangan',
                    'lok_det.lokasi_nama as lokasi_opname'
                )->get();



        $not_found_a = $data = DB::select('SELECT 
                                        "'. $p_a->nomor_seri .'" as nomor_seri,
                                        aset.aset_barcode,
                                        aset.aset_nama,
                                        aset_jenis.asetjenis_nama,
                                        lokasi.lokasi_nama,
                                        aset.aset_id,
                                        "" as lokasi_opname,
                                        "" as note,
                                        "" as aset_stokopname_created_at,
                                        "" as kondisi,
                                        (SELECT lokasi.lokasi_nama FROM aset_stokopname_detail 
                                        INNER JOIN lokasi ON lokasi.lokasi_id = aset_stokopname_detail.lokasi_kd WHERE aset_stokopname_detail.aset_stokopname_periode_kd ='. $periode_a .' AND aset_stokopname_detail.aset_kd = aset.aset_id) as lokasi_ditemukan

                                FROM aset
                                            INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                                INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                                WHERE aset.aset_id 
                                NOT IN (SELECT aset_stokopname_detail.aset_kd FROM aset_stokopname_detail WHERE aset_stokopname_detail.aset_stokopname_periode_kd = :periode) 
                                AND aset.asetstatus_id != "2"' , ['periode' => $periode_a]);
         $not_found_b = DB::select('SELECT 
                                "'. $p_b->nomor_seri .'" as nomor_seri,
                                aset.aset_barcode,
                                aset.aset_nama,
                                aset_jenis.asetjenis_nama,
                                lokasi.lokasi_nama,
                                aset.aset_id,
                                "" as lokasi_opname,
                                "" as note,
                                "" as aset_stokopname_created_at,
                                "" as kondisi,
                                (SELECT lokasi.lokasi_nama FROM aset_stokopname_detail 
                                INNER JOIN lokasi ON lokasi.lokasi_id = aset_stokopname_detail.lokasi_kd WHERE aset_stokopname_detail.aset_stokopname_periode_kd ='. $periode_b .' AND aset_stokopname_detail.aset_kd = aset.aset_id) as lokasi_ditemukan

                        FROM aset
                                    INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                        INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id
                        WHERE aset.aset_id 
                        NOT IN (SELECT aset_stokopname_detail.aset_kd FROM aset_stokopname_detail WHERE aset_stokopname_detail.aset_stokopname_periode_kd = :periode) 
                        AND aset.asetstatus_id != "2"' , ['periode' => $periode_b]);



                $resp['data_prd_a'] = $data_prd_a;
                $resp['data_prd_b'] = $data_prd_b;

                $resp['data_not_found_a'] = $not_found_a;
                $resp['data_not_found_b'] = $not_found_b;

                $resp['_token'] = csrf_token();
                return response()->json($resp);
            }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function show(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function edit(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompareStokopname $compareStokopname)
    {
        //
    }
}
