<?php
namespace App\Http\Controllers\GeneralAffair\ManajemenAset;
use App\Http\Controllers\Controller;

use App\Models\Aset_Stokopname_periodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\AsetModel;
use App\Models\Aset_mutasiModel;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class ReportMutasiAset extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/reportmutasiaset';
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['csrf_token'] = csrf_token();
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
       
        global $periode_a;
        global $periode_b;
     
        $periode_a = $request->periode_a;
        $periode_b = $request->periode_b;
        $lok_a = $request->lok_a;
        $lok_b = $request->lok_b;
        if($periode_a == null && $periode_b == null && $lok_a == null && $lok_b == null){
            $query = DB::select('SELECT 
                                    aset.aset_nama, 
                                    aset_mutasi_jenis.asetmutasijenis_nama, 
                                    lokasi_dari.lokasi_nama AS lokasi_dari,  
                                    lokasi_ke.lokasi_nama AS lokasi_ke, 
                                    aset.aset_barcode, 
                                    aset.aset_tglperolehan, 
                                    aset_status.asetstatus_nama, 
                                    aset_mutasi.asetmutasi_keterangan,
                                    aset_mutasi.asetmutasi_updated,
                                    aset_mutasi.asetmutasi_tanggal
                                FROM aset_mutasi
                                LEFT JOIN aset ON aset.aset_id = aset_mutasi.aset_id
                                LEFT JOIN aset_mutasi_jenis ON aset_mutasi_jenis.asetmutasijenis_id = aset_mutasi.asetmutasijenis_id
                                LEFT JOIN lokasi AS lokasi_dari ON lokasi_dari.lokasi_id = aset_mutasi.asetmutasi_lokasidari
                                LEFT JOIN lokasi AS lokasi_ke ON lokasi_ke.lokasi_id = aset_mutasi.asetmutasi_lokasike
                                LEFT JOIN aset_status ON aset_status.asetstatus_id = aset_mutasi.asetstatus_id
                                WHERE aset_status.asetstatus_nama = "Active"');
        }else if($lok_a == null && $lok_b == null && $periode_a != null && $periode_b != null){
            $query = DB::select('SELECT 
                        aset.aset_nama, 
                        aset_mutasi_jenis.asetmutasijenis_nama, 
                        lokasi_dari.lokasi_nama AS lokasi_dari,  
                        lokasi_ke.lokasi_nama AS lokasi_ke, 
                        aset.aset_barcode, 
                        aset.aset_tglperolehan, 
                        aset_status.asetstatus_nama, 
                        aset_mutasi.asetmutasi_keterangan,
                        aset_mutasi.asetmutasi_updated,
                        aset_mutasi.asetmutasi_tanggal
                    FROM aset_mutasi
                    LEFT JOIN aset ON aset.aset_id = aset_mutasi.aset_id
                    LEFT JOIN aset_mutasi_jenis ON aset_mutasi_jenis.asetmutasijenis_id = aset_mutasi.asetmutasijenis_id
                    LEFT JOIN lokasi AS lokasi_dari ON lokasi_dari.lokasi_id = aset_mutasi.asetmutasi_lokasidari
                    LEFT JOIN lokasi AS lokasi_ke ON lokasi_ke.lokasi_id = aset_mutasi.asetmutasi_lokasike
                    LEFT JOIN aset_status ON aset_status.asetstatus_id = aset_mutasi.asetstatus_id
                    WHERE aset_status.asetstatus_nama = "Active" AND MONTH(aset_mutasi.asetmutasi_updated) = ' . $periode_a . ' AND YEAR(aset_mutasi.asetmutasi_updated) ='. $periode_b);

        }else{
            $query = DB::select('SELECT 
                                aset.aset_nama, 
                                aset_mutasi_jenis.asetmutasijenis_nama, 
                                lokasi_dari.lokasi_nama AS lokasi_dari,  
                                lokasi_ke.lokasi_nama AS lokasi_ke, 
                                aset.aset_barcode, 
                                aset.aset_tglperolehan, 
                                aset_status.asetstatus_nama, 
                                aset_mutasi.asetmutasi_keterangan,
                                aset_mutasi.asetmutasi_updated,
                                aset_mutasi.asetmutasi_tanggal
                            FROM aset_mutasi
                            LEFT JOIN aset ON aset.aset_id = aset_mutasi.aset_id
                            LEFT JOIN aset_mutasi_jenis ON aset_mutasi_jenis.asetmutasijenis_id = aset_mutasi.asetmutasijenis_id
                            LEFT JOIN lokasi AS lokasi_dari ON lokasi_dari.lokasi_id = aset_mutasi.asetmutasi_lokasidari
                            LEFT JOIN lokasi AS lokasi_ke ON lokasi_ke.lokasi_id = aset_mutasi.asetmutasi_lokasike
                            LEFT JOIN aset_status ON aset_status.asetstatus_id = aset_mutasi.asetstatus_id
                            WHERE aset_status.asetstatus_nama = "Active" AND lokasi_dari.lokasi_id = '.$lok_a.' AND lokasi_ke.lokasi_id = '.$lok_b.' AND MONTH(aset_mutasi.asetmutasi_updated) = ' . $periode_a . ' AND YEAR(aset_mutasi.asetmutasi_updated) ='. $periode_b);
        }

                $resp['datax'] =  $query; 
                $resp['b'] =  $periode_a;   
                $resp['t'] =  $periode_b; 
                $resp['_token'] = csrf_token();
                return response()->json($resp);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function show(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function edit(CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompareStokopname $compareStokopname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompareStokopname  $compareStokopname
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompareStokopname $compareStokopname)
    {
        //
    }
}
