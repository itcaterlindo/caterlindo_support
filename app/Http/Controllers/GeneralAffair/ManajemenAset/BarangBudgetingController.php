<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Aset_jenisModel;
use App\Models\TagModel;
use App\Models\BudgetingModel;
use App\Models\Budgetingdetail_has_monthModel;
use App\Models\BarangBudgetingModel;
use App\Models\Category_budgetingModel;
use App\Models\PPIC\Td_purchaserequestion_detailModel;
use Illuminate\Support\Facades\Auth;

class BarangBudgetingController extends Controller
{
private $class_link = 'general_affair/asetmanajemen/detailbudgeting';

    public function index(Request $request)
    {
        $data['class_link'] = $this->class_link;
        $data['kode'] = $request->kode_budgeting;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function getCtg(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['ctg'] = Category_budgetingModel::all();
        $data['bhm'] = Budgetingdetail_has_monthModel::all();

        return response()->json($data);
    }

    public function getPrdetail(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }
        $paramSearch = $request->paramSearch;

        $rmgrs = Td_purchaserequestion_detailModel::leftJoin('tm_purchaserequisition', 'tm_purchaserequisition.pr_kd', '=', 'td_purchaserequisition_detail.pr_kd')
            ->where('td_purchaserequisition_detail.budgeting_detail_kd', '=',  $paramSearch)
            ->select('tm_purchaserequisition.pr_no', 'td_purchaserequisition_detail.*')
            ->get()->toArray();
        $arrayRmgrs = [];
        foreach ($rmgrs as $rmgr) {
            $rmgrx['tesx'] = $rmgr['pr_no'] . ' - ' . $rmgr['prdetail_deskripsi'] . ' | ' . $rmgr['prdetail_qty'] . ' - ' . $rmgr['prdetail_duedate'];

            $arrayRmgrs[] = $rmgrx;
        }
        return response()->json($arrayRmgrs);
    }


    public function table_data(Request $request)
    {
        // if (!($request->ajax())) {
        //     exit('No direct script access allowed');
        // }
       

        //$asetJeniss = BarangBudgetingModel::where('id_budgeting', '=',  $request->kode_budgeting)->get();

        $asetJeniss = DB::table('td_barang_budgeting')
            ->leftJoin('tm_budgeting', 'tm_budgeting.kode_budgeting', '=', 'td_barang_budgeting.id_budgeting')
            ->leftJoin('db_caterlindo_ppic.td_purchaserequisition_detail', 'db_caterlindo_ppic.td_purchaserequisition_detail.budgeting_detail_kd', '=', 'td_barang_budgeting.id_barang')
            ->leftJoin('db_caterlindo_ppic.tm_purchaserequisition', 'db_caterlindo_ppic.tm_purchaserequisition.pr_kd', '=', 'db_caterlindo_ppic.td_purchaserequisition_detail.pr_kd')
            ->select('td_barang_budgeting.*', 'td_barang_budgeting.*', 'td_barang_budgeting.keterangan as ket1', 'tm_purchaserequisition.pr_no', 'td_purchaserequisition_detail.*')
            ->where('td_barang_budgeting.id_budgeting', '=',  $request->kode_budgeting)->get();

        return DataTables::of($asetJeniss)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetJenis) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu"> ';
                if ($asetJenis->stts == "pending") {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=delete_data("' . $asetJenis->id_barang . '")> <i class="fas fa-trash"></i> Hapus</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("' . $asetJenis->id_barang . '","edit_data")> <i class="fas fa-check-square"></i> Edit</a>';
                }if (Auth::user()->can('BUDGETING_CHECK')) {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("' . $asetJenis->id_barang . '","edit_harga")> <i class="fas fa-caret-up"></i> Edit Harga</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=check_data("' . $asetJenis->id_barang . '")> <i class="fas fa-check"></i> Check</a>';
                 }
                 if (Auth::user()->can('BUDGETING_APPROVE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=approve_data("' . $asetJenis->id_barang . '")> <i class="fas fa-toggle-on"></i> Setujui</a>';
                 }
                 if (Auth::user()->can('BUDGETING_REJECT')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=reject_data("' . $asetJenis->id_barang . '")> <i class="fas fa-ban"></i> Tolak</a>';
                 }
                $html .= '</div>
                </div>
                    ';



                return $html;
            })
            ->rawColumns(['opsi'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
                ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
                ->where('aset_jenis.asetjenis_id', $id)
                ->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'id_budgeting'  =>  'required',
            'nm_barang'  =>  'required',
            'spesifikasi_barang' => 'required',
            'keterangan'  =>  'required',
            'jumlah'  =>  'required',
            'harga_anggaran'  =>  'required',
        ], 
        [
            'id_budgeting.required' => 'id_budgeting tidak boleh kosong',
            'nm_barang.required' => 'nm_barang tidak boleh kosong',
            'spesifikasi_barang.required' => 'spesifikasi_barang tidak boleh kosong',
            'keterangan.required' => 'keterangan tidak boleh kosong',
            'jumlah.required' => 'jumlah tidak boleh kosong',
            'harga_anggaran.required' => 'harga_anggaran tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {  
                $periode_bulan = $request->periode_bulan;
                $hck = $request->jumlah / count($periode_bulan);
                
                if((fmod($hck, 1) !== 0.00)){
                    $resp['code'] = 401;
                    $resp['messages'] = 'Gagal Simpan, Quantity & periode bulan tidak sinkron!';
                    $resp['data'] = '';
                }else{                
                    $arrayData = [
                        'id_budgeting'  => $request->id_budgeting,
                        'nm_barang'  =>  $request->nm_barang,
                        'stts'  =>  "pending",
                        'spesifikasi_barang' => $request->spesifikasi_barang,
                        'keterangan'  =>  $request->keterangan,
                        'jumlah'  =>  $request->jumlah,
                        'satuan'  =>  $request->satuan,
                        'jumlah_harga'  =>  $request->jumlah_harga,
                        'id_category'  =>  $request->id_category,
                        'harga_anggaran'  =>  $request->harga_anggaran,
                    ];

                    $save = BarangBudgetingModel::create($arrayData);
                    BudgetingModel::where('kode_budgeting',$request->id_budgeting)->update([
                        'status'=> 'pending',
                        'admin_kd' => Auth::user()->id
                    ]);

                    $bgd = BudgetingModel::where('kode_budgeting', $request->id_budgeting)->first();

                    $periode_bulan = $request->periode_bulan;
                    $arrayPeriode = [];

                    for ($i = 0; $i < count($periode_bulan); $i++) {
                        Budgetingdetail_has_monthModel::create([
                            'tahun' => $bgd->th_anggaran,
                            'bulan' => $periode_bulan[$i],
                            'id_barang_budgeting' => $save->id_barang,
                            'qty' =>  $request->jumlah / count($periode_bulan)
                        ]);
                        //$arrayPeriode['id_barang_budgeting'] = $save->id_barang;
                    }
                    

                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil Add Data.';
                    $resp['data'] = $arrayData;
                    $resp['data_periode_bulan'] = $arrayPeriode;
                    $resp['data_bulan'] = $periode_bulan;
                }
                //$resp['data_real'] = $save->id_barang;
            } catch (Exception $e) {    
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id_barang)) {
            try {
                $id = $request->id_barang;
                BarangBudgetingModel::destroy($id);
                Budgetingdetail_has_monthModel::where('id_barang_budgeting', $id)->delete(); 

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function edit(Request $request){
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $id = $request->id_barang;
        $resp['brg_bgd'] = BarangBudgetingModel::where('id_barang', $id)->first(); 
        $resp['bulan'] = Budgetingdetail_has_monthModel::where('id_barang_budgeting', $id)->get(); 

        return response()->json($resp);
    }

    public function update(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'id_budgeting'  =>  'required',
            'nm_barang'  =>  'required',
            'spesifikasi_barang' => 'required',
            'keterangan'  =>  'required',
            'jumlah'  =>  'required',
            'harga_anggaran'  =>  'required',
        ], 
        [
            'id_budgeting.required' => 'id_budgeting tidak boleh kosong',
            'nm_barang.required' => 'nm_barang tidak boleh kosong',
            'spesifikasi_barang.required' => 'spesifikasi_barang tidak boleh kosong',
            'keterangan.required' => 'keterangan tidak boleh kosong',
            'jumlah.required' => 'jumlah tidak boleh kosong',
            'harga_anggaran.required' => 'harga_anggaran tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $periode_bulan = $request->periode_bulan;
                $hck = $request->jumlah / count($periode_bulan);
                
                if((fmod($hck, 1) !== 0.00)){
                    $resp['code'] = 401;
                    $resp['messages'] = 'Gagal Simpan, Quantity & periode bulan tidak sinkron!';
                    $resp['data'] = '';
                }else{   
                    $BarangBud = BarangBudgetingModel::find($request->id_barang_budgeting);
                
                    $BarangBud->id_budgeting = $request->id_budgeting;
                    $BarangBud->nm_barang  =  $request->nm_barang;
                    $BarangBud->spesifikasi_barang = $request->spesifikasi_barang;
                    $BarangBud->keterangan  =  $request->keterangan;    
                    $BarangBud->jumlah  =  $request->jumlah;
                    $BarangBud->satuan  =  $request->satuan;
                    $BarangBud->jumlah_harga  =  $request->jumlah_harga;
                    $BarangBud->id_category  =  $request->id_category ? $request->id_category : $BarangBud->id_category;
                    $BarangBud->harga_anggaran  =  $request->harga_anggaran;
                    
                    Budgetingdetail_has_monthModel::where('id_barang_budgeting', $request->id_barang_budgeting)->delete();
                    $BarangBud->save();
                
                    

                    $bgd = BudgetingModel::where('kode_budgeting', $request->id_budgeting)->first();

                    $periode_bulan = $request->periode_bulan;
                    $arrayPeriode = [];

                    for ($i = 0; $i < count($periode_bulan); $i++) {
                        Budgetingdetail_has_monthModel::create([
                            'tahun' => $bgd->th_anggaran,
                            'bulan' => $periode_bulan[$i],
                            'id_barang_budgeting' => $request->id_barang_budgeting,
                            'qty' =>  $request->jumlah / count($periode_bulan)
                        ]);
                        //$arrayPeriode['id_barang_budgeting'] = $save->id_barang;
                    }
                    

                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil Edit Data.';
                }
                // $resp['data'] = $arrayData;
                // $resp['data_periode_bulan'] = $arrayPeriode;
                // $resp['data_bulan'] = $periode_bulan;
                //$resp['data_real'] = $save->id_barang;
            } catch (Exception $e) {    
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }


    public function approve(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id_barang)) {
            try {
                $id = $request->id_barang;
                $brg_bgd = BarangBudgetingModel::where('id_barang', $id)->first(); 

                $brg_bgd->stts = 'Disetujui';

                $brg_bgd->save();

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
                $resp['data'] =$this->checkStatusMaster($brg_bgd->id_budgeting);
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function reject(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id_barang)) {
            try {
                $id = $request->id_barang;
                $brg_bgd = BarangBudgetingModel::where('id_barang', $id)->first(); 

                $brg_bgd->stts = 'Ditolak';

                $brg_bgd->save();

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
                $resp['data'] =$this->checkStatusMaster($brg_bgd->id_budgeting);
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function check(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id_barang)) {
            try {
                $id = $request->id_barang;
                $brg_bgd = BarangBudgetingModel::where('id_barang', $id)->first(); 

                $brg_bgd->stts = 'Check';

                $brg_bgd->save();

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil Check';

                $resp['data'] = $this->checkStatusMaster($brg_bgd->id_budgeting);
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Check';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function checkStatusMaster($id) {
        $kd_aset = DB::select('SELECT tm_budgeting.*,
                                    (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting AND stts = "pending") as jml_pending,
                                    (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting AND stts = "Check") as jml_check,
                                    (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting AND stts = "Ditolak") as jml_reject,
                                    (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting AND stts = "Disetujui") as jml_disetujui
                             FROM tm_budgeting WHERE tm_budgeting.kode_budgeting = "'.$id.'"');


        if($kd_aset[0]->jml_check == 0 && $kd_aset[0]->jml_reject == 0 && $kd_aset[0]->jml_disetujui == 0 ){
            BudgetingModel::where('kode_budgeting',$id)->update([
                'status'=> 'pending',
            ]);

            $resp['code'] = 900;
            $resp['messages'] = 'Pending Success.';
        }
        else if($kd_aset[0]->jml_pending != 0){
            BudgetingModel::where('kode_budgeting',$id)->update([
                'status'=> 'pending',
            ]);

            $resp['code'] = 900;
            $resp['messages'] = 'Pending Success.';
        }else if($kd_aset[0]->jml_disetujui != 0 && $kd_aset[0]->jml_check == 0 && $kd_aset[0]->jml_pending == 0){
            BudgetingModel::where('kode_budgeting',$id)->update([
                'status'=> 'approve',
            ]);

            $resp['code'] = 900;
            $resp['messages'] = 'Approve Success.';
        }else{
            BudgetingModel::where('kode_budgeting',$id)->update([
                'status'=> 'check',
            ]);

            $resp['code'] = 900;
            $resp['messages'] = 'Check Success.';
        }
    }
}
