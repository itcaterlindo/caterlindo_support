<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use App\Models\Aset_Stokopname_detailBarcodeModel;
use App\Models\Aset_Stokopname_detailModel;
use App\Models\Aset_Stokopname_jenisModel;
use App\Models\Aset_Stokopname_periodeModel;
use App\Models\AsetModel;use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;


class AsetStokOpnamePeriodeController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/asetstokopname/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function getActive(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $periode = Aset_Stokopname_periodeModel::where('status','aktif')->first();

        return response()->json($periode);
    }


    public function partial_form_main(Request $request)
    {
        // return response()->json('Oke mass siap tok wesss');
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        // $id = $request->id;
        // $sts = $request->sts;

        // if (!empty($id)) {
        //     $data['row'] = Aset_mutasi_jenisModel::where('asetmutasijenis_id', $id)->first()->toArray();
        // }

        // $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . 'partial_form_add_periode', $data)->render();
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main_periode', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetStokopnamePeriode = Aset_Stokopname_periodeModel::join('aset_stokopname_jenis', 'aset_stokopname_jenis.aset_stokopname_jenis_kd', '=', 'aset_stokopname_periode.aset_stokopname_jenis_kd')->get();


        return DataTables::of($asetStokopnamePeriode)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetStokopnamePeriode) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetStokopnamePeriode->aset_stokopname_periode_kd . '" data-token="' . csrf_token() . '" onclick="delete_periode(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetStokopnamePeriode->aset_stokopname_periode_kd . '" data-token="' . csrf_token() . '" onclick="action_active(this)"> <i class="fas fa-check"></i> Active</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetStokopnamePeriode->aset_stokopname_periode_kd . '" data-token="' . csrf_token() . '" onclick="action_non_active(this)"> <i class="fas fa-power-off"></i> Non-Active</a>';
                    $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="' . $asetStokopnamePeriode->aset_stokopname_periode_kd . '" data-token="' . csrf_token() . '" onclick="action_finish(this)"> <i class="fas fa-check-square"></i> Finish</a>';
                // }
                // if (Auth::user()->can('KATEGORI_DELETE')) {
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->rawColumns(['opsi'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actionActive(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Kode Periode tidak boleh kosong',
        ]);

        $periode = Aset_Stokopname_periodeModel::where('status', 'aktif')->first();

        if($periode){
            $resp['code'] = 400;
            $resp['messages'] = 'Data Periode sudah Aktif';
            $resp['_token'] = csrf_token();

            return response()->json($resp);
        }

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
            
                $periode = Aset_Stokopname_periodeModel::find($request->id);
                $periode->status = 'aktif';
                $periode->save();
                    
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function actionNonActive(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Kode Periode tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
            
                $periode = Aset_Stokopname_periodeModel::find($request->id);
                $periode->status = 'non-aktif';
                $periode->save();
                    
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
   
    }

    public function actionFinish(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Kode Periode tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
            
                $periode = Aset_Stokopname_periodeModel::find($request->id);
                $periode->status = 'finish';
                $periode->save();
                    
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }
     
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         {
            if (!($request->ajax())) {
                exit('No direct script access allowed');
            }
         
            $validator = Validator::make($request->all(), [
                'periode_tahun' => 'required',
                'periode_jenis' => 'required',
                'periode_bulan' => 'required',
                'periode_nomor_seri'    => 'required'
            ], [
                'periode_tahun.required' => 'Tahun tidak boleh kosong',
                'periode_jenis.required' => 'Jenis tidak boleh kosong',
                'periode_bulan.required' => 'Bulan tidak boleh kosong',
                'periode_nomor_seri.required' => 'Nomor Seri tidak boleh kosong',
            ]);

            $duplicateData = Aset_Stokopname_periodeModel::where('aset_stokopname_jenis_kd',$request->periode_jenis)
            ->where('bulan', $request->periode_bulan)->where('tahun', $request->periode_tahun)->first();

            if($duplicateData){
                $resp['code'] = 400;
                $resp['messages'] = 'Data Sudah ada';
                $resp['_token'] = csrf_token();

                return response()->json($resp);
            }

            if ($validator->fails()) {
                $resp['code'] = 401;
                $resp['messages'] = 'Error Validasi';
                $resp['data'] = $validator->errors()->all();
            } else {
                try {
                        $nomor_seri = Aset_Stokopname_periodeModel::where('nomor_seri', $request->periode_nomor_seri)->first();
                        if($nomor_seri){
                            $resp['code'] = 400;
                            $resp['messages'] = 'Nomor seri sudah masuk Periode!';
                            $resp['_token'] = csrf_token();

                            return response()->json($resp);
                        }
                        
                        $arrayData = [
                            'aset_stokopname_jenis_kd' => $request->periode_jenis,
                            'bulan' => $request->periode_bulan,
                            'tahun' => $request->periode_tahun,
                            'nomor_seri' => $request->periode_nomor_seri,
                            'status' => 'non-aktif'
                        ];
                        Aset_Stokopname_periodeModel::create($arrayData);
                            
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil';
                } catch (Exception $e) {
                    $resp['code'] = 400;
                    $resp['messages'] = 'Gagal Simpan';
                    $resp['data'] = $e->getMessage();
                }
            }
    
            $resp['_token'] = csrf_token();
            return response()->json($resp);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function show(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok_Opname $stok_Opname)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                Aset_Stokopname_periodeModel::where('aset_stokopname_periode_kd', $id)->delete();

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
