<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Models\Aset_jenisModel;
use App\Models\Budgetingdetail_has_monthModel;
use App\Models\TagModel;
use App\Models\BudgetingModel;
use App\Models\BarangBudgetingModel;
use Illuminate\Support\Facades\Auth;

class BudgetingController extends Controller
{
    private $class_link = 'general_affair/asetmanajemen/budgeting';

    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $asetJeniss = BudgetingModel::where('admin_kd', Auth::user()->id)->get();
        
        $users = User::select('users.id', 'users.name as user_name', 'users.username as user_username', 'users.email', 'users.updated_at', 'roles.name as role_name')
        ->leftJoin('model_has_roles', 'users.id','=', 'model_has_roles.model_id')
        ->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
        ->where('users.id', Auth::user()->id)->first();
        
       if($users->role_name == "ADMINISTRATOR" || $users->role_name == "ADMIN_ASSETS"){
           // $asetJeniss = BudgetingModel::all();

            $asetJeniss = DB::select('SELECT tm_budgeting.*,
            (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting) as jml_detail
             FROM tm_budgeting');
       }else{
            //$asetJeniss = BudgetingModel::where('admin_kd', Auth::user()->id)->get();

            $asetJeniss = DB::select('SELECT tm_budgeting.*,
            (SELECT COUNT(*) FROM td_barang_budgeting WHERE id_budgeting = tm_budgeting.kode_budgeting) as jml_detail
     FROM tm_budgeting WHERE tm_budgeting.admin_kd = "'.Auth::user()->id.'"');
       };

        return DataTables::of($asetJeniss)
            ->addIndexColumn()
            ->addColumn('opsi', function ($asetJenis) {
                $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>   
                        <div class="dropdown-menu" role="menu">';                               
                // if (Auth::user()->can('KATEGORI_UDPATE')) {
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=open_data("' . $asetJenis->kode_budgeting . '")> <i class="fas fa-eye"></i> Lihat data</a>';
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick="edit_data(`' . $asetJenis->kode_budgeting . '`, `' . $asetJenis->id_bagian . '`, `' . $asetJenis->th_anggaran . '`, `' . $asetJenis->keterangan . '`)"> <i class="fas fa-arrow-right"></i> Edit data</a>';
                $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=dulplicate_data("' . $asetJenis->kode_budgeting . '")> <i class="fas fa-copy"></i> Duplicat Data</a>';
                // }
                $html .= '</div>
                    </div>
                    ';
                return $html;
            })
            ->rawColumns(['opsi'])
            ->toJson();
    }

    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if (!empty($id)) {
            $data['row'] = Aset_jenisModel::leftJoin('aset_jenis as asetjenis_parent', 'asetjenis_parent.asetjenis_id', '=', 'aset_jenis.asetjenis_parent')
                ->select('aset_jenis.*', 'asetjenis_parent.asetjenis_nama as asetjenis_parent_nama')
                ->where('aset_jenis.asetjenis_id', $id)
                ->first()->toArray();
        }

        $data['id'] = $id;
        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function store(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $validator = Validator::make($request->all(), [
            'bagian_id' => 'required',
            'th_anggaran' => 'required',
            'keterangan' => 'required',
        ], 
        [
            'bagian_id.required' => 'bagian_id tidak boleh kosong',
            'th_anggaran.required' => 'th_anggaran tidak boleh kosong',
            'keterangan.required' => 'keterangan tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {

                if(!empty($request->kd_bgd)){
                    BudgetingModel::where('kode_budgeting',$request->kd_bgd)->update([
                        'id_bagian'=> $request->bagian_id,
                        'keterangan'=> $request->keterangan,
                        'th_anggaran'=>$request->th_anggaran,
                        'admin_kd' => Auth::user()->id
                    ]);

                    $resp['code'] = 900;
                    $resp['messages'] = 'Fungsi Edit active';
                }else{
                    $bgd = BudgetingModel::where(['id_bagian' => $request->bagian_id, 'th_anggaran' => $request->th_anggaran])->get();
                    if(count($bgd) == 0){
                            $arrayData = [
                                'kode_budgeting' => BudgetingModel::generate_asetmutasiKode(date("Y-m-d")),
                                'id_bagian' => $request->bagian_id,
                                'th_anggaran' => $request->th_anggaran,
                                'keterangan' => $request->keterangan,
                                'status' => "pending",
                                'admin_kd' => Auth::user()->id
                            ];
                            BudgetingModel::create($arrayData);
            
                            $resp['code'] = 200;
                            $resp['messages'] = 'Berhasil';
                            $resp['datax'] = $arrayData;
                        }
                    else{
                        $resp['code'] = 400;
                        $resp['messages'] = 'Gagal Simpan, Data Sudah ada';
                    }
                }
               
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id_tag)) {
            try {
                $id = $request->id_tag;
                TagModel::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function getAll(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->kode_budgeting)) {
            try {
                $id = $request->kode_budgeting;
                $master = BudgetingModel::where('kode_budgeting',$id)->get();
                $detail = BarangBudgetingModel::where('id_budgeting',$id)->get();

               $bgd = BudgetingModel::where(['id_bagian' =>$master[0]->id_bagian, 'th_anggaran' => ($master[0]->th_anggaran + 1)])->get();

               if(count($bgd) == 0){
                
                $masterx = [
                    'kode_budgeting' => BudgetingModel::generate_asetmutasiKode(date("Y-m-d")),
                    'id_bagian' => $master[0]->id_bagian,
                    'th_anggaran' =>($master[0]->th_anggaran + 1),
                    'keterangan' => $master[0]->keterangan,
                    'status' => "pending",
                    'admin_kd' => Auth::user()->id
                ];
                $xxmaster = BudgetingModel::create($masterx);
                $detailx = [];
                for ($i=0; $i < count($detail); $i++) { 
                    $bbm = BarangBudgetingModel::create([
                        'id_budgeting'  =>  $xxmaster->kode_budgeting,
                        'nm_barang'  =>  $detail[$i]->nm_barang,
                        'stts'  =>  "pending",
                        'spesifikasi_barang' => $detail[$i]->spesifikasi_barang,
                        'keterangan'  =>  $detail[$i]->keterangan,
                        'jumlah'  => $detail[$i]->jumlah,
                        'satuan'  => $detail[$i]->satuan,
                        'jumlah_harga'  =>  $detail[$i]->jumlah_harga,
                        'id_category'  =>  $detail[$i]->id_category,
                        'harga_anggaran'  =>  $detail[$i]->harga_anggaran,
                    ]);

                    $bdhm = Budgetingdetail_has_monthModel::where('id_barang_budgeting',$detail[$i]->id_barang)->get();
                    for ($x=0; $x < count($bdhm); $x++) { 
                        Budgetingdetail_has_monthModel::create([
                            'tahun' => $bdhm[$x]->tahun,
                            'bulan' => $bdhm[$x]->bulan,
                            'id_barang_budgeting' => $bbm->id_barang,
                            'qty' =>  0
                         ]);
                     }
                }
              


                    $resp['master'] = $masterx;
                    $resp['detail'] = $detailx;
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil.';
                }else{
                    $resp['code'] = 400;
                    $resp['messages'] = 'Gagal Simpan, Data Sudah ada';
                }
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function getCode(Request $request)
    {
        return response()->json(BudgetingModel::generate_asetmutasiKode(date("Y-m-d")));
    }
}
