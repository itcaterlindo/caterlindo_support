<?php

namespace App\Http\Controllers\GeneralAffair\ManajemenAset;

use App\Models\ticket_fileModel;
use App\Models\ticketModel;
use App\Models\AsetModel;
use App\Models\Aset_mutasiModel;
use App\Models\ticket_noteModel;
use App\Models\ticket_has_rawmaterial;
use App\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AllTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $class_link = 'general_affair/asetmanajemen/ticket/allticket';

    public function index()
    {
        if (Auth::user()->can('IS_ADMIN_TIKET')) {
            $active = ticketModel::where('status_ticket', 'active')->count();
            $close = ticketModel::where('status_ticket', 'close')->count();
            $delive = ticketModel::where('status_ticket', 'delive')->count();
            $cancel = ticketModel::where('status_ticket', 'cancel')->count();
        }else{
            $active = ticketModel::where('status_ticket', 'active')->where('user_id', Auth::id())->count();
            $close = ticketModel::where('status_ticket', 'close')->where('user_id', Auth::id())->count();
            $delive = ticketModel::where('status_ticket', 'delive')->where('user_id', Auth::id())->count();
            $cancel = ticketModel::where('status_ticket', 'cancel')->where('user_id', Auth::id())->count();
        }

        $data['active'] = $active;
        $data['cancel'] = $cancel;
        $data['close'] = $close;
        $data['delive'] = $delive;
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/index", $data);
    }

    public function partial_table_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_table_main', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function partial_form_main(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/' . $this->class_link . '/partial_form_main', $data)->render();
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setKodeTicket(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        $ticket = ticketModel::whereDate('created_ticket', Carbon::today())->count();
        return response()->json($ticket);
    }

    public function store(Request $request)
    {   
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

            $validator = Validator::make($request->all(), [
                'ticket_keterangan' => 'required',
                'kode_ticket' => 'required',
                'status_ticket' => 'required',
                'id_aset' => 'required',
            ], [
                'ticket_keterangan.required' => 'Ticket Keterangan tidak boleh kosong',
                'kode_ticket.required' => 'Kode Ticket tidak boleh kosong',
                'status_ticket.required' => 'Status Ticket tidak boleh kosong',
                'id_aset.required' => 'Id Asset Barang tidak boleh kosong',
            ]);

            if($request->id){
                $ticket = ticketModel::where('ticket_id', $request->id)->first(); 

                    $file = $request->file('ticket_file');
                    if($file){
                        $nama_file = time()."_".$file->getClientOriginalName();
                                // isi dengan nama folder tempat kemana file diupload
                        $tujuan_upload = 'data_ticket';
                        $file->move($tujuan_upload,$nama_file);
                        
                        if(!$ticket->file_id){
                            $inp_file = ticket_fileModel::create([
                                'name' => $nama_file,
                            ]);
                            $ticket->file_id = ($file ? $inp_file->file_id : null);
                        }else{
                            $_upd = ticket_fileModel::where('file_id', $ticket->file_id)->first();
                            $_upd->name = $nama_file;
                            $_upd->save();
                        }
                        
                    }

                    $ticket->aset_id = $request->id_aset;
                    $ticket->problem  = $request->ticket_keterangan;
                    $ticket->status_ticket = $request->status_ticket;
                    $ticket->save();

                    $resp['ticket'] = $ticket;
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil Update.';

            }else{
                $duplicateData = ticketModel::where('ticket_kode', '#' . $request->kode_ticket)->first();

                if($duplicateData){
                    $resp['code'] = 400;
                    $resp['messages'] = 'Kode ticket anda telah di gunakan user lain, silahkan mulai ulang untuk melanjutkan!';
                    $resp['_token'] = csrf_token();
    
                    return response()->json($resp);
                }
    
                // menyimpan data file yang diupload ke variabel $file
                $file = $request->file('ticket_file');
    
                if($file){
                    $nama_file = time()."_".$file->getClientOriginalName();
                            // isi dengan nama folder tempat kemana file diupload
                    $tujuan_upload = 'data_ticket';
                    $file->move($tujuan_upload,$nama_file);
                
                
                    $inp_file = ticket_fileModel::create([
                        'name' => $nama_file,
                    ]);
                }
    
                if ($validator->fails()) {
                    $resp['code'] = 401;
                    $resp['messages'] = 'Error Validasi';
                    $resp['data'] = $validator->errors()->all();
                } else {
                    try {
                        ticketModel::create([
                            'aset_id' => $request->id_aset,
                            'ticket_kode' => '#' . $request->kode_ticket,
                            'problem'  => $request->ticket_keterangan,
                            'prioritas'  => $request->prioritas,
                            'status_ticket' => $request->status_ticket,
                            'file_id' => ($file ? $inp_file->file_id : null),
                            'user_id' => Auth::id(),
                        ]);
                                
                        $resp['code'] = 200;
                        $resp['messages'] = 'Berhasil Menambahkan';
                    } catch (Exception $e) {
                        $resp['code'] = 400;
                        $resp['messages'] = 'Gagal Simpan';
                        $resp['data'] = $e->getMessage();
                    }
                }
            }
         
            $resp['_token'] = csrf_token();
            return response()->json($resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function getTable(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

        if (Auth::user()->can('IS_ADMIN_TIKET')) {
            if($request->status){
                $query = DB::select('SELECT 
                                    ticket.ticket_id as opsi,
                                    ticket.ticket_kode,
                                    aset.aset_kode,
                                    aset.aset_nama,
                                    aset_jenis.asetjenis_nama,
                                    lokasi.lokasi_nama,
                                    users.username as users,
                                    ticket.status_ticket,
                                    ticket.created_ticket,
                                    ticket.problem
                            FROM ticket
                            INNER JOIN aset ON aset.aset_id = ticket.aset_id
                            INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                INNER JOIN users ON users.id = ticket.user_id
                                    INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id WHERE ticket.status_ticket = "' . $request->status . '"');
            }else{
                $query = DB::select('SELECT 
                                    ticket.ticket_id as opsi,
                                    ticket.ticket_kode,
                                    aset.aset_kode,
                                    aset.aset_nama,
                                    aset_jenis.asetjenis_nama,
                                    lokasi.lokasi_nama,
                                    users.username as users,
                                    ticket.status_ticket,
                                    ticket.created_ticket,
                                    ticket.problem
                            FROM ticket
                            INNER JOIN aset ON aset.aset_id = ticket.aset_id
                            INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                INNER JOIN users ON users.id = ticket.user_id
                                    INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id');
        
            }
        }else{
            if($request->status){
                $query = DB::select('SELECT 
                                    ticket.ticket_id as opsi,
                                    ticket.ticket_kode,
                                    aset.aset_kode,
                                    aset.aset_nama,
                                    aset_jenis.asetjenis_nama,
                                    lokasi.lokasi_nama,
                                    users.username as users,
                                    ticket.status_ticket,
                                    ticket.created_ticket,
                                    ticket.problem
                            FROM ticket
                            INNER JOIN aset ON aset.aset_id = ticket.aset_id
                            INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                INNER JOIN users ON users.id = ticket.user_id
                                    INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id WHERE ticket.status_ticket = "' . $request->status . '" AND users.id =' . Auth::id());
            }else{
                $query = DB::select('SELECT 
                                    ticket.ticket_id as opsi,
                                    ticket.ticket_kode,
                                    aset.aset_kode,
                                    aset.aset_nama,
                                    aset_jenis.asetjenis_nama,
                                    lokasi.lokasi_nama,
                                    users.username as users,
                                    ticket.status_ticket,
                                    ticket.created_ticket,
                                    ticket.problem
                            FROM ticket
                            INNER JOIN aset ON aset.aset_id = ticket.aset_id
                            INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                                INNER JOIN users ON users.id = ticket.user_id
                                    INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id  WHERE users.id =' . Auth::id());
        
            }
        }
        
        return DataTables::of($query)
        ->addIndexColumn()
        ->toJson();
    }

    public function statusTicket(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Id tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
            
                $status = ticketModel::where('ticket_id', $request->id)->first();
                $status->status_ticket = $request->status_ticket;
                $status->save();
                    
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function dataSparepart(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Id tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $status = ticketModel::where('ticket_id', $request->id)->first();
                if($request->status == 'perbaikan' || $request->status == 'ganti baru' || $request->status == 'perlu tindak lanjut'){
                    if($status->sparepart == $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->sparepart = null;
                        $status->save();
                    }elseif($status->sparepart != $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->sparepart = $request->status;
                        $status->save();
                    }
                }elseif($request->status == 'urgent' || $request->status == 'emergency' || $request->status == 'biasa'){
                    if($status->prioritas == $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->prioritas =null;
                        $status->save();
                    }elseif($status->prioritas != $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->prioritas = $request->status;
                        $status->save();
                    }
                }elseif($request->status == 'short rervice' || $request->status == 'long service'){
                    if($status->jenis_service == $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->jenis_service = null;
                        $status->save();
                    }elseif($status->jenis_service != $request->status){
                        $status = ticketModel::where('ticket_id', $request->id)->first();
                        $status->jenis_service = $request->status;
                        $status->save();
                    }
                }

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function rawmat(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'ticket_id' => 'required',
            'rm_kd' => 'required',
        ], [
            'ticket_id.required' => 'Ticket id tidak boleh kosong',
            'rm_kd.required' => 'Rm kd tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
          
                ticket_has_rawmaterial::create([
                    'ticket_id' => $request->ticket_id,
                    'rm_kd' => $request->rm_kd,
                    'rm_nama'  => $request->rm_nama,
                ]);
        

                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function detail($id)
    {
        $data_tiket = ticketModel::where('ticket_id', $id)->first();
        $data['title'] = $data_tiket->ticket_kode . ' || ' . $data_tiket->problem;
        $data['q_id'] = $id;
        $data['rm_kd'] = $data_tiket->rm_kd ? $data_tiket->rm_kd : '';
        $data['aset_id'] = $data_tiket->aset_id;
        $data['class_link'] = $this->class_link;
        return view("page/$this->class_link/detail", $data);
    }

    public function getDataDetail(Request $request){
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $query = DB::select('SELECT 
                                ticket.ticket_id as opsi,
                                ticket.ticket_kode,
                                ticket.prioritas,
                                ticket.sparepart,
                                ticket.jenis_service,
                                ticket.problem,
                                aset.aset_nama,
                                aset.aset_barcode,
                                aset.aset_id,
                                aset.asetstatus_id,
                                aset_jenis.asetjenis_nama,
                                lokasi.lokasi_nama,
                                ticket_file.name,
                                users.username as users,
                                ticket.status_ticket,
                                ticket.created_ticket,
                                asm.asetmutasi_kode
                        FROM ticket
                        INNER JOIN aset ON aset.aset_id = ticket.aset_id
                          INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                            INNER JOIN users ON users.id = ticket.user_id
                                LEFT JOIN (SELECT * FROM aset_mutasi WHERE aset_mutasi.asetstatus_id = "4") AS asm ON asm.aset_id = aset.aset_id
                                LEFT JOIN ticket_file ON ticket_file.file_id = ticket.file_id
                                INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id WHERE ticket.ticket_id =' . $request->id);

        return response()->json($query);
    }

    public function allTicketActive()
    {
        $query = DB::select('SELECT 
                                ticket.ticket_id as opsi,
                                ticket.ticket_kode,
                                ticket.problem,
                                ticket.ticket_kode,
                                ticket.prioritas,
                                ticket.sparepart,
                                ticket.jenis_service,
                                ticket.problem, 
                                aset.aset_nama,
                                aset.aset_barcode,
                                aset.aset_id,
                                aset_jenis.asetjenis_nama,
                                lokasi.lokasi_nama,
                                ticket_file.name,
                                users.username as users,
                                ticket.status_ticket,
                                ticket.created_ticket
                        FROM ticket
                        INNER JOIN aset ON aset.aset_id = ticket.aset_id
                          INNER JOIN lokasi ON aset.lokasi_id = lokasi.lokasi_id
                            INNER JOIN users ON users.id = ticket.user_id
                                LEFT JOIN ticket_file ON ticket_file.file_id = ticket.file_id
                                INNER JOIN aset_jenis ON aset.asetjenis_id = aset_jenis.asetjenis_id WHERE ticket.status_ticket ="active"');

        return response()->json($query);
    }

    public function getRiwayatDetail(Request $request){
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $data_tiket = ticketModel::where('aset_id', $request->id)->get();

        return response()->json($data_tiket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function getComment(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $data =  DB::table('ticket_note')
                    ->join('users', 'users.id', '=', 'ticket_note.user_id')->where('ticket_id', $request->id)
                    ->orderBy('created_note', 'desc')->get();
        return response()->json($data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function storedComment(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }

            $validator = Validator::make($request->all(), [
                'id_ticket' => 'required',
                'note' => 'required',
            ], [
                'id_ticket.required' => 'id ticket tidak boleh kosong',
                'note.required' => 'Note tidak boleh kosong',
            ]);
            $model = ticket_noteModel::where('ticket_id', $request->id_ticket)->get()->count();

            if($model == '0'){
                $status = ticketModel::where('ticket_id', $request->id_ticket)->first();
                $status->status_ticket = 'active';
                $status->save();
                    
                $resp['autochange'] = 'ok';
            }

            if ($validator->fails()) {
                $resp['code'] = 401;
                $resp['messages'] = 'Error Validasi';
                $resp['data'] = $validator->errors()->all();
            } else {
                try {
                    ticket_noteModel::create([
                        'ticket_id' => $request->id_ticket,
                        'note' => $request->note,
                        'user_id' => Auth::id(),
                    ]);             
                    $resp['code'] = 200;
                    $resp['messages'] = 'Berhasil';
                } catch (Exception $e) {
                    $resp['code'] = 400;
                    $resp['messages'] = 'Gagal Simpan';
                    $resp['data'] = $e->getMessage();
                }
            }

         
            $resp['_token'] = csrf_token();
            return response()->json($resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stok_Opname  $stok_Opname
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)) {
            try {
                $id = $request->id;
                ticket_noteModel::where('note_id', $id)->delete();

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function create_table_rm(Request $request){
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        $query = DB::select('SELECT * FROM ticket_has_rawmaterial WHERE ticket_id =' . $request->id);

        return response()->json($query);
    }

    public function delete_rm(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
        if (!empty($request->ticket_id)) {
            try {
                $query = DB::select('DELETE FROM ticket_has_rawmaterial WHERE ticket_id ="'.$request->ticket_id.'" AND rm_kd ="'.$request->rm_kd.'" AND rm_nama ="'.$request->rm_nama.'"');

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        } else {
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function statusAsset(Request $request)
    {
        if (!($request->ajax())) {
            exit('No direct script access allowed');
        }
     
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Id tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();
        } else {
            try {
                $user_id = Auth::user()->id;

                $asetmutasi_kode = Aset_mutasiModel::generate_asetmutasiKode(date('Y-m-d'), null);
                $arrayDataMutasis = [
                    'asetmutasi_kode' => Aset_mutasiModel::generate_asetmutasiKode(date('Y-m-d'), $asetmutasi_kode),
                    'aset_id' => $request->id,
                    'asetmutasijenis_id' => 10, // Add
                    'asetstatus_id' => $request->asetstatus_id,
                    'asetmutasi_tanggal' => date('Y-m-d'),
                    'asetmutasi_penyusutan' => null,
                    'asetmutasi_keterangan' => null,
                    'asetmutasi_qty' => 1,
                    'asetmutasi_lokasidari' => $request->lokasi_id,
                    'asetmutasi_lokasike' => $request->lokasi_id,
                    'asetmutasi_bagiandari' => $request->kd_bagian,
                    'asetmutasi_bagianke' => $request->kd_bagian,
                    'asetmutasi_status' => 'pending',
                    'asetmutasi_updated' => date('Y-m-d H:i:s'),
                    'user_id' => $user_id,
                ];
            
                Aset_mutasiModel::insert($arrayDataMutasis);
                // $status = AsetModel::where('aset_id', $request->id)->first();
                // $status->asetstatus_id = $request->asetstatus_id;
                // $status->save();
                    
                $resp['code'] = 200;
                $resp['messages'] = 'Berhasil';
            } catch (Exception $e) {
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }
}
