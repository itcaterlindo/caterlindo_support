<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Models\SidebarModel;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    private $class_link = 'setting/role';
   
    public function index()
    {
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/index', $data);
    }

    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $roles = Role::all();

        return DataTables::of($roles)
                ->addIndexColumn()
                ->addColumn('opsi', function ($role){
                    $html = '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">';
                    if (Auth::user()->can('ROLE_UPDATE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" onclick=permission_data("'.$role->id.'")> <i class="fas fa-radiation"></i> Role Permissions</a>
                                <div class="dropdown-divider"></div> 
                                <a class="dropdown-item" href="javascript:void(0)" onclick=edit_data("Edit","'.$role->id.'")> <i class="fas fa-edit"></i> Edit</a>';
                    }
                    if (Auth::user()->can('ROLE_DELETE')) {
                        $html .= '<a class="dropdown-item" href="javascript:void(0)" data-id="'.$role->id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>';
                    }
                    $html .= '</div>
                    </div>
                    ';
                    return $html;
                })
                ->editColumn('updated_at', function($role) {
                    return $role->updated_at->toDateTimeString();
                })
                ->rawColumns(['opsi'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $sts = $request->sts;

        if ($sts == 'Edit'){
            $role = Role::find($id);
            $data['role'] = $role->toArray();
        }

        $data['id'] = $id;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

    public function rolepermission(Request $request)
    {
        $id = $request->id;
        $data['role_id'] = $id;
        $data['rowRole'] = Role::find($id);
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/rolepermission_box', $data)->render(); 
    }

    public function rolepermission_tablemain(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        $role_id = $request->role_id;

        $data['role_id'] = $role_id;
        $data['class_link'] = $this->class_link;
        $data['sidebars'] = SidebarModel::all();
        $data['permissions'] = Permission::all();
        $data['role_permissions'] = Role::find($role_id)->permissions; 
        return view('page/'.$this->class_link.'/rolepermission_tablemain', $data)->render();        
    }
   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ], [
            'name.required' => 'Nama tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $sts = $request->sts;
                if ($sts == 'Add'){
                    Role::create([
                        'name' => strtoupper($request->name),
                    ]);
                }elseif($sts == 'Edit'){
                    $role = Role::find($request->id);
                    $role->name = $request->name;
                    $role->save();
                }
                
                $resp['code'] = 200;
                $resp['messages'] = 'Tersimpan';
             }
             catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
             }
            
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Role::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }

    public function rolepermission_store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'role_id' => 'required',
        ], [
            'role_id.required' => 'Role tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            
            try{
                $chkpermission_id = $request->chkpermission_id;
                $sts = $request->sts;
                if (!empty($chkpermission_id)){
                    $role = Role::find($request->role_id);
                    
                    $role->syncPermissions([$chkpermission_id]);
                
                    $resp['code'] = 200;
                    $resp['messages'] = 'Tersimpan';
                }else{
                    $resp['code'] = 400;
                    $resp['messages'] = 'Tidak ada permission yg dipilih';
                }
             }
             catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
             }
            
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    }

    public function rolepermission_destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->role_id)){
            try{
                $role_id = $request->role_id;
                $role = Role::find($role_id);
                $role->revokePermissionTo($request->permission_id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
