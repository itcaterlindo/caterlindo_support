<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    private $class_link = 'setting/permission';
   
    public function index()
    {        
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/index', $data);
    }

    public function partial_table_main(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_table_main', $data)->render();
    }

    public function table_data(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }

        $permissions = Permission::all();
    
        return DataTables::of($permissions)
                ->addIndexColumn()
                ->addColumn('opsi', function ($permission){
                    return '
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            Opsi <span class="sr-only"></span>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" data-id="'.$permission->id.'" data-token="'.csrf_token().'" onclick="edit_data(this)"> <i class="fas fa-edit"></i> Edit</a>
                            <a class="dropdown-item" href="javascript:void(0)" data-id="'.$permission->id.'" data-token="'.csrf_token().'" onclick="delete_data(this)"> <i class="fas fa-trash"></i> Delete</a>
                        </div>
                    </div>
                    ';
                })
                ->rawColumns(['opsi'])
                ->toJson();
    }

    public function partial_form_main(Request $request){
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }      

        $id = $request->id;
        $sts = $request->sts;

        if(!empty($id)){
            $data['rowData'] = Permission::find($id)->toArray();
        }

        $data['id'] = $id;
        $data['sts'] = $sts;
        $data['class_link'] = $this->class_link;
        return view('page/'.$this->class_link.'/partial_form_main', $data)->render();
    }

   
    public function store(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        
        $validator = Validator::make($request->all(),[
            'permission_name' => 'required',
        ], [
            'permission_name.required' => 'Permission tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            $resp['code'] = 401;
            $resp['messages'] = 'Error Validasi';
            $resp['data'] = $validator->errors()->all();

        }else{
            try{
                $sts = $request->sts;
                if ($sts == 'Add'){
                    Permission::create([
                        'name' => strtoupper($request->permission_name),
                    ]);
                }elseif($sts == 'Edit'){
                    $permission = Permission::find($request->id);
                    $permission->name =strtoupper($request->permission_name);

                    $permission->save();
                }
                
                $resp['code'] = 200;
                $resp['messages'] = 'Tersimpan';
            }

            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Simpan';
                $resp['data'] = $e->getMessage();
            }
            
        }

        $resp['_token'] = csrf_token();
        return response()->json($resp);
    
    }
    
    public function temp(){
        $role = Role::find(1);
        $role->givePermissionTo(3);
        echo 'a';
    }

    public function destroy(Request $request)
    {
        if( !($request->ajax()) )
        {
            exit('No direct script access allowed');
        }
        if (!empty($request->id)){
            try{
                $id = $request->id;
                Permission::destroy($id);

                $resp['code'] = 200;
                $resp['messages'] = 'Terhapus';
            }
            catch(Exception $e){
                $resp['code'] = 400;
                $resp['messages'] = 'Gagal Hapus';
                $resp['data'] = $e->getMessage();
            }
        }else{
            $resp['code'] = 400;
            $resp['messages'] = 'Id Kosong';
        }

        return response()->json($resp);
    }
}
