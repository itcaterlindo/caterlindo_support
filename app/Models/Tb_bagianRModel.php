<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tb_bagianRModel extends Model
{
    protected $table = 'tb_bagian';
    protected $primaryKey = 'bagian_kd';
    protected $fillable = [
        'bagian_nama', 'bagian_lokasi'
    ];

    public $timestamps = FALSE;

    const CREATED_AT = 'bagian_tglinput';
    const UPDATED_AT = 'bagiantgledit';

}
