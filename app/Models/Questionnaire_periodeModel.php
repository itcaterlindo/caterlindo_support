<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire_periodeModel extends Model
{
    protected $table = 'questionnaire_periode';
    protected $primaryKey = 'periode_id';
    protected $fillable = [
        'q_id', 'periode_start', 'periode_end', 'periode_active', 'user_id', 'periode_note'
    ];
    const CREATED_AT = 'periode_created';
    const UPDATED_AT = 'periode_updated';
}
