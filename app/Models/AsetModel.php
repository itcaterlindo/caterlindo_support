<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\LokasiModel;
use App\Models\Aset_jenisModel;

class AsetModel extends Model
{
    protected $table = 'aset';
    protected $primaryKey = 'aset_id';
    protected $fillable = [
        'aset_id',
        'aset_parent',
        'rmgr_kd',
        'asetjenis_id',
        'lokasi_id',
        'asetstatus_id',
        'bagian_kd',
        'aset_kode',
        'aset_barcode',
        'aset_nama',
        'aset_tglperolehan',
        'aset_nilaiperolehan',
        'aset_qty',
        'aset_satuan',
        'aset_keterangan',
        'aset_status',
        'user_id',
    ];
    const CREATED_AT = 'aset_created';
    const UPDATED_AT = 'aset_updated';

    public static function aset_last_id()
    {
        $maxId = 1;
        $aset = AsetModel::selectRaw('MAX(aset_id) as maxId')->first();
        if (!empty($aset)) {
            $maxId = $aset->maxId;
        }
        return $maxId;
    }

    public static function aset_generate_barcode($aset_id, $lokasi_id, $aset_nama)
    {
        $lokasi = LokasiModel::find($lokasi_id);
        $aset = AsetModel::where('aset_id', '=', $aset_id)->first();
        $aset_qty = 1 + AsetModel::where('aset_kode', '=', $aset->aset_kode)->whereNotNull('aset_barcode')->count();

        
        $tgl_perolehan = date('ymd', strtotime($aset->aset_tglperolehan));
        $lokasi_kode = isset($lokasi->lokasi_kode) ? $lokasi->lokasi_kode : 'CAT';
        $jml = str_pad($aset_qty,2,'0',STR_PAD_LEFT);
        $barcode  = "$aset->aset_kode-$jml-$tgl_perolehan-$lokasi_kode";

        return $barcode;
    }

    public static function aset_generate_kode($asetjenis_id, $aset_kode = null) :string
    {
        $num = 1;
        $asetjenis_kode="";
        if (empty($aset_kode)) {
            $asetjenis = Aset_jenisModel::where('asetjenis_id', $asetjenis_id)->first();
            $aset = AsetModel::where('aset_kode', 'like', "$asetjenis->asetjenis_kode%")->orderBy('aset_kode', 'desc')->first();
            if (!empty($aset)) {
                $num = (int) substr($aset->aset_kode, -5);
                $asetjenis_kode = $asetjenis->asetjenis_kode;
            }
        } else {
            $asetjenis_kode = substr($aset_kode, 0, 1);
            $num = (int) substr($aset_kode, -5);
        }
        $num++;
        $strNum = Str::padLeft($num, 4, '0');
        $kode = "$asetjenis_kode$strNum";

        return $kode;
    }
}
