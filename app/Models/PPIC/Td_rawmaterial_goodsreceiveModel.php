<?php

namespace App\Models\PPIC;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Td_rawmaterial_goodsreceiveModel extends Model
{
    protected $connection = 'mysql_ppic';
    protected $table = 'td_rawmaterial_goodsreceive';
    protected $primaryKey = 'rmgr_kd';
    // protected $fillable = [
    //     'q_id', 'question_id', 'quesanswer_text', 'quesanswer_value'
    // ];
    public $timestamps = false;
    public $incrementing = false;
}
