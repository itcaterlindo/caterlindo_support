<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lelang_has_fileModel extends Model
{
    public $timestamps = false;
    protected $table = 'lelang_has_file';
    protected $fillable = [
        'id_lelang',
        'id_file'
    ];
}
