<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Aset_statusModel extends Model
{
    protected $table = 'aset_status';
    protected $primaryKey = 'asetstatus_id';
    protected $fillable = [
        'asetstatus_id',
        'asetstatus_nama',
        'user_id',
    ];
    const CREATED_AT = 'asetstatus_created';
    const UPDATED_AT = 'asetstatus_updated';
}
