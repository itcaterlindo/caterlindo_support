<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FileModel extends Model
{
    public $timestamps = false;
    protected $table = 'file';
    protected $primaryKey = 'file_id';
    protected $fillable = [
        'file_id',
        'name'
    ];
}
