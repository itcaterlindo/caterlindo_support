<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question_answerModel extends Model
{
    protected $table = 'question_answer';
    protected $primaryKey = 'quesanswer_id';
    protected $fillable = [
        'q_id', 'question_id', 'quesanswer_text', 'quesanswer_value', 'quesanswer_point', 'user_id'
    ];
    const CREATED_AT = 'quesanswer_created';
    const UPDATED_AT = 'quesanswer_updated';
}
