<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aset_Stokopname_jenisModel extends Model
{
    protected $table = 'aset_stokopname_jenis';
    protected $primaryKey = 'aset_stokopname_jenis_kd';
    protected $fillable = [
        'aset_stokopname_jenis_kd', 
        'name',

    ];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
