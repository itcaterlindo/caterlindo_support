<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tb_karyawanModel extends Model
{
    protected $connection = 'mysql_hrm';
    protected $table = 'tb_karyawan';
    protected $primaryKey = 'id';
    // protected $fillable = [
    //     'q_id', 'question_id', 'quesanswer_text', 'quesanswer_value'
    // ];
    public $timestamps = false;
}
