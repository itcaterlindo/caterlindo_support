<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tb_bagianModel extends Model
{
    protected $connection = 'mysql_hrm';
    protected $table = 'tb_bagian';
    protected $primaryKey = 'id';
    // protected $fillable = [
    //     'q_id', 'question_id', 'quesanswer_text', 'quesanswer_value'
    // ];
    public $timestamps = false;

    public static function allBagianAsets() {
        $bagians = Tb_bagianModel::where('bagian_aset', '1')->get();
        return $bagians;
    }
}
