<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ticket_noteModel extends Model
{
    protected $table = 'ticket_note';
    protected $primaryKey = 'note_id';
    protected $fillable = [
        'note_id',
        'note',
        'ticket_id',
        'file_id',
        'user_id',
    ];
    const CREATED_AT = 'created_note';
    const UPDATED_AT = 'updated_note';
}
