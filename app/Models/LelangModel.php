<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LelangModel extends Model
{
    protected $table = 'lelang';
    protected $primaryKey = 'id_lelang';
    protected $fillable = [
        'id_lelang',
        'kode_lelang',
        'id_aset',
        'nama',
        'jumlah',
        'status',
        'start_harga',
        'open_coment',
        'note',
        'kelipatan_bit',
        'tanggal_open',
        'tanggal_close'

    ];
    const CREATED_AT = 'created_lelang';
    const UPDATED_AT = 'updated_lelang';
}
