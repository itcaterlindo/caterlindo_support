<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question_typeModel extends Model
{
    protected $table = 'question_type';
    protected $primaryKey = 'questype_id';
    protected $fillable = [
        'questype_name', 'questype_html', 'questype_htmlscript'
    ];
    const CREATED_AT = 'questype_created';
    const UPDATED_AT = 'questype_updated';
}
