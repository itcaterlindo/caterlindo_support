<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ticket_fileModel extends Model
{
    protected $table = 'ticket_file';
    protected $primaryKey = 'file_id';
    protected $fillable = [
        'file_id',
        'name'
    ];
    const CREATED_AT = 'created_ticket_file';
    const UPDATED_AT = 'updated_ticket_file';
}
