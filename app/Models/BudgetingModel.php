<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BudgetingModel extends Model
{
    protected $table = 'tm_budgeting';
    protected $primaryKey = 'id_budgeting';
    protected $fillable = [
        'kode_budgeting', 'id_bagian', 'status', 'keterangan', 'admin_kd', 'th_anggaran'

    ];
 
    const CREATED_AT = 'tgl_input';
    const UPDATED_AT = 'tgl_upd';


    public static function generate_asetmutasiKode($date, $kode = null) :string
    {
        $num = 0;
        if (empty($kode)){
            $budget = BudgetingModel::whereDate('tgl_input', '=', $date)->orderBy('id_budgeting', 'desc')->first();
            if (!empty($budget)) {
                $num = (int) Str::substr($budget->kode_budgeting, -3);
            }
        }else{
            $num = (int) Str::substr($kode, -3);
        }
        $num++;
        $kode = 'BGD' . date('ymd', strtotime($date)) . Str::padLeft($num, 3, '0');
        return $kode;
    }
}
