<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aset_Stokopname_detailBarcodeModel extends Model
{
    protected $table = 'aset_stokopname_detail_barcode';
    protected $primaryKey = 'aset_stokopname_detail_barcode_kd';
    protected $fillable = [
        'aset_stokopname_detail_barcode_kd', 
        'aset_stokopname_detail_kd',
        'aset_kd', 
        'barcode_batang',

    ];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
