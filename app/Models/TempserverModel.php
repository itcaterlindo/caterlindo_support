<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TempserverModel extends Model
{
    protected $table = 'tempserver';
    protected $primaryKey = 'tempserver_id';
    protected $fillable = [
        'tempserver_temp', 'tempserver_humidity'
    ];
    const CREATED_AT = 'tempserver_created_at';
    const UPDATED_AT = 'tempserver_updated_at';
}
