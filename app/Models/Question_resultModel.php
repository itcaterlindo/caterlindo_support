<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question_resultModel extends Model
{
    protected $table = 'question_result';
    protected $primaryKey = 'quesresult_id';
    protected $fillable = [
        'q_id', 'periode_id', 'kd_karyawan', 'kd_bagian', 'user_id'
    ];
    const CREATED_AT = 'quesresult_created';
    const UPDATED_AT = 'quesresult_updated';
}
