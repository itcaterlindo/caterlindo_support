<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenawaranModel extends Model
{
    protected $table = 'penawaran_lelang';
    protected $primaryKey = 'id_penawaran';
    protected $fillable = [
        'id_penawaran',
        'nama',
        'jumlah',
        'id_bagian',
        'id_lelang',
    ];
    const CREATED_AT = 'created_penawaran';
    const UPDATED_AT = 'updated_penawaran';
}
