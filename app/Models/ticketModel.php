<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ticketModel extends Model
{
    protected $table = 'ticket';
    protected $primaryKey = 'ticket_id';
    protected $fillable = [
        'ticket_id',
        'aset_id',
        'ticket_kode',
        'problem',
        'sparepart',
        'jenis_service',
        'prioritas',
        'status_ticket',
        'rm_kd',
        'file_id',
        'user_id',
    ];
    const CREATED_AT = 'created_ticket';
    const UPDATED_AT = 'updated_ticket';
}
