<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Aset_mutasiModel extends Model
{
    protected $table = 'aset_mutasi';
    protected $primaryKey = 'asetmutasi_id';
    protected $fillable = [
        'asetmutasi_id', 'asetmutasi_kode', 'aset_id', 'asetmutasijenis_id', 'asetmutasi_tanggal', 'asetmutasi_penyusutan',
        'asetmutasi_keterangan', 'asetmutasi_qty', 'asetmutasi_lokasidari', 'asetmutasi_lokasike', 'asetmutasi_bagiandari', 'asetmutasi_bagianke',
        'asetmutasi_status', 'asetstatus_id','user_id', 'user_approve_id'

    ];
    const CREATED_AT = 'asetmutasi_created';
    const UPDATED_AT = 'asetmutasi_updated';

    public static function generate_asetmutasiKode($date, $kode = null) :string
    {
        $num = 0;
        if (empty($kode)){
            $asetMutasi = Aset_mutasiModel::where('asetmutasi_tanggal', '=', $date)->orderBy('asetmutasi_id', 'desc')->first();
            if (!empty($asetMutasi)) {
                $num = (int) Str::substr($asetMutasi->asetmutasi_kode, -3);
            }
        }else{
            $num = (int) Str::substr($kode, -3);
        }
        $num++;
        $kode = 'ASETMUTASI-' . date('ymd', strtotime($date)) . '-' . Str::padLeft($num, 3, '0');
        return $kode;
    }
}
