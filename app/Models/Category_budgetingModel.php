<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category_budgetingModel extends Model
{
    protected $table = 'category_budgeting';
    protected $primaryKey = 'id_category_budgeting';
    protected $fillable = [
        'nm_category_budgeting',
    ];
    const CREATED_AT = 'tgl_input';
    const UPDATED_AT = 'tgl_update';
}
