<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuestionModel extends Model
{
    protected $table = 'question';
    protected $primaryKey = 'question_id';
    protected $fillable = [
        'q_id', 'quescategory_id', 'question_text', 'questype_id', 'question_parent', 'question_required', 'question_active', 'question_activenote', 'question_squence', 'question_bobot'
    ];
    const CREATED_AT = 'question_created';
    const UPDATED_AT = 'question_updated';

    function get_byparam_detail ($param = []) 
    {
        $questions = DB::table('question')
                    ->where($param)
                    ->leftJoin('questionnaire', 'question.q_id', '=', 'questionnaire.q_id')
                    ->leftJoin('question_category', 'question.quescategory_id', '=', 'question_category.quescategory_id')
                    ->leftJoin('question_type', 'question.questype_id', '=', 'question_type.questype_id')
                    ->orderBy('question.question_squence', 'asc')
                    ->get();
        return $questions;
    }
}
