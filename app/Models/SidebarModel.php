<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SidebarModel extends Model
{
    protected $table = 'sidebars';
    protected $primaryKey = 'sidebar_id';
    protected $fillable = [
        'sidebar_name', 'sidebar_level', 'sidebar_parent', 'sidebar_squence', 'sidebar_type'
    ];
    public $timestamps = false; 
}
