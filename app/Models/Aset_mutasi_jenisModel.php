<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aset_mutasi_jenisModel extends Model
{
    protected $table = 'aset_mutasi_jenis';
    protected $primaryKey = 'asetmutasijenis_id';
    protected $fillable = [
        'asetmutasijenis_id', 'asetmutasijenis_nama'

    ];
    const CREATED_AT = 'asetmutasijenis_created';
    const UPDATED_AT = 'asetmutasijenis_updated';
}
