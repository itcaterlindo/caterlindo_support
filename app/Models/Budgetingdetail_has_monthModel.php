<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Budgetingdetail_has_monthModel extends Model
{
    protected $table = 'detail_budgeting_month';
    protected $fillable = [
        'id_barang_budgeting', 'tahun', 'bulan', 'qty'

    ];
    const CREATED_AT = 'tgl_input';
    const UPDATED_AT = 'tgl_update';
}
