<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ticket_has_rawmaterial extends Model
{
    protected $table = 'ticket_has_rawmaterial';
    protected $primaryKey = 'ticket_id';
    protected $fillable = [
        'ticket_id',
        'rm_kd',
        'rm_nama',
  
    ];
}
