<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aset_Stokopname_detailModel extends Model
{
    protected $table = 'aset_stokopname_detail';
    protected $primaryKey = 'aset_stokopname_detail_kd';
    protected $fillable = [
        'aset_stokopname_detail_kd', 
        'aset_stokopname_periode_kd',
        'aset_kd',
        'lokasi_kd', 
        'barcode',
        'note',
        'kondisi',
        'checker',
        'user_id'

    ];
    const CREATED_AT = 'aset_stokopname_created_at';
    const UPDATED_AT = 'aset_stokopname_updated_at';
}
