<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lelang_has_tagModel extends Model
{
    public $timestamps = false;
    protected $table = 'lelang_has_tag';
    protected $fillable = [
        'id_lelang',
        'id_tag'
    ];
}
