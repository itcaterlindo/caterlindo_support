<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireModel extends Model
{
    protected $table = 'questionnaire';
    protected $primaryKey = 'q_id';
    protected $fillable = [
        'q_title', 'q_active', 'user_id', 'q_usercreated'
    ];
    const CREATED_AT = 'q_created';
    const UPDATED_AT = 'q_updated';
}
