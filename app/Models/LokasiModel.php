<?php

namespace App\Models;  

use Illuminate\Database\Eloquent\Model;

class LokasiModel extends Model
{
    protected $table = 'lokasi';
    protected $primaryKey = 'lokasi_id';
    protected $fillable = [
        'lokasi_id',
        'kd_bagian',
        'kd_unit',
        'lokasi_nama',
        'lokasi_keterangan',
        'lokasi_kode',
        'pic',
        'user_id',
    ];
    const CREATED_AT = 'lokasi_created';
    const UPDATED_AT = 'lokasi_updated';
}
