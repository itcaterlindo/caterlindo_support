<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aset_Stokopname_periodeModel extends Model
{
    protected $table = 'aset_stokopname_periode';
    protected $primaryKey = 'aset_stokopname_periode_kd';
    protected $fillable = [
        'aset_stokopname_periode_kd', 
        'aset_stokopname_jenis_kd',
        'bulan',
        'tahun',
        'nomor_seri',
        'status'

    ];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}