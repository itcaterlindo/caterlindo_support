<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangBudgetingModel extends Model
{
    protected $table = 'td_barang_budgeting';
    protected $primaryKey = 'id_barang';
    protected $fillable = [
        'id_budgeting', 'id_aset', 'nm_barang', 'stts', 'spesifikasi_barang', 'keterangan', 'jumlah', 'satuan', 'harga_anggaran', 'admin_kd', 'jumlah_harga', 'id_category'

    ];

    public $timestamps = FALSE;

    const CREATED_AT = 'tgl_input';
    const UPDATED_AT = 'tgl_upd';

}
