<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TagModel extends Model
{
    protected $table = 'tag';
    protected $primaryKey = 'id_tag';
    protected $fillable = [
        'id_tag',
        'name',
        'kode_tag',
        'file_id',
        'user_id',
    ];
    const CREATED_AT = 'created_tag';
    const UPDATED_AT = 'updated_tag';
}
