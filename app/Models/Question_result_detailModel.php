<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question_result_detailModel extends Model
{
    protected $table = 'question_result_detail';
    protected $primaryKey = 'quesresultdetail_id';
    protected $fillable = [
        'quesresult_id', 'question_id', 'quesanswer_id', 'questype_id', 'quesresultdetail_value', 
        'quesresultdetail_text', 'quesresultdetail_point', 'quesresultdetail_questionbobot'
    ];
    const CREATED_AT = 'quesresultdetail_created';
    const UPDATED_AT = 'quesresultdetail_updated';
}
