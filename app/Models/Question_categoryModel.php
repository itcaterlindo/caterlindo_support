<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question_categoryModel extends Model
{
    protected $table = 'question_category';
    protected $primaryKey = 'quescategory_id';
    protected $fillable = [
        'quescategory_name'
    ];
    const CREATED_AT = 'quescategory_created';
    const UPDATED_AT = 'quescategory_updated';
}
