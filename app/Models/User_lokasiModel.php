<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User_lokasiModel extends Model
{
    protected $table = 'user_has_lokasi';
    protected $fillable = [
        'user_id',
        'lokasi_id'
    ];
}
