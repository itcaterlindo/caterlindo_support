<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Aset_jenisModel extends Model
{
    protected $table = 'aset_jenis';
    protected $primaryKey = 'asetjenis_id';
    protected $fillable = [
        'asetjenis_id',
        'asetjenis_nama',
        'asetjenis_parent',
        'asetjenis_kode',
        'user_id',
    ];
    const CREATED_AT = 'asetjenis_created';
    const UPDATED_AT = 'asetjenis_updated';
}
