<?php

function buildSpan($paramText = '', $paramColor = 'warning')
{
    return '<span class="badge bg-' . $paramColor . '">' . $paramText . '</span>';
}

function spanStatus($paramStatus = null)
{
    $paramColor = 'success';
    $paramText = 'Aktif';
    if (empty($paramStatus)) {
        $paramColor = 'warning';
        $paramText = 'Tidak Aktif';
    }
    return buildSpan($paramText, $paramColor);
}

function spanMapStatus($paramStatus = 'pending')
{
    $arrayStatus['pending'] = 'warning';
    $arrayStatus['approved'] = 'success';
    $arrayStatus['cancel'] = 'danger';
    $arrayStatus['finish'] = 'primary';
    return buildSpan(ucwords($paramStatus), $arrayStatus[strtolower($paramStatus)]);
}
