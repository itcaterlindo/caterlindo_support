@extends('layouts.questionnaire')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php 
    $master_var = isset($master->q_title) ? str_replace(' ', '_', $master->q_title) : 'master_var';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
@endphp

@section('content')
<div class="error-page">
    <h2 class="headline text-info"> {{ !empty($headline) ? $headline : '-' }}</h2>

    <div class="error-content">
      <h2><i class="fas fa-exclamation-triangle text-info"></i> {{ !empty($header) ? $header : '-' }} </h2>
        
      <p>
        <h3>{{ !empty($message) ? $message : '-' }}</h3>
      </p>

    </div>
    <!-- /.error-content -->
</div>

@endsection