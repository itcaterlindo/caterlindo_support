@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
	$periode_range = $periode_start.' - '.$periode_end;
}

@endphp
<form id="{{ $form_id }}" action="{{ route('ListQuestionnaire.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idq_id" class="col-md-2 col-form-label">Kuesioner</label>
			<div class="col-sm-8 col-xs-12">
				{{Form::select('q_id', isset($opsiKuesioner) ? $opsiKuesioner : array(), isset($q_id) ? $q_id:null, ['class' => 'form-control form-control-sm select2', 'id' => 'idq_id'])}}
			</div>	
		</div>

		<div class="form-group row">
			<label for="idperiode_range" class="col-md-2 col-form-label">Range</label>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="far fa-clock"></i></span>
					</div>
					<div class="errInput" id="idErrperiode_range"></div>
					<input type="text" class="form-control datetimepicker" name="periode_range" id="idperiode_range" placeholder="Start" value="{{ isset($periode_range) ? $periode_range : date('Y-m-d H:i:s') }}">
				</div>	
			</div>	
		</div>

		<div class="form-group row">
			<label for="idperiode_note" class="col-md-2 col-form-label">Note</label>
			<div class="col-sm-8 col-xs-12">
				<textarea name="periode_note" class="form-control" rows="2" placeholder="Note">{{ isset($periode_note) ? $periode_note : null }}</textarea>
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>