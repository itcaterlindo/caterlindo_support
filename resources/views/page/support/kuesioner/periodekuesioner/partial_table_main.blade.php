<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:10%; text-align:center;">Kuesioner</th>
			<th style="width:10%; text-align:center;">Start</th>
			<th style="width:10%; text-align:center;">End</th>
			<th style="width:10%; text-align:center;">Note</th>
			<th style="width:10%; text-align:center;">Status</th>
			<th style="width:10%; text-align:center;">Last Modified</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "{{ url($class_link.'/table_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "q_title", name: "q_title" },
            { data: "periode_start", name: "periode_start" },
            { data: "periode_end", name: "periode_end" },
            { data: "periode_note", name: "periode_note" },
            { data: "periode_active", name: "periode_active" },
            { data: "periode_updated", name: "periode_updated" },
        ],
		"order":[7, 'desc'],
	});
</script>