@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/

@endphp
<form id="{{ $form_id }}" action="{{ route('Kuesioner.store') }}" role="form">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="q_id" id="idq_id" placeholder="idq_id" value="{{ isset($periode->q_id) ? $periode->q_id : null }}">
<input type="hidden" name="periode_id" id="idperiode_id" placeholder="idperiode_id" value="{{ isset($periode->periode_id) ? $periode->periode_id : null }}">
<input type="hidden" name="kd_bagian" id="idkd_bagian" placeholder="idkd_bagian" value="{{ isset($karyawan->kd_bagian) ? $karyawan->kd_bagian : null }}">
<input type="hidden" name="kd_karyawan" id="idkd_karyawan" placeholder="idkd_karyawan" value="{{ isset($kd_karyawan) ? $kd_karyawan : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<strong> NIK :</strong> {{ $karyawan->nik_karyawan }} <br>
		<strong> Nama Karyawan :</strong> {{ $karyawan->nm_karyawan }} <br>
		<strong> Bagian :</strong> {{ $karyawan->nm_bagian }} <br>
	</div>
	<!-- /.col -->
	<div class="col-sm-4 invoice-col">
	</div>
	<div class="col-sm-4 invoice-col">
		<img id="avatar" class="editable img-responsive float-right" alt="Avatar" src="/caterlindo_hrm/karyawan_page/../assets/img/karyawan/{{ $karyawan->pas_foto }}" width="90px" height="100px">
	</div>
	<!-- /.col -->
</div>
<hr>
<div class="row">

	<div class="col-md-12">

	@php 
	foreach ($questions as $q) :
	@endphp
	<div class="form-group">
		<label>{!! "$q->question_squence.  $q->question_text" !!}</label>
		<div class="form-group">
			@php 
			$html = '';
			$htmlText = '';
			$valType = '';
			foreach ($answers as $anw) :
				if ($anw->question_id == $q->question_id) {
					if ($q->questype_name == 'option' || $q->questype_name == 'select') {
						if ($q->questype_name == 'select') {
							$valType = '[]';
						}
						$text = str_replace(' ', '_',$anw->quesanswer_text);
						$htmlText =  $q->questype_htmlscript;
						$htmlText =  str_replace('{name}', "txt$anw->question_id$valType", $htmlText);
						$htmlText =  str_replace('{label}', $anw->quesanswer_text, $htmlText);
						$htmlText =  str_replace('{id}', "id$anw->quesanswer_id", $htmlText);
						$htmlText =  str_replace('{value}', $anw->quesanswer_id, $htmlText);
						$htmlText =  str_replace('{checked}', '', $htmlText);
						$htmlText =  str_replace('{disabled}', '', $htmlText);
					}elseif ($q->questype_name == 'text' || $q->questype_name == 'number') {
						$text = str_replace(' ', '_',$anw->quesanswer_text);
						$htmlText =  $q->questype_htmlscript;
						$htmlText =  str_replace('{type}', $q->questype_html, $htmlText);
						$htmlText =  str_replace('{name}', "txt$anw->question_id$valType", $htmlText);
						$htmlText =  str_replace('{placeholder}', $anw->quesanswer_text, $htmlText);
						$htmlText =  str_replace('{id}', "id$anw->quesanswer_id", $htmlText);
						$htmlText =  str_replace('{value}', '', $htmlText);
						$htmlText =  str_replace('{disabled}', '', $htmlText);
					}

					$html .= $htmlText;
				}
			endforeach;	
			echo $html;
			@endphp
		</div>	
	</div>
	@php
	endforeach;
	@endphp

	<hr>
	<div class="form-group float-right">
		<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
			<i class="fas fa-save"></i> Simpan
		</button>
	</div>
	
	</div>
</div>

</form>