<script type="text/javascript">
	open_form_main('{{ $periode_id }}', '{{ $kd_karyawan }}');
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
	generate_timer ();

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_form_main(periode_id, kd_karyawan) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/kuesioner/partial_form_main") }}',
			data: {periode_id: periode_id, kd_karyawan: kd_karyawan},
			success: function(html) {
				$('#{{ $idBoxContent }}').html(html);
			}
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}

	function permission_data(role_id){
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/rolepermission") }}',
			data: {id: role_id},
			success: function(html) {
				$('#idMainContent').prepend(html);
				moveTo('idMainContent');	
			}
		});
	}

	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ route('Role.destroy') }}",
				type: 'PUT',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'PUT',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				}
			});
		}
	}

	function generate_timer () {
		// Set the date we're counting down to
		var countDownDate = new Date("{!! $periode_end !!}").getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

		// Get today's date and time
		var now = new Date().getTime();
			
		// Find the distance between now and the count down date
		var distance = countDownDate - now;
			
		// Time calculations for days, hours, minutes and seconds
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
		// Output the result in an element with id="demo"
		document.getElementById("idtimer").innerHTML = days + "d " + hours + "h "
		+ minutes + "m " + seconds + "s ";
			
		// If the count down is over, write some text 
		if (distance < 0) {
			clearInterval(x);
			document.getElementById("idtimer").innerHTML = "EXPIRED";
		}
		}, 1000);
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 7000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

	function render_select2(elem){
		$(elem).select2({
			theme: 'bootstrap4'
		});
	}

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('Kuesioner.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200){
					$('.errInput').html('');
					window.location.reload();
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

</script>