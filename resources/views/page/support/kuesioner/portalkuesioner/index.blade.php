@extends('layouts.questionnaire')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php 
    $master_var = isset($karyawan->nm_karyawan) ? str_replace(' ', '_', $karyawan->nm_karyawan) : 'master_var';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
@endphp

@section('content')
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
          <div class="card-tools">
          </div>
        </div>
        <div class="card-body">
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <strong> NIK :</strong> {{ $karyawan->nik_karyawan }} <br>
                <strong> Nama Karyawan :</strong> {{ $karyawan->nm_karyawan }} <br>
                <strong> Bagian :</strong> {{ $karyawan->nm_bagian }} <br>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
              </div>
              <div class="col-sm-4 invoice-col">
                <img id="avatar" class="editable img-responsive float-right" alt="Avatar" src="/caterlindo_hrm/karyawan_page/../assets/img/karyawan/{{ $karyawan->pas_foto }}" width="90px" height="100px">
              </div>
              <!-- /.col -->
            </div>
            <hr>

            <div class="card-body table-responsive p-0">
              <table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap">
                <thead>
                  <tr style="text-align: center;">
                    <th>No</th>
                    <th>Kuesioner</th>
                    <th>Periode</th>
                    <th>Opsi</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $no = 1;
                    foreach ($kuesioners as $each ) :
                  @endphp
                  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $each['q_title'] }}</td>
                    <td>{{ $each['periode_start']." s/d ".$each['periode_end'] }}</td>
                    <td style="text-align: center;"> <a class="btn btn-success" href="{{ url( '/support/kuesioner/kuesioner/'.Crypt::encrypt($each['periode_id']).'/'.Crypt::encrypt($karyawan->kd_karyawan) ) }}"> Pilih </a> </td>
                  </tr>
                  @php
                      $no++;
                    endforeach;
                  @endphp
                </tbody>
              </table>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  

@endsection

@section('scriptJS')
  {{-- @include('page/'.$class_link.'/index_js', $data) --}}
@append
