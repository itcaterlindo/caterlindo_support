@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
	$periode_range = $periode_start.' - '.$periode_end;
}

@endphp
<form id="{{ $form_id }}" action="{{ route('ListQuestionnaire.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}

<div class="row">
	<div class="col-md-12">

		<div class="form-group">
			<label>Periode</label>
			<select id="idperiode" class="form-control select2periode" style="width: 100%;">
				<option value="">-- Pilih Periode --</option>
				@foreach ($periodes as $periode)
					<option value="{{ $periode->quesresult_id }}">{{ "$periode->periode_note($periode->periode_start - $periode->periode_end)" }}</option>
				@endforeach
			</select>
		</div>
		<br>
		<hr>
		<div id="idpreview"></div>
	
	</div>
</div>

</form>

<script type="text/javascript">
	$('#idperiode').on('select2:select', function (e) {
		var data = e.params.data;
		preview_result(data.id);
	});
</script>