<script type="text/javascript">
	render_kuesioner();
	render_karyawan();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function render_kuesioner() {
		$("#idq_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url($class_link.'/get_kuesioner') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramActive: 1,
						paramKuesioner: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_karyawan() {
		$("#idkd_karyawan").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url($class_link.'/get_karyawan') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramKaryawan: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function cari() {
		event.preventDefault();
		let q_id = $('#idq_id').val();
		let kd_karyawan = $('#idkd_karyawan').val();
		hasilkaryawan_preview (q_id, kd_karyawan);
	}

	function hasilkaryawan_preview (q_id, kd_karyawan) {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/reporthasilkaryawankuesioner/hasilkaryawan_preview") }}',
				data: {q_id:q_id, kd_karyawan:kd_karyawan},
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					render_select2('.select2periode');
					moveTo('idMainContent');
				}
			});
		});
	}

	function preview_result(quesresult_id) {
		event.preventDefault();
		$('#idpreview').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url("support/kuesioner/listkuesioner/preview_main") }}?quesresult_id='+quesresult_id,
				data: {quesresult_id:quesresult_id},
				success: function(html) {
					$('#idpreview').html(html);
					$('#idpreview').slideDown();
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function render_select2(elem){
		$(elem).select2({
			theme: 'bootstrap4'
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

</script>