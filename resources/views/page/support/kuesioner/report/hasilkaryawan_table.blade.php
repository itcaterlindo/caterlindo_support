<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:10%; text-align:center;">Pertanyaan</th>
			<th style="width:10%; text-align:center;">Jawaban</th>
		</tr>
		</thead>
		<tbody>
            @php
                foreach ($questions as $question) {
                    $groupQuestions["$question->question_id;$question->question_squence;$question->question_text"][] = $question;
                }
            
			
			foreach ($groupQuestions as $groupQuestion => $answers):
                $expGroup = explode(';', $groupQuestion);
            @endphp
			<tr style="font-weight: bold;">
				<td>{{ $expGroup[1] }}</td>
				<td colspan="2">{{ $expGroup[2] }}</td>
                <td style="display: none;"></td>
			</tr>
            @php 
            foreach ($answers as $answer):
            @endphp
                <tr>
                    <td></td>
                    <td>{{ $answer->quesanswer_text }}</td>
                    <td>
                    @php 
                    if ($answer->questype_id == 1 || $answer->questype_id == 2){
                        echo isset($resultOpsis[$answer->quesanswer_id]) ? $resultOpsis[$answer->quesanswer_id]: 0;
                    }else{
                        echo isset($resultVals[$answer->quesanswer_id]) ? $resultVals[$answer->quesanswer_id]: '-'; 
                    }
                    @endphp
                    </td>
                </tr>
            @php 
            endforeach;
			endforeach;
            @endphp
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"dom": 'Bfrtip',
		"paging" : false,
		"ordering" : false,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
		"buttons" : [{
			"extend" : "excel",
			"title" : "Report Per-Periode",
			"exportOption" : {
				"columns": [0,1,2,3],
			}
		}],
	});
</script>