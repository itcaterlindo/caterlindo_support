<script type="text/javascript">
	// open_table();
	render_kuesioner();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	$('#idq_id').on('select2:select', function (e) {
		var data = e.params.data;
		$('#idperiode_id').val(null).trigger('change');
		render_periode(data.id); 
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function render_kuesioner() {
		$("#idq_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url($class_link.'/get_kuesioner') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramActive: 1,
						paramKuesioner: params.term
					};
				},
				processResults: function (response) {
					console.log(response);
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_periode(q_id = null) {
		$("#idperiode_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url($class_link.'/get_periode') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q_id: q_id,
						paramPeriode: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function cariKaryawanKuesioner() {
		event.preventDefault();
		let q_id = $('#idq_id').val();
		let periode_id = $('#idperiode_id').val();
		let status = $('#idstatus').val();
		reportkaryawankuesioner_table (q_id, periode_id, status);
	}

	function reportkaryawankuesioner_table (q_id, periode_id, status) {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/karyawankuesioner/reportkaryawankuesioner_table") }}',
				data: {q_id:q_id, periode_id:periode_id, status:status},
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

</script>