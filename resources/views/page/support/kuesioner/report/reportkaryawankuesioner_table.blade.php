<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:10%; text-align:center;">Nama</th>
			<th style="width:10%; text-align:center;">Bagian</th>
			<th style="width:10%; text-align:center;">Skor</th>
			<th style="width:10%; text-align:center;">Status</th>
		</tr>
		</thead>
		<tbody>
			@php ($no = 1)
			@foreach ($results as $result)
				
			<tr>
				<td>{{ $no }}</td>
				<td>{{ $result['nm_karyawan'] }}</td>
				<td>{{ $result['nm_bagian'] }}</td>
				<td class="dt-right">{{ isset($resulSkors[$result['kd_karyawan']]) ? $resulSkors[$result['kd_karyawan']] : 0 }}</td>
				<td>
				<?php
				$paramText = $result['status'];
				$paramColor = 'warning';
				if ($paramText == 'INPUT') {
					$paramColor = 'success';
				}
				echo buildSpan ($paramText, $paramColor)
				?>
				</td>
			</tr>
			@php ($no++)
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"dom": 'Bfrtip',
		"paging" : false,
		"ordering" : true,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
		"buttons" : [{
			"extend" : "excel",
			"title" : "Report Karyawan Kuesioner",
			"exportOption" : {
				"columns": [0,1,2,3],
			}
		}],
	});
</script>