@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('ListQuestion.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="q_id" id="idq_id" placeholder="idq_id" value="{{ isset($q_id) ? $q_id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idquescategory_id" class="col-md-2 col-form-label">Kategori</label>
			<div class="col-sm-4 col-xs-12">
				{{Form::select('quescategory_id', isset($opsiCategory) ? $opsiCategory : array(), isset($quescategory_id) ? $quescategory_id:null, ['class' => 'form-control form-control-sm select2', 'id' => 'idquescategory_id'])}}
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquestype_id" class="col-md-2 col-form-label">Type</label>
			<div class="col-sm-4 col-xs-12">
				{{Form::select('questype_id', isset($opsiType) ? $opsiType : array(), isset($questype_id) ? $questype_id:null, ['class' => 'form-control form-control-sm select2', 'id' => 'idquestype_id'])}}
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquestion_squence" class="col-md-2 col-form-label">Squence</label>
			<div class="col-sm-2 col-xs-12">
				<div class="errInput" id="idErrquestion_squence"></div>
				<input type="number" class="form-control" name="question_squence" id="idquestion_squence" placeholder="Squence" value="{{ isset($question_squence) ? $question_squence : '' }}">
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquestion_text" class="col-md-2 col-form-label">Pertanyaan</label>
			<div class="col-sm-8 col-xs-12">
				<div class="errInput" id="idErrquestion_text"></div>
				<textarea class="form-control" rows="2" name="question_text" placeholder="Pertanyaan">{{ isset($question_text) ? $question_text : '' }}</textarea>
			</div>
		</div>

		<div class="form-group row">
			<label for="idquestion_active" class="col-md-2 col-form-label">Status</label>
			<div class="col-sm-4 col-xs-12">
				{{Form::select('question_active', isset($opsiAktif) ? $opsiAktif : array(1 => 'Aktif', 0 => 'Tidak Aktif'), isset($question_active) ? $question_active:null, ['class' => 'form-control form-control-sm select2', 'id' => 'idquestion_active'])}}
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquestion_required" class="col-md-2 col-form-label">Required</label>
			<div class="col-sm-4 col-xs-12">
				{{Form::select('question_required', isset($opsiAktif) ? $opsiAktif : array(1 => 'Ya', 0 => 'Tidak'), isset($question_required) ? $question_required:null, ['class' => 'form-control form-control-sm select2', 'id' => 'idquestion_required'])}}
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquestion_bobot" class="col-md-2 col-form-label">Bobot (%)</label>
			<div class="col-sm-2 col-xs-12">
				<div class="errInput" id="idErrquestion_bobot"></div>
				<input type="number" class="form-control" name="question_bobot" id="idquestion_bobot" placeholder="Bobot (%)" value="{{ isset($question_bobot) ? $question_bobot : '' }}">
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>