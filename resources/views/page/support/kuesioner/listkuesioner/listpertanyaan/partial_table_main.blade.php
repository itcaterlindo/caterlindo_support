<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:7%; text-align:center;">Kategori</th>
			<th style="width:15%; text-align:center;">Pertanyaan</th>
			<th style="width:10%; text-align:center;">Tipe Pertanyaan</th>
			<th style="width:5%; text-align:center;">Squence</th>
			<th style="width:5%; text-align:center;">Bobot(%)</th>
			<th style="width:7%; text-align:center;">Status</th>
			<th style="width:10%; text-align:center;">Last Modified</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "{{ url($class_link.'/table_data?q_id='.$q_id) }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "quescategory_name", name: "quescategory_name" },
            { data: "question_text", name: "question_text" },
            { data: "questype_name", name: "questype_name" },
            { data: "question_squence", name: "question_squence" },
            { data: "question_bobot", name: "question_bobot" },
            { data: "question_active", name: "question_active" },
            { data: "question_updated", name: "question_updated" },
        ],
		"order":[5, 'asc'],
	});
</script>