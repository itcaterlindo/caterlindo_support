<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No</th>
			<th style="width:4%; text-align:center;">Poin</th>
			<th style="width:10%; text-align:center;">Bobot</th>
			<th style="width:10%; text-align:center;">Hasil</th>
		</tr>
		</thead>
        <tbody>
            @php
            $sumHasil = 0;
            foreach ($quesresults as $quesresult ) :
                $hasil = $quesresult->quesresultdetail_questionbobot / 100 * $quesresult->quesresultdetail_point;
                $sumHasil += $hasil;
            @endphp
            <tr>
                <td>{{ $quesresult->question_squence }}</td>
                <td class="dt-right">{{ $quesresult->quesresultdetail_point }}</td>
                <td class="dt-right">{{ $quesresult->quesresultdetail_questionbobot.' %' }}</td>
                <td class="dt-right">{{ $hasil }}</td>
            </tr>
            @php
            endforeach;   
            @endphp
        </tbody>
        <tfoot>
            <tr style="font-weight: bold;">
                <td colspan="3" class="dt-right">Jumlah : </td>
                <td class="dt-right">{{ $sumHasil }}</td>
            </tr>
        </tfoot>
	</table>
</div>
<script type="text/javascript">
	// var table = $('#idTable').DataTable({
	// 	"processing": true,
	// 	"serverSide": true,
	// 	"ordering" : true,
	// 	"ajax": "{{ url($class_link.'/table_data') }}",
	// 	"language" : {
	// 		"lengthMenu" : "Tampilkan _MENU_ data",
	// 		"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
	// 		"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
	// 		"infoFiltered": "",
	// 		"infoEmpty" : "Tidak ada data yang ditampilkan",
	// 		"search" : "Cari :",
	// 		"loadingRecords": "Memuat Data...",
	// 		"processing":     "Sedang Memproses...",
	// 		"paginate": {
	// 			"first":      '<span class="fas fa-fast-backward"></span>',
	// 			"last":       '<span class="fas fa-fast-forward"></span>',
	// 			"next":       '<span class="fas fa-forward"></span>',
	// 			"previous":   '<span class="fas fa-backward"></span>'
	// 		}
	// 	},
    //     "columns": [
    //         { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
    //         { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
    //         { data: "q_title", name: "q_title" },
    //         { data: "q_active", name: "q_active" },
    //         { data: "q_updated", name: "q_updated" },
    //     ],
	// 	"order":[4, 'desc'],
	// });
</script>