@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
@endphp
<form id="{{ $form_id }}" role="form">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="q_id" id="idq_id" placeholder="idq_id" value="{{ isset($q_id) ? $q_id : null }}">
<input type="hidden" name="quesresult_id" id="idquesresult_id" placeholder="idquesresult_id" value="{{ isset($quesresult_id) ? $quesresult_id : null }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
		<strong> NIK :</strong> {{ isset($karyawan->nik_karyawan) ? $karyawan->nik_karyawan : '-' }} <br>
		<strong> Nama Karyawan :</strong> {{ isset($karyawan->nm_karyawan) ? $karyawan->nm_karyawan: '-'  }} <br>
		<strong> Bagian :</strong> {{ isset($karyawan->nm_bagian) ? $karyawan->nm_bagian: '-' }} <br>
	</div>
    @if (!empty($quesresult_id))
	<div class="col-sm-8 invoice-col">
        <strong> Kuesioner :</strong> {{ isset($kuesioner->q_title) ? $kuesioner->q_title : null }} <br>
        <strong> Periode :</strong> {{ isset($kuesioner->periode_start) ? $kuesioner->periode_note.'('.date('Y-m-d', strtotime($kuesioner->periode_start)).' s/d '.date('Y-m-d', strtotime($kuesioner->periode_end)).')' : null }} <br>
    </div>   
    @endif
</div>
<hr>
<div class="row">

	<div class="col-md-12">

	@php 
	foreach ($questions as $q) :
	@endphp
	<div class="form-group">
		<label>{!! "$q->question_squence.  $q->question_text" !!}</label>
		<div class="form-group">
			@php 
			$html = '';
			$htmlText = '';
			$valType = '';
			foreach ($answers as $anw) :
                $checked = ''; 
                $value = null;
                if ($anw->question_id == $q->question_id) {
                    if (isset($quesresults)) {
                        foreach ($quesresults as $quesresult) {
                            if ($quesresult['question_id'] == $q->question_id ){
                                if ($quesresult['quesanswer_id'] == $anw->quesanswer_id) {
                                    $checked = 'checked';
                                }
                                if ($quesresult['questype_name'] == 'text') {
                                    $value = $quesresult['quesresultdetail_text'];
                                }elseif($quesresult['questype_name'] == 'number') {
                                    $value = $quesresult['quesresultdetail_value'];
                                }
                            }
                        }
                    }
                    if ($q->questype_name == 'option' || $q->questype_name == 'select') {
                        if ($q->questype_name == 'select') {
                            $valType = '[]';
                        }
                        $text = str_replace(' ', '_',$anw->quesanswer_text);
                        $htmlText =  $q->questype_htmlscript;
                        $htmlText =  str_replace('{name}', "txt$anw->question_id$valType", $htmlText);
                        $htmlText =  str_replace('{label}', $anw->quesanswer_text, $htmlText);
                        $htmlText =  str_replace('{id}', "id$anw->quesanswer_id", $htmlText);
                        $htmlText =  str_replace('{value}', $anw->quesanswer_id, $htmlText);
                        $htmlText =  str_replace('{checked}', $checked, $htmlText);
                        $htmlText =  str_replace('{disabled}', 'disabled', $htmlText);
                    }elseif ($q->questype_name == 'text' || $q->questype_name == 'number') {
                        $text = str_replace(' ', '_',$anw->quesanswer_text);
                        $htmlText =  $q->questype_htmlscript;
                        $htmlText =  str_replace('{type}', $q->questype_html, $htmlText);
                        $htmlText =  str_replace('{name}', "txt$anw->question_id$valType", $htmlText);
                        $htmlText =  str_replace('{placeholder}', $anw->quesanswer_text, $htmlText);
                        $htmlText =  str_replace('{id}', "id$anw->quesanswer_id", $htmlText);
                        $htmlText =  str_replace('{value}', $value, $htmlText);
                        $htmlText =  str_replace('{disabled}', 'disabled', $htmlText);
                    }

                    $html .= $htmlText;
                }
			endforeach;	
			echo $html;
			@endphp
		</div>	
	</div>
	@php
	endforeach;
	@endphp

	<hr>	
	</div>
</div>

</form>