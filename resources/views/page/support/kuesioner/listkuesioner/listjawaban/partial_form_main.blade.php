@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('ListAnswer.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="q_id" id="idq_id" placeholder="idq_id" value="{{ isset($q_id) ? $q_id : '' }}">
<input type="hidden" name="question_id" id="idquestion_id" placeholder="idquestion_id" value="{{ isset($question_id) ? $question_id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idquesanswer_text" class="col-md-2 col-form-label">Text</label>
			<div class="col-sm-8 col-xs-12">
				<div class="errInput" id="idErrquesanswer_text"></div>
				<input type="text" class="form-control" name="quesanswer_text" id="idquesanswer_text" placeholder="Text" value="{{ isset($quesanswer_text) ? $quesanswer_text : '' }}">
			</div>	
		</div>

		<div class="form-group row">
			<label for="idquesanswer_point" class="col-md-2 col-form-label">Poin</label>
			<div class="col-sm-2 col-xs-12">
				<div class="errInput" id="idErrquesanswer_point"></div>
				<input type="number" class="form-control" name="quesanswer_point" id="idquesanswer_point" placeholder="Poin" value="{{ isset($quesanswer_point) ? $quesanswer_point : '' }}">
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>