<script type="text/javascript">
	preview_main('{{ isset($q_id) ? $q_id:"" }}', '{{ isset($quesresult_id) ? $quesresult_id:"" }}');
	preview_tablepoin('{{ isset($quesresult_id) ? $quesresult_id:"" }}');
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function preview_main(q_id = null, quesresult_id = null) {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/preview_main") }}',
                data: { q_id:q_id, quesresult_id:quesresult_id },
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function preview_tablepoin (quesresult_id = null) {
		$('#<?php echo $idBoxContentPoin; ?>').slideUp(function(){
			$.ajax({
					type: 'GET',
					url: '{{ url($class_link."/preview_tablepoin") }}',
					data: { quesresult_id:quesresult_id },
					success: function(html) {
						$('#{{ $idBoxContentPoin }}').html(html);
						$('#{{ $idBoxContentPoin }}').slideDown();
					}
				});
		});
	}

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

</script>