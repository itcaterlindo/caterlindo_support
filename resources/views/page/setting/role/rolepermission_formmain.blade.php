@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
extract($rowRole);
if ($sts == 'Edit'){
	extract($role);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('Role.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="role_id" id="idrole_id" placeholder="idrole_id" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="permission_id" id="idpermissionid" placeholder="idpermission_id" value="{{ isset($permission_id) ? $permission_id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idname" class="col-md-2 col-form-label">Role</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrname"></div>
				<input type="text" class="form-control" name="name" id="idname" placeholder="Name" readonly="true" value="{{ isset($name) ? $name : '' }}" autofocus>
			</div>	
		</div>
		<div class="form-group row">
			<label for="idpermission_id" class="col-md-2 col-form-label">Permission</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrpermission_id"></div>
				{{ Form::select('permission_id',  $permissions, null, ['class' => 'form-control select2']) }}
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Save
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>