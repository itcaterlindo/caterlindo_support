<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>

@php 
$results = []; $arrayPermissionHtml = [];
foreach ($permissions as $permission) {
	$results[$permission['sidebar_id']][] = $permission;
}
foreach ($results as $key => $value) {
	$arrayKey[] = $key; 
	$chkHtml = '<div class="form-group">';
	foreach ($value as $v) {
		$checked = '';
		foreach ($role_permissions as $role_permission) {
			if ($role_permission->id == $v->id) {
				$checked = 'checked';
			}
		}
		$chkHtml .= "<div class='form-check'><input class='form-check-input' name='chkpermission_id[]' id='id$v->id' value='$v->id' type='checkbox' $checked>
	  		<label class='form-check-label' for='id$v->id'><strong> $v->name </strong></label></div>";
	}
	$chkHtml .= '</div>';
	$arrayPermissionHtml[$key] = $chkHtml;
}

@endphp

<form id="idForm" class="form-horizontal">
@method('POST')
@csrf
<input type="hidden" name="role_id" id="idrole_id" value="{{ $role_id }}" >
<table id="idTable" class="table table-bordered display responsive" style="width:100%;">
	<tbody>
		@foreach ($sidebars as $sidebar0)
			@if ($sidebar0->sidebar_level == 0)
			<tr>
				<td> 
					<li> @php 
						echo $sidebar0->sidebar_name; 
						if (in_array($sidebar0->sidebar_id, $arrayKey)){
							echo $arrayPermissionHtml[$sidebar0->sidebar_id];
						}
						@endphp
						<ul>
						@foreach ($sidebars as $sidebar1)
							@if ($sidebar1->sidebar_level == 1 && $sidebar1->sidebar_parent == $sidebar0->sidebar_id)
								<li> @php 
									echo $sidebar1->sidebar_name; 
									if (in_array($sidebar1->sidebar_id, $arrayKey)){
										echo $arrayPermissionHtml[$sidebar1->sidebar_id];
									}
									@endphp
									<ul>
										@foreach ($sidebars as $sidebar2)
											@if ($sidebar2->sidebar_level == 2 && $sidebar2->sidebar_parent == $sidebar1->sidebar_id)
												<li>
													@php 
													echo $sidebar2->sidebar_name; 
													if (in_array($sidebar2->sidebar_id, $arrayKey)){
														echo $arrayPermissionHtml[$sidebar2->sidebar_id];
													}
													@endphp
													<ul>
														@foreach ($sidebars as $sidebar3)
															@if ($sidebar3->sidebar_level == 3 && $sidebar3->sidebar_parent == $sidebar2->sidebar_id)
																<li>
																	@php 
																	echo $sidebar3->sidebar_name; 
																	if (in_array($sidebar3->sidebar_id, $arrayKey)){
																		echo $arrayPermissionHtml[$sidebar3->sidebar_id];
																	}
																	@endphp
																	</li>
															@endif
														@endforeach	
													</ul>
												</li>
											@endif
										@endforeach	
									</ul>
								</li>
							@endif
						@endforeach	
						</ul>
					</li>
				</td>
			</tr>
			@endif
		@endforeach	
	</tbody>
</table>
<button id="idbtnSubmitidForm" class="btn btn-sm btn-info float-right" onclick="submitData('idForm')"> <i class="fa fa-save"></i> Simpan </button>
</form>