@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/* Spreat rowData*/
if (isset($rowData)){
	extract($rowData);
}
	
@endphp
<form id="{{ $form_id }}" action="{{ route('User.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idpermission_name" class="col-md-2 col-form-label">Permission</label>
			<div class="col-sm-4 col-xs-12">
				<div class="errInput" id="idErrpermission_name"></div>
				<input type="text" class="form-control" name="permission_name" id="idpermission_name" placeholder="Permission" value="{{ isset($name) ? $name : '' }}" autofocus>
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Save
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>