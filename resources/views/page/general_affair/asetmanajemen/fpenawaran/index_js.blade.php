<script type="text/javascript">
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}


	// function openList() {
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: '{{ url("lelang/penawaran/partial_list_main") }}',
	// 		success: function(html) {
	// 			$('#{{ $idBoxContent }}').html(html);
	// 		}
	// 	});
	// }

	function openView(items, key, kd_k) { 
		getView(items, key, kd_k);
	 }

	 function getView(id, key, kd_k){

		// console.log('{{ url("lelang/penawaran/'+ key +'/open_view") }}');
		// console.log(window.location.origin)
		console.log({id: id, kd_k : kd_k});
		$.ajax({
			type: 'GET',
			url: window.location.origin + '/caterlindo_support/public/lelang/penawaran/'+ key +'/open_view',
			data: {id: id, kd_k : kd_k},
			success: function(html) {
				toggle_modal('Form ', html);
			}
		});
	 }

	 function stored(){
		 var items = {'nama' : $('#nama').val(), 'id_bagian' : $('#bagian').val(), 'id_lelang' : $('#id_lelang').val(), 'jumlah' : $('#penawaran').val()};

		 $.ajax({
			 	type: 'POST',
				url: "{{ url('api/support/lelang/v1/store_penawaran') }}",
				data: items,
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
			 	success: function (data) {
					if (data.code == 200){
						sweetalert2 ('success', data.messages);
						refresh();
						// openList();
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
						refresh();
						// openList();
					}else if(data.code == 403){
						sweetalert2 ('error', data.messages);
						refresh();
						// openList();
					}else{
						sweetalert2 ('error', 'Unknown Error');
						refresh()
					}
				}
		 });
	 }

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function refresh() {

		setTimeout(function () {
			location.reload()
		}, 50);
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}


    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 7000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }


</script>