
@extends('layouts.questionnaire')
@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php 
    // $master_var = isset($karyawan->nm_karyawan) ? str_replace(' ', '_', $karyawan->nm_karyawan) : 'master_var';
    $master_var = 'Default';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
@endphp

@section('content')
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'barang_lelang')) }}</h3>          
          <div class="card-tools">
          </div>
        </div>
        <div class="card-body">
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <strong> NIK :</strong> {{ $karyawan->nik_karyawan }} <br>
                <strong> Nama Karyawan :</strong> {{ $karyawan->nm_karyawan }} <br>
                <strong> Bagian :</strong> {{ $karyawan->nm_bagian }} <br>
              </div>

              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
              </div>
              <div class="col-sm-4 invoice-col">
                <img id="avatar" class="editable img-responsive float-right" alt="Avatar" src="https://cdn.pixabay.com/photo/2020/07/14/13/07/icon-5404125_1280.png" width="90px" height="100px">
              </div>
              <!-- /.col -->
            </div>
            <hr>

            <div class="card-body table-responsive p-0">
                <div id="">
                  <div class="row">
                    <div class="col-md-6">
                    <div class="btn-group dropright">
                      <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-align-justify"></i> Kategori / Tag
                      </button>
                      <div class="dropdown-menu" x-placement="right-start" style="position: absolute; transform: translate3d(111px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                        @foreach($alltag as $tag)
                        <a class="dropdown-item" href="{{ url('lelang/penawaran/' . $key . '/search?tag=' . $tag->id_tag) }}"><b>#{{ $tag->name }}</b></a>
                        @endforeach
                      </div>
                    </div> 
                    <br>
                    <br>
                  </div>
                  <div class="col-md-6">
                    <form action="{{url('lelang/penawaran/'. $key .'/search')}}" method="GET">
                    <div class="input-group">
                      <input type="text" class="form-control" name="search" id="search" placeholder="" aria-label="" aria-describedby="basic-addon1">
                      <div class="input-group-append">
                        <button class="btn btn-success" type="submit">Cari</button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>

                  <hr>
                  <div class="album py-5 bg-light">
                      
                      <div class="container">
                      
                        <div class="row">
                          @foreach($items as $dat)
                          <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                              <div id="carouselExampleControls{{ $dat['id_lelang'] }}" class="carousel slide" data-ride="carousel">
                                  <div class="carousel-inner">
                                    <div class="carousel-item active">
                                      <img class="d-block w-100" src="{{url('data_lelang/' . $dat['filename'][0])}}" alt="" style="width: 225%; height: 225px">
                                    </div>
                                      @foreach($dat['filename'] as $it)
                                      <div class="carousel-item">
                                        <img class="d-block w-100" src="{{url('data_lelang/' . $it)}}" alt="" style="width: 225%; height: 225px">
                                      </div>
                                      @endforeach
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselExampleControls{{ $dat['id_lelang'] }}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleControls{{ $dat['id_lelang'] }}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                              <div class="card-body">
                                <p class="card-text" style="  display: inline-block; white-space: nowrap; width:100%; overflow: hidden; text-overflow: ellipsis; "><b>{{$dat['nama']}}</b> -- {{ $dat['note'] }}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary" onclick="openView('{{ $dat['id_lelang'] }}', '{{ $key }}', '{{ $karyawan->kd_karyawan }}')"><i class="fa fa-shopping-cart"></i>&nbsp;Lihat</button>
                                    @if($dat['status'] != "close")
                                     <button type="button" class="btn btn-sm btn-success">Rp.{{ isset($dat['penawaran']) ? number_format($dat['penawaran'],2,',','.') : number_format($dat['start_harga'],2,',','.')  }}</button>
                                    @else
                                    <button type="button" class="btn btn-sm btn-danger">Rp.{{ isset($dat['penawaran']) ? number_format($dat['penawaran'],2,',','.')  : number_format($dat['start_harga'],2,',','.')  }}</button>
                                    @endif
                                  </div>
                                  <small class="text-muted">{{ date('Y-m-d H:i:s', strtotime($dat['created_lelang'])) }}</small>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                        </div>
                        <br/>
                        Halaman : {{ $items->currentPage() }} <br/>
                        Jumlah Data : {{ $items->total() }} <br/>
                        Data Per Halaman : {{ $items->perPage() }} <br/>
                        <hr>

                        {{ $items->links() }}
                  
  
                      </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  

@endsection

@section('scriptJS')
@include('page/'.$class_link.'/index_js', $data)
  {{-- @include('page/'.$class_link.'/index_js', $data) --}}
@append
