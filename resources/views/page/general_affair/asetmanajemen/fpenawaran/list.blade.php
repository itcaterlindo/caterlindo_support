{{-- <div class="btn-group dropright">
  <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   <i class="fa fa-align-justify"></i> Kategori / Tag
  </button>
  <div class="dropdown-menu" x-placement="right-start" style="position: absolute; transform: translate3d(111px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
    @foreach($alltag as $tag)
     <a class="dropdown-item" href="#"><b>#{{ $tag->name }}</b></a>
    @endforeach
  </div>
</div>
<hr>
<div class="album py-5 bg-light">
    
    <div class="container">
    
      <div class="row">
        @foreach($items as $dat)
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <div id="carouselExampleControls{{ $dat['id_lelang'] }}" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="https://media.karousell.com/media/photos/products/2020/7/13/sepeda_lipat_exotic_merah_1594630672_d0463da5_progressive.jpg" alt="First slide" style="width: 225%; height: 225px">
                    </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="https://apollo-singapore.akamaized.net/v1/files/yctyw3p0trdg1-ID/image;s=850x0" alt="Second slide" style="width: 225%; height: 225px">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="https://media.karousell.com/media/photos/products/2020/7/15/sepeda_lipat_20_inch_siap_paka_1594856278_3a470c51_progressive.jpg" alt="Third slide" style="width: 225%; height: 225px">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls{{ $dat['id_lelang'] }}" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls{{ $dat['id_lelang'] }}" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            <div class="card-body">
              <p class="card-text" style="  display: inline-block; white-space: nowrap; width:100%; overflow: hidden; text-overflow: ellipsis; "><b>{{$dat['nama']}}</b> -- {{ $dat['note'] }}</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary" onclick="openView('{{ $dat['id_lelang'] }}')"><i class="fa fa-shopping-cart"></i>&nbsp;Lihat</button>
                  <button type="button" class="btn btn-sm btn-success">Rp.{{ isset($dat['penawaran']) ? sprintf("%.2f", $dat['penawaran']) : sprintf("%.2f", $dat['start_harga']) }}</button>
                </div>
                <small class="text-muted">{{ date('Y-m-d H:i:s', strtotime($dat['created_lelang'])) }}</small>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <br/>
      Halaman : {{ $items->currentPage() }} <br/>
      Jumlah Data : {{ $items->total() }} <br/>
      Data Per Halaman : {{ $items->perPage() }} <br/>

      {{ $items->links() }}

      <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
      </nav>
    </div>
  </div> --}}