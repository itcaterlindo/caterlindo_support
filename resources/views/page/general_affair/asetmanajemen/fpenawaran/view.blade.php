<section class="content">

    <div class="card card-solid">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <h3 class="d-inline-block d-sm-none">{{ $row[0]['kode_lelang'] }}</h3>
                    <div class="col-12 product-image-thumbs">
                        @foreach($image as $images)
                            <div class="product-image-thumb"><img src="{{url('data_lelang/' . $images->name)}}" alt="Product Image"></div>
                        @endforeach
                    </div> 
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <div id="sliders{{ $row[0]['id_lelang'] }}" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img class="d-block w-100" src="{{url('data_lelang/' . $image[0]->name)}}" alt="" style="width: 225%; height: 500px">
                          </div>
                          @foreach($image as $images)
                            <div class="carousel-item">
                              <img class="d-block w-100" src="{{url('data_lelang/' . $images->name)}}" alt="" style="width: 225%; height: 500px">
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#sliders{{ $row[0]['id_lelang'] }}" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#sliders{{ $row[0]['id_lelang'] }}" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    {{-- <div class="col-12">
                        <img src="{{url('data_lelang/' . $image[0]->name)}}" class="product-image" alt="Product Image">
                    </div>--}}
                    
                </div>
                <div class="col-12 col-sm-6">
                    <h3 class="my-3">{{ $row[0]['kode_lelang'] }} -- {{ $row[0]['nama'] }}</h3>
                    <hr>
                    <div class="card card-secondary">
                        <div class="card-header">
                        <h3 class="card-title">Detail Barang</h3>
                        </div>
                        
                        <div class="card-body">
                            <strong><i class="fa fa-vote-yea mr-1"></i> Nama Barang</strong>
                                <p class="text-muted">{{ $row[0]['kode_lelang'] }} -- {{ $row[0]['nama'] }}</p>
                                    <hr>
                                <strong><i class="far fa-file-alt mr-1"></i> Keterangan/Spesifikasi</strong>
                                <p class="text-muted">{{ $row[0]['note'] }}</p>
                                    <hr>
                                <strong><i class="fa fa-dollar-sign mr-1"></i> Start Harga</strong>
                                <p class="text-muted">Rp.{{ number_format($row[0]['start_harga'],2,',','.') }}</p>
                                
                                    <hr>
                                <strong><i class="fas fa-list-ol mr-1"></i> Jumlah</strong>
                                <p class="text-muted">{{ $row[0]['jumlah'] }} Unit</p>
                                    <hr>
                                <strong><i class="fas fa-yen-sign mr-1"></i> Kelipatan Bit</strong>
                                <p class="text-muted">&nbsp;Rp.{{ number_format($row[0]['kelipatan_bit'],2,',','.') }}</p>
                                    <hr>
                                <strong><i class="fas fa-cog mr-1"></i> Status</strong>
                                <p class="text-muted"><i class="fa fa-clipboard-check"></i>&nbsp;{{ ucwords($row[0]['status']) }}</p>
                                <hr>
                                <strong><i class="fas fa-book mr-1"></i> Tag</strong>
                                <p class="text-muted">
                                    @foreach($tag as $tags)
                                            <span class="tag tag-danger"><button class="btn btn-outline-dark">#{{ $tags->name }} </button></span>
                                    @endforeach
                                </p>
                                <hr>
                                <strong><i class="fa fa-calendar-check mr-1"></i> Tanggal Start</strong>
                                @if($row[0]['tanggal_start'])
                                    <p class="text-muted">{{ date('Y-m-d H:i:s', strtotime($row[0]['tanggal_start'])) }}</p>
                                @else
                                    <p class="text-muted">{{ date('Y-m-d H:i:s', strtotime($row[0]['created_lelang'])) }}</p>
                                @endif
                                <hr>
                                <strong><i class="fa fa-calendar-times mr-1"></i> Tanggal Close</strong>
                                    <p class="text-muted">{{ $row[0]['tanggal_close'] }}</p>

                                @if($row[0]['status'] == "close")
                                <hr>
                                <strong><i class="fa fa-trophy mr-1"></i> Pemenang </strong>
                                    <p class="text-muted">Terjual Kepada "{{ isset($penawaran_tertinggi[0]->nama) ? ucwords($penawaran_tertinggi[0]->nama) : 'Belum Laku.' }}"</p>
                                @endif
                                
                        </div>
                        
                    </div>
                    <div class="bg-gray py-2 px-3 mt-4">
                        <h2 class="mb-0">
                            
                        </h2>
                        <h4 class="mt-0">
                            <small>Penawaran Tertinggi: {{ isset($penawaran_tertinggi[0]->jumlah) ? 'Rp.' . number_format($penawaran_tertinggi[0]->jumlah,2,',','.') : 'Belum Ada Penawaran.' }} </small>
                        </h4>
                    </div>
                    <div class="mt-4">
                        <div class="card card-secondary">
                            <div class="card-body">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><button class="btn btn-success" id="plus"><i class="fa fa-plus"></i></button></span>
                                    </div>
                                    <input type="hidden" name="nama" id="nama" value="{{ $karyawan->nm_karyawan }}">
                                    <input type="hidden" name="bagian" id="bagian" value="{{ $karyawan->kd_bagian }}">
                                    <input type="hidden" name="id_lelang" id="id_lelang" value="{{ $row[0]['id_lelang'] }}">
                                    <input type="text" class="form-control" id="penawaran" name="penawaran" readonly>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><button class="btn btn-warning" id="min"><i class="fa fa-minus"></button></i></span>
                                    </div>
                                </div>
                                <br>
                                @if($row[0]['status'] != "close")
                                <div class="btn btn-primary btn-lg btn-flat" onclick="stored()">
                                    <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                    Ajukan Penawaran
                                </div>     
                                @else
                                <div class="btn btn-secondary btn-lg btn-flat" disabled>
                                    <i class="fas fa-dolly-flatbed fa-lg mr-2"></i>
                                    Close / Terjual Kepada "{{ isset($penawaran_tertinggi[0]->nama) ? ucwords($penawaran_tertinggi[0]->nama) : 'Belum Laku.' }}"
                                </div>
                                @endif
                                
                            </div>
                        </div>
                       
                        

                    </div>
                    <div class="mt-4 product-share">
                        <a href="#" class="text-gray">
                            <i class="fab fa-facebook-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fab fa-twitter-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fas fa-envelope-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fas fa-rss-square fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <nav class="w-100">
                    <div class="nav nav-tabs" id="product-tab" role="tablist">
                       <a class="nav-item nav-link active" id="product-comments-tab" data-toggle="tab" href="#product-comments" role="tab" aria-controls="product-comments" aria-selected="true">Penawaran Lelang</a>
                        <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab" href="#product-rating" role="tab" aria-controls="product-rating" aria-selected="false">Rating</a>
                    </div>
                </nav>
                <div class="tab-content p-3" id="nav-tabContent">
                   <div class="tab-pane fade active show" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab" style="overflow-y: visible; overflow-x: hidden; height:500px;"> 
                       <div class="col-md-12">
                        @if($row[0]['open_coment'] == 'yes')
                            <div class="timeline">
    
                                <div class="time-label">
                                <span class="bg-red">Semua Penawaran</span>
                                </div>
                                
                                @foreach($penawaran as $tawar)
                                    <div style="width: 100%">
                                        <i class="fas fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fas fa-clock"></i> {{ date('Y-m-d H:i:s', strtotime($tawar->created_penawaran)) }} </span>
                                            <h3 class="timeline-header"><a href="#">{{ $tawar->nama }}</a> Mengajukan tawaran</h3>
                                            <div class="timeline-body">
                                                Mengajukan tawaran u/ barang ini sebesar Rp.{{ number_format($tawar->jumlah,2,',','.') }}
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-primary btn-sm">Read more</a>
                                                <a class="btn btn-danger btn-sm">----</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach


                            
                            
                            </div>
                        @else
                            <p>Penawaran di sembunyikan</p>
                        @endif
                   </div>
                   </div>
                   @if($jml_penawaran != 0)
                        <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">Sejauh ini sudah ada {{ $jml_penawaran }} penawaran pada barang ini, 
                            Jika anda berminat. Ajukan penawaran anda sekarang juga!</div>
                   @else
                        <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">Sejauh ini belum ada penawaran pada barang ini, 
                            Jika anda berminat. Ajukan penawaran anda sekarang juga!</div>
                    @endif
                </div>
            </div>
        </div>
    
    </div>
    
    </section>

    <script>
        var max_value = `{{ isset($penawaran_tertinggi[0]->jumlah) ? $penawaran_tertinggi[0]->jumlah : $row[0]['start_harga']  }}`;
        var start_price = `{{ $row[0]['start_harga'] }}`;

        $("#min").hide();
        $('#penawaran').val(max_value);



        $('#plus').click(function (e) { 
            e.preventDefault();
            var first = $('#penawaran').val();

            var countinue = parseInt(first) + parseInt(`{{ $row[0]['kelipatan_bit'] }}`);
            $('#penawaran').val(countinue);
            
            if($('#penawaran').val() >= countinue){
                $("#min").show();
            }
        });

        $('#min').click(function (e) { 
            e.preventDefault();
            var first = $('#penawaran').val();

            var countinue = parseInt(first) - parseInt(`{{ $row[0]['kelipatan_bit'] }}`);
            $('#penawaran').val(countinue);
            
            if($('#penawaran').val() == max_value){
                $("#min").hide();
            }
        });
    </script>