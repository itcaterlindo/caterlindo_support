@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('MutasiJenisAset.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">
		
		<div class="form-group row">
			<label for="idasetmutasijenis_nama" class="col-md-2 col-form-label">Nama</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="asetmutasijenis_nama" id="idasetmutasijenis_nama" placeholder="Nama" value="{{ isset($asetmutasijenis_nama) ? $asetmutasijenis_nama : '' }}" >
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>