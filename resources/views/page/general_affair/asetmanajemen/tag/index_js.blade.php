<script>
        open_table();

        $('#save').click(function (e) { 
            e.preventDefault();
            var items = {tag_name : $('#tag').val()};
            submit(items);
        });

        function submit (form_id) {
            var url = "{{ url('general_affair/asetmanajemen/lelang/tag/add') }}?tag_name="+form_id.tag_name;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function moveTo(div_id) {
            $('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
        }

        function open_table() {
            $('#<?php echo $idBoxContent; ?>').slideUp(function(){
                $.ajax({
                    type: 'GET',
                    url: '{{ url("general_affair/asetmanajemen/lelang/tag/partial_table_main") }}',
                    success: function(html) {
                        $('#{{ $idBoxContent }}').html(html);
                        $('#{{ $idBoxContent }}').slideDown();
                        moveTo('idMainContent');
                    }
                });
            });
        }

        function delete_data(items){
            var url = "{{ url('general_affair/asetmanajemen/lelang/tag/delete') }}?id_tag="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function sweetalert2 (type, title) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });

            // TYPE : success, info, error, warning
            Toast.fire({
                type: type,
                title: title
            })
        }
</script>