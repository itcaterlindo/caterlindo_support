@php
/* --Insert setting property form-- */
$form_id = 'idFormInputPeriode'; 
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">
		
		<div class="form-group row">
			<label for="periode_bulan" class="col-md-2 col-form-label">Bulan</label>
			<div class="col-sm-5 col-xs-12">
				<input type="number" class="form-control form-control-sm" name="periode_bulan" id="periode_bulan" placeholder="Bulan">
			</div>	
		</div>
        <div class="form-group row">
			<label for="periode_tahun" class="col-md-2 col-form-label">Tahun</label>
			<div class="col-sm-5 col-xs-12">
				<input type="numbwr" class="form-control form-control-sm" name="periode_tahun" id="periode_tahun" placeholder="Tahun" >
			</div>	
		</div>
        <div class="form-group row">
			<label for="periode_jenis" class="col-md-2 col-form-label">Periode Jenis</label>
			<div class="col-sm-4 col-xs-12">
				<select name="periode_jenis" class="form-control form-control-sm select2lokasi" id="periode_jenis">
					@if(isset($asetmutasi_lokasidari))
					<option value="{{ $asetmutasi_lokasidari }}" selected>{{ $asetmutasi_lokasidari_nama }}</option>
					@endif
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="periode_nomor_seri" class="col-md-2 col-form-label">Nomor Seri</label>
			<div class="col-sm-5 col-xs-12">
				<input type="numbwr" class="form-control form-control-sm" name="periode_nomor_seri" id="periode_nomor_seri" placeholder="Nomor Seri" >
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitDataPeriode('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>