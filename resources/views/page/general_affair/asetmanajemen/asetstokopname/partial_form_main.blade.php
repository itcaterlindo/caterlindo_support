@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
    <h4 style="position: center">Periode</h4>
    <hr>
	<div class="col-md-12">
		<div class="form-group row">
            <label for="opname_note" class="col-md-2 col-form-label">Nomor Seri</label>
            <div class="col-sm-5 col-xs-12">
                <input type="hidden" id="periode_kd" name="periode_kd">
                <input type="text" class="form-control form-control-sm" name="periode_nomor_seri" id="periode_nomor_seri" placeholder="Nomor Seri" readonly>
            </div>
        </div>
        <hr>
        <div class="form-group row">
			<label for="opname_lokasi" class="col-md-2 col-form-label">Lokasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="opname_lokasi" class="form-control form-control-sm select2lokasi" id="opname_lokasi">
					@if(isset($asetmutasi_lokasidari))
					<option value="{{ $asetmutasi_lokasidari }}" selected>{{ $asetmutasi_lokasidari_nama }}</option>
					@endif
				</select>
			</div>
        </div>
		<div class="form-group row">
			<label for="opname_note" class="col-md-2 col-form-label">Checker</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="checker" id="Checker" placeholder="Checker">
			</div>	
		</div>
		<div class="form-group row">
			<label for="opname_kondisi" class="col-md-2 col-form-label">Kondisi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="opname_kondisi" class="form-control form-control-sm" id="opname_kondisi">
					<option value="">--Pilih Kondisi--</option>
					<option value="baik">Baik</option>
					<option value="kurang baik">Kurang Baik</option>
					<option value="rusak">Rusak</option>
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="opname_note" class="col-md-2 col-form-label">Note</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="opname_note" id="opname_note" placeholder="Note">
			</div>	
		</div>
        <div class="form-group row">
			<label for="opname_barcode" class="col-md-2 col-form-label">Barcode</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="opname_barcode" id="opname_barcode" placeholder="Barcode" >
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>