<script type="text/javascript">

    open_form_main('Add', '');
	
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main_periode();
		
	}); 
	

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function getActiveperiode() {
		$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/periode/active") }}',
				success: function(data) {
					if(data.length != 0){
						$('#periode_kd').val(data.aset_stokopname_periode_kd);
						$('#periode_nomor_seri').val(data.nomor_seri);
					}else{
						$('#periode_kd').val('');
						$('#periode_nomor_seri').val('');
					}
				}
			});
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_table_periode() {
		$('#<?php echo $idBoxContentPeriode; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/periode/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContentPeriode }}').html(html);
					$('#{{ $idBoxContentPeriode }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				getForm('Form '+ sts, html);
				$("#opname_kondisi").select2();
				render_aset();	
				render_jenismutasi();
				render_bagianAset();
				render_lokasi();
				render_asetStatus();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
				if (sts == 'view') {
					$('#idFormInput').find('.form-control').prop('disabled', true);

					// $('#idFormInput').find('.btn').hide();
				}
				getActiveperiode();
				open_table();
			}
		});
	}

	function render_bagianAset() {
		$(".select2bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_aset() {
		$("#idaset_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_jenismutasi() {
		$("#idasetmutasijenis_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetmutasijenisSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_lokasi() {
		$(".select2lokasi").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_jenis_periode() {
		$("#periode_jenis").select2({
			placeholder: '-- Cari Jenis --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetOpnamejenisSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					console.log(params);
					// return {
					// 	paramSearch: params.term
					// };
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetStatus() {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idaset_id').on('select2:select', '#idaset_id', function(e) {
		var data = e.params.data;
		console.log(data);
		$('.select2bagian').append('<option selected value="'+data.bagian_kd+'">'+data.nm_bagian+'</option>');
		$('.select2lokasi').append('<option selected value="'+data.lokasi_id+'">'+data.lokasi_nama+'</option>');
		$('#idasetstatus_id').append('<option selected value="'+data.asetstatus_id+'">'+data.asetstatus_nama+'</option>');
		$('#asetmutasi_qty').val(data.aset_qty);
		$('#idaset_keterangan').val(data.aset_keterangan);
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function getForm(modalTitle, htmlContent){
		$('#{{ $idModalContent }}').html(htmlContent);
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}

	function view_data(id) {
		open_form_main('view', id);
	}

	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			console.log(data);
			$.ajax({
				url: "{{ route('AsetStokname.destroy') }}",
				type: 'DELETE',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'DELETE',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						$('#opname_note').val('');
						$('#opname_barcode').val('');
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_approve(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_approve') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_cancel(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_cancel') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}
	function open_form_main_periode() {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/periode/partial_form_main") }}',
			success: function(html) {
				toggle_modal('Form Add Periode', html);
				render_jenis_periode();
			}
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentPeriode }}').slideUp();
		$('#{{ $idModalContentPeriode }}').html(htmlContent);
		$('#periode_bulan').val(new Date().getMonth() + 1);
		$('#periode_tahun').val(new Date().getFullYear());
		 get_table_periode('');

		$('#{{ $idModalContentPeriode }}').slideDown();
	}

	function get_table_periode(params) {
		open_table_periode();
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('AsetStokname.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200){
					play('success');
					$('.errInput').html('');
					open_table();
					$('#opname_note').val('');
					$('#opname_barcode').val('');
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 420){
					play('disposed');
					sweetalert2 ('warning', data.messages);
					$('#opname_note').val('');
					$('#opname_barcode').val('');
					generateToken (data._token);
				}else if ( data.code == 401){
					play('err');
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					$('#opname_note').val('');
					$('#opname_barcode').val('');
					generateToken (data._token);
				}else if (data.code == 400){
					play('err');
					sweetalert2 ('error', data.messages);
					$('#opname_note').val('');
					$('#opname_barcode').val('');
					generateToken (data._token);
				}else{
					play('same');
					sweetalert2 ('error', data.messages);
					$('#opname_note').val('');
					$('#opname_barcode').val('');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function play(param){
        	var audio = document.getElementById(param);
            audio.play()
        }

	function submitDataPeriode(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('addPeriode.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200){
					$('.errInput').html('');
					open_table_periode();
					$('#periode_nomor_seri').val('');
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					$('#periode_nomor_seri').val('');
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					$('#periode_nomor_seri').val('');
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					$('#periode_nomor_seri').val('');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function action_active(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_active') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table_periode();
						sweetalert2 ('success', data.messages);
						getActiveperiode();
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_non_active(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_non_active') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table_periode();
						sweetalert2 ('success', data.messages);
						getActiveperiode();
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_finish(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/finish') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table_periode();
						sweetalert2 ('success', data.messages);
						getActiveperiode();
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function delete_periode(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/deleteperiode') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table_periode();
						sweetalert2 ('success', data.messages);
						getActiveperiode();
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

</script>