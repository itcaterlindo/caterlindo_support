<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTablePeriode" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:10%; text-align:center;">Nomor Seri</th>
			<th style="width:10%; text-align:center;">Jenis Periode</th>
			<th style="width:10%; text-align:center;">Bulan</th>
			<th style="width:10%; text-align:center;">Tahun</th>
			<th style="width:10%; text-align:center;">Status</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTablePeriode').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "{{ url($class_link.'periode/table_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "nomor_seri", name: "nomor_seri" },
            { data: "name", name: "name" },
            { data: "bulan", name: "bulan" },
            { data: "tahun", name: "tahun" },
			{ data: 'status', 
              render: function(data) { 
                if(data == 'aktif') {
                  return '<button class="btn btn-primary">Aktif</button>' 
                }
                else if(data == 'non-aktif') {
					return '<Button class="btn btn-danger">Non Aktif</Button>' 
				} 
				else{
					return '<Button class="btn btn-success">Finish</Button>' 
				}
              },
			  className: "text-center"
        	},
        ],
		"order":[6, 'desc'],
	});
</script>