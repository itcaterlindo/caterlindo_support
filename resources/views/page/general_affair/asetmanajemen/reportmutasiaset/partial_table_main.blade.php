<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:10%; text-align:center;">Barcode Opname</th>
			<th style="width:10%; text-align:center;">Qrcode Aset</th>
			<th style="width:10%; text-align:center;">Kode Aset</th>
			<th style="width:10%; text-align:center;">Nama</th>
			<th style="width:10%; text-align:center;">Jenis</th>
			<th style="width:10%; text-align:center;">Lokasi Aset</th>
			<th style="width:10%; text-align:center;">Lokasi Opname</th>
            <th style="width:10%; text-align:center;">Kondisi</th>
			<th style="width:10%; text-align:center;">Tanggal Opname</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"dom": 'Bfrtip',
		"responsive" : false,
		"scrollY": "450px",
		"scrollX":        true,
        "scrollCollapse": true,
		"paging": false,
		"buttons": [
            'copy', 'csv', {
                extend: 'excel',
                title: 'Data export Asset dan Inventaris di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
            }, {
                extend: 'print',
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// messageTop: function () {
                //     return `<p style="font-size:22pt">
				// 		Laporan Asset ditemukan<br>
				// 		Periode :  Desember - 2021</p>`;
                // },
				title: `&nbsp;`,
				// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '13pt' )
                        .prepend(`<div class="container" style="margin-left:-10px;"><br>
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 50%;">
												<h2><b>KERTAS KERJA PERHITUNGAN ASET DAN INVENTARIS</b></h2>
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
													<h3 style="margin-top: 13%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p></h3>
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 260%;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: center;">No. Dokumen</th>
															<td>CAT4-FAA-008</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Tanggal Terbit</th>
															<td>29 Jan 2022</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Revisi</th>
															<td>01</td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
											</div>
											</div>`);
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape }',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }}, 
				{
                extend: 'collection',
                text: 'Get Report',
                buttons: [
                    {
                        text: '<i class="fas fa-tablet-alt fa-xs"></i> By Perhitungan',
                        action: function ( e, dt, node, config ) {
                            getByBagian();
                        }
                    }
                ]
            }, 'colvis',
        ],
		"ajax": {
			"url" : "{{ url($class_link.'/get_data') }}",
			"type": "POST",
			"data": {
					"_token" : '{{ csrf_token() }}',
					"periode_nomor_seri": $('#periode_nomor_seri').val(),
					"asetjenis_id": $('#idasetjenis_id').val(),
					"report_lokasi": $('#report_lokasi').val(),
				}
			},
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "barcode", name: "barcode" },
			{ data: "aset_barcode", name: "aset_barcode" },
			{ data: "aset_kode", name: "aset_kode" },
            { data: "aset_nama", name: "aset_nama" },
            { data: "asetjenis_nama", name: "asetjenis_nama" },
            { data: "lokasi_aset", name: "lokasi_aset" },
            { data: "lokasi_opname", name: "lokasi_opname" },
            { data: "kondisi", name: "kondisi" },
            { data: "aset_stokopname_created_at", name: "aset_stokopname_created_at" }
        ],
		"order":[9, 'desc'],
	});
</script>