<script type="text/javascript">
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
    open_form_main('Add', '');
	closeTableReport("idBoxBypreport_opname_aset");
	getDataxxx(data = {"_token" : "{{ csrf_token() }}", 'periode_a' : null, 'periode_b' : null, 'lok_a' : null, 'lok_b' : null})
	var id_cetak = [];
	
	// $(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
	// 	open_form_main('Add', '');
	// });

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function setNomorSeri(hm){
        var val_a = $('#render_periode_A').val();
        var val_b = $('#render_periode_B').val();

    }


	function getCheck(){
		$('#finalReport').on('click', 'input[type="checkbox"]', function() {
			var id = $(this).attr("data-id");
			console.log(this);
			if ($(this).is(':checked')) {
            	id_cetak.push(id);
       		 }else{
				id_cetak = id_cetak.filter(function(elem){
					return elem != id; 
				});
			}
		});   
	}


	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#{{ $idModalContent }}').html(html);
				render_periode_A();
                render_periode_B();
				render_lokasi();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
				//$('#idbtnSubmitidFormInput').attr('disabled', true);
			}
		});
	}

    function render_year() {
        $('#dropdown_year').select2();
        $('#dropdown_mounth').select2();
        $('#dropdown_jenis').select2();
        $('#dropdown_year').each(function() {
            var year = (new Date()).getFullYear();
            var current = year;
            year -= 5;
            for (var i = 0; i < 10; i++) {
            if ((year+i) == current)
                $(this).append('<option selected value="' + (year + i) + '">' + (year + i) + '</option>');
            else
                $(this).append('<option value="' + (year + i) + '">' + (year + i) + '</option>');
            }

            })
    }

	function view_data(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_aset") }}',
			data: {id: id},
			success: function(html) {
				toggle_modal('View', html);
			}
		});
	}

	function view_barcode_pdf(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id},
			success: function(hm) {
				toggle_modal('View', hm);
			}
		});
		// var ba = '{{ url($class_link."/view_barcode_pdf") }}';
		// console.log(ba);
	}

	function render_aset() {
		$("#idaset_parent").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagianAset() {
		$("#idkd_bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_periode_B() {
		$("#render_periode_B").select2();
	}

    function render_periode_A() {
		$("#render_periode_A").select2();
	}

	function render_lokasi() {
		$("#report_lokasi").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});


		$("#report_lokasi2").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetstatus () {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}


    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function submitData(form_id) {
        var val_a = $('#render_periode_A').val();
        var val_b = $('#render_periode_B').val();
		var lok_a = $('#report_lokasi').val();
        var lok_b = $('#report_lokasi2').val();


		var lok_a_text = $('#report_lokasi').text();
        var lok_b_text = $('#report_lokasi2').text();


        var data = {"_token" : "{{ csrf_token() }}", 'periode_a' : val_a, 'periode_b' : val_b, 'lok_a' : lok_a, 'lok_b' : lok_b};
		getDataxxx(data)  // console.log(data);
          
		}

		function getDataxxx(data) { 
			$.ajax({
                type: "POST",
                url: "{{ route('reportmutasiaset.getData') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					console.log(response);
					if(response){
						compare_hasil_stoktake(response);
						//get_all_notfound(response);
						
					
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
		 }

	function compare_hasil_stoktake(param) { 
		var lok_a_text = $('#report_lokasi').text();
        var lok_b_text = $('#report_lokasi2').text();		
		var val_a = $('#render_periode_A').val();
        var val_b = $('#render_periode_B').val();

		var dateee = val_b + '-' + val_a;


		var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
		let date_today = new Date(dateee);
		let lastDay = new Date(date_today.getFullYear(), date_today.getMonth() + 1, 0);
		var cons = lastDay.toLocaleDateString("id-ID", options);

		var table = $('#idTable').dataTable({
				"processing": true,
				"ordering" : true,
				"dom": 'Bfrtip',
				"responsive" : false,
				"bDestroy": true,
				"scrollY": "450px",
				"scrollX":        true,
				"scrollCollapse": true,
				"paging": false,
				"buttons": [
					'copy', 'csv', {
						extend: 'excel',
						title: 'Data export Asset dan Inventaris di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
					}, {
						extend: 'print',
						exportOptions:{
							columns: ':visible'
							
						},
						// messageTop: function () {
						//     return `<p style="font-size:18pt">
						// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
						// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
						// },
						// messageTop: function () {
						//     return `<p style="font-size:22pt">
						// 		Laporan Asset ditemukan<br>
						// 		Periode :  Desember - 2021</p>`; 
						// },
						title: `&nbsp;`,
						// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
						customize: function ( win ) {
							$(win.document.body)
								.css( 'font-size', '13pt' )
								.prepend(`<div class="container" style="margin-left:-10px;"><br>
														<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 107%;">
														<h2><b>SERAH TERIMA ASET DAN INVENTARIS</b></h2>
														</div>
													<br>
													<br>
														<div class="row">
															<div class="col-md-8">
															<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
															</div>
														
															<div class="col-md-4">
															<table class="table" style="margin-left: 185%;">
																<tbody>
																<tr>
																	<th scope="row" style="text-align: center;">No. Dokumen</th>
																	<td>CAT4-FAA-006</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Tanggal Terbit</th>
																	<td>21 AUG 2023</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Revisi</th>
																	<td>01</td>
																</tr>
																</tbody>
															</table>
															</div>
														</div>
													</div>
													</div>`);

								$(win.document.body)
								.css( 'font-size', '13pt' )
								.append(`<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<table style="border-collapse: collapse; margin-left: 5.32pt; width:100%" cellspacing="0">
										<tbody>
										<tr style="height: 13pt;">
										<td style="width: 142pt;">
										<p class="s3" style="padding-left: 8pt; text-indent: 0pt; line-height: 11pt; text-align: left;">Pihak yang menyerahkan</p>
										</td>
										<td style="width: 56pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 149pt;">
										<p class="s3" style="padding-left: 21pt; text-indent: 0pt; line-height: 11pt; text-align: left;">Pihak yang menerima</p>
										</td>
										<td style="width: 64pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 142pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 71pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 141pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										</tr>
										<tr style="height: 44pt;">
										<td style="width: 142pt;">
										<p class="s4" style="padding-left: 5pt; text-indent: 0pt; line-height: 8pt; text-align: left;">Tgl: `+cons+`</p>
										</td>
										<td style="width: 56pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 149pt;">
										<p class="s4" style="padding-left: 5pt; text-indent: 0pt; line-height: 8pt; text-align: left;">Tgl: `+cons+`</p>
										</td>
										<td style="width: 64pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 142pt;">
										<p class="s4" style="padding-left: 5pt; text-indent: 0pt; line-height: 8pt; text-align: left;">Tgl: `+cons+`</p>
										</td>
										<td style="width: 71pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 141pt;">
										<p class="s4" style="padding-left: 5pt; text-indent: 0pt; line-height: 8pt; text-align: left;">Tgl: `+cons+`</p>
										</td>
										</tr>
										<tr style="height: 46pt;">
										<td style="width: 142pt; border-bottom-style: solid; border-bottom-width: 1pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<p class="s4" style="padding-left: 31pt; text-indent: 0pt; text-align: left;"></p>
										</td>
										<td style="width: 56pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 149pt; border-bottom-style: solid; border-bottom-width: 1pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<p class="s4" style="padding-left: 35pt; text-indent: 0pt; text-align: left;"></p>
										</td>
										<td style="width: 64pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 142pt; border-bottom-style: solid; border-bottom-width: 1pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<p class="s4" style="padding-left: 31pt; text-indent: 0pt; text-align: left;"></p>
										</td>
										<td style="width: 71pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 141pt; border-bottom-style: solid; border-bottom-width: 1pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										<p class="s4" style="padding-left: 31pt; text-indent: 0pt; text-align: left;"></p>
										</td>
										</tr>
										<tr style="height: 13pt;">
										<td style="width: 142pt; border-top-style: solid; border-top-width: 1pt;">
										<p class="s3" style="padding-left: 48pt; padding-right: 48pt; text-indent: 0pt; line-height: 12pt; text-align: center;">PIC `+ lok_a_text +`</p>
										</td>
										<td style="width: 56pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 149pt; border-top-style: solid; border-top-width: 1pt;">
										<p class="s3" style="padding-left: 48pt; padding-right: 48pt; text-indent: 0pt; line-height: 12pt; text-align: center;">PIC `+ lok_b_text +`</p>
										</td>
										<td style="width: 64pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 142pt; border-top-style: solid; border-top-width: 1pt;">
										<p class="s3" style="padding-left: 48pt; padding-right: 48pt; text-indent: 0pt; line-height: 12pt; text-align: center;">HRD/GA</p>
										</td>
										<td style="width: 71pt;">
										<p style="text-indent: 0pt; text-align: left;">&nbsp;</p>
										</td>
										<td style="width: 141pt; border-top-style: solid; border-top-width: 1pt;">
										<p class="s3" style="padding-left: 39pt; text-indent: 0pt; line-height: 12pt; text-align: left;">Head of FAA</p>
										</td>
										</tr>
										</tbody>
										</table>`);
		
							$(win.document.body).find( 'table' )
								.addClass( 'compact' )
								.css( 'font-size', 'inherit' );
								var last = null;
								var current = null;
								var bod = [];
				
								var css = '@page { size: landscape }',
									head = win.document.head || win.document.getElementsByTagName('head')[0],
									style = win.document.createElement('style');
				
								style.type = 'text/css';
								style.media = 'print';
				
								if (style.styleSheet)
								{
									style.styleSheet.cssText = css;
								}
								else
								{
									style.appendChild(win.document.createTextNode(css));
								}
				
								head.appendChild(style);
						}}, 
						{
						extend: 'collection',
						text: 'Get Report',
						buttons: [
							{
								text: '<i class="fas fa-tablet-alt fa-xs"></i> By Perhitungan',
								action: function ( e, dt, node, config ) {
									getByBagian();
								}
							}
						]
					}, 'colvis',
				],
				"data": param.datax,
				"language" : {
					"lengthMenu" : "Tampilkan _MENU_ data",
					"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
					"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty" : "Tidak ada data yang ditampilkan",
					"search" : "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing":     "Sedang Memproses...",
					"paginate": {
						"first":      '<span class="fas fa-fast-backward"></span>',
						"last":       '<span class="fas fa-fast-forward"></span>',
						"next":       '<span class="fas fa-forward"></span>',
						"previous":   '<span class="fas fa-backward"></span>'
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: "aset_nama", name: "aset_nama" },
					{ data: "asetmutasijenis_nama", name: "asetmutasijenis_nama" },
					{ data: "lokasi_dari", name: "lokasi_dari" },
					{ data: "lokasi_ke", name: "lokasi_ke" },
					{ data: "aset_barcode", name: "aset_barcode" },
					
					{ data: "aset_barcode", name: "aset_barcode" },
					{ data: "aset_tglperolehan", name: "aset_tglperolehan" },
					{ data: "asetmutasi_tanggal", name: "asetmutasi_tanggal" },
					{ data: "asetstatus_nama", name: "asetstatus_nama" },
					{ data: "asetmutasi_keterangan", name: "asetmutasi_keterangan" }
				],
			});
	 }

	function closeTableReport(param){
		var x = document.getElementById(param);
		x.classList.add("hide");

	}

	function openTableReport(param){
		var x = document.getElementById(param);
		x.classList.remove("hide");
	}
	
	function openFinalReport(param){
		closeTableReport('idModalContentFinalReportDatareport_opname_aset');
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_final_report_main") }}',
			success: function(html) {
				toggle_modal('Cari barang yg ' + param, html)
				getLokasiinPeriode();
			}
		});
		$('#{{ $statusGet }}').val(param);
	}

	function getLokasiinPeriode() { 
		var data = {"_token" : "{{ csrf_token() }}", "nomor_seri" : $('#periode_nomor_seri').val()};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('ReportOpname.setLokasi') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					if(response){
						$.each(response, function( key, value ) {
							$('#report_lokasi_final').append( '<option value="'+value.lokasi_id+'">'+ value.lokasi_nama+'</option>' );
						})
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
			$('#report_lokasi_final').select2();
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentFinalReport }}').slideUp();
		$('#{{ $idModalContentFinalReport }}').html(htmlContent);
		$('#{{ $idModalContentFinalReport }}').slideDown();
		setNomorSeri('final');
		$('#{{ $idModalContentFinalReportData }}').html('');
	}

	function toggle_modal_barcode(modalTitle, htmlContent, id){
		$('#{{ $idModalBarcode }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentBarcode }}').slideUp();
		$('#{{ $idModalContentBarcode }}').html(htmlContent);
		$('#{{ $idModalContentBarcode }}').slideDown();

	}

	function getFinalReport(param) { 
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_report_final_main") }}',
				success: function(html) {
					$('#{{ $idModalContentFinalReportData }}').html(html);
					$('#{{ $idModalContentFinalReportData }}').slideDown();
					moveTo('idMainContent');
					getCheck();
				}
			});
	 }

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}




</script>