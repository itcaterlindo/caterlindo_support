@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
$csrf = csrf_token();
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
    <hr>
	<div class="col-md-12">
        <div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Bulan</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control-sm" id="render_periode_A">
					<option value="1">Jan</option>
					<option value="2">Feb</option>
					<option value="3">Mar</option>
					<option value="4">Apr</option>
					<option value="5">Mei</option>
					<option value="6">Jun</option>
					<option value="7">Jul</option>
					<option value="8">Ags</option>
					<option value="9">Sep</option>
					<option value="10">Okt</option>
					<option value="11">Nov</option>
					<option value="12">Des</option>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Tahun</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control-sm" id="render_periode_B">
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
					<option value="2023">2023</option>
					<option value="2024">2024</option>
					<option value="2025">2025</option>
					<option value="2026">2026</option>
					<option value="2027">2027</option>
					<option value="2028">2028</option>
					<option value="2029">2029</option>
					<option value="2030">2030</option>
					<option value="2031">2031</option>


				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="report_lokasi" class="col-md-2 col-form-label">Lokasi Dari</label>
			<div class="col-sm-4 col-xs-12">
				<select name="report_lokasi" class="form-control form-control-sm" id="report_lokasi">
			
				</select>
			</div>
        </div>
		<div class="form-group row">
			<label for="report_lokasi" class="col-md-2 col-form-label">Lokasi Ke</label>
			<div class="col-sm-4 col-xs-12">
				<select name="report_lokasi" class="form-control form-control-sm" id="report_lokasi2">
				
				</select>
			</div>
        </div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="button" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Compare
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>


