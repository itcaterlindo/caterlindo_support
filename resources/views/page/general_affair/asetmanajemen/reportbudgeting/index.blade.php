@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
    @include('layouts.plugins.datetimepicker')
@endsection
<style>
  #idBoxBypreport_opname_aset {
      opacity: 1;
      transition: opacity 1s;
    }

  #idBoxBypreport_opname_aset.hide {
      opacity: 0;
    }
    .modal { overflow: auto !important; }

button {
  background: none;
}

button:active{
    background:olive;
}

button:focus{
    background:olive;
}
</style>

@php
    $master_var = 'report_opname_aset';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxByp'] = 'idBoxByp'.$master_var;
    $data['idBoxContentByp'] = 'idBoxContentByp'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;
    $data['idModalContentFinalReport'] = 'idModalContentFinalReport'.$master_var;
    $data['idModalContentFinalReportData'] = 'idModalContentFinalReportData'.$master_var;
    $data['statusGet'] = 'statusGet';

    $data['idModalBarcode'] = 'idModalBarcode'.$master_var;
    $data['idModalContentBarcode'] = 'idModalContentBarcode'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
@endphp

@section('content')
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-5">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'Form Filter Report Budgeting Asset Belum PR')) }}</h3>          
        </div>
        <div class="card-body">
            <div id="<?php echo $data['idBoxLoader']; ?>" align="middle">
              {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
            </div>
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
      </div>
    </div>
    <div class="col-7" id="{{ $data['idBoxByp'] }}" class="idBoxByp">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'By Perhitungan')) }}</h3>          
          <div class="card-tools">
            @if (auth()->user()->can('KATEGORI_CREATE')) 
            <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
              <i class="fas fa-plus-circle"></i></button>
            @endif
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
            <div id="<?php echo $data['idBoxLoader']; ?>" align="middle">
              {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
            </div>
            <div id="{{ $data['idBoxContentByp'] }}"></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>



<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title" id="set_name_p_a"></h3>          
          <div class="card-tools">
            @if (auth()->user()->can('KATEGORI_CREATE')) 
            <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
              <i class="fas fa-plus-circle"></i></button>
            @endif
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
            <div id="<?php echo $data['idBoxLoader']; ?>" align="middle">
              {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
            </div>
            <div id="{{ $data['idBoxContent'] }}">
              
                <style type="text/css">
                  td.dt-center { text-align: center; }
                  td.dt-right { text-align: right; }
                  td.dt-left { text-align: left; }
                </style>
                <div class="card-body table-responsive p-0">
                  <table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
                    <thead>
                    <tr>
                      <th style="width:1%; text-align:center;" class="all">No.</th>
                      <th style="width:20%; text-align:center;">Nama Barang / Inventaris & Spesifikasi</th>
                      <th style="width:7%; text-align:center;">Category</th>
                      <th style="width:7%; text-align:center;">Periode</th>
                      <th style="width:3%; text-align:center;">Th</th>
                      <th style="width:7%; text-align:center;">Bagian</th>
                      <th style="width:7%; text-align:center;">Jumlah</th>
                      <th style="width:10%; text-align:center;">Keterangan</th>
                      <th style="width:10%; text-align:center;">PR NO</th>
                      
                    </tr>
                    </thead>
                  </table>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

@endsection

@section('scriptJS')
  @include('page/'.$class_link.'/index_js', $data)
@append

