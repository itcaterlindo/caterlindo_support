<script type="text/javascript">
	open_table();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				toggle_modal('Form '+ sts, html);
				render_aset();
				render_jenismutasi();
				render_bagianAset();
				render_lokasi();
				render_asetStatus();
				selectAsset();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
				if (sts == 'view') {
					$('#idFormInput').find('.form-control').prop('disabled', true);
					$('#idFormInput').find('.btn').hide();
				}
			}
		});
	}

	function selectAsset(){
		$('#idaset_id').change(function () { 
			//alert($(this).val());
		});
	}

	function render_bagianAset() {
		$(".select2bagian").select2({
			placeholder: '-- Cari Opsi --',
			width: '100%',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_aset() {
		$("#idaset_id").select2({
			placeholder: '-- Cari Opsi --',
			width: '100%',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_jenismutasi() {
		$("#idasetmutasijenis_id").select2({
			placeholder: '-- Cari Opsi --',
			width: '100%',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetmutasijenisSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_lokasi() {
		$(".select2lokasi").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			width: '100%',
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetStatus() {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			width: '100%',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idaset_id').on('select2:select', '#idaset_id', function(e) {
		var data = e.params.data;
		console.log(data);
		$('.select2bagian').append('<option selected value="'+data.bagian_kd+'">'+data.nm_bagian+'</option>');
		$('.select2lokasi').append('<option selected value="'+data.lokasi_id+'">'+data.lokasi_nama+'</option>');
		$('#idasetstatus_id').append('<option selected value="'+data.asetstatus_id+'">'+data.asetstatus_nama+'</option>');
		$('#asetmutasi_qty').val(data.aset_qty);
		$('#idaset_keterangan').val(data.aset_keterangan);
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}

	function view_data(id) {
		open_form_main('view', id);
	}

	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ route('AsetMutasi.destroy') }}",
				type: 'DELETE',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'DELETE',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_approve(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_approve') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

	function action_cancel(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ url($class_link.'/action_cancel') }}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"id": id,
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('AsetMutasi.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200){
					$('.errInput').html('');
					toggle_modal('', '');
					open_table();
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

</script>