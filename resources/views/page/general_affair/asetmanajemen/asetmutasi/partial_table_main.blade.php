<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:10%; text-align:center;">Kode Mutasi</th>
			<th style="width:10%; text-align:center;">Jenis</th>
			<th style="width:10%; text-align:center;">Kode Aset</th>
			<th style="width:10%; text-align:center;">Nama Aset</th>
			<th style="width:10%; text-align:center;">Keterangan</th>
			<th style="width:10%; text-align:center;">Status</th>
			<th style="width:10%; text-align:center;">Tgl di buat</th>
			<th style="width:10%; text-align:center;">Tgl di approve</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "{{ url($class_link.'/table_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "asetmutasi_kode", name: "asetmutasi_kode" },
            { data: "asetmutasijenis_nama", name: "asetmutasijenis_nama" },
            { data: "aset_kode", name: "aset_kode" },
            { data: "aset_nama", name: "aset_nama" },
            { data: "asetmutasi_keterangan", name: "asetmutasi_keterangan" },
            { data: "asetmutasi_status", name: "asetmutasi_status", className: "dt-center" },
			{ 
              data: 'asetmutasi_created', 
              render: function(data) { 
				const inputDatetime = '2023-12-11T03:35:10.000000Z';

				// Create a new Date object with the input datetime
				const date = new Date(data);

				// Convert the datetime to local time
				//const localDatetime = date.toLocaleString('en-US', {timeZone: 'Asia/Jakarta'});
				const options = { timeZone: 'Asia/Jakarta', hour12: false, year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };
				const localDatetime = date.toLocaleString('en-US', options);

				return localDatetime
              }
            },
			{ 
              data: 'asetmutasi_updated', 
              render: function(data) { 
				const inputDatetime = '2023-12-11T03:35:10.000000Z';

				// Create a new Date object with the input datetime
				const date = new Date(data);

				// Convert the datetime to local time
				// /const localDatetime = date.toLocaleString('en-US', {timeZone: 'Asia/Jakarta'});
				const options = { timeZone: 'Asia/Jakarta', hour12: false, year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };
				const localDatetime = date.toLocaleString('en-US', options);


				return localDatetime
              }
            },
        ],
		"order":[8, 'desc'],
	});
</script>