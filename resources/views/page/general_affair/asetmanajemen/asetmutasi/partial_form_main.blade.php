@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('AsetMutasi.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idaset_id" class="col-md-2 col-form-label">Tanggal</label>
			<div class="col-sm-3 col-xs-12">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
					</div>
					<input type="text" name="asetmutasi_tanggal" class="form-control form-control-sm datetimepicker" value="{{ isset($asetmutasi_tanggal) ? $asetmutasi_tanggal : date('Y-m-d') }}">
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetmutasijenis_id" class="col-md-2 col-form-label">Mutasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetmutasijenis_id" class="form-control form-control-sm" id="idasetmutasijenis_id">
					@if(isset($asetmutasijenis_id))
					<option value="{{ $asetmutasijenis_id }}" selected>{{ $asetmutasijenis_nama }}</option>
					@endif
				</select>
			</div>	
		</div>
		<hr>

		<div class="form-group row">
			<label for="idaset_id" class="col-md-2 col-form-label">Aset</label>
			<div class="col-sm-6 col-xs-12">
				<select name="aset_id" class="form-control form-control-sm" id="idaset_id">
					@if(isset($aset_id))
					<option value="{{ $aset_id }}" selected>{{ "$aset_kode | $aset_nama" }}</option>
					@endif
				</select>
			</div>
			<label for="idasetmutasi_qty" class="col-md-1 col-form-label">Qty</label>
			<div class="col-sm-2 col-xs-12">
				<input type="number" name="asetmutasi_qty" id="asetmutasi_qty" placeholder="Qty" readonly="readonly" class="form-control form-control-sm" value="{{ isset($asetmutasi_qty) ? $asetmutasi_qty : null }}">
			</div>
		</div>

		<div class="form-group row">
			<label for="idaset_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-9 col-xs-12">
				<textarea name="aset_keterangan" id="idaset_keterangan" readonly="readonly"  class="form-control form-control-sm" rows="2" placeholder="Keterangan">{{ isset($aset_keterangan) ? $aset_keterangan : '' }}</textarea>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idasetstatus_id" class="col-md-2 col-form-label">Aset Status</label>
			<div class="col-sm-6 col-xs-12">
				<select name="asetstatus_id" class="form-control form-control-sm" id="idasetstatus_id">
					@if(isset($asetstatus_id))
					<option value="{{ $asetstatus_id }}" selected>{{ $asetstatus_nama }}</option>
					@endif
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetmutasi_bagiandari" class="col-md-2 col-form-label">Bagian Dari</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetmutasi_bagiandari" class="form-control form-control-sm select2bagian" id="idasetmutasi_bagiandari">
					@if(isset($asetmutasi_bagiandari))
					<option value="{{ $asetmutasi_bagiandari }}" selected>{{ $asetmutasi_bagiandari_nama }}</option>
					@endif
				</select>
			</div>	
			<label for="idasetmutasi_bagianke" class="col-md-1 col-form-label">Bagian Ke</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetmutasi_bagianke" class="form-control form-control-sm select2bagian" id="idasetmutasi_bagianke">
					@if(isset($asetmutasi_bagianke))
					<option value="{{ $asetmutasi_bagianke }}" selected>{{ $asetmutasi_bagianke_nama }}</option>
					@endif
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idasetmutasi_lokasidari" class="col-md-2 col-form-label">Lokasi Dari</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetmutasi_lokasidari" class="form-control form-control-sm select2lokasi" id="idasetmutasi_lokasidari">
					@if(isset($asetmutasi_lokasidari))
					<option value="{{ $asetmutasi_lokasidari }}" selected>{{ $asetmutasi_lokasidari_nama }}</option>
					@endif
				</select>
			</div>	
			<label for="idasetmutasi_lokasike" class="col-md-1 col-form-label">Lokasi Ke</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetmutasi_lokasike" class="form-control form-control-sm select2lokasi" id="idasetmutasi_lokasike">
					@if(isset($asetmutasi_lokasike))
					<option value="{{ $asetmutasi_lokasike }}" selected>{{ $asetmutasi_lokasike_nama }}</option>
					@endif
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idasetmutasi_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-9 col-xs-12">
				<textarea name="asetmutasi_keterangan" id="idasetmutasi_keterangan" class="form-control" rows="2" placeholder="Keterangan">{{ isset($asetmutasi_keterangan) ? $asetmutasi_keterangan : '' }}</textarea>
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>