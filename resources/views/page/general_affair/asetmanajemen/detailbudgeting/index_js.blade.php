<script>
        open_table();
        render_bagianAset();

        var bulan = [];
        var ctg = [];
        var bhm = [];

        $(document).ready(function() {
        var ckbox = $("input[name='ips']");
        var chkId = '';
            $("input[name='ips']").on('click', function() {
                
                if(bulan.includes($(this).val())){
                    bulan = bulan.filter(e => e !== $(this).val());
                }else{
                    bulan.push($(this).val())
                }
                //console.log(bulan);
            });

            //get Ctg
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/getctg') }}";
            var riwayat = ''
            $.ajax({
                type: "GET",
                url: url,
                dataType: "Json",
                success: function (response) {
                    ctg = response.ctg;
                    bhm = response.bhm;
                    if(response.ctg){
                        for(var i of response.ctg) {
                            riwayat += `<option value="`+i.id_category_budgeting+`">`+i.nm_category_budgeting+`</option>`
                        }
                    }
                    $("#ctg_bgd").append(riwayat);

                    $('#ctg_bgd').select2({
                        placeholder: '-- Cari Opsi --',
                    });
                }
            });

            //count total
            $('#harga').keyup(function (e) { 
                if($('#jumlah').val() !== ""){
                    var val = $(this).val() * $('#jumlah').val();
                    $('#totalharga').val(val);
                }
            });

            $('#jumlah').keyup(function (e) { 
                if($('#harga').val() !== ""){
                    var val = $(this).val() * $('#harga').val();
                    $('#totalharga').val(val);
                }
            });
        });
        // $('#save').click(function (e) { 
        //     e.preventDefault();
        //     var items = {bagian_id : $('#bagian_id').val(), th_anggaran : $('#th_anggaran').val(), keterangan : $('#keterangan').val()};
        //     submit(items);
        // });

        function myFunction () {
            //alert('jalan')
            $('#savexx').attr('disabled', true);
            var stts = $('#stts').val();
            var items = {
                "_token": '{{ csrf_token() }}',
                "id_barang_budgeting":  $('#id_barang_budgeting').val(),
                'id_budgeting' : $('#kode_budgeting').val(),
                'nm_barang' : $('#nm_asset').val(),
                'spesifikasi_barang' : $('#spesifikasi').val(),
                'keterangan' : $('#keterangan').val(),
                'satuan' : $('#satuan').val(),
                'jumlah' : $('#jumlah').val(),
                'harga_anggaran' : $('#harga').val(),
                'jumlah_harga' : $('#totalharga').val(),
                'id_category' : $('#ctg_bgd').val(),
                'periode_bulan' : bulan
            }
            //console.log(items)
            if(stts == "edit"){
                var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/update') }}";
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    url: url ,
                    type: "POST",
                    data: JSON.stringify(items),
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data){
                        console.log(data);
                        if (data.code == 200){
                            bulan = []
                            open_table();
                            sweetalert2 ('success', data.messages);
                            open_table();
                            setTimeout(function wait(){
                            // After waiting for five seconds, submit the form.
                            location.reload();
                        }, 2500);
                        }else{
                            bulan = []
                            sweetalert2 ('error', data.messages);
                            $('#savexx').attr('disabled', false);
                        }
                       
                        
                    } 	        
                });
            }else{
                var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/add') }}";
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    url: url ,
                    type: "POST",
                    data: JSON.stringify(items),
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function(data){
                        console.log(data);
                        if (data.code == 200){
                            bulan = []
                            // open_table();
                            sweetalert2 ('success', data.messages);
                            open_table();
                            setTimeout(function wait(){
                            // After waiting for five seconds, submit the form.
                            location.reload();
                        }, 2500);
                        }else{
                            bulan = []
                            sweetalert2 ('error',  data.messages);
                            $('#savexx').attr('disabled', false);
                        }
                        
                    } 	        
                });
            }
        }

        function moveTo(div_id) {
            $('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
        }

        function open_table() {
            $('#nm_asset').val('')
            $('#spesifikasi').val('')
            $('#keterangan').val('')
            $('#jumlah').val('')
            $('#satuan').val('')
            $('#harga').val('')
            $('#totalharga').val('')
            $('input:checkbox').removeAttr('checked');
            $('#<?php echo $idBoxContent; ?>').slideUp(function(){
                $.ajax({
                    type: 'GET',
                    url: '{{ url("general_affair/asetmanajemen/budgeting/detail/partial_table_main") }}',
                    success: function(html) {
                        $('#{{ $idBoxContent }}').html(html);
                        $('#{{ $idBoxContent }}').slideDown();
                        moveTo('idMainContent');
                    }
                });
            });
        }

        function edit_data(params, stts) {
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/edit') }}?id_barang="+params;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    //SSalert(stts)
                   
                    if (stts == "edit_data"){
                        setForm(data);
                        moveTo('idMainContent');
                    }else{
                        setForm(data);
                        hide_form() 
                        //sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }
        function setForm(param) { 
            var d_barang = param.brg_bgd;
            var d_bulan = param.bulan;
            $('#nm_asset').val(d_barang.nm_barang)
            $('#id_barang_budgeting').val(d_barang.id_barang)
            $('#spesifikasi').val(d_barang.spesifikasi_barang)
            $('#keterangan').val(d_barang.keterangan)
            $('#jumlah').val(d_barang.jumlah)
            $('#harga').val(d_barang.harga_anggaran)
            $('#satuan').val(d_barang.satuan)
            $('#totalharga').val(d_barang.jumlah_harga)
            $('#totalharga').val(d_barang.jumlah_harga)
            $('#stts').val('edit')  
            for (let index = 0; index < d_bulan.length; index++) {
                $("input[value='"+d_bulan[index].bulan+"']").prop("checked", true);
                bulan.push(d_bulan[index].bulan.toString())
                
            }
            
         }

         function hide_form() { 
            $('.nm_asset').hide();
            $('.id_barang_budgeting').hide();
            $('.spesifikasi').hide();
            $('.keterangan').hide();
            //$('.jumlah').hide();
            $('.kode_budgeting').hide();
            $('.ctg_bgd').hide();
            //$('#harga').hide();
            //$('.totalharga').hide();
            $('.ips').hide();
          }

        function delete_data(items){
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/delete') }}?id_barang="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function approve_data(items){
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/approve') }}?id_barang="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', 'Disetujui.');
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function reject_data(items){
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/edit') }}?id_barang="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    //SSalert(stts)
                    setForm(data);
                    hide_form_wh_ket()
                } 	        
            });
            // document.getElementsByClassName("alasan").style.display = "block";
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/reject') }}?id_barang="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    // if (data.code == 200){
                    //     // open_table();
                    //     sweetalert2 ('success', data.messages);
                    //     open_table();
                    // }else{
                    //     sweetalert2 ('error', 'Unknown Error');
                    // }
                } 	        
            });
        }

        function hide_form_wh_ket() { 
            $('.nm_asset').hide();
            $('.id_barang_budgeting').hide();
            $('.spesifikasi').hide();
            //$('.keterangan').hide();
            $('.jumlah').hide();
            $('.kode_budgeting').hide();
            $('.ctg_bgd').hide();
            $('.harga').hide();
            $('.totalharga').hide();
            $('.ips').hide();
          }

        function check_data(items){
            var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/check') }}?id_barang="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function render_bagianAset() {
            $("#bagian_id").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0,
                ajax: {
                    url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
                    dataType: 'json',
                    headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
                    delay: 250,
                    data: function (params) {
                        return {
                            paramSearch: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#th_anggaran").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0
            });
	    }

        function sweetalert2 (type, title) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });

            // TYPE : success, info, error, warning
            Toast.fire({
                type: type,
                title: title
            })
        }
</script>