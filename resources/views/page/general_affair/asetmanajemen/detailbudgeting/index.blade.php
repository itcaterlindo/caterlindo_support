@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php
    $master_var = 'detail_budgeting';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
@endphp

@section('content')
<div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
        </div>
        <div class="card-body">
            <form>
            <div class="form-group row kode_budgeting">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Kode Budgeting</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="kode_budgeting" name="nm_asset" placeholder="Kode Budgeting" value="{{ $kode }}" readonly>
                  <input type="hidden" id="id_barang_budgeting">
                  <input type="hidden" id="stts">
                </div>
              </div>
            <div class="form-group row nm_asset">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Asset</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nm_asset" name="nm_asset" placeholder="Nama Asset">
                </div>
              </div>
              <div class="form-group row spesifikasi">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Spesifikasi</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="spesifikasi" name="spesifikasi" placeholder="Spesifikasi">
                </div>
              </div>
              <div class="form-group row keterangan">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
                </div>
              </div>
              <div class="form-group row ctg_bgd">
              <label for="opname_lokasi" class="col-md-2 col-form-label">Category Barang</label>
              <div class="col-sm-4 col-xs-12">
                <select name="ctg_bgd" class="form-control form-control-sm select2" id="ctg_bgd">
                  <option value="">'-- Cari Opsi --'</option>
                </select>
              </div>
             </div>
              <div class="form-group row jumlah">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Jumlah</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah">
                </div>
              </div>
              <div class="form-group row satuan">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Satuan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan">
                </div>
              </div>
              <div class="form-group row harga">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Harga</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga">
                </div>
              </div>

              <div class="form-group row totalharga">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Total Harga</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="totalharga" name="totalharga" placeholder="0" readonly>
                </div>
              </div>
            

            <div class="form-group row ips">
              <label for="opname_lokasi" class="col-md-2 col-form-label">Periode Bulan</label>
              <div class="col-sm-4 col-xs-12">
              <div class="form-check">
              <input class="form-check-input" name="ips" type="checkbox" value="1">Jan</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="2">Feb</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="3">Mar</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="4">Apr</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="5">Mei</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="6">Jun</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="7">Jul</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="8">Ags</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="9">Sep</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="10">Okt</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="11">Nov</input><br>
              <input class="form-check-input" name="ips" type="checkbox" value="12">Des</input><br>
              </div>
            </div>
          </div>


                          
              <div class="form-group row">
                <div class="col-sm-10 offset-sm-2">
                  <button type="button" onclick="myFunction()" id="savexx" class="btn btn-primary"> Save </button>
                </div>
              </div>
            </form>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
    </div>
</div>

<div class="row" id="{{ $data['idBox'] }}">
  <div class="col-12">
    <!-- Default box -->
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'data_detail_budgeting')) }}</h3>          
        <div class="card-tools">
          @if (auth()->user()->can('KATEGORI_CREATE')) 
          <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
            <i class="fas fa-plus-circle"></i></button>
          @endif
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <div class="card-body">
       
          <div id="{{ $data['idBoxContent'] }}"></div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->
    <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
      <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
  </div>
</div>


<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

@section('scriptJS')
  @include('page/'.$class_link.'/index_js', $data)
@append

