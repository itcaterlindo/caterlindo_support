<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:5%; text-align:center;">Ctgry</th>
			<th style="width:15%; text-align:center;">Nama Barang</th>
			<th style="width:9%; text-align:center;">Prd Bulan</th>
			<th style="width:5%; text-align:center;">Status</th>
			<th style="width:4%; text-align:center;">Jumlah</th>
			<th style="width:4%; text-align:center;">Satuan</th>
			<th style="width:8%; text-align:center;">Kisaran Hrg</th>
			<th style="width:8%; text-align:center;">Total</th>
			<th style="width:15%; text-align:center;">Status PR</th>
			<th style="width:20%; text-align:center;">Keterangan</th>
			<th style="width:10%; text-align:center;">Tgl Updated</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	console.log(ctg)
	console.log(bhm)
	var bgd = $('#kode_budgeting').val();
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"dom": 'Bfrtip',
		"responsive" : false,
		"scrollY": "450px",
		"scrollX":        true,
        "scrollCollapse": true,
		"paging": false,
		"buttons": [
            'copy', 'csv', {
                extend: 'excel',
                title: 'Data export ANGGARAN TRAINING, MARKETING ASET DAN INVENTARIS SERTA BARANG/JASA LAINYA'
            }, {
                extend: 'print',
				exportOptions:{
							columns: ':visible'
							
						},
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// messageTop: function () {
                //     return `<p style="font-size:22pt">
				// 		Laporan Asset ditemukan<br>
				// 		Periode :  Desember - 2021</p>`;
                // },
				title: `&nbsp;`,
				// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '13pt' )
                        .prepend(`<div class="container" style="margin-left:-10px;"><br>
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 60%;">
												<h2><b>ANGGARAN TRAINING, MARKETING ASET DAN INVENTARIS SERTA BARANG/JASA LAINYA</b></h2>
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 260%;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: center;">No. Dokumen</th>
															<td> CAT4-FAA-003</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Tanggal Terbit</th>
															<td>31 Aug 2023</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Revisi</th>
															<td>01</td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
											</div>
											</div>
									<table style="width:100%; border: 1px solid transparent">
										<tr>
											<td>Tanggal:</td>
											<td>Bagian:</td>
											<td>Th anggaran:</td>
										</tr>
									</table>`)
							.append(`<br><br><table style="border-collapse:collapse;margin-left:20.5pt; width:100%" cellspacing="0"><tbody><tr style="height:13pt"><td style="width:415pt"><p style="text-indent: 0pt;text-align: left;"><br></p></td><td style="width:135pt"><p class="s2" style="padding-left: 34pt;text-indent: 0pt;line-height: 11pt;text-align: left;">Head of FAA</p></td><td style="width:42pt"><p style="text-indent: 0pt;text-align: left;"><br></p></td><td style="width:142pt"><p class="s2" style="padding-left: 16pt;text-indent: 0pt;line-height: 11pt;text-align: left;">Operational Manager</p></td></tr><tr style="height:72pt"><td style="width:415pt"><p class="s4" style="padding-left: 2pt;padding-right: 105pt;text-indent: 0pt;text-align: left;">Dokumen ini sebagai acuan dalam melakukan proses Pengadaan dan / atau Inventaris sebagaimana tercantum pada daftar diatas</p></td><td style="width:135pt;border-bottom-style:solid;border-bottom-width:1pt"><p class="s4" style="padding-left: 5pt;text-indent: 0pt;line-height: 8pt;text-align: left;">Tanggal:</p></td><td style="width:42pt"><p style="text-indent: 0pt;text-align: left;"><br></p></td><td style="width:142pt;border-bottom-style:solid;border-bottom-width:1pt"><p class="s4" style="padding-left: 5pt;text-indent: 0pt;line-height: 8pt;text-align: left;">Tanggal :</p></td></tr><tr style="height:9pt"><td style="width:415pt"><p style="text-indent: 0pt;text-align: left;"><br></p></td><td style="width:135pt;border-top-style:solid;border-top-width:1pt"><p class="s4" style="padding-left: 25pt;text-indent: 0pt;line-height: 7pt;text-align: left;">(Tanda Tangan dan Nama)</p></td><td style="width:42pt"><p style="text-indent: 0pt;text-align: left;"><br></p></td><td style="width:142pt;border-top-style:solid;border-top-width:1pt"><p class="s4" style="padding-left: 28pt;text-indent: 0pt;line-height: 7pt;text-align: left;">(Tanda Tangan dan Nama)</p></td></tr></tbody></table>`);
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape }',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }}, 'colvis',
        ],
		"ajax": "{{ url('general_affair/asetmanajemen/budgeting/detail/table_data') }}?kode_budgeting=" + bgd,
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
			{ data: "id_category",
				render: function(data, type, row) {
					var isi = "";
					var newArray = ctg.filter(function (el) {
						if (el.id_category_budgeting == data){
							isi = el.nm_category_budgeting;
							return el.nm_category_budgeting;
						}
					});

					return isi;
						
				}
			},

			{ data: "nm_barang",
			render: function(data, type, row) {

				return data + ' - ' + row.spesifikasi_barang
			}
			},
            { data: "id_barang",
				render: function(data, type, row) {
					var isi = "";
					var newArray = bhm.filter(function (el) {
						if (el.id_barang_budgeting == data){
							return el.bulan;
						}
					});
					let result = newArray.map(a => a.bulan).join(';');
					//console.log(result)
					return result.toString();
						
				}
			},
			{ data: "stts",
 
				render: function(data, type, row) {

					if(data == 'pending') {
							
						return '<span class="badge badge-warning">Pending</span>';
						
					}else if(data == 'Disetujui') {

							return '<span class="badge badge-primary">Disetujui</span>';
							
					}else if(data == 'Check') {

					return '<span class="badge badge-success">Checked</span>';

					}else{
						return '<span class="badge badge-danger">Belum di setujui</span>';
					}
				}
			},
			
            { data: "jumlah", name: "jumlah" },
			{ data: "satuan", name: "satuan" },
			{ data: "harga_anggaran", name: "harga_anggaran" },
            { data: "jumlah_harga", name: "jumlah_harga" },
			{ data: "id_barang",
				render: function(data, type, row) {
					//return data;
					if(row.pr_no == null){
						var items = "Belum di PR!"
					}else{
						var items = row.pr_no + ' - ' + row.prdetail_deskripsi + ' | ' + row.prdetail_qty + ' - ' + row.prdetail_duedate;
					}
					return items;
					// var isi = "";
					// var newArray = bhm.filter(function (el) {
					// 	if (el.id_barang_budgeting == data){
					// 		return el.bulan;
					// 	}
					// });
					// let result = newArray.map(a => a.bulan).join(';');
					// //console.log(result)
					// return result.toString();
						
				}
			},
            { data: "ket1", name: "ket1" },
            { data: "tgl_upd", name: "tgl_upd" },
        ],
	});
</script>