@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" enctype="multipart/form-data" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">
<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="id_aset" class="col-md-2 col-form-label">Aset</label>
			<div class="col-sm-6 col-xs-12">
				<select name="id_aset" class="form-control form-control-sm" id="id_aset">
					@if(!empty($row[0]['id_aset']))         
						<option value="{{ $row[0]['id_aset'] }}">{{ $row[0]['nama'] }}</option>       
					@endif
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="kode" class="col-md-2 col-form-label">Kode Lelang</label>
			<div class="col-sm-2 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="kode" id="kode" placeholder="Kode" value="{{ isset($row[0]['kode_lelang']) ? $row[0]['kode_lelang'] : $kode_lelang }}" readonly >
			</div>
		</div>

		<div class="form-group row">
			<label for="nama_lelang" class="col-md-2 col-form-label">Barang Lelang</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="nama_lelang" id="nama_lelang" placeholder="Nama" value="{{ isset($row[0]['nama']) ? $row[0]['nama'] : '' }}" >
			</div>		
		</div>
        <div class="form-group row">
            <label for="start_harga" class="col-md-2 col-form-label">Harga Perolehan</label>
			<div class="col-sm-3 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="harga_perolehan" id="harga_perolehan" placeholder="Harga Perolehan" value="{{ isset($lokasi_nama) ? $lokasi_nama : '' }} " readonly>
			</div>
        </div>
        <div class="form-group row">
			<label for="start_harga" class="col-md-2 col-form-label">Start Harga</label>
			<div class="col-sm-3 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="start_harga" id="start_harga" placeholder="Start Harga" value="{{ isset($row[0]['start_harga']) ? $row[0]['start_harga'] : '' }}" >
			</div>	

			<label for="jumlah" class="col-md-1 col-form-label">Jumlah</label>
			<div class="col-sm-2 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="jumlah" id="jumlah" placeholder="Jml" value="{{ isset($row[0]['jumlah']) ? $row[0]['jumlah'] : '' }}" >
			</div>	
		</div>
		<div class="form-group row">
			<label for="bit" class="col-md-2 col-form-label">Kelipatan Bit</label>
			<div class="col-sm-6 col-xs-12">
				<input type="number" class="form-control form-control-sm" name="bit" id="bit" placeholder="Bit" value="{{ isset($row[0]['kelipatan_bit']) ? $row[0]['kelipatan_bit'] : '' }}" >
			</div>	
		</div>
        <div class="form-group row">
			<label for="status" class="col-md-2 col-form-label">Status</label>
			<div class="col-sm-6 col-xs-12">
				<select name="status" class="form-control form-control-sm" id="status">
                    <option value="open" selected> Open </option>
					<option value="draft" selected> Draft </option>
					@if($sts == 'Edit')
					<option value="close"> Close </option>
					@endif
				</select>
			</div>	
		</div>
		<div class="form-group row">
			<label for="status" class="col-md-2 col-form-label">Open Comment</label>
			<div class="col-sm-6 col-xs-12">
				<select name="open_coment" class="form-control form-control-sm" id="open_coment">
					<option value=""> -- Cari Opsi -- </option>
					@if(isset($row[0]['open_coment']) && $row[0]['open_coment'] == 'yes')
						<option value="yes" selected> Open </option>
						<option value="no"> Tutup </option>
					@elseif(isset($row[0]['open_coment']) && $row[0]['open_coment'] == 'no')
						<option value="yes"> Open </option>
						<option value="no" selected> Tutup </option>
					@else
						<option value="yes"> Open </option>
						<option value="no"> Tutup </option>
					@endif
				</select>
			</div>	
		</div>
		<div class="form-group row" style="display: none">
			<label class="col-md-2 col-form-label">Tanggal Open:</label>
			<div class="col-sm-6 col-xs-12">
				<div class="input-group date" id="tanggal_open" data-target-input="nearest">
					<input type="text" name="tanggal_open" id="tanggal_open" class="form-control datetimepicker-input" data-target="#tanggal_open">
					<div class="input-group-append" data-target="#tanggal_open" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-md-2 col-form-label">Tanggal Close:</label>
			<div class="col-sm-6 col-xs-12">
				<div class="input-group date" id="tanggal_close" data-target-input="nearest">
					<input type="text" name="tanggal_close" id='tanggal_close' class="form-control tgl_close" value="{{ isset($row[0]['tanggal_close']) ? $row[0]['tanggal_close'] : '' }}" data-target="#tanggal_close">
					<div class="input-group-append" data-target="#tanggal_close" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div>
		</div>
        <div class="form-group row">
			<label for="File" class="col-md-2 col-form-label">File 1</label>
			<div class="col-sm-6 col-xs-12">
				<input required type="file" class="form-control" name="images1" id="images1" placeholder="Images" multiple>
			</div>	
		</div>
        <div class="form-group row">
			<label for="File" class="col-md-2 col-form-label">File 2</label>
			<div class="col-sm-6 col-xs-12">
				<input required type="file" class="form-control" name="images2" id="images2" placeholder="Images" multiple>
			</div>	
		</div>
        <div class="form-group row">
			<label for="File" class="col-md-2 col-form-label">File 3</label>
			<div class="col-sm-6 col-xs-12">
				<input required type="file" class="form-control" name="images3" id="images3" placeholder="Images" multiple>
			</div>	
		</div>
        
		<div class="form-group row">
			<label for="idlokasi_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-8 col-xs-12">
				<textarea name="keterangan" id="keterangan" class="form-control form-control-sm" rows="2" placeholder="Keterangan">{{ isset($row[0]['note']) ? $row[0]['note'] : '' }}</textarea>
			</div>	
		</div>

        <div class="form-group row">
			<label for="idaset_parent" class="col-md-2 col-form-label">Tag</label>
			<div class="col-sm-6 col-xs-12">
				<select name="id_tag[]"  multiple="multiple" class="form-control form-control-sm select2" id="id_tag">
				</select>
			</div>	
		</div>


		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>

<script>
</script>
