<section class="content">

    <div class="card card-solid">
        <div class="card-body">
            <div class="row">
                
                <div class="col-12 col-sm-6">
                    <h3 class="d-inline-block d-sm-none">{{ $row[0]['kode_lelang'] }}</h3>
                    @if($image != 0)
                    <div class="col-12">
                        <img src="{{url('data_lelang/' . $image[0]->name)}}" class="product-image" alt="Product Image">
                    </div>
                    <div class="col-12 product-image-thumbs">
                        @foreach($image as $images)
                            <div class="product-image-thumb"><img src="{{url('data_lelang/' . $images->name)}}" alt="Product Image"></div>
                        @endforeach
                    </div>
                    @endif
                </div>
               
                <div class="col-12 col-sm-6">
                    <h3 class="my-3">{{ $row[0]['kode_lelang'] }} -- {{ $row[0]['nama'] }}</h3>
                    <hr>
                    <div class="card card-secondary">
                        <div class="card-header">
                        <h3 class="card-title">Detail Barang</h3>
                        </div>
                        
                        <div class="card-body">
                            <strong><i class="fa fa-vote-yea mr-1"></i> Nama Barang</strong>
                                <p class="text-muted">{{ $row[0]['kode_lelang'] }} -- {{ $row[0]['nama'] }}</p>
                                    <hr>
                                <strong><i class="far fa-file-alt mr-1"></i> Keterangan/Spesifikasi</strong>
                                <p class="text-muted">{{ $row[0]['note'] }}</p>
                                    <hr>
                                <strong><i class="fa fa-dollar-sign mr-1"></i> Start Harga</strong>
                                <p class="text-muted">Rp.{{ sprintf("%.2f", $row[0]['start_harga']) }}</p>
                                    <hr>
                                <strong><i class="fas fa-list-ol mr-1"></i> Jumlah</strong>
                                <p class="text-muted">{{ $row[0]['jumlah'] }} Unit</p>
                                    <hr>
                                <strong><i class="fas fa-yen-sign mr-1"></i> Kelipatan Bit</strong>
                                <p class="text-muted">&nbsp;Rp.{{ sprintf("%.2f", $row[0]['kelipatan_bit']) }}</p>
                                    <hr>
                                <strong><i class="fas fa-cog mr-1"></i> Status</strong>
                                <p class="text-muted"><i class="fa fa-eye"></i>&nbsp;{{ ucwords($row[0]['status']) }}</p>
                                <hr>
                                <strong><i class="fas fa-cog mr-1"></i> Perlihatkan Penawaran</strong>
                                    @if($row[0]['open_coment'] == 'yes')
                                        <p class="text-muted">&nbsp; Status Penawaran Terbuka</p>
                                    @else
                                        <p class="text-muted">&nbsp; Status Penawaran Tertutup</p>
                                    @endif
                                <hr>
                                <strong><i class="fas fa-book mr-1"></i> Tag</strong>
                                <p class="text-muted">
                                    @foreach($tag as $tags)
                                            <span class="tag tag-danger"><button class="btn btn-outline-dark">#{{ $tags->name }} </button></span>
                                    @endforeach
                                </p>
                                <hr>
                                <strong><i class="fa fa-calendar-check mr-1"></i> Tanggal Start</strong>
                                @if($row[0]['tanggal_start'])
                                    <p class="text-muted">{{ date('Y-m-d H:i:s', strtotime($row[0]['tanggal_start'])) }}</p>
                                @else
                                    <p class="text-muted">{{ date('Y-m-d H:i:s', strtotime($row[0]['created_lelang'])) }}</p>
                                @endif
                                <hr>
                                <strong><i class="fa fa-calendar-times mr-1"></i> Tanggal Close</strong>
                                    <p class="text-muted">{{ $row[0]['tanggal_close'] }}</p>
                                <hr>

                                @if(($row[0]['status']) == 'open')
                                <strong><i class="fa fa-trophy mr-1"></i> Pemenang </strong>
                                    <p class="text-muted">Penawaran Tertinggi Sementara "{{ ucwords($penawaran_tertinggi) }}"</p>
                                @else
                                <strong><i class="fa fa-trophy mr-1"></i> Pemenang </strong>
                                    <p class="text-muted">Terjual Kepada "{{ ucwords($penawaran_tertinggi) }}"</p>
                                @endif
                                
                        </div>
                        
                    </div>
                    <div class="bg-gray py-2 px-3 mt-4">
                        <h2 class="mb-0">
                            
                        </h2>
                        <h4 class="mt-0">
                            <small>Ex Tax: Dicari penawaran tertinggi </small>
                        </h4>
                    </div>
                    <div class="mt-4">
                        <div class="btn btn-primary btn-lg btn-flat" onclick="open_form_main(`Edit`, {{ $row[0]['id_lelang'] }})">
                                <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                Edit Data
                        </div>

                    </div>
                    <div class="mt-4 product-share">
                        <a href="#" class="text-gray">
                            <i class="fab fa-facebook-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fab fa-twitter-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fas fa-envelope-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                            <i class="fas fa-rss-square fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <nav class="w-100">
                    <div class="nav nav-tabs" id="product-tab" role="tablist">
                       <a class="nav-item nav-link active" id="product-comments-tab" data-toggle="tab" href="#product-comments" role="tab" aria-controls="product-comments" aria-selected="true">Apply Lelang</a>
                        <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab" href="#product-rating" role="tab" aria-controls="product-rating" aria-selected="false">Rating</a>
                    </div>
                </nav>
                <div class="tab-content p-3" id="nav-tabContent">
                   <div class="tab-pane fade active show" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab" style="overflow-y: visible; overflow-x: hidden; height:500px; width:352%"> 
                        <div class="timeline">
 
                            <div class="time-label">
                            <span class="bg-red">Semua Penawaran</span>
                            </div>
                            
                            @foreach($penawaran as $tawar)
                                <div style="width: 355%">
                                    <i class="fas fa-envelope bg-blue"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fas fa-clock"></i> {{ date('Y-m-d H:i:s', strtotime($tawar->created_penawaran)) }} </span>
                                        <h3 class="timeline-header"><a href="#">{{ $tawar->nama }}</a> Mengajukan tawaran</h3>
                                        <div class="timeline-body">
                                            Mengajukan tawaran u/ barang ini sebesar Rp.{{ sprintf("%.2f", $tawar->jumlah) }}
                                        </div>
                                        <div class="timeline-footer">
                                            <a class="btn btn-primary btn-sm">Read more</a>
                                            <a class="btn btn-danger btn-sm">----</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                           
                        
                        </div>
                   </div>
                   @if($jml_penawaran != 0)
                        <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">Sejauh ini sudah ada {{ $jml_penawaran }} penawaran pada barang ini, 
                            Jika anda berminat. Ajukan penawaran anda sekarang juga!</div>
                    @else
                        <div class="tab-pane fade" id="product-rating" role="tabpanel" aria-labelledby="product-rating-tab">Sejauh ini belum ada penawaran pada barang ini, 
                            Jika anda berminat. Ajukan penawaran anda sekarang juga!</div>
                    @endif   
                </div>
            </div>
        </div>
    
    </div>
    
    </section>