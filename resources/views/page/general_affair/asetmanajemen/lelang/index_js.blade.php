<script type="text/javascript">
	open_table();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				toggle_modal('Form '+ sts, html);
				render_aset();
                render_tag();
				render_datetimepicker ('.tgl_close');
			}
		});
	}

	function get_view_data(sts, id) {
		$('#{{ $idBoxContentV }}').html('');
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_data") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#{{ $idBoxContentV }}').html(html);
				$('#{{ $idBoxContentV }}').slideDown();
				// moveTo('idMainContentV');
			}
		});
	  }

    function render_aset() {
		$("#id_aset").select2({
			placeholder: '-- Cari Opsi --',
			dropdownAutoWidth : true,
			width : '300px',
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					console.log(params);
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					response = response.filter(function (el) {
						return el.asetstatus_id == '2'
					});

					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_tag() {
		$("#id_tag").select2({
			placeholder: '-- Cari Opsi --',
            multiple: true,
			ajax: {
				url: "{{ url('api/support/aset/v1/allTag') }}",
				dataType: 'json',
                multiple: true,
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}


	$(document).off('select2:select', '#id_aset').on('select2:select', '#id_aset', function(e) {
		var data = e.params.data;
        $('#nama_lelang').val(data.aset_nama);
        $('#harga_perolehan').val(data.aset_nilaiperolehan);
        $('#jumlah').val(data.aset_qty);
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(sts, id){
		// alert('oke mas e')
		open_form_main(sts, id);
	}

	function view_data(sts, id){
		get_view_data(sts, id);
	}


	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ route('lelang.del') }}",
				type: 'GET',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'DELETE',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('Lelang.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200 && data.status == 'edit'){
					$('.errInput').html('');
					$('#{{ $idBoxContentV }}').html('');
					toggle_modal('', '');
					open_table();
					sweetalert2 ('success', data.messages);
				}else if (data.code == 200 && data.status == 'add'){
					$('.errInput').html('');
					$('#{{ $idBoxContentV }}').html('');
					toggle_modal('', '');
					open_table();
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element) {
		$(element).datetimepicker({
			format:'YYYY-MM-DD HH:mm:00',
			useCurrent: false,
			showTodayButton: true,
			showClear: true,
			toolbarPlacement: 'bottom',
			sideBySide: true,
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down",
				previous: "fa fa-chevron-left",
				next: "fa fa-chevron-right",
				today: "fa fa-clock-o",
				clear: "fa fa-trash-o"
			}
		});
	}

</script>