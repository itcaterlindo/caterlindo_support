<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:10%; text-align:center;">Kode Lelang</th>
			<th style="width:10%; text-align:center;">Nama Barang</th>
			<th style="width:10%; text-align:center;">Jml</th>
			<th style="width:10%; text-align:center;">Start Harga</th>
			<th style="width:10%; text-align:center;">P Tertinggi</th>
			<th style="width:10%; text-align:center;">Nm Penawar</th>
			<th style="width:10%; text-align:center;">Status</th>
			<th style="width:10%; text-align:center;">Keterangan</th>
            <th style="width:10%; text-align:center;">Tanggal Created</th>
			<th style="width:10%; text-align:center;">Tanggal Close</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
    function strtrunc(str, max, add){
		add = add || '...';
		return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
	};
	var table = $('#idTable').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": "{{ url($class_link.'/table_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "kode_lelang", name: "kode_lelang" },
            { data: "nama", name: "nama" },
            { data: "jumlah", name: "jumlah" },
			{ 
              data: 'start_harga', 
              render: function(data) { 
				return new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'IDR' }).format(data)
              }
            },
			{ 
              data: 'penawaran_tertinggi', 
              render: function(data) { 
				return '<b>' + new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'IDR' }).format(data) + '<b>'
              }
            },
            { data: "penawar", name: "penawar" },
			{ 
              data: 'status', 
              render: function(data) { 
                if(data == 'close') {
                  return '<img src="https://www.onlygfx.com/wp-content/uploads/2020/05/closed-stamp-2.png" atl img style="width:80px; height:50px"/>' 
                }
                else if(data == 'open'){
					return '<img src="https://cdn-icons-png.flaticon.com/512/176/176080.png" atl img style="width:80px; height:50px"/>' 
                }else{
					return '<img src="https://w7.pngwing.com/pngs/653/502/png-transparent-gray-draft-text-on-black-background-postage-stamps-draft-miscellaneous-angle-white-thumbnail.png" atl img style="width:80px; height:50px"/>' 
				}

              }
            },
            { data: "note", name: "note" },
            { data: "created_lelang", name: "created_lelang" },
			{ data: "tanggal_close", name: "tanggal_close" }
        ],
        'columnDefs': [
		{
                "targets": 8, // your case first column
                "className": "text-center",
                "width": "4%"
        }, {
                "targets": 5, // your case first column
                "className": "text-center",
                "width": "2%"
        },{
                "targets": 4, // your case first column
                "className": "text-center",
                "width": "3%"
        },{
                "targets": 9, // your case first column
                'render': function(data, type, full, meta){
					if(type === 'display'){
						data = strtrunc(data, 30);
					}
					
					return data;
				}
        }],
		"order":[10, 'desc'],
	});
</script>