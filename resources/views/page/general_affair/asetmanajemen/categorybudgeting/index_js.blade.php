<script>
        open_table();
        render_bagianAset();
        
        $('#save').click(function (e) { 
            e.preventDefault();
            var items = {category : $('#category').val()};
            submit(items);
        });

        function submit (form_id) {
            console.log(form_id)
            var url = "{{ url('general_affair/asetmanajemen/budgeting/category/add') }}?category="+form_id.category;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        open_table();
                        sweetalert2 ('success', data.messages);
                        //open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function moveTo(div_id) {
            $('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
        }

        function open_table() {
            $('#<?php echo $idBoxContent; ?>').slideUp(function(){
                $.ajax({
                    type: 'GET',
                    url: '{{ url("general_affair/asetmanajemen/budgeting/category/partial_table_main") }}',
                    success: function(html) {
                        $('#{{ $idBoxContent }}').html(html);
                        $('#{{ $idBoxContent }}').slideDown();
                        moveTo('idMainContent');
                    }
                });
            });
        }

        function delete_data(items){
            var url = "{{ url('general_affair/asetmanajemen/lelang/tag/delete') }}?id_tag="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function render_bagianAset() {
            $("#bagian_id").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0,
                ajax: {
                    url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
                    dataType: 'json',
                    headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
                    delay: 250,
                    data: function (params) {
                        return {
                            paramSearch: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#th_anggaran").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0
            });
	    }

        function sweetalert2 (type, title) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });

            // TYPE : success, info, error, warning
            Toast.fire({
                type: type,
                title: title
            })
        }
</script>