@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
$csrf = csrf_token();
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
    <hr>
	<div class="col-md-12">
		<div class="form-group row">
            <label for="opname_note" class="col-md-2 col-form-label">Nomor Seri</label>
            <div class="col-sm-5 col-xs-12">
                <input type="hidden" id="csrf" name="csrf" value="{{ $csrf }}">
                <input type="text" class="form-control form-control-sm" name="periode_nomor_seri" id="periode_nomor_seri" placeholder="Nomor Seri" readonly>
            </div>
        </div>
        <div class="form-group row">
			<label for="dropdown_jenis" class="col-md-2 col-form-label">Jenis Periode</label>
			<div class="col-sm-4 col-xs-12">
				<select name="dropdown_jenis" class="form-control form-control-sm" id="dropdown_jenis">
                    <option value="" selected>--Pilih Jenis Periode--</option>
                    <option value="1">Kecil</option>
					<option value="2">Besar</option>                    
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="dropdown_mounth" class="col-md-2 col-form-label">Periode Bulan</label>
			<div class="col-sm-4 col-xs-12">
				<select name="dropdown_mounth" class="form-control form-control-sm select2lokasi" id="dropdown_mounth">
                    <option value="" selected>--Pilih Periode Bulan--</option>
					<option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="dropdown_year" class="col-md-2 col-form-label">Periode Tahun</label>
			<div class="col-sm-4 col-xs-12">
				<select name="dropdown_year" class="form-control form-control-sm" id="dropdown_year">
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="report_lokasi" class="col-md-2 col-form-label">Lokasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="report_lokasi" class="form-control form-control-sm" id="report_lokasi">
					@if(isset($asetmutasi_lokasidari))
					<option value="{{ $asetmutasi_lokasidari }}" selected>{{ $asetmutasi_lokasidari_nama }}</option>
					@endif
				</select>
			</div>
        </div>
        <div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Jenis Aset</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control-sm" id="idasetjenis_id">
				</select>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Cari
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>