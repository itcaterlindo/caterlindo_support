<script type="text/javascript">
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
    open_form_main('Add', '');
	closeTableReport("idBoxBypreport_opname_aset");
	var id_cetak = [];
	// $(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
	// 	open_form_main('Add', '');
	// });

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function setNomorSeri(hm){

       var jenis = $('#dropdown_jenis').val();
       var tahun = $('#dropdown_year').val();
       var bulan = $('#dropdown_mounth').val(); 

        if(jenis && tahun && bulan != ''){
            var data = {"_token" : "{{ csrf_token() }}", "jenis" : jenis, "bulan" : bulan, "tahun" : tahun};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('ReportOpname.setNomorseri') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					if(hm == "first"){
						$('#periode_nomor_seri').val('');
						if(response.length != 0){
							$('#idbtnSubmitidFormInput').attr('disabled', false);
							$('#periode_nomor_seri').val(response[0].nomor_seri)
						}else{
							$('#idbtnSubmitidFormInput').attr('disabled', true);
							sweetalert2 ('error', 'Data Periode Tidak ada');
						}
					}else{
						$('#periode_nomor_seri_final').val('');
						if(response.length != 0){
							$('#getFinalReportidFormInputFinalReport').attr('disabled', false);
							$('#periode_nomor_seri_final').val(response[0].nomor_seri)
						}else{
							$('#getFinalReportidFormInputFinalReport').attr('disabled', true);
							sweetalert2 ('error', 'Data Periode Tidak ada');
						}
					}
                }
            });
        }
    }

    $(document).on('select2:select', '#dropdown_jenis', function(e) {
		setNomorSeri('first');
	});
    $(document).on('select2:select', '#dropdown_year', function(e) {
		setNomorSeri('first');
	});
    $(document).on('select2:select', '#dropdown_mounth', function(e) {
		setNomorSeri('first');
	});

	function ActionInFinal(){
		$('#finalReport').on('click', 'input[type="checkbox"]', function() {
			var id = $(this).attr("data-id");
			console.log(this);
			if ($(this).is(':checked')) {
            	id_cetak.push(id);
       		 }else{
				id_cetak = id_cetak.filter(function(elem){
					return elem != id; 
				});
			}
		});   
	}

	function pindah_ke_ctr_area(item){
		var periode = $('#periode_nomor_seri_final').val();
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/sendmutasi") }}',
			data: {id: item, per: periode},
			success: function (data)
				{
					if (data.code == 200){
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else if (data.code == 409){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
		});
	}

	function pindah_sesuai_dtmkn (param) { 
		
	 }

	function cetak_barcode_group(){
		id_cetak = JSON.stringify(id_cetak);
		$.ajax({
			type: 'POST',
			url: '{{ url("general_affair/asetmanajemen/asetdata/call_barcode_pdf") }}',
			data: {id: id_cetak, _token : '{{ csrf_token() }}'},
			success: function(hm) {
				console.log(hm);
				id_cetak = [];
				if($('#finalReport', 'input[type="checkbox"]').checked = true){
					$('#finalReport', 'input[type="checkbox"]').checked = false
				};
				toggle_modal_barcode('View', hm);

			}
		});
	}


	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#{{ $idModalContent }}').html(html);
                render_year();
				render_rmgr();
				render_bagianAset();
				render_aset();
				render_asetJenis();
				render_lokasi();
				render_asetstatus ();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
				$('#idbtnSubmitidFormInput').attr('disabled', true);
			}
		});
	}

    function render_year() {
        $('#dropdown_year').select2();
        $('#dropdown_mounth').select2();
        $('#dropdown_jenis').select2();
        $('#dropdown_year').each(function() {
            var year = (new Date()).getFullYear();
            var current = year;
            year -= 5;
            for (var i = 0; i < 10; i++) {
            if ((year+i) == current)
                $(this).append('<option selected value="' + (year + i) + '">' + (year + i) + '</option>');
            else
                $(this).append('<option value="' + (year + i) + '">' + (year + i) + '</option>');
            }

            })
    }

	function view_data(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_aset") }}',
			data: {id: id},
			success: function(html) {
				toggle_modal('View', html);
			}
		});
	}

	function view_barcode_pdf(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id},
			success: function(hm) {
				toggle_modal('View', hm);
			}
		});
		// var ba = '{{ url($class_link."/view_barcode_pdf") }}';
		// console.log(ba);
	}

	function render_aset() {
		$("#idaset_parent").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagianAset() {
		$("#idkd_bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetJenis() {
		$("#idasetjenis_id").select2({
			placeholder: '-- Cari Jenis Aset --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetjenisSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_lokasi() {
		$("#report_lokasi").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetstatus () {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_rmgr() {
		$("#idrmgr_kd").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url($class_link.'/get_rmgr') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}

	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ route('AsetData.destroy') }}",
				type: 'DELETE',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'DELETE',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table();
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		closeTableReport("idBoxBypreport_opname_aset");
		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);
		open_table();
		$('#idbtnSubmit'+form_id).attr('disabled', false);
		resetButtonSubmit('#idbtnSubmit'+form_id);
	}

	function getByBagian(){
			openTableReport("idBoxBypreport_opname_aset");

			$('#<?php echo $idBoxContentByp; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_report_main") }}',
				success: function(html) {
					$('#{{ $idBoxContentByp }}').html(html);
					$('#{{ $idBoxContentByp }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
			
	}
	function closeTableReport(param){
		var x = document.getElementById(param);
		x.classList.add("hide");

	}

	function openTableReport(param){
		var x = document.getElementById(param);
		x.classList.remove("hide");
	}
	
	function openFinalReport(param){
		closeTableReport('idModalContentFinalReportDatareport_opname_aset');
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_final_report_main") }}',
			success: function(html) {
				toggle_modal('Cari barang yg ' + param, html)
				getLokasiinPeriode();
			}
		});
		$('#{{ $statusGet }}').val(param);
	}

	function getLokasiinPeriode() { 
		var data = {"_token" : "{{ csrf_token() }}", "nomor_seri" : $('#periode_nomor_seri').val()};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('ReportOpname.setLokasi') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					if(response){
						$.each(response, function( key, value ) {
							$('#report_lokasi_final').append( '<option value="'+value.lokasi_id+'">'+ value.lokasi_nama+'</option>' );
						})
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
			$('#report_lokasi_final').select2();
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentFinalReport }}').slideUp();
		$('#{{ $idModalContentFinalReport }}').html(htmlContent);
		$('#{{ $idModalContentFinalReport }}').slideDown();
		setNomorSeri('final');
		$('#{{ $idModalContentFinalReportData }}').html('');
	}

	function toggle_modal_barcode(modalTitle, htmlContent, id){
		$('#{{ $idModalBarcode }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentBarcode }}').slideUp();
		$('#{{ $idModalContentBarcode }}').html(htmlContent);
		$('#{{ $idModalContentBarcode }}').slideDown();

	}

	function getFinalReport(param) { 
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_report_final_main") }}',
				success: function(html) {
					$('#{{ $idModalContentFinalReportData }}').html(html);
					$('#{{ $idModalContentFinalReportData }}').slideDown();
					moveTo('idMainContent');
					ActionInFinal();
				}
			});
	 }

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

</script>