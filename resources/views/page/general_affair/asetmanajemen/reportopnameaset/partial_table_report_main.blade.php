<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="TabReport" class="table table-bordered table-striped table-hover display responsive nowrap" style=" margin: 0 auto;
	width: 100%;
	clear: both;
	border-collapse: collapse;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
            <th style="width:10%; text-align:center;">Lokasi</th>
			<th style="width:10%; text-align:center;">Checker</th>
			<th style="width:10%; text-align:center;">PIC</th>
			<th style="width:10%; text-align:center;">Jml Opname</th>
			<th style="width:10%; text-align:center;">Jml Opname Rusak</th>
			<th style="width:10%; text-align:center;">Jml Actv sys</th>
			<th style="width:10%; text-align:center;">Jml Rsk sys</th>
			<th style="width:10%; text-align:center;">Pindah keluar</th>
			<th style="width:10%; text-align:center;">Rsk tdk dtmkn</th>
			<th style="width:10%; text-align:center;">Tdk dtmkn</th>
			<th style="width:10%; text-align:center;">Perpindahan</th>
			<th style="width:10%; text-align:center;">Jml Baik</th>
			<th style="width:10%; text-align:center;">jml Krg Baik</th>
			<th style="width:10%; text-align:center;">Jml + Rusak</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#TabReport').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"paging": false,
		"scrollY": "222px",
		"scrollX":        true,
        "scrollCollapse": true,
		"dom": 'Bfrtip',
		"buttons": [
            'copy', 'csv', {
                extend: 'excel',
                title: 'Data export dan Inventaris Laporan perhitungan Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
            }, {
                extend: 'print',
				exportOptions:{
					columns: ':visible',
					autoPrint: true,
					orientation: 'landscape'
				},
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Laporan Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// // messageTop: function () {
                // //     return `<p style="font-size:22pt">
				// // 		Laporan perhitungan Asset ditemukan<br>
				// // 		Periode : Desember - 2021</p>`;
                // // },
				title: "&nbsp;",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '12pt' )
						.prepend(`<div class="container" style="margin-left:-10px;">
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 43%; margin-top:2%">
													<h2><b>&nbsp;&nbsp;&nbsp;LAPORAN PERHITUNGAN FISIK ASET DAN INVENTARIS </b> </h2>
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													<img src="https://www.caterlindo.co.id/web/images/logo_header.png" style="width: 35%; height: 80px; margin-top:-50px;" />
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 100%;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: center;">No. Dokumen</th>
															<td>CAT4-FAA-009</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Tanggal Terbit</th>
															<td>29 Jan 2022</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Revisi</th>
															<td>00</td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<h3 style="margin-top: 25%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</h3>
													</div>
													<div class="col-md-4">

													</div>
													<div class="col-md-4">
													<img src="https://i.ibb.co/dGqNbj8/ttd-ori.png"  style="margin-left: 100%;" />
													</div>
											</div>
											</div>`);
 
						$(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );

						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape;}',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }, 
            },,
				{
                extend: 'collection',
                text: 'Get Report',
                buttons: [
                    {
                        text: '<i class="fas fa-ban fa-xs"></i> Lihat Barang tidak di temukan!',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('not-found');
                        }
                    },
					{
                        text: '<i class="fas fa-paste fa-xs"></i> Lihat Barang Perpindahan dr tempat lain',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('perpindahan');
                        }
                    },
					{
                        text: '<i class="fas fa-exchange-alt fa-xs"></i> Lihat Barang Ditemukan di tempat lain',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('pindah_ke_tempat_lain');
                        }
                    },
					{
                        text: '<i class="fas fa-thumbs-up fa-xs"></i> Lihat Barang Baik',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('baik');
                        }
                    },
					{
                        text: '<i class="fas fa-eraser fa-xs"></i> Lihat Barang Kurang baik',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('kurang baik');
                        }
                    },
					{
                        text: '<i class="fas fa-hand-holding fa-xs"></i> Lihat Barang Rusak',
                        action: function ( e, dt, node, config ) {
                            openFinalReport('rusak');
                        }
                    },
                ]
            }, 'colvis'
        ],
		"ajax": {
			"url" : "{{ url($class_link.'/getDataReport') }}",
			"type": "POST",
			"data": {
					"_token" : '{{ csrf_token() }}',
					"periode_nomor_seri": $('#periode_nomor_seri').val()
				}
			},
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
		"columns": [
			{ "width": "2%", "targets" : 0 },
			{ "width": "20%" },
			{ "width": "20%" },
			{ "width": "2%" , "targets" : 3},
			{ "width": "2%" },
			{ "width": "2%" },
			{ "width": "2%" },
			{ "width": "2%" },
			{ "width": "2%" },
			{ "width": "2%" },
			{ "width": "2%" }
		],
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: "checker", name: "checker" },
            { data: "pic", name: "pic" },
            { data: "jumlah_opname", name: "jumlah_opname" },
			{ data: "jumlah_rusak_opname", name: "jumlah_rusak_opname" },
            { data: "jumlah_disystem", name: "jumlah_disystem" },
			{ data: "jumlah_rusak_disystem", name: "jumlah_rusak_disystem" },
			{ data: "pindah_tpt_lain", name: "pindah_tpt_lain" },
			{ data: "rusak_tidak_ditemukan", name: "rusak_tidak_ditemukan" },
            { data: "tidak_ditemukan", name: "tidak_ditemukan" },
			
            { data: "perpindahan", name: "perpindahan" },
            { data: "kondisi_baik", name: "kondisi_baik" },
            { data: "kondisi_kurang_baik", name: "kondisi_kurang_baik" },
            { data: "kondisi_rusak", name: "kondisi_rusak" }
        ],
	});
</script>