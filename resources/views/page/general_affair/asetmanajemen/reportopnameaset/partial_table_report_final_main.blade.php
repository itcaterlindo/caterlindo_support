<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="finalReport" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:10%; text-align:center;">Action</th>
            <th style="width:10%; text-align:center;">Opsi</th>
			<th style="width:10%; text-align:center;">Barcode</th>
			<th style="width:10%; text-align:center;">Nama</th>
			<th style="width:10%; text-align:center;">Jenis</th>
			<th style="width:10%; text-align:center;">Lok Aset</th>
			<th style="width:10%; text-align:center;">Lok Opname</th>
            <th style="width:10%; text-align:center;">Noted</th>
			<th style="width:10%; text-align:center;">Tgl Opname</th>
			<th style="width:10%; text-align:center;">Kondisi</th>
			<th style="width:10%; text-align:center;">Lokasi Ditemukan</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var status = ($('#statusGet').val() == 'not-found') ? ('"TIDAK DITEMUKAN"') : (($('#statusGet').val() == 'perpindahan') ? ('"PERPINDAHAN"') : ('KONDISI "' + $('#statusGet').val()) + '"');
	var table = $('#finalReport').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"dom": 'Bfrtip',
		"scrollY": "500px",
		"scrollX":        true,
        "scrollCollapse": true,
		"bPaginate": false,
		"buttons": [
            'copy', 'csv', {
                extend: 'excel',
                title: 'Data export Asset dan Inventaris ' + status + ' di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
            },  {
                extend: 'print',
				exportOptions:{
					columns: ':visible',
					autoPrint: true,
					orientation: 'landscape'
				},
				title: `&nbsp;`,

                customize: function ( win ) {
                    $(win.document.body)
					.css( 'font-size', '16pt' )
					.prepend(`<div class="container" style="margin-left:-10px;">
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 55%; margin-top:2%">
													<h1 style="text-transform: capitalize;"><b> REPORT ASET DAN INVENTARIS `+ status +`</b> </h1>
												</div>
											<br>
											<br>
												<div class="row">	
													<div class="col-md-8">
													<img src="https://www.caterlindo.co.id/web/images/logo_header.png" style="width: 35%; height: 80px; margin-top:-98px;" />
													<h3 style="margin-top: 13%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p></h3>
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 100%;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: center;">No. Dokumen</th>
															<td>CAT4-FAA-008</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Tanggal Terbit</th>
															<td>29 Jan 2022</td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Revisi</th>
															<td>01</td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
											</div>
											</div>`);
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );

						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape;}',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }
            }, 'colvis', {
                extend: 'collection',
                text: 'Cetak Barcode',
                buttons: [
                    {
                        text: '<i class="fa fa-grip-lines"></i>&nbsp;&nbsp; Cetak All Barcode!',
                        action: function ( e, dt, node, config ) {
                            cetak_barcode_group();
                        }
                    },
                ]
            }
        ],
		"ajax": {
			"url" : "{{ url($class_link.'/get_data_final') }}",
			"type": "POST",
			"data": {
					"_token" : '{{ csrf_token() }}',
					"periode_nomor_seri": $('#periode_nomor_seri').val(),
					"status": $('#statusGet').val(),
					"report_lokasi": $('#report_lokasi_final').val(),
				}
			},
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
			{ data: "pindah", name: "pindah", className: "dt-center", orderable: "false", searchable: "false" },
			{ data: "cetak", name: "cetak", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "aset_barcode", name: "aset_barcode" },
            { data: "aset_nama", name: "aset_nama" },
            { data: "asetjenis_nama", name: "asetjenis_nama" },
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: "lokasi_opname", name: "lokasi_opname" },
            { data: "note", name: "note" },
            { data: "aset_stokopname_created_at", name: "aset_stokopname_created_at" },
            { data: "kondisi", name: "kondisi" },
			{ data: "lokasi_ditemukan", name: "lokasi_ditemukan" },
        ],
		"order":[6, 'desc'],
	});
</script>