@php
/* --Insert setting property form-- */
$form_id_final = 'idFormInputFinalReport'; 
$csrf = csrf_token();
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id_final }}" action="" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}

<div class="row">
    <hr>
	<div class="col-md-12">
		<div class="form-group row">
            <label for="opname_note" class="col-md-2 col-form-label">Nomor Seri</label>
            <div class="col-sm-5 col-xs-12">
                <input type="hidden" id="csrf" name="csrf" value="{{ $csrf }}">
                <input type="text" class="form-control form-control-sm" name="periode_nomor_seri_final" id="periode_nomor_seri_final" placeholder="Nomor Seri" readonly>
            </div>
        </div>
        <div class="form-group row">
			<label for="report_lokasi" class="col-md-2 col-form-label">Lokasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="report_lokasi_final" class="form-control form-control-sm" id="report_lokasi_final">
				</select>
			</div>
        </div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="button" id="getFinalReport{{ $form_id_final }}" onclick="getFinalReport('{{ $form_id_final }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Cari
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>