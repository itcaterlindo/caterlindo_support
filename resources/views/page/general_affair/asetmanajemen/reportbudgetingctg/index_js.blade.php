<script type="text/javascript">
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
    open_form_main('Add', '');
	closeTableReport("idBoxBypreport_opname_aset");
	var id_cetak = [];
	
	// $(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
	// 	open_form_main('Add', '');
	// });

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function getCheck(){
		$('#finalReport').on('click', 'input[type="checkbox"]', function() {
			var id = $(this).attr("data-id");
			console.log(this);
			if ($(this).is(':checked')) {
            	id_cetak.push(id);
       		 }else{
				id_cetak = id_cetak.filter(function(elem){
					return elem != id; 
				});
			}
		});   
	}

	function cetak_barcode_group(){
		id_cetak = JSON.stringify(id_cetak);
		$.ajax({
			type: 'POST',
			url: '{{ url("general_affair/asetmanajemen/asetdata/call_barcode_pdf") }}',
			data: {id: id_cetak, _token : '{{ csrf_token() }}'},
			success: function(hm) {
				console.log(hm);
				id_cetak = [];
				if($('#finalReport', 'input[type="checkbox"]').checked = true){
					$('#finalReport', 'input[type="checkbox"]').checked = false
				};
				toggle_modal_barcode('View', hm);

			}
		});
	}


	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#{{ $idModalContent }}').html(html);
				render_periode_A();
                render_periode_B();
				render_ctg();
				render_lokasi();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
			}
		});
	}

	function render_ctg() { 
		var url = "{{ url('general_affair/asetmanajemen/budgeting/detail/getctg') }}";
            var riwayat = ''
            $.ajax({
                type: "GET",
                url: url,
                dataType: "Json",
                success: function (response) {
                    ctg = response.ctg;
                    bhm = response.bhm;
                    if(response.ctg){
                        for(var i of response.ctg) {
                            riwayat += `<option value="`+i.id_category_budgeting+`">`+i.nm_category_budgeting+`</option>`
                        }
                    }
                    $("#ctg_bgd").append(riwayat);

                    $('#ctg_bgd').select2({
                        placeholder: '-- Cari Opsi --',
                    });
                }
            });
	 }

    function render_year() {
        $('#dropdown_year').select2();
        $('#dropdown_mounth').select2();
        $('#dropdown_jenis').select2();
        $('#dropdown_year').each(function() {
            var year = (new Date()).getFullYear();
            var current = year;
            year -= 5;
            for (var i = 0; i < 10; i++) {
            if ((year+i) == current)
                $(this).append('<option selected value="' + (year + i) + '">' + (year + i) + '</option>');
            else
                $(this).append('<option value="' + (year + i) + '">' + (year + i) + '</option>');
            }

            })
    }

	function view_data(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_aset") }}',
			data: {id: id},
			success: function(html) {
				toggle_modal('View', html);
			}
		});
	}

	function view_barcode_pdf(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id},
			success: function(hm) {
				toggle_modal('View', hm);
			}
		});
		// var ba = '{{ url($class_link."/view_barcode_pdf") }}';
		// console.log(ba);
	}

	function render_aset() {
		$("#idaset_parent").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagianAset() {
		$("#idkd_bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_periode_B() {
		$("#render_periode_B").select2();
	}

    function render_periode_A() {
		$("#render_periode_A").select2();
	}

	function render_lokasi() {
		$("#report_lokasi").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});


		$("#report_lokasi2").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetstatus () {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}


    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
		
	}

	function submitData(form_id) {
        var val_a = $('#ctg_bgd').val();
        var val_b = $('#render_periode_B').val();

		// var lok_a_text = $('#report_lokasi').text();
        // var lok_b_text = $('#report_lokasi2').text();
		getValdata(val_a, val_b);

	}

	function getValdata(val_a, val_b) { 
		var data = {"_token" : "{{ csrf_token() }}", 'ctg_bgd' : val_a, 'tahun' : val_b};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('reportbudgetingctg.getData') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					console.log(response);
					if(response.datax.length != 0){
						ambil_data_report(response, val_a);
						//get_all_notfound(response);
						
					
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
		}

	function ambil_data_report(param, val_a) { 
		var ctg = param.ctg;
		var bhm = param.bhm;
		// var cons = lastDay.toLocaleDateString("id-ID", options);

		var table = $('#idTable').dataTable({
				"processing": true,
				"ordering" : true,
				"dom": 'Bfrtip',
				"responsive" : false,
				"bDestroy": true,
				"scrollY": "450px",
				"scrollX":        true,
				"scrollCollapse": true,
				"paging": false,
				"buttons": [
					'copy', 'csv', {
						extend: 'excel',
						title: 'Data Budgeting by Category'
					}, {
						extend: 'print',
						exportOptions:{
							columns: ':visible'
							
						},
						// messageTop: function () {
						//     return `<p style="font-size:18pt">
						// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
						// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
						// },
						// messageTop: function () {
						//     return `<p style="font-size:22pt">
						// 		Laporan Asset ditemukan<br>
						// 		Periode :  Desember - 2021</p>`; 
						// },
						title: `&nbsp;`,
						// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
						customize: function ( win ) {
							$(win.document.body)
								.css( 'font-size', '13pt' )
								.prepend(`<div class="container" style="margin-left:-10px;"><br>
														<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 150%;">
														<h2><b>Report Budgeting</b></h2>
														</div>
													<br>
													<br>
														<div class="row">
															<div class="col-md-8">
															<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
															</div>
														
															<div class="col-md-4">
															<table class="table" style="margin-left: 255%;">
																<tbody>
																<tr>
																	<th scope="row" style="text-align: center;">No. Dokumen</th>
																	<td>-</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Tanggal Terbit</th>
																	<td>-</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Revisi</th>
																	<td>-</td>
																</tr>
																</tbody>
															</table>
															</div>
														</div>
													</div>
													</div>`);

							$(win.document.body).find( 'table' )
								.addClass( 'compact' )
								.css( 'font-size', 'inherit' );
								var last = null;
								var current = null;
								var bod = [];
				
								var css = '@page { size: landscape }',
									head = win.document.head || win.document.getElementsByTagName('head')[0],
									style = win.document.createElement('style');
				
								style.type = 'text/css';
								style.media = 'print';
				
								if (style.styleSheet)
								{
									style.styleSheet.cssText = css;
								}
								else
								{
									style.appendChild(win.document.createTextNode(css));
								}
				
								head.appendChild(style);	
						}}, 
				],
				"data": param.datax,
				"language" : {
					"lengthMenu" : "Tampilkan _MENU_ data",
					"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
					"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty" : "Tidak ada data yang ditampilkan",
					"search" : "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing":     "Sedang Memproses...",
					"paginate": {
						"first":      '<span class="fas fa-fast-backward"></span>',
						"last":       '<span class="fas fa-fast-forward"></span>',
						"next":       '<span class="fas fa-forward"></span>',
						"previous":   '<span class="fas fa-backward"></span>'
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: "nm_barang",
					render: function(data, type, row) {

						return data + ' - ' + row.spesifikasi_barang
					}},
					{ data: "nm_category_budgeting", name: "nm_category_budgeting" },
					{ data: "id_barang",
						render: function(data, type, row) {
							var isi = "";
							var newArray = bhm.filter(function (el) {
								if (el.id_barang_budgeting == data){
									return el.bulan;
								}
							});
							let result = newArray.map(a => a.bulan).join(';');
							//console.log(result)
							return result.toString();
								
						}
					},
					{ data: "th_anggaran", name: "th_anggaran" },
					{ data: "bagian_nama", name: "bagian_nama" },
					{ data: "harga_anggaran", // mengganti indeks kolom sesuai kebutuhan  
						render: function(data, type, row) {  
							if (type === 'display' || type === 'filter') {  
								return 'Rp ' + data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ',00';  
							}  
							return data;  	
							}  
					},
					//{ data: "harga_anggaran", name: "harga_anggaran" },
					{ data: "jumlah", name: "jumlah" },
					{ data: "keterangan", name: "keterangan"}
				]
			});
	 }

	function closeTableReport(param){
		var x = document.getElementById(param);
		x.classList.add("hide");

	}

	function openTableReport(param){
		var x = document.getElementById(param);
		x.classList.remove("hide");
	}
	
	function openFinalReport(param){
		closeTableReport('idModalContentFinalReportDatareport_opname_aset');
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_final_report_main") }}',
			success: function(html) {
				toggle_modal('Cari barang yg ' + param, html)
				getLokasiinPeriode();
			}
		});
		$('#{{ $statusGet }}').val(param);
	}

	function getLokasiinPeriode() { 
		var data = {"_token" : "{{ csrf_token() }}", "nomor_seri" : $('#periode_nomor_seri').val()};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('ReportOpname.setLokasi') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					if(response){
						$.each(response, function( key, value ) {
							$('#report_lokasi_final').append( '<option value="'+value.lokasi_id+'">'+ value.lokasi_nama+'</option>' );
						})
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
			$('#report_lokasi_final').select2();
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentFinalReport }}').slideUp();
		$('#{{ $idModalContentFinalReport }}').html(htmlContent);
		$('#{{ $idModalContentFinalReport }}').slideDown();
		setNomorSeri('final');
		$('#{{ $idModalContentFinalReportData }}').html('');
	}

	function toggle_modal_barcode(modalTitle, htmlContent, id){
		$('#{{ $idModalBarcode }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentBarcode }}').slideUp();
		$('#{{ $idModalContentBarcode }}').html(htmlContent);
		$('#{{ $idModalContentBarcode }}').slideDown();

	}

	function getFinalReport(param) { 
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_report_final_main") }}',
				success: function(html) {
					$('#{{ $idModalContentFinalReportData }}').html(html);
					$('#{{ $idModalContentFinalReportData }}').slideDown();
					moveTo('idMainContent');
					getCheck();
				}
			});
	 }

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}




</script>