@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
$csrf = csrf_token();
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
    <hr>
	<div class="col-md-12">
        <div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Periode A</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control-sm" id="render_periode_A">
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Periode B</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control-sm" id="render_periode_B">
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Lokasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="report_lokasi" class="form-control form-control-sm" id="report_lokasi">
				</select>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="button" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Compare
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>


