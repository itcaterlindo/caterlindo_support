<script type="text/javascript">
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
    open_form_main('Add', '');
	closeTableReport("idBoxBypreport_opname_aset");
	var id_cetak = [];
	
	// $(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
	// 	open_form_main('Add', '');
	// });

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

    function setNomorSeri(hm){
        var val_a = $('#render_periode_A').val();
        var val_b = $('#render_periode_B').val();


						if(!val_a || !val_b || val_b == val_a){
							$('#idbtnSubmitidFormInput').attr('disabled', true);
						}else{
							$('#idbtnSubmitidFormInput').attr('disabled', false);
						}
	
    }

    $(document).on('select2:select', '#render_periode_A', function(e) {
		setNomorSeri();
	});
    $(document).on('select2:select', '#render_periode_B', function(e) {
		setNomorSeri();
	});

	function getCheck(){
		$('#finalReport').on('click', 'input[type="checkbox"]', function() {
			var id = $(this).attr("data-id");
			console.log(this);
			if ($(this).is(':checked')) {
            	id_cetak.push(id);
       		 }else{
				id_cetak = id_cetak.filter(function(elem){
					return elem != id; 
				});
			}
		});   
	}

	function cetak_barcode_group(){
		id_cetak = JSON.stringify(id_cetak);
		$.ajax({
			type: 'POST',
			url: '{{ url("general_affair/asetmanajemen/asetdata/call_barcode_pdf") }}',
			data: {id: id_cetak, _token : '{{ csrf_token() }}'},
			success: function(hm) {
				console.log(hm);
				id_cetak = [];
				if($('#finalReport', 'input[type="checkbox"]').checked = true){
					$('#finalReport', 'input[type="checkbox"]').checked = false
				};
				toggle_modal_barcode('View', hm);

			}
		});
	}


	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				$('#{{ $idModalContent }}').html(html);
				render_periode_A();
                render_periode_B();
				render_lokasi();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
				$('#idbtnSubmitidFormInput').attr('disabled', true);
			}
		});
	}

    function render_year() {
        $('#dropdown_year').select2();
        $('#dropdown_mounth').select2();
        $('#dropdown_jenis').select2();
        $('#dropdown_year').each(function() {
            var year = (new Date()).getFullYear();
            var current = year;
            year -= 5;
            for (var i = 0; i < 10; i++) {
            if ((year+i) == current)
                $(this).append('<option selected value="' + (year + i) + '">' + (year + i) + '</option>');
            else
                $(this).append('<option value="' + (year + i) + '">' + (year + i) + '</option>');
            }

            })
    }

	function view_data(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_aset") }}',
			data: {id: id},
			success: function(html) {
				toggle_modal('View', html);
			}
		});
	}

	function view_barcode_pdf(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id},
			success: function(hm) {
				toggle_modal('View', hm);
			}
		});
		// var ba = '{{ url($class_link."/view_barcode_pdf") }}';
		// console.log(ba);
	}

	function render_aset() {
		$("#idaset_parent").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagianAset() {
		$("#idkd_bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_periode_B() {
		$("#render_periode_B").select2({
			placeholder: '-- Cari Periode Stokopname Aset --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetOpnamePeriodeSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

    function render_periode_A() {
		$("#render_periode_A").select2({
			placeholder: '-- Cari Periode Stokopname Aset --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetOpnamePeriodeSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_lokasi() {
		$("#report_lokasi").select2({
			placeholder: '-- Cari Lokasi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetstatus () {
		$("#idasetstatus_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetstatusSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}


    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function submitData(form_id) {
        var val_a = $('#render_periode_A').val();
        var val_b = $('#render_periode_B').val();

        var data = {"_token" : "{{ csrf_token() }}", 'periode_a' : val_a, 'periode_b' : val_b};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('CompareStokopname.getData') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					console.log(response);
					if(response){
						compare_hasil_stoktake(response);
						get_all_notfound(response);
						
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
	}

	function get_all_notfound(param) {
		var arrayOne = param.data_not_found_a;
		var arrayTwo = param.data_not_found_b;

		// console.log(arrayOne);
		// console.log(arrayTwo);
		var data_all_notfound = arrayOne.filter(({ aset_id: id1 }) => arrayTwo.some(({ aset_id: id2 }) => id2 === id1));

		var result = data_all_notfound.filter(data => data.lokasi_ditemukan == null);
 
		load_data_all_notfound(result);

		// console.log(data_all_notfound)
	}

	function compare_hasil_stoktake(param) { 
		var arrayOne = param.data_prd_a;
		var arrayTwo = param.data_prd_b;
		
		var data_terlewat_diperiode_A = arrayOne.filter(({ aset_kd: id1 }) => !arrayTwo.some(({ aset_kd: id2 }) => id2 === id1));
		var data_terlewat_diperiode_B = arrayTwo.filter(({ aset_kd: id1 }) => !arrayOne.some(({ aset_kd: id2 }) => id2 === id1));


		load_data_periode_a(data_terlewat_diperiode_A)
		load_data_periode_b(data_terlewat_diperiode_B)
	 }

	function closeTableReport(param){
		var x = document.getElementById(param);
		x.classList.add("hide");

	}

	function openTableReport(param){
		var x = document.getElementById(param);
		x.classList.remove("hide");
	}
	
	function openFinalReport(param){
		closeTableReport('idModalContentFinalReportDatareport_opname_aset');
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_final_report_main") }}',
			success: function(html) {
				toggle_modal('Cari barang yg ' + param, html)
				getLokasiinPeriode();
			}
		});
		$('#{{ $statusGet }}').val(param);
	}

	function getLokasiinPeriode() { 
		var data = {"_token" : "{{ csrf_token() }}", "nomor_seri" : $('#periode_nomor_seri').val()};
            // console.log(data);
            $.ajax({
                type: "POST",
                url: "{{ route('ReportOpname.setLokasi') }}",
                data: data,
                dataType: "JSON",
                success: function (response) {
					if(response){
						$.each(response, function( key, value ) {
							$('#report_lokasi_final').append( '<option value="'+value.lokasi_id+'">'+ value.lokasi_nama+'</option>' );
						})
					}else{
						sweetalert2 ('error', 'Data Periode Tidak ada');
					}
                }
            });
			$('#report_lokasi_final').select2();
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentFinalReport }}').slideUp();
		$('#{{ $idModalContentFinalReport }}').html(htmlContent);
		$('#{{ $idModalContentFinalReport }}').slideDown();
		setNomorSeri('final');
		$('#{{ $idModalContentFinalReportData }}').html('');
	}

	function toggle_modal_barcode(modalTitle, htmlContent, id){
		$('#{{ $idModalBarcode }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContentBarcode }}').slideUp();
		$('#{{ $idModalContentBarcode }}').html(htmlContent);
		$('#{{ $idModalContentBarcode }}').slideDown();

	}

	function getFinalReport(param) { 
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_report_final_main") }}',
				success: function(html) {
					$('#{{ $idModalContentFinalReportData }}').html(html);
					$('#{{ $idModalContentFinalReportData }}').slideDown();
					moveTo('idMainContent');
					getCheck();
				}
			});
	 }

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

	function load_data_periode_a(jsonData) { 
		var hasan = [];
		if($('#report_lokasi').val()){
			if (jsonData.length != 0){
				string = $('#report_lokasi').text().trim();
				var hasan = jsonData.filter(data => data.lokasi_opname == string);
				jsonData = hasan;
			}
		}
		$('#set_name_p_a').append('Data di periode ' + $('#render_periode_A').text() + ', yg tidak ada di periode ' + $('#render_periode_B').text())
				var table = $('#idTable').dataTable({
				"processing": true,
				"ordering" : true,
				"dom": 'Bfrtip',
				"responsive" : false,
				"scrollY": "450px",
				"scrollX":        true,
				"scrollCollapse": true,
				"paging": false,
				"buttons": [
					'copy', 'csv', {
						extend: 'excel',
						title: 'Data export Asset dan Inventaris di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
					}, {
						extend: 'print',
						// messageTop: function () {
						//     return `<p style="font-size:18pt">
						// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
						// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
						// },
						// messageTop: function () {
						//     return `<p style="font-size:22pt">
						// 		Laporan Asset ditemukan<br>
						// 		Periode :  Desember - 2021</p>`;
						// },
						title: `&nbsp;`,
						// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
						customize: function ( win ) {
							$(win.document.body)
								.css( 'font-size', '13pt' )
								.prepend(`<div class="container" style="margin-left:-10px;"><br>
														<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 50%;">
														<h2><b>KERTAS KERJA PERHITUNGAN ASET DAN INVENTARIS</b></h2>
														</div>
													<br>
													<br>
														<div class="row">
															<div class="col-md-8">
															<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
															<h3 style="margin-top: 13%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p></h3>
															</div>
														
															<div class="col-md-4">
															<table class="table" style="margin-left: 260%;">
																<tbody>
																<tr>
																	<th scope="row" style="text-align: center;">No. Dokumen</th>
																	<td>CAT4-FAA-008</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Tanggal Terbit</th>
																	<td>29 Jan 2022</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Revisi</th>
																	<td>01</td>
																</tr>
																</tbody>
															</table>
															</div>
														</div>
													</div>
													</div>`);
		
							$(win.document.body).find( 'table' )
								.addClass( 'compact' )
								.css( 'font-size', 'inherit' );
								var last = null;
								var current = null;
								var bod = [];
				
								var css = '@page { size: landscape }',
									head = win.document.head || win.document.getElementsByTagName('head')[0],
									style = win.document.createElement('style');
				
								style.type = 'text/css';
								style.media = 'print';
				
								if (style.styleSheet)
								{
									style.styleSheet.cssText = css;
								}
								else
								{
									style.appendChild(win.document.createTextNode(css));
								}
				
								head.appendChild(style);
						}}, 
						{
						extend: 'collection',
						text: 'Get Report',
						buttons: [
							{
								text: '<i class="fas fa-tablet-alt fa-xs"></i> By Perhitungan',
								action: function ( e, dt, node, config ) {
									getByBagian();
								}
							}
						]
					}, 'colvis',
				],
				"data": jsonData,
				"language" : {
					"lengthMenu" : "Tampilkan _MENU_ data",
					"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
					"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty" : "Tidak ada data yang ditampilkan",
					"search" : "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing":     "Sedang Memproses...",
					"paginate": {
						"first":      '<span class="fas fa-fast-backward"></span>',
						"last":       '<span class="fas fa-fast-forward"></span>',
						"next":       '<span class="fas fa-forward"></span>',
						"previous":   '<span class="fas fa-backward"></span>'
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: "barcode", name: "barcode" },
					{ data: "aset_barcode", name: "aset_barcode" },
					{ data: "aset_kode", name: "aset_kode" },
					{ data: "aset_nama", name: "aset_nama" },
					{ data: "asetjenis_nama", name: "asetjenis_nama" },
					{ data: "lokasi_aset", name: "lokasi_aset" },
					{ data: "lokasi_opname", name: "lokasi_opname" },
					{ data: "kondisi", name: "kondisi" },
					{ data: "aset_stokopname_created_at", name: "aset_stokopname_created_at" }
				],
				"order":[9, 'desc'],
			});
	 }

	 function load_data_periode_b(jsonData) { 
		var hasan = [];
		if($('#report_lokasi').val()){
			if (jsonData.length != 0){
				string = $('#report_lokasi').text().trim();
				var hasan = jsonData.filter(data => data.lokasi_opname == string);
				jsonData = hasan;
			}
		}
		$('#set_name_p_b').append('Data di periode ' + $('#render_periode_B').text() + ', yg tidak ada di periode ' + $('#render_periode_A').text())
				var table = $('#idTableB').dataTable({
				"processing": true,
				"ordering" : true,
				"dom": 'Bfrtip',
				"responsive" : false,
				"scrollY": "450px",
				"scrollX":        true,
				"scrollCollapse": true,
				"paging": false,
				"buttons": [
					'copy', 'csv', {
						extend: 'excel',
						title: 'Data export Asset dan Inventaris di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
					}, {
						extend: 'print',
						// messageTop: function () {
						//     return `<p style="font-size:18pt">
						// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
						// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
						// },
						// messageTop: function () {
						//     return `<p style="font-size:22pt">
						// 		Laporan Asset ditemukan<br>
						// 		Periode :  Desember - 2021</p>`;
						// },
						title: `&nbsp;`,
						// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
						customize: function ( win ) {
							$(win.document.body)
								.css( 'font-size', '13pt' )
								.prepend(`<div class="container" style="margin-left:-10px;"><br>
														<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 50%;">
														<h2><b>KERTAS KERJA PERHITUNGAN ASET DAN INVENTARIS</b></h2>
														</div>
													<br>
													<br>
														<div class="row">
															<div class="col-md-8">
															<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
															<h3 style="margin-top: 13%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p></h3>
															</div>
														
															<div class="col-md-4">
															<table class="table" style="margin-left: 260%;">
																<tbody>
																<tr>
																	<th scope="row" style="text-align: center;">No. Dokumen</th>
																	<td>CAT4-FAA-008</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Tanggal Terbit</th>
																	<td>29 Jan 2022</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Revisi</th>
																	<td>01</td>
																</tr>
																</tbody>
															</table>
															</div>
														</div>
													</div>
													</div>`);
		
							$(win.document.body).find( 'table' )
								.addClass( 'compact' )
								.css( 'font-size', 'inherit' );
								var last = null;
								var current = null;
								var bod = [];
				
								var css = '@page { size: landscape }',
									head = win.document.head || win.document.getElementsByTagName('head')[0],
									style = win.document.createElement('style');
				
								style.type = 'text/css';
								style.media = 'print';
				
								if (style.styleSheet)
								{
									style.styleSheet.cssText = css;
								}
								else
								{
									style.appendChild(win.document.createTextNode(css));
								}
				
								head.appendChild(style);
						}}, 
						{
						extend: 'collection',
						text: 'Get Report',
						buttons: [
							{
								text: '<i class="fas fa-tablet-alt fa-xs"></i> By Perhitungan',
								action: function ( e, dt, node, config ) {
									getByBagian();
								}
							}
						]
					}, 'colvis',
				],
				"data": jsonData,
				"language" : {
					"lengthMenu" : "Tampilkan _MENU_ data",
					"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
					"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty" : "Tidak ada data yang ditampilkan",
					"search" : "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing":     "Sedang Memproses...",
					"paginate": {
						"first":      '<span class="fas fa-fast-backward"></span>',
						"last":       '<span class="fas fa-fast-forward"></span>',
						"next":       '<span class="fas fa-forward"></span>',
						"previous":   '<span class="fas fa-backward"></span>'
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: "barcode", name: "barcode" },
					{ data: "aset_barcode", name: "aset_barcode" },
					{ data: "aset_kode", name: "aset_kode" },
					{ data: "aset_nama", name: "aset_nama" },
					{ data: "asetjenis_nama", name: "asetjenis_nama" },
					{ data: "lokasi_aset", name: "lokasi_aset" },
					{ data: "lokasi_opname", name: "lokasi_opname" },
					{ data: "kondisi", name: "kondisi" },
					{ data: "aset_stokopname_created_at", name: "aset_stokopname_created_at" }
				],
				"order":[9, 'desc'],
			});
	 }


	function load_data_all_notfound(params) {
		var hasan = [];
		if($('#report_lokasi').val()){
				if (params.length != 0){
					string = $('#report_lokasi').text().trim();
					var hasan = params.filter(data => data.lokasi_nama == string);
					params = hasan;
				}
		}
		$('#set_name_p_NT').append('Data tidak di temulan di periode ' + $('#render_periode_B').text() + ' dan ' + $('#render_periode_A').text())
				var table = $('#idTableNT').dataTable({
				"processing": true,
				"ordering" : true,
				"dom": 'Bfrtip',
				"responsive" : false,
				"scrollY": "450px",
				"scrollX":        true,
				"scrollCollapse": true,
				"paging": false,
				"buttons": [
					'copy', 'csv', {
						extend: 'excel',
						title: 'Data export Asset dan Inventaris di stoktake Periode "' + $('#periode_nomor_seri').val() +'"'
					}, {
						extend: 'print',
						// messageTop: function () {
						//     return `<p style="font-size:18pt">
						// 		Kertas Kerja Perhitungan Fisik Aset dan Inventaris<br>
						// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
						// },
						// messageTop: function () {
						//     return `<p style="font-size:22pt">
						// 		Laporan Asset ditemukan<br>
						// 		Periode :  Desember - 2021</p>`;
						// },
						title: `&nbsp;`,
						// title: 'PT Caterlindo -- Asset di stoktake Periode "' + $('#periode_nomor_seri').val() +'"',
						customize: function ( win ) {
							$(win.document.body)
								.css( 'font-size', '13pt' )
								.prepend(`<div class="container" style="margin-left:-10px;"><br>
														<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 50%;">
														<h2><b>KERTAS KERJA PERHITUNGAN ASET DAN INVENTARIS</b></h2>
														</div>
													<br>
													<br>
														<div class="row">
															<div class="col-md-8">
															<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
															<h3 style="margin-top: 13%; margin-left: 10px;">Periode : `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p></h3>
															</div>
														
															<div class="col-md-4">
															<table class="table" style="margin-left: 260%;">
																<tbody>
																<tr>
																	<th scope="row" style="text-align: center;">No. Dokumen</th>
																	<td>CAT4-FAA-008</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Tanggal Terbit</th>
																	<td>29 Jan 2022</td>
																</tr>
																<tr>
																	<th scope="row" style="text-align: center;">Revisi</th>
																	<td>01</td>
																</tr>
																</tbody>
															</table>
															</div>
														</div>
													</div>
													</div>`);
		
							$(win.document.body).find( 'table' )
								.addClass( 'compact' )
								.css( 'font-size', 'inherit' );
								var last = null;
								var current = null;
								var bod = [];
				
								var css = '@page { size: landscape }',
									head = win.document.head || win.document.getElementsByTagName('head')[0],
									style = win.document.createElement('style');
				
								style.type = 'text/css';
								style.media = 'print';
				
								if (style.styleSheet)
								{
									style.styleSheet.cssText = css;
								}
								else
								{
									style.appendChild(win.document.createTextNode(css));
								}
				
								head.appendChild(style);
						}}, 
						{
						extend: 'collection',
						text: 'Get Report',
						buttons: [
							{
								text: '<i class="fas fa-tablet-alt fa-xs"></i> By Perhitungan',
								action: function ( e, dt, node, config ) {
									getByBagian();
								}
							}
						]
					}, 'colvis',
				],
				"data": params,
				"language" : {
					"lengthMenu" : "Tampilkan _MENU_ data",
					"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
					"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
					"infoFiltered": "",
					"infoEmpty" : "Tidak ada data yang ditampilkan",
					"search" : "Cari :",
					"loadingRecords": "Memuat Data...",
					"processing":     "Sedang Memproses...",
					"paginate": {
						"first":      '<span class="fas fa-fast-backward"></span>',
						"last":       '<span class="fas fa-fast-forward"></span>',
						"next":       '<span class="fas fa-forward"></span>',
						"previous":   '<span class="fas fa-backward"></span>'
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: "aset_barcode", name: "aset_barcode" },
					{ data: "aset_nama", name: "aset_nama" },
					{ data: "asetjenis_nama", name: "asetjenis_nama" },
					{ data: "lokasi_nama", name: "lokasi_nama" },
				],
			});
	}

</script>