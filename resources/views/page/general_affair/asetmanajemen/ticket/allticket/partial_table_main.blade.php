<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
            <th style="width:1%; text-align:center;" class="all">opsi</th>
			<th style="width:10%; text-align:center;">Pembuat</th>
            <th style="width:20%; text-align:center;">Kode Aset</th>
			<th style="width:20%; text-align:center;">Kode Ticket</th>
			<th style="width:20%; text-align:center;">Nama Aset</th>
			<th style="width:10%; text-align:center;">Problem</th>
			<th style="width:20%; text-align:center;">Aset Jenis</th>
			<th style="width:10%; text-align:center;">Lokasi</th>
            <th style="width:10%; text-align:center;">Status Ticket</th>
			<th style="width:10%; text-align:center;">Created</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
	function strtrunc(str, max, add){
		add = add || '...';
		return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
	}

	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": {
			"url" : "{{ url($class_link.'/gettable') }}",
			"type": "POST",
			"data": {
					"_token" : '{{ csrf_token() }}',
					"status": $('#status').val()
				}
			},
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name:"opsi"},
            { data: 'users', 
              render: function(data) {  
                  return '<font color="#4a7b8c" face="Comic Sans MS"><b>'+data+'</b></font>'

              },},
			{ data: "aset_kode", name: "aset_kode" },
            { data: "ticket_kode", name: "ticket_kode" },
            { data: "aset_nama", name: "aset_nama" },
			{ data: "problem", name: "problem" },
            { data: "asetjenis_nama", name: "asetjenis_nama" },
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: 'status_ticket', 
              render: function(data) { 
                if(data == 'delive') {
                  return '<span class="badge badge-primary">Delive</span>' 
                }
                else if(data == 'active') {
                  return '<span class="badge badge-success">Active</span>'
                }
				else if(data == 'cancel') {
                  return '<span class="badge badge-danger">cancel</span>'
                }
				else{
                    return '<span class="badge badge-secondary">Close</span>'
                }

              },},
            { data: "created_ticket", name: "created_ticket" },
        ],
        'columnDefs': [
        {
                "targets": 9, // your case first column
                "className": "text-center",
                "width": "4%"
        },{
                "targets": 2, // your case first column
                "className": "text-center",
                "width": "4%"
        },{
                "targets": 6, // your case first column
                'render': function(data, type, full, meta){
					if(type === 'display'){
						data = strtrunc(data, 30);
					}
					
					return data;
				},
				"width": "13%"
        }, {
                "targets": 5, // your case first column
                "width": "15%"
        }, {
                "targets": 7, // your case first column
                "width": "13%"
        },{
                "targets": 3, // your case first column
                "className": "text-center",
                "width": "8%"
        },{
                "targets": 4, // your case first column
                "className": "text-center",
                "width": "8%"
        },{"targets" : [ 1 ],
				render : function (data, type, row) {
					
					if(row.status_ticket == 'active' || row.status_ticket == 'close'){
					
						var url = '{{ url("general_affair/asetmanajemen/ticket/allticket/detail/") }}/' + row.opsi
						return '<div class="btn-group"><button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">Opsi <span class="sr-only"></span></button> <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="'+ url +'"> <i class="fas fa-eye"></i> Lihat</a></div></div>'
					
					}else{
						
						var url = '{{ url("general_affair/asetmanajemen/ticket/allticket/detail/") }}/' + row.opsi
						return '<div class="btn-group"><button type="button" class="btn btn-sm btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">Opsi <span class="sr-only"></span></button> <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="javascript:void(0)" data-token="csrf_token()" onclick="edit_data('+ row.opsi +')"> <i class="fas fa-trash"></i> Edit</a><a class="dropdown-item" href="'+ url +'"> <i class="fas fa-eye"></i> Lihat</a></div></div>'
					
					}
				}
		}],
		"order":[10, 'desc'],

		
	});
</script>