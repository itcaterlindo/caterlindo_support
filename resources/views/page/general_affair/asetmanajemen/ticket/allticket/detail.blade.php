@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
    @include('layouts.plugins.datetimepicker')
    @include('layouts.plugins.summernote')
@endsection

@php
    $master_var = 'all_Ticket';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
    $data['id'] = $q_id;
    $data['rm_kd'] = $rm_kd;
    $data['title'] = $title;
    $data['aset_id'] = $aset_id;
@endphp

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<div class="container-fluid">
    <div class="row">
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h1 class="card-title">{{ $data['title'] }}</h1>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="form-group">
                <textarea id="summernote" name="editordata"></textarea>
            
            </div>
            <div class="row">
                <div class="col-md-3" id="check">

                </div>
                <div class="col-md-3" id="priority">
                    
                </div>
                <div class="col-md-3" id="type">
                
                </div>
                <div class="col-md-3">
                  <div class="float-right">
                    <div class="btn-group">
                      <button type="button" disabled>Ubah Status</button>
                      <button type="button" class="dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                        @if (auth()->user()->can('IS_ADMIN_TIKET'))
                        <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-1px, 37px, 0px);">
                        <a class="dropdown-item" data-id="close" id="p1_format" href=""><i class="fa fa-power-off"></i>&nbsp; Close</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" data-id="active" id="p2_format" href=""><i class="fa fa-check"></i>&nbsp; Active</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" data-id="delive" id="p3_format" href=""><i class="fa fa-redo"></i>&nbsp; Delive</a>
                        </div>
                        @endif
                      </button>
                    </div>
                    <button type="button" id="send"><i class="far fa-envelope"></i> Send</button>
                  </div>
                </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                @if (auth()->user()->can('IS_ADMIN_TIKET'))
                <div class="form-group row">
                  <label for="idaset_parent" class="col-md-12 col-form-label">Raw Material</label>
                  <div class="col-md-5 col-md-5">
                    <select name="rawmat" class="form-control form-control-sm select2 ppb" id="rawmat">
    
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="idaset_parent" class="col-md-12 col-form-label">Kondisi</label>
                  <div class="col-md-5 col-md-5 bosse">
                    <select name="kondisi" class="form-control form-control-sm select2" id="kondisi">
                        <option value="1">Active</option>
                        <option value="4">Rusak</option>
                    </select>
                  </div>
                  <li class="ntv">Melaporkan bahwa aset rusak!</li>
                </div>
                <div class="form-group row is_special">
                  <label for="idaset_parent" class="col-md-12 col-form-label">Nama Material</label>
                  <div class="col-md-5 col-md-5">
                    <input type="text" class="form-control" id="nm_rm" placeholder="Nama Material">
                  </div>
                  <hr>
                  &nbsp;&nbsp;&nbsp;<button type="button" id="submit_rm" class="btn btn-primary">Save</button>
                </div>
                @endif
              </div>

              <div class="col-md-5">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Material</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody id="add">
                  </tbody>
                </table>
              </div>
      
            </div>
          </div>
           
          
          <!-- /.card-body -->
          <div class="card-footer" id="options">
            
         
          </div>
          <!-- /.card-footer -->
        </div>
        <div class="timeline" id="add_timeline">
            <!-- timeline time label -->

          
          </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
      <div class="col-md-3">

        <!-- Profile Image -->
        <div id="detail_ticket">

        </div>
        <!-- /.card -->

        <!-- About Me Box -->
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Riwayat Ticket</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0" style="display: block;">
              <ul class="nav nav-pills flex-column" id="ul_riwayat">
                
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
        <!-- /.card -->
      </div>
    </div>
    <!-- /.row -->
  </div>
@endsection

@section('scriptJS')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
   
    $(document).ready(function () {
      var rm = [];
      $(".is_special").css("display", "none");

        $('#summernote').summernote({
            placeholder: 'Comment Disini..',
            tabsize: 2,
            height: 250
        });

       
        $("#rawmat").val("{{ $data['rm_kd']}}");
        // $('#mySelrawmatct2').trigger('change'); 

        });


   

        
     
 </script>
  @include('page/'.$class_link.'/index_detail_js', $data)
@append