<script type="text/javascript">
	open_table();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	function setKodeTicket(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + mm + dd;

		$("#kode_ticket").val('');

		$.ajax({
			type: "GET",
			url: '{{ url($class_link."/getkode") }}',
			dataType: "JSON",
			success: function (response) {	
                var data = response + 1;
				$("#kode_ticket").val(today + "-0" + data);
			}
		});
	}

	function open_table() {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
				}
			});
		});
	}

	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			success: function(html) {
				toggle_modal('Form '+ sts, html);
				render_aset();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
                setKodeTicket();
				$("#idid").val(id);
			}
		});
	}

	function render_aset() {
		$("#id_aset").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});

		$("#id_aset").select2({
			placeholder: '-- Cari Opsi --',
			dropdownAutoWidth : true,
			width : '300px',
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(data){
		open_form_main('edit', data)
	}


    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}

	function sendNotifWhatsaap(xixi){
		var d = new Date();
			var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
			var nowa = '';
			if(xixi == 'hrga'){
				nowa = 6281133327173;
			}else if(xixi == 'it'){
				nowa = 6283112232212;
			}else if(xixi == 'mtc'){
				nowa = 6282230187188;
			}
			var message = 	`Dear Maintenance%0A
							%0A
							kode tiket :`+ $('#kode_ticket').val() +` %0A
							%0A
							Dengan pesan "`+ $('#ticket_keterangan').val() + `".%0A
							Telah di apply pada `+ strDate +`,%0A
							Lihat selengkapnya di system!%0A
							%0A
							Regards {{ Auth::user()->username }}`;

			var url =' https://api.whatsapp.com/send/?phone='+nowa+'&text='+ message +'&app_absent=0';
    		window.open(url, '_blank');
	}

	function sendNotifMail() {
			var data = {
					service_id: 'service_89wfszf',
					template_id: 'template_nxv3oks',
					user_id: 'user_gqom4wS5NcBXsu8XDttVI',
					template_params: {
					to_name: 'All Maintenance',
					kode_tiket: $('#kode_ticket').val(),
					reply_to: '{{ Auth::user()->email }}',
					message: $('#ticket_keterangan').val(),
					nama_aset: $("#id_aset :selected").text()
				}
			};
			$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
			type: 'POST',
			data: JSON.stringify(data),
			contentType: 'application/json'
				}).done(function() {
					sweetalert2 ('success', 'Email dikirim ke Maintenance.');
				}).fail(function(error) {
					alert('Oops… ' + JSON.stringify(error));
				})
	}

	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('ticket.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);
		
		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data)
				if (data.code == 200){
					if($('#send_wa_hrga').is(":checked") == true){
						sendNotifWhatsaap('hrga');
					}
					if($('#send_wa_it').is(":checked") == true){
						sendNotifWhatsaap('it');
					}
					if($('#send_wa_mtc').is(":checked") == true){
						sendNotifWhatsaap('mtc');
					}
					if($('#send_email').is(":checked") == true){
						sendNotifMail();
					}
					$('.errInput').html('');
					toggle_modal('', '');
					open_table();
					sweetalert2 ('success', data.messages);
					
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

	function make(status){
		$('#status').val(status);
		open_table();
	}

</script>