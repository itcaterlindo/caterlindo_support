@php
/* --Insert setting property form-- */
$form_id = 'form_ticket';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}"  class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">
		<div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Kode ticket</label>
			<div class="col-sm-4 col-xs-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">#</span>
                    </div>
                    <input type="text" name="kode_ticket" id="kode_ticket" class="form-control" placeholder="Kode Tiket" readonly>
                  </div>
			</div>	
			<label for="status_ticket" class="col-md-1 col-form-label">Status</label>
			<div class="col-sm-4 col-xs-12">
				<select name="status_ticket" class="form-control form-control-sm" id="status_ticket">
					<option value="delive" selected="Entered">Delive</option>
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idaset_parent" class="col-md-2 col-form-label">Aset</label>
			<div class="col-sm-6 col-xs-12">
				<select name="id_aset" class="form-control form-control-lg" id="id_aset" data-width="100%">
				</select>
			</div>	
		</div>
		<div class="form-group row">
			<label for="idaset_parent" class="col-md-2 col-form-label">Prioritas</label>
			<div class="col-sm-6 col-xs-12">
				<select name="prioritas" class="form-control form-control-sm select2" id="prioritas">
					<option value="emergency" selected="Entered">Emergency</option>
					<option value="urgent" selected="Entered">Urgent</option>
				</select>
			</div>	
		</div>
		<div class="form-group row">
			<label for="idaset_nama" class="col-md-2 col-form-label">File</label>
			<div class="col-sm-9 col-xs-12">
                <input type="file" class="custom-file-input" id="ticket_file" name="ticket_file" onchange="loadFile(event)">
                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                <br>
                <br>
                <img id="output" style="height:100px; width:150px;"/>
                <script>
                    var loadFile = function(event) {
                        var output = document.getElementById('output');
                        output.src = URL.createObjectURL(event.target.files[0]);
                        output.onload = function() {
                        URL.revokeObjectURL(output.src) // free memory
                        }
                    };
                </script>
			</div>	
		</div>
		<div class="form-group row">
			<label for="idaset_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-9 col-xs-12">
				<textarea name="ticket_keterangan" id="ticket_keterangan" class="form-control form-control-sm" rows="2" placeholder="Keterangan">{{ isset($aset_keterangan) ? $aset_keterangan : '' }}</textarea>
			</div>	
		</div>
		<div class="form-check" style="margin-left:17%">
			<input type="checkbox" class="form-check-input" id="send_wa_hrga" name="send_wa">
			<label class="form-check-label" for="exampleCheck1">Send Whatsapp Notification (HR-GA)</label>
		  </div>
		  <br>
		  <br>
		  <div class="form-check" style="margin-left:17%">
			<input type="checkbox" class="form-check-input" id="send_wa_it" name="send_wa">
			<label class="form-check-label" for="exampleCheck1">Send Whatsapp Notification (IT)</label>
		  </div>
		  <br>
		  <br>
		  <div class="form-check" style="margin-left:17%">
			<input type="checkbox" class="form-check-input" id="send_wa_mtc" name="send_wa">
			<label class="form-check-label" for="exampleCheck1">Send Whatsapp Notification (Maintenance)</label>
		  </div>
		  <hr>
		  <div class="form-check" style="margin-left:17%">
			<input type="checkbox" class="form-check-input" id="send_email" name="send_email">
			<label class="form-check-label" for="exampleCheck1">Send Email Notification</label>
		  </div>
		  <br>
		  <br>
		  <br>
		  {{-- <div class="form-group row" id="subject_row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Subjek Email</label>
			<div class="col-sm-4 col-xs-12">
                <div class="input-group mb-3">
                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject Email">
                  </div>
			</div>
		</div> --}}
		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>

<script>
	$(document).ready(function () {
		// $("#subject_row").hide();
	$('#send_wa_hrga').change(function() {
        if(this.checked) {
            var returnVal = confirm("Are you sure?");
            $(this).prop("checked", returnVal);
			$('#send_wa_it').attr("checked", false);
			$('#send_wa_mtc').attr("checked", false);
        }else{
			$('#send_wa_hrga').val(this.checked);   
		}
             
    });
	$('#send_wa_it').change(function() {
        if(this.checked) {
            var returnVal = confirm("Are you sure?");
            $(this).prop("checked", returnVal);
			$('#send_wa_hrga').attr("checked", false);
			$('#send_wa_mtc').attr("checked", false);
        }else{
			$('#send_wa_it').val(this.checked); 
		}
              
    });

	$('#send_wa_mtc').change(function() {
        if(this.checked) {
            var returnVal = confirm("Are you sure?");
            $(this).prop("checked", returnVal);
			$('#send_wa_hrga').attr("checked", false);
			$('#send_wa_it').attr("checked", false);
        }else{
        $('#send_wa_mtc').val(this.checked);       
		} 
    });


		// $("#send_email").change(function() {
		// 	$("#subject_row").hide();
		// 	if(this.checked) {
		// 		$("#subject_row").show();
		// 	}
		// });

		var param_id = $('#idid').val();
		if(param_id){
			$(".form-check").hide();
			$("#status_ticket").append('<option value="cancel">Cancel</option>');
			$.ajax({
               type: "POST",
               url: "{{ url($class_link."/data_detail_ticket") }}",
               data: {
					"id": param_id,
					"_token": '{{ csrf_token() }}',
				},
               dataType: "JSON",
               success: function (response) {
                   console.log(response);
				   $('#kode_ticket').val(response[0].ticket_kode);
				   $("#id_aset").append('<option value="'+ response[0].aset_id +'" selected>'+ response[0].aset_nama +'</option>');
				   $("#status_ticket").append('<option value="'+ response[0].status_ticket +'" selected>'+ response[0].status_ticket +'</option>');
				   $("#ticket_keterangan").val(response[0].problem);
				   if(response[0].name){
						$("#output").attr("src", `{{url('data_ticket/`+response[0].name+`')}}`);
				   }
               }
           });
		}
	});
</script>