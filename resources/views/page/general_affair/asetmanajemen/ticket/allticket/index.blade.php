@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
    @include('layouts.plugins.datetimepicker')
@endsection

@php
    $master_var = 'all_Ticket';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
    $data['close'] = $close;
    $data['active'] = $active;
    $data['delive'] = $delive;
    $data['cancel'] = $cancel;
@endphp

@section('content')
<style>
    @media screen {
    #printSection {
        display: none;
    }
  }

  @media print {
    body * {
      visibility:hidden;
    }
    #printSection, #printSection * {
      visibility:visible;
    }  
    .modal-dialog {
    max-width: 100%;
    width: 100%;
   }

    #printSection {
      position:absolute;
      left:0;
      top:0;
    }
  }

</style>
<div class="row">
  <input type="hidden" name="status" id="status">
  <div class="col-lg-3 col-6">
    <!-- small card -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{ $delive }}</h3>

        <p>Ticket Delive</p>
      </div>
      <div class="icon">
        <i class="fas fa-spinner"></i>
      </div>
      <a href="#" onclick="make('delive')" class="small-box-footer">
        Lihat Data  <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small card -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ $active }}</h3>

        <p>Ticket Active</p>
      </div>
      <div class="icon">
        <i class="fas fa-vote-yea"></i>
      </div>
      <a href="#" onclick="make('active')" class="small-box-footer">
        Lihat Data  <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small card -->
    <div class="small-box bg-secondary">
      <div class="inner">
        <h3>{{ $close }}</h3>

        <p>Ticket Closed</p>
      </div>
      <div class="icon">
        <i class="fas fa-tasks"></i>
      </div>
      <a href="#" onclick="make('close')" class="small-box-footer">
        Lihat Data <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <!-- small card -->
    <div class="small-box bg-danger">
      <div class="inner">
        <h3>{{ $cancel }}</h3>

        <p>Ticket Cancel</p>
      </div>
      <div class="icon">
        <i class="fas fa-times-circle"></i>
      </div>
      <a href="#" onclick="make('cancel')" class="small-box-footer">
        Lihat Data <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <!-- ./col -->
</div>
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
          <div class="card-tools">
            <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
              <i class="fas fa-plus-circle"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
            <div id="<?php echo $data['idBoxLoader']; ?>" align="middle">
              <i class="fas fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
              {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
            </div>
            <div id="{{ $data['idBoxContent'] }}"></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div id="printThis">
<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>
</div>

@endsection

@section('scriptJS')
  @include('page/'.$class_link.'/index_js', $data)
@append

