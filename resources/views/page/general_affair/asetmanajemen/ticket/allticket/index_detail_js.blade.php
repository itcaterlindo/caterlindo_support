<script>
   $(document).ready(function () {
       openComent();
       getTicketDetail();
       getRiwayatTicket();
       raw_mat();
       table_rm();
      
       

       var rm = [];

       $("#send").click(function (e) { 
           e.preventDefault();
           storedNote('');
           
       });
        $('#kondisi').change(function (e) { 
            change_condition($(this).val());
        });

        $("#check, #priority, #type").on("change", "input[type=checkbox]", function() {
            var d = $(this).attr('id'); 
            $.ajax({
                type: 'POST',
                url: '{{ url($class_link."/change") }}',
                data: {status: d, id: '{{ $data['id']}}', _token : '{{ csrf_token() }}'},
                success: function(data) {
                    console.log(d)
                    if (data.code == 200){
					    sweetalert2 ('success', data.messages);
                        getTicketDetail();
                    }else if ( data.code == 401){
                        sweetalert2 ('warning', data.messages + '<br>' + data.data);
                        generateToken (data._token);
                        getTicketDetail();
                    }else if (data.code == 400){
                        sweetalert2 ('error', data.messages);
                        generateToken (data._token);
                        getTicketDetail();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                        generateToken (data._token);
                        getTicketDetail();
                    }
                }
		    });
        });

        $("#add_timeline").on("click", "#del_com", function() {
            var d = $(this).data('id'); 
            delete_data(d);
        });


        $('#p1_format, #p2_format, #p3_format').on('click', function() {
            changeStatus($(this).data('id'));
        });

       function sweetalert2(type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
        }
       
        function openComent() { 
           var coment = '';
           $.ajax({
               type: "POST",
               url: "{{ url($class_link."/get_comment") }}",
               data: {
					"id": '{{ $data['id']}}',
					"_token": '{{ csrf_token() }}',
				},
               dataType: "JSON",
               success: function (response) {

                   if(response){
                        $("#add_timeline").html("");
                        for(var i of response) {
                            if(i.id == "{{ auth::id() }}"){
                                coment += `<div>
                                                    <i class="fas fa-envelope bg-blue"></i>
                                                    <div class="timeline-item" style="background:#ddfcff">
                                                    <span class="time"><i class="fas fa-clock"></i>'` + i.created_note + `</span>
                                                    <h3 class="timeline-header"><a href="#">` + 'Anda || ' + i.username +`</a> Membalas Percakapan.</h3>
                                    
                                                    <div class="timeline-body">
                                                        `+ i.note +`
                                                    </div>
                                                    </div>
                                                </div>`
                            }else{
                                    coment += `<div>
                                                    <i class="fas fa-envelope bg-red"></i>
                                                    <div class="timeline-item" style="background:#ffeaea">
                                                    <span class="time"><i class="fas fa-clock"></i>'` + i.created_note + `</span>
                                                    <h3 class="timeline-header"><a href="#" style="color: #ff7373;">` + i.name + ' || ' + i.username +`</a> Membalas Percakapan.</h3>
                                    
                                                    <div class="timeline-body">
                                                        `+ i.note +`
                                                    </div>
                                                    </div>
                                                </div>`
                            }
                        }
                        $("#add_timeline").append(coment);
                   }
               }
           });
        }

         function getTicketDetail() { 
          var detail = '';
           $.ajax({
               type: "POST",
               url: "{{ url($class_link."/data_detail_ticket") }}",
               data: {
					"id": '{{ $data['id']}}',
					"_token": '{{ csrf_token() }}',
				},
               dataType: "JSON",
               success: function (response) {
                    loadCheckbox(response)
                    if(response[0].asetmutasi_kode  !== null){
                        $('.bosse').hide();
                        $('.ntv').show();
                    }else{
                        $('.ntv').hide();
                    }
                   var status = "";
                    if(response.length != 0){
                        $("#detail_ticket").html("");
                        if(response[0].status_ticket == 'close'){   
                            status += `<a href="#" class="btn btn-secondary btn-block"><b>Status Closed</b></a>`
                        }else if(response[0].status_ticket == 'active'){
                            status+= `<a href="#" class="btn btn-success btn-block"><b>Status Active</b></a>`
                        }else{
                            status+= `<a href="#" class="btn btn-primary btn-block"><b>Status Delive</b></a>`
                        }

                    var gambar = "";

                    if(response[0].name){
                        gambar += `<img id="output" src="{{url('data_ticket/`+ response[0].name +`')}}" style="height:250px; width: 100%;"/>`
                    }else{
                        gambar += 'Tanpa Gambar!'
                    }
                        detail += ` <div class="card card-primary card-outline">
                                        <div class="card-body box-profile">
                                        ` +  status + `
                                        <br>
                                        <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle" src="http://pem.fisip.unmuhjember.ac.id/wp-content/uploads/2015/08/model-icon3.png" alt="User profile picture">
                                        </div>
                                        <h3 class="profile-username text-center">`+ response[0].users +`</h3>
                                        <p class="text-muted text-center">Pembuat Ticket</p>
                                        <div class="card-body">
                                            <strong><i class="fas fa-qrcode mr-1"></i> Kode Ticket</strong>
                                
                                            <p class="text-muted">
                                                `+ response[0].ticket_kode +`
                                            </p>
                                
                                            <hr>

                                            <strong><i class="fas fa-calendar-alt mr-1"></i> Tanggal pembuatan</strong>
                                
                                            <p class="text-muted">
                                                `+ response[0].created_ticket +`
                                            </p>
                                
                                            <hr>

                                            <strong><i class="fas fa-book mr-1"></i> Nama Asset || Barcode</strong>
                                
                                            <p class="text-muted">
                                                `+ response[0].aset_nama +` || <b>`+ response[0].aset_barcode +`</b>
                                            </p>
                                
                                            <hr>
                                
                                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi Asset</strong>
                                
                                            <p class="text-muted">`+ response[0].lokasi_nama +`</p>
                                
                                            <hr>
                                
                                            <strong><i class="fas fa-pencil-alt mr-1"></i> Jenis Asset</strong>
                                
                                            <p class="text-muted">
                                                `+ response[0].asetjenis_nama +`
                                            </p>
                                
                                            <hr>
                                
                                            <strong><i class="fas fa-hand-holding mr-1"></i> Problem</strong>
                                
                                            <p class="text-muted">`+ response[0].problem +`</p>

                                            <hr>
                                
                                            <strong><i class="far fa-file-alt mr-1"></i> Gambar</strong>
                                
                                            <p class="text-muted">`+ gambar +`</p>
                                            </div>

                                        
                                        </div>
                                        <!-- /.card-body -->
                                    </div>`;
                        $("#detail_ticket").append(detail);
                    }
               }
           });
        }

        function loadCheckbox(data){
            $("#check").html("");
            $("#type").html("");
            $("#priority").html("");
            var perbaikan = "";
            var ganti = "";
            var tindaklanjut = ""; 
                if('{{ Auth::user()->can("IS_ADMIN_TIKET") }}'){
                    if(data[0].sparepart == "perbaikan"){
                        perbaikan += `<input type="checkbox" name="checkbox" class="form-check-input" id="perbaikan" checked>`;
                    }else{
                        perbaikan += `<input type="checkbox" name="checkbox" class="form-check-input" id="perbaikan">`;
                    }
                    if(data[0].sparepart == "ganti baru"){
                        ganti += `<input type="checkbox" name=checkbox class="form-check-input" id="ganti baru" checked>`;
                    }else{
                        ganti += `<input type="checkbox" name=checkbox class="form-check-input" id="ganti baru">`;
                    }
                    if(data[0].sparepart == "perlu tindak lanjut"){
                        tindaklanjut += `<input type="checkbox" name=checkbox class="form-check-input" id="perlu tindak lanjut" checked>`;
                    }else{
                        tindaklanjut += `<input type="checkbox" name=checkbox class="form-check-input" id="perlu tindak lanjut">`;
                    }
                    var pr = `<label> Perbaikan </label><div class="form-check">
                                `+ perbaikan +`
                                <label class="form-check-label" for="exampleCheck1">Perbaikan</label>
                            </div>
                            <div class="form-check">
                                `+ ganti +`
                                <label class="form-check-label" for="exampleCheck1">Ganti Sprpart Baru</label>
                            </div>
                            <div class="form-check">
                                `+ tindaklanjut +`
                                <label class="form-check-label" for="exampleCheck1">Tindak Lanjut</label>
                            </div>`;
                    $("#check").append(pr);


                    var urgent = "";
                    var emergency = "";
                    var biasa = "";

                    if(data[0].prioritas == "urgent"){
                        urgent += `<input type="checkbox" name="checkbox" class="form-check-input" id="urgent" checked>`;
                    }else{
                        urgent += `<input type="checkbox" name="checkbox" class="form-check-input" id="urgent">`;
                    }
                    if(data[0].prioritas == "emergency"){
                        emergency += `<input type="checkbox" name=checkbox class="form-check-input" id="emergency" checked>`;
                    }else{
                        emergency += `<input type="checkbox" name=checkbox class="form-check-input" id="emergency">`;
                    }
                    if(data[0].prioritas == "biasa"){
                        biasa += `<input type="checkbox" name=checkbox class="form-check-input" id="biasa" checked`;
                    }else{
                        biasa += `<input type="checkbox" name=checkbox class="form-check-input" id="biasa"`;
                    }
                    var pr = `<label> Prioritas </label><div class="form-check">
                                `+ urgent +`
                                <label class="form-check-label" for="exampleCheck1">Urgent</label>
                            </div>
                            <div class="form-check">
                                `+ emergency +`
                                <label class="form-check-label" for="exampleCheck1">Emergency</label>
                            </div>
                            <div class="form-check">
                                `+ biasa +`
                                <label class="form-check-label" for="exampleCheck1">Biasa</label>
                            </div>`;
                    $("#priority").append(pr);


                    var short = "";
                    var long = "";
                    if(data[0].jenis_service == "short rervice"){
                        short += `<input type="checkbox" name="checkbox" class="form-check-input" id="short rervice" checked>`;
                    }else{
                        short += `<input type="checkbox" name="checkbox" class="form-check-input" id="short rervice">`;
                    }
                    if(data[0].jenis_service == "long service"){
                        long += `<input type="checkbox" name=checkbox class="form-check-input" id="long service" checked>`;
                    }else{
                        long += `<input type="checkbox" name=checkbox class="form-check-input" id="long service">`;
                    }
                    var pr = `<label> Jenis Service </label><div class="form-check">
                                `+ short +`
                                <label class="form-check-label" for="exampleCheck1">Short Service</label>
                            </div>
                            <div class="form-check">
                                `+ long +`
                                <label class="form-check-label" for="exampleCheck1">Long service</label>
                            </div>`;
                    $("#type").append(pr);
                }else{
                    if(data[0].sparepart == "perbaikan"){
                        perbaikan += `<input type="checkbox" name="checkbox" class="form-check-input" id="perbaikan" checked disabled="disabled">`;
                    }else{
                        perbaikan += `<input type="checkbox" name="checkbox" class="form-check-input" id="perbaikan" disabled="disabled">`;
                    }
                    if(data[0].sparepart == "ganti baru"){
                        ganti += `<input type="checkbox" name=checkbox class="form-check-input" id="ganti baru" checked disabled="disabled">`;
                    }else{
                        ganti += `<input type="checkbox" name=checkbox class="form-check-input" id="ganti baru" disabled="disabled">`;
                    }
                    if(data[0].sparepart == "perlu tindak lanjut"){
                        tindaklanjut += `<input type="checkbox" name=checkbox class="form-check-input" id="perlu tindak lanjut" checked disabled="disabled">`;
                    }else{
                        tindaklanjut += `<input type="checkbox" name=checkbox class="form-check-input" id="perlu tindak lanjut" disabled="disabled">`;
                    }
                    var pr = `<label> Perbaikan </label><div class="form-check">
                                `+ perbaikan +`
                                <label class="form-check-label" for="exampleCheck1">Perbaikan</label>
                            </div>
                            <div class="form-check">
                                `+ ganti +`
                                <label class="form-check-label" for="exampleCheck1">Ganti Sprpart Baru</label>
                            </div>
                            <div class="form-check">
                                `+ tindaklanjut +`
                                <label class="form-check-label" for="exampleCheck1">Tindak Lanjut</label>
                            </div>`;
                    $("#check").append(pr);


                    var urgent = "";
                    var emergency = "";
                    var biasa = "";
                    if(data[0].prioritas == "urgent"){
                        urgent += `<input type="checkbox" name="checkbox" class="form-check-input" id="urgent" checked disabled="disabled">`;
                    }else{
                        urgent += `<input type="checkbox" name="checkbox" class="form-check-input" id="urgent" disabled="disabled">`;
                    }
                    if(data[0].prioritas == "emergency"){
                        emergency += `<input type="checkbox" name=checkbox class="form-check-input" id="emergency" checked disabled="disabled">`;
                    }else{
                        emergency += `<input type="checkbox" name=checkbox class="form-check-input" id="emergency" disabled="disabled">`;
                    }
                    if(data[0].prioritas == "biasa"){
                        biasa += `<input type="checkbox" name=checkbox class="form-check-input" id="biasa" checked disabled="disabled">`;
                    }else{
                        biasa += `<input type="checkbox" name=checkbox class="form-check-input" id="biasa" disabled="disabled">`;
                    }
                    var pr = `<label> Prioritas </label><div class="form-check">
                                `+ urgent +`
                                <label class="form-check-label" for="exampleCheck1">Urgent</label>
                            </div>
                            <div class="form-check">
                                `+ emergency +`
                                <label class="form-check-label" for="exampleCheck1">Emergency</label>
                            </div>
                            <div class="form-check">
                                `+ biasa +`
                                <label class="form-check-label" for="exampleCheck1">Biasa</label>
                            </div>`;
                    $("#priority").append(pr);


                    var short = "";
                    var long = "";
                    if(data[0].jenis_service == "short rervice"){
                        short += `<input type="checkbox" name="checkbox" class="form-check-input" id="short rervice" checked> disabled="disabled">`;
                    }else{
                        short += `<input type="checkbox" name="checkbox" class="form-check-input" id="short rervice" disabled="disabled">`;
                    }
                    if(data[0].jenis_service == "long service"){
                        long += `<input type="checkbox" name=checkbox class="form-check-input" id="long service" checked disabled="disabled">`;
                    }else{
                        long += `<input type="checkbox" name=checkbox class="form-check-input" id="long service" disabled="disabled">`;
                    }
                    var pr = `<label> Jenis Service </label><div class="form-check">
                                `+ short +`
                                <label class="form-check-label" for="exampleCheck1">Short Service</label>
                            </div>
                            <div class="form-check">
                                `+ long +`
                                <label class="form-check-label" for="exampleCheck1">Long service</label>
                            </div>`;
                    $("#type").append(pr);
                }
        }

        function getRiwayatTicket(){
            var riwayat = "";
            $.ajax({
                type: "POST",
                url: "{{ url($class_link."/data_riwayat_ticket") }}",
                data: {
                        "id": '{{ $data['aset_id']}}',
                        "_token": '{{ csrf_token() }}',
                    },
                dataType: "JSON",
                success: function (response) {
                    console.log(response)
                    if(response){
                        $("#ul_riwayat").html("");
                        for(var i of response) {
                            riwayat += `<li class="nav-item">
                                    <a href="{{ url("/general_affair/asetmanajemen/ticket/allticket/detail/`+ i.ticket_id +`") }}" class="nav-link">
                                        <i class="far fa-envelope"></i> `+ i.ticket_kode + ' || ' + i.problem +`
                                    </a>
                                </li>`
                        }
                    }
                    $("#ul_riwayat").append(riwayat);
                }
            })
        }
    
        function storedNote (val) {
            $.ajax({
               type: "POST",
               url: "{{ url($class_link."/stored_comment") }}",
               data: {
                    "id_ticket": '{{ $data['id']}}',
                    "_token": '{{ csrf_token() }}',
                    "note" : val ? val : $('#summernote').summernote('code'),
                },
               dataType: "JSON",
               success: function (data) {
                if (data.code == 200){
					$('#summernote').summernote('reset');
					openComent();
					sweetalert2 ('success', data.messages);
                    if (data.autochange){
                        getTicketDetail()
                    }
				}else if ( data.code == 401){
                    $('#summernote').code('reset');
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
                    $('#summernote').code('reset');
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
                    $('#summernote').code('reset');
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}

               }});
        }

        function delete_data(id){
            var conf = confirm('Apakah Anda yakin ?');
            if (conf){
                $.ajax({
                    url: "{{ route('delete.coment') }}",
                    type: 'POST',
                    dataType: "JSON",
                    data: {
                        "id": id,
                        "_token": '{{ csrf_token() }}',
                    },
                    success: function (data)
                    {
                        if (data.code == 200){
                            sweetalert2 ('success', data.messages);
                            openComent();
                        }else if (data.code == 400){
                            sweetalert2 ('error', data.messages);
                        }else{
                            sweetalert2 ('error', 'Unknown Error');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        sweetalert2 (ajaxOptions, thrownError);
                    }
                });
            }
        }

        function changeStatus(data){
            var conf = confirm('Apakah Anda ingin mengubah status menjadi "'+ data +'"?');
            if (conf){
                $.ajax({
                	url: "{{ route('status.ticket') }}",
                	type: 'POST',
                	dataType: "JSON",
                	data: {
                		"id": '{{ $data["id"] }}',
                		"_token": '{{ csrf_token() }}',
                        "status_ticket": data
                	},
                	success: function (data)
                	{
                		if (data.code == 200){
                			sweetalert2 ('success', data.messages);
                            getTicketDetail();
                		}else if (data.code == 400){
                			sweetalert2 ('error', data.messages);
                		}else{
                			sweetalert2 ('error', 'Unknown Error');
                		}
                	},
                	error: function (xhr, ajaxOptions, thrownError) {
                		sweetalert2 (ajaxOptions, thrownError);
                	}
                });
            }
        }

        function change_condition(data) {
            var conf = confirm('Apakah Anda ingin mengubah Kondisi?');
            if(conf){
                if(data == 4){
                    storedNote('Barang dinyatakan Rusak!');
                }
                $.ajax({
                	url: "{{ route('kondisi.asset') }}",
                	type: 'POST',
                	dataType: "JSON",
                	data: {
                		"id": '{{ $data["aset_id"] }}',
                		"_token": '{{ csrf_token() }}',
                        "asetstatus_id": data
                	},
                	success: function (data)
                	{
                		if (data.code == 200){
                			sweetalert2 ('success', data.messages);
                            getTicketDetail();
                		}else if (data.code == 400){
                			sweetalert2 ('error', data.messages);
                		}else{
                			sweetalert2 ('error', 'Unknown Error');
                		}
                	},
                	error: function (xhr, ajaxOptions, thrownError) {
                		sweetalert2 (ajaxOptions, thrownError);
                	}
                });
            }
        }

        function raw_mat(){ 
                $("#rawmat").select2({
                        placeholder: '-- Cari Material --',
                        allowClear: true,
                        ajax: {
                            type: "GET",
                            url: "{{ url('https://caterlindo.co.id/caterlindo_ppic/api/rawmaterial/select2') }}",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                            return {
                                searchTerm: params.term
                                
                            };
                            },
                            processResults: function (response) {
                            console.log(response);
                            
                            return {
                                results: response.data
                                
                            };
                            },
                            cache: true
                        }
                    }).on("change", function(e) {

                    var lastValue = $("#rawmat").val();
                    var lastText = $("#rawmat option:last-child").text();

                    if(lastValue == "SPECIAL"){
                    
                        $(".is_special").css("display", "block");

                       
                        get_nm_rm(lastValue)
                    }else{
                        $(".is_special").css("display", "none");
                        var params = {
                            ticket_id : "{{ $data['id'] }}",
                            rm_kd : lastValue,
                            rm_nama : lastText,
                            _token : '{{ csrf_token() }}'
                        }

                        save_rm(params);
                    }
                        
                });
         }

        function get_nm_rm(param) { 
            $('#submit_rm').click(function (e) { 
                e.preventDefault();
                
            
                var params = {
                                ticket_id : "{{ $data['id'] }}",
                                rm_kd : param,
                                rm_nama : 'SPECIAL -' + $('#nm_rm').val(),
                                _token : '{{ csrf_token() }}'
                            }
                $.ajax({
                    type: 'POST',
                    url: '{{ url($class_link."/change_rawmat") }}',
                    data: params,
                    success: function(data) {
                        if (data.code == 200){
                            sweetalert2 ('success', data.messages);
                            table_rm();
                        }else if ( data.code == 401){
                            sweetalert2 ('warning', data.messages + '<br>' + data.data);
                            generateToken (data._token);
                            table_rm();
                        }else if (data.code == 400){
                            sweetalert2 ('error', data.messages);
                            generateToken (data._token);
                            table_rm();
                        }else{
                            sweetalert2 ('error', 'Unknown Error');
                            generateToken (data._token);
                            getTicketDetail();
                        }
                    }
                });
            });
        }


        function save_rm(params) { 
            $.ajax({
                            type: 'POST',
                            url: '{{ url($class_link."/change_rawmat") }}',
                            data: params,
                            success: function(data) {
                                if (data.code == 200){
                                    sweetalert2 ('success', data.messages);
                                    table_rm();
                                }else if ( data.code == 401){
                                    sweetalert2 ('warning', data.messages + '<br>' + data.data);
                                    generateToken (data._token);
                                    table_rm();
                                }else if (data.code == 400){
                                    sweetalert2 ('error', data.messages);
                                    generateToken (data._token);
                                    table_rm();
                                }else{
                                    sweetalert2 ('error', 'Unknown Error');
                                    generateToken (data._token);
                                    getTicketDetail();
                                }
                            }
            });
        }

        function table_rm() { 
            var element = '';
            $.ajax({
                type: "GET",
                url: '{{ url($class_link."/get_rm") . '/' . $data["id"] }}',
                dataType: "json",
                success: function (response) {
                    if(response){
                        for (let index = 0; index < response.length; index++) {
                           element+= `<tr>
                                        <td>`+ response[index].rm_nama+`</td>
                                        <td><button type="button" id="del_rm" rm_kd="`+ response[index].rm_kd +`" rm_nm="`+ response[index].rm_nama +`" class="btn btn-primary del_rm">Hapus</button></td>
                                        </tr>`;
                            
                        }

                        $('#add').empty().append(element);
                        act_dlt()
                    }
                    console.log(response);
                }
            });
        }
    
        function act_dlt() { 
            $('.del_rm').click(function (e) { 
                e.preventDefault();
                var rm_kd = $(this).attr('rm_kd');
                var rm_nm = $(this).attr('rm_nm');
                
                var params = {
                                ticket_id : "{{ $data['id'] }}",
                                rm_kd : rm_kd,
                                rm_nama : rm_nm,
                                _token : '{{ csrf_token() }}'
                            }
                $.ajax({
                    type: "POST",
                    url: '{{ url($class_link."/delete_rm") }}',
                    data: params,
                    dataType: "json",
                    success: function (data) {
                        if (data.code == 200){
                                sweetalert2 ('success', data.messages);
                                table_rm();
                            }else if ( data.code == 401){
                                sweetalert2 ('warning', data.messages + '<br>' + data.data);
                                generateToken (data._token);
                                table_rm();
                            }else if (data.code == 400){
                                sweetalert2 ('error', data.messages);
                                generateToken (data._token);
                                table_rm();
                            }else{
                                sweetalert2 ('error', 'Unknown Error');
                                generateToken (data._token);
                                            
                        }
                    }
            });
                
            });
          
        }
        
   });

</script>