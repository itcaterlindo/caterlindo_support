<script type="text/javascript">
    $(document).ready(function () {
        $('#reservation').daterangepicker();
        render_asetJenis();

        function moveTo(div_id) {
            $('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
        }

        function render_asetJenis() {
            $("#idasetjenis_id").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0,
                ajax: {
                    url: "{{ url('api/support/aset/v1/allAsetjenisSelect2') }}",
                    dataType: 'json',
                    headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
                    delay: 250,
                    data: function (params) {
                        return {
                            paramSearch: params.term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }

        $(function() {
            $('#reservation').daterangepicker({
                opens: 'left'
            }, function(start, end, label) {
                $('#{{ $idBoxContent }}').html('');
                var data = {"_token" : "{{ csrf_token() }}", "start" : start.format('YYYY-MM-DD'), "end" : end.format('YYYY-MM-DD'), "asetjenis_id" : $('#idasetjenis_id').val()};
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: '{{ url($class_link."/partial_table_main") }}',
                    success: function(html) {
                        $('#{{ $idBoxContent }}').html(html);
                        $('#{{ $idBoxContent }}').slideDown();
                        moveTo('idMainContent');
                    }
                });
            });
        });
    });
</script>