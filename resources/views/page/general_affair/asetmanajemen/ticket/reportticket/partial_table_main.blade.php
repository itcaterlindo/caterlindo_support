<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
	td{
		text-overflow: ellipsis; // this is producing "..."
		white-space: nowrap;
		overflow: hidden;
	}
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive nowrap" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
            <th style="width:10%; text-align:center;">Kode Ticket</th>
			<th style="width:10%; text-align:center;">Barcode</th>
            <th style="width:10%; text-align:center;">Nama</th>
			<th style="width:10%; text-align:center;">Problem</th>
			<th style="width:10%; text-align:center;">Status Ticket</th>
			<th style="width:10%; text-align:center;">Lokasi Aset</th>
            <th style="width:10%; text-align:center;">Jumlah Aset</th>
            <th style="width:10%; text-align:center;">Perbaikan</th>
			<th style="width:10%; text-align:center;">Tanggal Ticket</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
function strtrunc(str, max, add){
   add = add || '...';
   return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};
var start = '{{ $start }}';
var end = '{{ $end }}';
var asetjenis_id = '{{ $asetjenis_id }}';
var data = {"_token" : "{{ csrf_token() }}", "start" : start.toString(), "end" : end.toString(), "asetjenis_id" : asetjenis_id};
	var table = $('#idTable').DataTable({
		"processing": true,
		// "serverSide": true,
        // "serverside": true,
		"ordering" : true,
    "paging": false,
		"scrollY": "222px",
		"scrollX":        true,
        "scrollCollapse": true,
		"dom": 'Bfrtip',
		"buttons": [
            'copy', 'csv', {
                extend: 'excel',
                title: 'LAPORAN PERHITUNGAN TICKET ASET DAN INVENTARIS "' + start + '--' + end +'"'
            }, {
                extend: 'print',
				exportOptions:{
					columns: ':visible',
					autoPrint: true,
					orientation: 'landscape'
				},
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Laporan Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// // messageTop: function () {
                // //     return `<p style="font-size:22pt">
				// // 		Laporan perhitungan Asset ditemukan<br>
				// // 		Periode : Desember - 2021</p>`;
                // // },
				title: "&nbsp;",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '12pt' )
						.prepend(`<div class="container" style="margin-left:-10px;">
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 43%; margin-top:2%">
													<h2><b>&nbsp;&nbsp;&nbsp;LAPORAN PERHITUNGAN TICKET ASET DAN INVENTARIS </b> </h2>
                          
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													  <img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
                            						  <br>
													</div>
												
													<div class="col-md-4">
													<table class="table" style="margin-left: 200%; margin-top:-98px;">
														<tbody>
														<tr>
															<th scope="row" style="text-align: center;">Periode</th>
															<td><b>Tahun-Bulan-Tgl</b></td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Dari</th>
															<td><b>`+ start + `</b></td>
														</tr>
														<tr>
															<th scope="row" style="text-align: center;">Sampai</th>
															<td><b>`+ end + `</b></td>
														</tr>
														</tbody>
													</table>
													</div>
												</div>
											</div>`);
 
						$(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );

						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape;}',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }, 
            }
        ],
		"ajax": {
			"url" : "{{ url($class_link.'/getData') }}",
			"type": "POST",
			"data": data
			},
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "ticket_kode", name: "ticket_kode"},
            { data: "aset_barcode", name: "aset_barcode" },
            { data: "aset_nama", name: "aset_nama" },
			{ data: "problem", name: "problem" },
            { data: 'status_ticket', 
              render: function(data) { 
                if(data == 'delive') {
                  return '<span class="badge badge-primary">Delive</span>' 
                }
                else if(data == 'active') {
                  return '<span class="badge badge-success">Active</span>'
                }
				        else if(data == 'cancel') {
                  return '<span class="badge badge-danger">cancel</span>'
                }
				        else{
                    return '<span class="badge badge-secondary">Close</span>'
                }

              },},
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: "jumlah", name: "jumlah" },
            { data: "sparepart", name: "sparepart"},
            { data: "created_ticket", name: "created_ticket" }
        ],
        'columnDefs': [
		{
                "targets": 2, // your case first column
                "className": "text-center",
                "width": "4%"
        }, {
                "targets": 7, // your case first column
                "className": "text-center",
                "width": "2%"
        },{
                "targets": 5, // your case first column
                "className": "text-center",
                "width": "8%"
        },{
                "targets": 4, // your case first column
                'render': function(data, type, full, meta){
					if(type === 'display'){
						data = strtrunc(data, 30);
					}
					
					return data;
				}
        }],
		"order":[8, 'desc'],
	});
</script>