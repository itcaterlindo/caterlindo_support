@php
/* --Insert setting property form-- */
$form_id = 'idFormInput';
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('AsetData.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idrmgr_kd" class="col-md-2 col-form-label">Barang GR</label>
			<div class="col-sm-6 col-xs-12">
				<select name="rmgr_kd" class="form-control" id="idrmgr_kd">
				</select>
			</div>
			<label for="idrmgr_qty" class="col-md-1 col-form-label">Qty</label>
			<div class="col-sm-2 col-xs-12">
				<input type="text" class="form-control" name="rmgr_qty" id="idrmgr_qty" placeholder="Qty" value="{{ isset($rmgr_qty) ? $rmgr_qty : '' }}" >
			</div>
		</div>

		<div class="form-group row">
			<label for="idasetjenis_id" class="col-md-2 col-form-label">Jenis Aset</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetjenis_id" class="form-control form-control" id="idasetjenis_id">
				</select>
			</div>	
			<label for="idasetstatus_id" class="col-md-1 col-form-label">Status Aset</label>
			<div class="col-sm-4 col-xs-12">
				<select name="asetstatus_id" class="form-control form-control-sm" id="idasetstatus_id" readonly>
					<option value="3" selected="Entered">Entered</option>
				</select>
			</div>	
		</div>
		<div class="form-group row">
			<label for="kode_aset" class="col-md-2 col-form-label">Kode Aset</label>
			<div class="col-sm-4 col-xs-12">
				<input type="text" name="kode_aset" class="form-control form-control-sm" id="kode_aset">
			</div>		
		</div>

		<div class="form-group row">
			<label for="idaset_tglperolehan" class="col-md-2 col-form-label">Tanggal Perolehan</label>
			<div class="col-sm-3 col-xs-12">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
					</div>
					<input type="text" name="aset_tglperolehan" class="form-control form-control-sm datetimepicker" id="idaset_tglperolehan" value="{{ isset($aset_tglperolehan) ? $aset_tglperolehan : date('Y-m-d') }}">
				</div>
			</div>
			<label for="idaset_nilaiperolehan" class="col-md-1 col-form-label">Nilai (Rp)</label>
			<div class="col-sm-3 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="aset_nilaiperolehan" id="idaset_nilaiperolehan" placeholder="Nilai" value="{{ isset($aset_nilaiperolehan) ? $aset_nilaiperolehan : '' }}" >
			</div>	
		</div>

		<div class="form-group row">
			<label for="idaset_parent" class="col-md-2 col-form-label">Aset Parent</label>
			<div class="col-sm-6 col-xs-12">
				<select name="aset_parent" class="form-control form-control-sm" id="idaset_parent">
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idkd_bagian" class="col-md-2 col-form-label">Bagian</label>
			<div class="col-sm-4 col-xs-12">
				<select name="kd_bagian" class="form-control form-control-sm" id="idkd_bagian">
				</select>
			</div>	
			<label for="idlokasi_id" class="col-md-1 col-form-label">Lokasi</label>
			<div class="col-sm-4 col-xs-12">
				<select name="lokasi_id" class="form-control form-control-sm" id="idlokasi_id">
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idaset_nama" class="col-md-2 col-form-label">Nama Aset</label>
			<div class="col-sm-9 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="aset_nama" id="idaset_nama" placeholder="Nama Aset" value="{{ isset($aset_nama) ? $aset_nama : '' }}" >
			</div>	
		</div>

		<div class="form-group row">
			<label for="idaset_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-9 col-xs-12">
				<textarea name="aset_keterangan" id="idaset_keterangan" class="form-control form-control-sm" rows="2" placeholder="Keterangan">{{ isset($aset_keterangan) ? $aset_keterangan : '' }}</textarea>
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>