@php
@endphp

<!-- title row -->
<div class="row">
  <div class="col-12">
    <h4>
      <i class="fas fa-tag"></i> {{ $aset['aset_barcode'] }}
      <small class="float-right">Tgl Perolehan: {{ $aset['aset_tglperolehan'] }}</small>
    </h4>
  </div>
  <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info" id="head">
  <div class="col-sm-6 invoice-col">
    <b>Nama:</b> {{ $aset['aset_nama'] }}<br>
    <b>Kode:</b> {{ $aset['aset_kode'] }}<br>
    <b>Barcode:</b> {{ $aset['aset_barcode'] }}<br>
    <b>Nilai Perolehan:</b> {{ "Rp " . number_format($aset['aset_nilaiperolehan'],2,',','.') }}<br>
  </div>
  <!-- /.col -->
  <div class="col-sm-6 invoice-col">
    <b>Bagian:</b> {{ $aset['nm_bagian'] }}<br>
    <b>Lokasi:</b> {{ $aset['lokasi_nama'] }}<br>
    <b>Status:</b> {{ $aset['asetstatus_nama'] }}<br>
    <b>Keterangan:</b> {{ $aset['aset_keterangan'] }}<br>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<hr>
<!-- Timelime example  -->
  <div class="row">
    <div class="col-md-6">
      <!-- The time line -->
      <div class="timeline">
          @foreach ($asetMutasis as $asetMutasi)
          <!-- timeline item -->
          <input type="button" name="btnprint" value="Print" onclick="PrintMe('{{ $asetMutasi['asetmutasi_id'] }}')"/>
          <div id="{{ $asetMutasi['asetmutasi_id'] }}">
            <i class="fas fa-envelope bg-yellow"></i>
            <div class="timeline-item">
              <span class="time"><i class="fas fa-clock"></i> {{ $asetMutasi['asetmutasi_updated'] }}</span>
              <h3 class="timeline-header"><b>{{ $asetMutasi['name'] }}</b> {{ $asetMutasi['asetmutasijenis_nama'] }} <i>{{ $asetMutasi['asetmutasi_updated']->diffForHumans() }}</i> </h3> 
              <div class="timeline-body">

                <table border=1 style="width: 80%;">
                  <tr>
                    <td><strong>Bagian Dari</strong></td>
                    <td>: {{ $asetMutasi['asetmutasi_bagiandari_nama'] }}</td>
                    <td><strong>Bagian Ke</strong></td>
                    <td>: {{ $asetMutasi['asetmutasi_bagianke_nama'] }}</td>
                  </tr>
                  <tr>
                    <td><strong>Lokasi Dari</strong></td>
                    <td>: {{ $asetMutasi['lokasiDari_nama'] }}</td>
                    <td><strong>Lokasi Ke</strong></td>
                    <td>: {{ $asetMutasi['lokasiKe_nama'] }}</td>
                  </tr>
                  <tr>
                    <td><strong>Keterangan</strong></td>
                    <td colspan="3">: {{ $asetMutasi['asetmutasi_keterangan'] }}</td>
                  </tr>
                </table>

              </div>
              <div class="timeline-footer"></div>
            </div>
          </div>
          <!-- END timeline item -->
            @endforeach
      </div>
    </div>
        <!-- /.col -->

    <div class="col-md-6">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Ticket Kode</th>
                <th scope="col">Problem</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($data_ticket as $data_ticket)
              <tr>
                <th scope="row">{{ $data_ticket->ticket_kode }}</th>
                <td>{{ $data_ticket->problem }}</td>
                <td>{{ $data_ticket ->status_ticket }}</td>
                <td>{{ $data_ticket->created_ticket }}</td>
              </tr>
              @endforeach
            </tbody>
        </table>
        <!-- /.col -->
    </div>
  </div>

  <script language="javascript">
    function PrintMe(DivID) {
     // alert(DivID)
    var disp_setting="toolbar=yes,location=no,";
    disp_setting+="directories=yes,menubar=yes,";
    disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25";

       var head = `<div>
                      <i class="fas fa-envelope bg-yellow"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i></span>
                        <h3 class="timeline-header">Aset yg di Mutasi</h3> 
                        <div class="timeline-body">

                          <table border=1 style="width: 80%;">
                            <tr>
                              <td><strong>Nama Asset</strong></td>
                              <td>: {{ $aset['aset_nama'] }}</td>
                              <td><strong>Bagian</strong></td>
                              <td>: {{ $aset['nm_bagian'] }}</td>
                            </tr>
                            <tr>
                              <td><strong>Kode Asset</strong></td>
                              <td>: {{ $aset['aset_kode'] }}</td>
                              <td><strong>Lokasi</strong></td>
                              <td>:{{ $aset['lokasi_nama'] }}</td>
                            </tr>
                            <tr>
                              <td><strong>Barcode</strong></td>
                              <td>: {{ $aset['aset_barcode'] }}</td>
                              <td><strong>Status</strong></td>
                              <td>:{{ $aset['asetstatus_nama'] }}</td>
                            </tr>
                          </table>

                        </div>
                        <div class="timeline-footer"></div>
                      </div>
                    </div> <br><br>`;

       var content_vlue = document.getElementById(DivID).innerHTML;
       var docprint=window.open("","",disp_setting);
       docprint.document.open();
       docprint.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"');
       docprint.document.write('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
       docprint.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">');
       docprint.document.write('<head><title>My Title</title>');
       docprint.document.write('<style type="text/css">body{ margin:0px;');
       docprint.document.write('font-family:verdana,Arial;color:#000;');
       docprint.document.write('font-family:Verdana, Geneva, sans-serif; font-size:12px;}');
       docprint.document.write('a{color:#000;text-decoration:none;} </style>');
       docprint.document.write('</head><body onLoad="self.print()"><center>');
       docprint.document.write(head + content_vlue);
       docprint.document.write('</center></body></html>');
       docprint.document.close();
       docprint.focus();
    }
    </script>
<!-- /.timeline -->
