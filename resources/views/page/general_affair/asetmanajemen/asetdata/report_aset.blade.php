<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="rep" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:1%; text-align:center;">Kode</th>
			<th style="width:20%; text-align:center;">Lokasi</th>
			<th style="width:13%; text-align:center;">PIC</th>
			<th style="width:10%; text-align:center;">Jml Aset</th>
			<th style="width:10%; text-align:center;">Jml Active</th>
			<th style="width:10%; text-align:center;">Jml Entered</th>
			<th style="width:10%; text-align:center;">Jml Disposed</th>
		</thead>
	</table>
</div>
<script type="text/javascript">
	var table = $('#rep').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
        "dom": 'Bfrtip',
		"buttons": [{
                extend: 'print',
				exportOptions:{
					columns: ':visible',
					autoPrint: true,
					orientation: 'landscape'
				},
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Laporan Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// // messageTop: function () {
                // //     return `<p style="font-size:22pt">
				// // 		Laporan perhitungan Asset ditemukan<br>
				// // 		Periode : Desember - 2021</p>`;
                // // },
				title: "&nbsp;",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '12pt' )
						.prepend(`<div class="container" style="margin-left:-10px;">
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 43%; margin-top:2%">
													<h2><b>&nbsp;&nbsp;&nbsp;LAPORAN PERHITUNGAN FISIK ASET DAN INVENTARIS </b> </h2>
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
													</div>
												
													<div class="col-md-4">
								
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														
													</div>
													<div class="col-md-4">

													</div>
													<div class="col-md-4">
												
													</div>
											</div>
											</div>`);
 
						$(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );

						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape;}',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }, 
            }],
		"responsive" : false,
        "scrollY": "290px",
		"scrollX":        true,
        "scrollCollapse": true,
		"paging": false,
		"ajax": "{{ url($class_link.'/report_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "l_k", name: "l_k", className: "dt-center"},
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: "pic", name: "pic" },
            { data: 'jumlah_disystem', 
              render: function(data) { 
                if(data != '0') {
                  return '<p style="color: blue"><b>'+ data +'</b></p>' 
                }
				else{
					return data
				}
              },
			  className: "text-center"
        	},
			{ data: 'jumlah_active', 
              render: function(data) { 
                if(data != '0') {
                  return '<p style="color: green"><b>'+ data +'</b></p>' 
                }
				else{
					return data
				}
              },
			  className: "text-center"
        	},
			{ data: 'jumlah_entered', 
              render: function(data) { 
                if(data != '0') {
                  return '<p style="color: red"><b>'+ data +'</b></p>' 
                }
				else{
					return data
				}
              },
			  className: "text-center"
        	},
			{ data: 'jumlah_disposed', 
              render: function(data) { 
                if(data != '0') {
                  return '<p style="color: grey"><b>'+ data +'</b></p>' 
                }
				else{
					return data
				}
              },
			  className: "text-center"
        	}
        ],
	});
</script>