<script type="text/javascript">
	open_table('');
	open_report();
	render_lokasi();
	first_load('{{ $idBoxLoader }}', '{{ $idBoxContent }}');
	var id_cetak = [];
	var id_cetak_barcode_batang = [];

	$(document).off('click', '#{{ $idBtnAdd }}').on('click', '#{{ $idBtnAdd }}', function() {
		open_form_main('Add', '');
	});

	function moveTo(div_id) {
		$('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
	}

	$(document).on('select2:select', '#idasetjenis_id', function(e) {
		setKodeAset();
	});

	$(document).on('select2:select', '.filter-lokasi', function(e) {
		open_table($(this).val(), $('.status').val());
	});

	function getval(sel)
	{
		open_table($('#filter-lokasi').val(), sel.value);
	}

	function setKodeAset(){
		var data = {"_token" : "{{ csrf_token() }}", "asetjenis_id" : $("#idasetjenis_id").val()};
		$("#kode_aset").val('');
		$.ajax({
			type: "POST",
			url: '{{ url($class_link."/getasetkode") }}',
			data: data,
			dataType: "JSON",
			success: function (response) {	
				$("#kode_aset").val(response);
			}
		});
	}

	function open_table(data_id, data_stts) {
		$('#<?php echo $idBoxContent; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				data: {"data_id": data_id, 'data_stts' : data_stts},
				url: '{{ url($class_link."/partial_table_main") }}',
				success: function(html) {
					$('#{{ $idBoxContent }}').html(html);
					$('#{{ $idBoxContent }}').slideDown();
					moveTo('idMainContent');
					getCheck();
				}
			});
		});
	}

	function getCheck(){
		$('#idTable').on('click', 'input[type="checkbox"]', function() {
			var id = $(this).attr("data-id");
			var name = $(this).attr("data-name");
			var barcode = $(this).attr("data-barcode");
			console.log(this);
			if ($(this).is(':checked')) {
            	id_cetak.push(id);
				id_cetak_barcode_batang.push({'aset_nama' : name, 'aset_barcode' : barcode });
       		 }else{
				id_cetak = id_cetak.filter(function(elem){
					return elem != id; 
				});
				id_cetak_barcode_batang = removeIdBatang(id_cetak_barcode_batang, {
					key: 'aset_barcode',
					value: barcode
				});
			}
		});   
	}

	function removeIdBatang(array, params){
      array.some(function(item, index) {
        return (array[index][params.key] === params.value) ? !!(array.splice(index, 1)) : false;
      });
      return array;
    }

	function cetak_barcode_group(data){
		id_cetak = JSON.stringify(id_cetak);
		$.ajax({
			type: 'POST',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id_cetak, _token : '{{ csrf_token() }}'},
			success: function(hm) {
				console.log(hm);
				id_cetak = [];
				if($('#idTable', 'input[type="checkbox"]').checked = true){
					$('#idTable', 'input[type="checkbox"]').checked = false
				};
				toggle_modal('View', hm);

			}
		});
	}

	function cetak_barcode_batang(){
		let pr = '<div class="row">';
		for(var data of id_cetak_barcode_batang){
			pr += '<div class="col-md-2"><img id="barcode'+ data.aset_barcode +'"/></div>'
		}
		pr += '</div>';
		toggle_modal('View', pr);

		for(var data of id_cetak_barcode_batang){
			JsBarcode("#barcode" + data.aset_barcode, data.aset_barcode, {
				height: 60,
				width: 1
        	});
		}

		cetak_batang();
	}

	function cetak_batang() { 
		$('#print_batang #btnPrint').click(function (e) { 
			e.preventDefault();
				printDoc = new jsPDF('l', 'mm', [320, 170]);

				for(var data of id_cetak_barcode_batang){
					const img = document.querySelector('img#barcode' + data.aset_barcode);
					printDoc.addPage()
					printDoc.text(12, 8, data.aset_nama)
					printDoc.addImage(img.src, 'JPEG', 2, 9, 107, 50);
				}
				id_cetak_barcode_batang = [];
				printDoc.autoPrint();
				printDoc.output("dataurlnewwindow"); 
		});
	 }

	function open_report() {
		$('#<?php echo $idBoxContentRep; ?>').slideUp(function(){
			$.ajax({
				type: 'GET',
				url: '{{ url($class_link."/partial_report_main") }}',
				success: function(html) {
					$('#{{ $idBoxContentRep }}').html(html);
					$('#{{ $idBoxContentRep }}').slideDown();
					moveTo('idMainContentRep');
				}
			});
		});
	}

	function open_form_main(sts, id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/partial_form_main") }}',
			data: {sts: sts, id: id},
			success: function(html) {
				toggle_modal('Form '+ sts, html);
				render_rmgr();
				render_bagianAset();
				render_aset();
				render_asetJenis();
				render_lokasi();
				render_datetimepicker ('.datetimepicker', 'YYYY-MM-DD');
			}
		});
	}

	function view_data(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/view_aset") }}',
			data: {id: id},
			success: function(html) {
				toggle_modal('View', html);
			}
		});
	}

	function view_barcode_pdf(id) {
		$.ajax({
			type: 'GET',
			url: '{{ url($class_link."/call_barcode_pdf") }}',
			data: {id: id},
			success: function(hm) {
				toggle_modal('View', hm);
			}
		});
		// var ba = '{{ url($class_link."/view_barcode_pdf") }}';
		// console.log(ba);
	}


	function render_aset() {
		$("#idaset_parent").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			allowClear: true,
   			width: "100%",
			ajax: {
				url: "{{ url('api/support/aset/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_bagianAset() {
		$("#idkd_bagian").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			allowClear: true,
   			width: "100%",
			ajax: {
				url: "{{ url('api/hrm/bagian/v1/asetSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_asetJenis() {
		$("#idasetjenis_id").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 0,
			allowClear: true,
   			width: "100%",
			ajax: {
				url: "{{ url('api/support/aset/v1/allAsetjenisSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function render_lokasi() {
		$("#idlokasi_id").select2({
			placeholder: '-- Cari Opsi --',   
			allowClear: true,
   			width: "100%",
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});

		$(".filter-lokasi").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			ajax: {
				url: "{{ url('api/support/lokasi/v1/allSelect2') }}",
				dataType: 'json',
				headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	function regenerate_barcode(params) {
		id_cetak = JSON.stringify(params);
		$.ajax({
			type: 'POST',
			url: '{{ url($class_link."/regenerate_barcode") }}',
			data: {id: params, _token : '{{ csrf_token() }}'},
			success: function (data)
				{
					if (data.code == 200){
						open_table('');
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
		});

	}

	function render_rmgr() {
		$("#idrmgr_kd").select2({
			placeholder: '-- Cari Opsi --',
			minimumInputLength: 2,
			allowClear: true,
   			width: "100%",
			ajax: {
				url: "{{ url($class_link.'/get_rmgr') }}",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						paramSearch: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true
			}
		});
	}

	$(document).off('select2:select', '#idrmgr_kd').on('select2:select', '#idrmgr_kd', function(e) {
		var data = e.params.data;
		console.log(data);
		let rmgr_tgldatang = data.rmgr_tgldatang;
		$('#idrmgr_qty').val(data.rmgr_qty);
		$('#idaset_nilaiperolehan').val(data.rmgr_hargaunitcurrency);
		$('#idaset_tglperolehan').val(rmgr_tgldatang.substring(0, 10));
	});

	function first_load(loader, content) {
		$('#'+loader).fadeOut(500, function(e){
			$('#'+content).slideDown();
		});
	}

	function toggle_modal(modalTitle, htmlContent){
		$('#{{ $idModal }}').modal('toggle');
		$('.modal-title').text(modalTitle);
		$('#{{ $idModalContent }}').slideUp();
		$('#{{ $idModalContent }}').html(htmlContent);
		$('#{{ $idModalContent }}').slideDown();
	}

	function edit_data(sts, id){
		open_form_main(sts, id);
	}

	function delete_data(data){
		var conf = confirm('Apakah Anda yakin ?');
		if (conf){
			var id = $(data).attr("data-id");
			var _token = $(data).attr("data-token");

			$.ajax({
				url: "{{ route('AsetData.destroy') }}",
				type: 'DELETE',
				dataType: "JSON",
				data: {
					"id": id,
					"_method": 'DELETE',
					"_token": _token,
				},
				success: function (data)
				{
					if (data.code == 200){
						open_table('');
						sweetalert2 ('success', data.messages);
					}else if (data.code == 400){
						sweetalert2 ('error', data.messages);
					}else{
						sweetalert2 ('error', 'Unknown Error');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					sweetalert2 (ajaxOptions, thrownError);
				}
			});
		}
	}

    function sweetalert2 (type, title) {
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
		});

		// TYPE : success, info, error, warning
      	Toast.fire({
        	type: type,
        	title: title
      	})
    }

    function generateToken (csrf){
        $('input[name="_token"]').val(csrf);
	}

	function resetButtonSubmit(attrBtnSubmit){
		$(attrBtnSubmit).html('<i class="fa fa-save"></i> Simpan');
		$(attrBtnSubmit).attr('disabled', false);
	}



	function submitData(form_id) {
		event.preventDefault();
		var form = $('#'+form_id)[0];
		var url = "{{ route('AsetData.store') }}";

		// Loading animate
		$('#idbtnSubmit'+form_id).html('<i class="fa fa-spinner fa-pulse"></i> Loading');
		$('#idbtnSubmit'+form_id).attr('disabled', true);

		$.ajax({
			url: url ,
			type: "POST",
			data:  new FormData(form),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				console.log(data);
				if (data.code == 200){
					$('.errInput').html('');
					toggle_modal('', '');
					open_table('');
					sweetalert2 ('success', data.messages);
				}else if ( data.code == 401){
					sweetalert2 ('warning', data.messages + '<br>' + data.data);
					generateToken (data._token);
				}else if (data.code == 400){
					sweetalert2 ('error', data.messages);
					generateToken (data._token);
				}else{
					sweetalert2 ('error', 'Unknown Error');
					generateToken (data._token);
				}
				resetButtonSubmit('#idbtnSubmit'+form_id);
			} 	        
		});
	}

	function render_select2(element){
		$(element).select2({
			theme: 'bootstrap4',
			placeholder: '--Pilih Opsi--',
		});
	}

	function render_datetimepicker (element, format = 'YYYY-MM-DD') {
		$(element).datetimepicker({
			format: format,
		});
	}

</script>