@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
    @include('layouts.plugins.datetimepicker')
@endsection
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.5/dist/JsBarcode.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>

@php
    $master_var = 'data_aset';
    $data['idBoxContentRep'] = 'idBoxContentRep'.$master_var;
    $data['idBoxLoaderRep'] = 'idBoxLoaderRep'.$master_var;
    $data['idBoxOverlayRep'] = 'idBoxOverlayRep'.$master_var;
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
    $data['periode_active'] = $periode_active;

@endphp

@section('content')
{{-- <style>
    @media screen {
    #printSection {
        display: none;
    }
  }

  @media print {
    body * {
      visibility:hidden;
    }
    #printSection, #printSection * {
      visibility:visible;
    }  
    .modal-dialog {
    max-width: 100%;
    width: 100%;
   }

    #printSection {
      position:absolute;
      left:0;
      top:0;
    }
  }

</style> --}}
<div class="row" id="{{ $data['idBox'] }}">
  <div class="col-12">
    <!-- Default box -->
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'report_aset')) }}</h3>          
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <div class="card-body">
          <div id="" align="middle">
            {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
          </div>
          <div id="{{ $data['idBoxContentRep'] }}"></div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->
    <div class="overlay" id="{{ $data['idBoxOverlayRep'] }}" style="display: none;">
      <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
  </div>
</div>


<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
          <div class="card-tools">
            @if (auth()->user()->can('CREATE_ASSET')) 
              @if(!$data['periode_active'])
                <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
                  <i class="fas fa-plus-circle"></i>
                </button>
              @endif
            @endif
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="p" class="col-md-1 col-form-label">Filter by Lokasi</label>
            <div class="col-sm-4 col-xs-12">
              <select name="lokasi_id" class="form-control form-control-sm filter-lokasi">
              </select>
            </div>	
            <br>
            <label for="p" class="col-md-1 col-form-label">Filter by Status</label>
            <div class="col-sm-4 col-xs-12">
              <select name="status" onchange="getval(this);" class="form-control form-control-sm status">
                <option value="1">Active</option>
                <option value="2">Disposed</option>
                <option value="3">Entered</option>
                <option value="4">Rusak</option>
              </select>
            </div>	
          </div>
            <div id="<?php echo $data['idBoxLoader']; ?>" align="middle">
              <i class="fas fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i>
              {{-- <i class="fa fa-spinner fa-pulse fa-2x" style="color:#31708f;"></i> --}}
            </div>
            <div id="{{ $data['idBoxContent'] }}"></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div id="printThis">
<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between" id="print_batang">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
          <button id="btnPrint" type="button" class="btn btn-info"> <i class="fa fa-print"></i> Print</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>
</div>

@endsection

@section('scriptJS')
  @include('page/'.$class_link.'/index_js', $data)
@append

