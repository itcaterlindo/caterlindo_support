<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Cetak</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:15%; text-align:center;">Barcode</th>
			<th style="width:20%; text-align:center;">Nama</th>
			<th style="width:10%; text-align:center;">Jenis</th>
			<th style="width:10%; text-align:center;">Bagian</th>
			<th style="width:10%; text-align:center;">Lokasi</th>
			<th style="width:5%; text-align:center;">Aset Status</th>
			<th style="width:5%; text-align:center;">Status</th>
			<th style="width:10%; text-align:center;">Last Modified</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
var today = new Date();
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + mm;

	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"ajax": {
			"url" : "{{ url($class_link.'/table_data') }}",
			"type": "POST",
			"data": {
					"_token" : '{{ csrf_token() }}',
					"data_id": $('.filter-lokasi').val(),
					"data_stts" : $('.status').val()
				}
			},
		"dom": 'Bfrtip',
		"responsive" : false,
        "scrollY": "590px",
		"scrollX":        true,
        "scrollCollapse": true,
		"paging": false,
		"buttons": [
            'copy', 'csv',{ extend:'excel',
							title: 'Data Asset di Periode "' + today +'"',
							exportOptions: {
								modifier: {
								page: 'all',
								search: 'none'   
								}
							},
							//the remaining buttons here 
					}, {
                extend: 'print',
				exportOptions:{
					columns: ':visible',
					autoPrint: true,
					orientation: 'landscape'
				},
				// messageTop: function () {
                //     return `<p style="font-size:18pt">
				// 		Laporan Perhitungan Fisik Aset dan Inventaris<br>
				// 		Periode :  `+ $('#dropdown_mounth option:selected').text() + ` - ` + $('#dropdown_year').val() + `</p>`;
                // },
				// // messageTop: function () {
                // //     return `<p style="font-size:22pt">
				// // 		Laporan perhitungan Asset ditemukan<br>
				// // 		Periode : Desember - 2021</p>`;
                // // },
				title: "&nbsp;",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '12pt' )
						.prepend(`<div class="container" style="margin-left:-10px;">
												<div style="clear: both; display: inline-block; overflow: hidden; white-space: nowrap; margin-left: 43%; margin-top:2%">
													<h2><b>&nbsp;&nbsp;&nbsp;LAPORAN PERHITUNGAN FISIK ASET DAN INVENTARIS </b> </h2>
												</div>
											<br>
											<br>
												<div class="row">
													<div class="col-md-8">
													<img src="https://caterlindo.co.id/web/Images/Logo-Utama.png" style="width: 35%; height: 80px; margin-top:-98px;" />
													</div>
												
													<div class="col-md-4">
										
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
													
													</div>
													<div class="col-md-4">

													</div>
													<div class="col-md-4">
													</div>
											</div>
											</div>`);
 
						$(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );

						var last = null;
						var current = null;
						var bod = [];
		
						var css = '@page { size: landscape;}',
							head = win.document.head || win.document.getElementsByTagName('head')[0],
							style = win.document.createElement('style');
		
						style.type = 'text/css';
						style.media = 'print';
		
						if (style.styleSheet)
						{
							style.styleSheet.cssText = css;
						}
						else
						{
							style.appendChild(win.document.createTextNode(css));
						}
		
						head.appendChild(style);
                }, 
            },
			{
                extend: 'collection',
                text: 'Cetak Barcode',
                buttons: [
                    {
                        text: '<i class="fa fa-grip-lines"></i>&nbsp;&nbsp; Cetak Barcode yg dipilih!',
                        action: function ( e, dt, node, config ) {
                            cetak_barcode_group('chcekbox');
                        }
                    }, {
                        text: '<i class="fa fa-grip-lines"></i>&nbsp;&nbsp; Cetak Barcode Batang yg dipilih!',
                        action: function ( e, dt, node, config ) {
                            cetak_barcode_batang();
                        }
                    },
                ]
            }
        ],
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
			{ data: "cetak", name: "cetak", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "aset_barcode", name: "aset_barcode" },
            { data: "aset_nama", name: "aset_nama" },
            { data: "asetjenis_nama", name: "asetjenis_nama" },
            { data: "nm_bagian", name: "nm_bagian" },
            { data: "lokasi_nama", name: "lokasi_nama" },
            { data: "asetstatus_nama", name: "asetstatus_nama" },
            { data: "aset_status", name: "aset_status" },
            { data: "aset_updated", name: "aset_updated" },
        ],
		"order":[9, 'desc'],
	});
</script>