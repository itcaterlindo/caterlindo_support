@php
/* --Insert setting property form-- */
$form_id = 'idFormInput'; 
/** If slug is edit*/
if (!empty($id)){
	extract($row);
}

@endphp
<form id="{{ $form_id }}" action="{{ route('LokasiAset.store') }}" class="form-horizontal">
@method('POST')
@csrf
{{-- hidden params --}}
<input type="hidden" name="id" id="idid" placeholder="idid" value="{{ isset($id) ? $id : '' }}">
<input type="hidden" name="sts" id="idsts" placeholder="idsts" value="{{ isset($sts) ? $sts : '' }}">
<input type="hidden" name="kd_unit" id="idkd_unit" placeholder="idkd_unit" value="{{ isset($kd_unit) ? $kd_unit : '' }}">

<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label for="idkd_bagian" class="col-md-2 col-form-label">Bagian</label>
			<div class="col-sm-4 col-xs-12">
				<select name="kd_bagian" class="form-control form-control-sm" id="idkd_bagian">
					@if(isset($kd_bagian))
					<option value="{{ $kd_bagian }}" selected>{{ $nm_bagian }}</option>
					@endif
				</select>
			</div>	
		</div>

		<div class="form-group row">
			<label for="idlokasi_nama" class="col-md-2 col-form-label">Nama Lokasi</label>
			<div class="col-sm-5 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="lokasi_nama" id="idlokasi_nama" placeholder="Nama" value="{{ isset($lokasi_nama) ? $lokasi_nama : '' }}" >
			</div>	
			<label for="idlokasi_kode" class="col-md-1 col-form-label">Kode</label>
			<div class="col-sm-2 col-xs-12">
				<input type="text" class="form-control form-control-sm" name="lokasi_kode" id="idlokasi_kode" placeholder="Kode" value="{{ isset($lokasi_kode) ? $lokasi_kode : '' }}" >
			</div>	
		</div>
		<div class="form-group row">
			<label for="idlokasi_keterangan" class="col-md-2 col-form-label">Keterangan</label>
			<div class="col-sm-8 col-xs-12">
				<textarea name="lokasi_keterangan" id="idlokasi_keterangan" class="form-control form-control-sm" rows="2" placeholder="Keterangan">{{ isset($lokasi_keterangan) ? $lokasi_keterangan : '' }}</textarea>
			</div>	
		</div>

		<div class="form-group row">
			<div class="col-sm-4 offset-md-2 col-xs-12">
				<button type="reset" name="btnReset" class="btn btn-default btn-flat">
					<i class="fas fa-sync-alt"></i> Reset
				</button>
			<button type="submit" name="btnSubmit" id="idbtnSubmit{{ $form_id }}" onclick="submitData('{{ $form_id }}')" class="btn btn-info">
					<i class="fas fa-save"></i> Simpan
				</button>
			</div>
		</div>
	
	</div>
</div>

</form>