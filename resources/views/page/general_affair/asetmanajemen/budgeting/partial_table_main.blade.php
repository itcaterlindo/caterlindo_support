<style type="text/css">
	td.dt-center { text-align: center; }
	td.dt-right { text-align: right; }
	td.dt-left { text-align: left; }
</style>
<div class="card-body table-responsive p-0">
	<table id="idTable" class="table table-bordered table-striped table-hover display responsive" style="width:100%;">
		<thead>
		<tr>
			<th style="width:1%; text-align:center;" class="all">No.</th>
			<th style="width:4%; text-align:center;" class="all">Opsi</th>
			<th style="width:20%; text-align:center;">Kode Budgeting</th>
			<th style="width:5%; text-align:center;">Divisi</th>
			<th style="width:5%; text-align:center;">Tahun</th>
			<th style="width:5%; text-align:center;">Status</th>
			<th style="width:5%; text-align:center;">Jml Detail</th>
			<th style="width:20%; text-align:center;">Ket</th>
			<th style="width:9%; text-align:center;">Tgl Input</th>
		</tr>
		</thead>
	</table>
</div>
<script type="text/javascript">
var newArray = data_bagian.filter(function (el) {
						if (el.id == 11){
							return el.text;
						}
					});
	console.log(newArray)
	var table = $('#idTable').DataTable({
		"processing": true,
		"serverSide": true,
		"ordering" : true,
		"order": [[4, 'desc']],
		"ajax": "{{ url('general_affair/asetmanajemen/budgeting/table_data') }}",
		"language" : {
			"lengthMenu" : "Tampilkan _MENU_ data",
			"zeroRecords" : "Maaf tidak ada data yang ditampilkan",
			"info" : "Menampilkan data _START_ sampai _END_ dari _TOTAL_ data",
			"infoFiltered": "",
			"infoEmpty" : "Tidak ada data yang ditampilkan",
			"search" : "Cari :",
			"loadingRecords": "Memuat Data...",
			"processing":     "Sedang Memproses...",
			"paginate": {
				"first":      '<span class="fas fa-fast-backward"></span>',
				"last":       '<span class="fas fa-fast-forward"></span>',
				"next":       '<span class="fas fa-forward"></span>',
				"previous":   '<span class="fas fa-backward"></span>'
			}
		},
        "columns": [
            { data: "DT_RowIndex", name: "DT_RowIndex", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "opsi", name: "opsi", className: "dt-center", orderable: "false", searchable: "false" },
            { data: "kode_budgeting", name: "kode_budgeting" },
			{ data: "id_bagian",
				render: function(data, type, row) {
					var isi = "";
					var newArray = data_bagian.filter(function (el) {
						if (el.id == data){
							isi = el.text;
							return el.text;
						}
					});

					return isi;
						
				},  searchable: "true" 
			},
			{ data: "th_anggaran", name: "th_anggaran" },
			{ data: "status",
 
 				render: function(data, type, row) {

					if(data == 'pending') {
							
						return '<span class="badge badge-primary">Pending</span>';
						
					}else if(data == 'approve'){

						return '<span class="badge badge-secondary">Approve</span>';
							
					}else if(data == 'check'){

						return '<span class="badge badge-success">Check</span>';
							
					}
				}
	  		},
			{ data: "jml_detail",

			render: function(data, type, row) {
							
						return '<span class="badge badge-secondary"><h5>'+data+'</h5></span>';	
				}
			},
			{ data: "keterangan", name: "keterangan" },
			{ data: "tgl_input", name: "tgl_input" },
        ],
		'columnDefs': [
		{
				"targets": 0, // your case first column
				"className": "text-center",
				"width": "4%"
		},
		{
				"targets": 2,
				"className": "text-center",
				"searchable": "true"
		},
		{
				"targets": 3,
				"className": "text-center",
				"searchable": "true" 
		},
		{
				"targets": 4,
				"className": "text-center",
				"searchable": "true" 
		}
		,{
				"targets": 5,
				"className": "text-center",
				"searchable": "true" 
		},{
				"targets": 6,
				"className": "text-center",
				"searchable": "true" 
		}
		]
	});
</script>