@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php
    $master_var = 'Master_Budgeting';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
    $data['class_link'] = $class_link;
@endphp

@section('content')
<div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
        </div>
        <div class="card-body">
            <form>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Bagian</label>
                <div class="col-sm-10">
                  <input type="hidden" id="kd_bgd">
                <select name="bagian_id" class="form-control form-control-sm select2" id="bagian_id"> </select> 
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan">
                </div>
              </div>
             <div class="form-group row">
              <label for="opname_lokasi" class="col-md-2 col-form-label">Tahun anggaran</label>
              <div class="col-sm-4 col-xs-12">
                <select name="th_anggaran" class="form-control form-control-sm select2" id="th_anggaran">
                  <option value="2018">2018</option>
                  <option value="2019">2019</option>       
                  <option value="2020">2020</option>       
                  <option value="2021">2021</option>                
                  <option value="2022">2022</option>       
                  <option value="2023">2023</option>       
                  <option value="2024">2024</option>       
                  <option value="2025">2025</option>       
                  <option value="2026">2026</option>       
                  <option value="2027">2027</option>       
                  <option value="2028">2028</option>       
                  <option value="2029">2029</option>       
                  <option value="2030">2030</option>       
                </select>
              </div>
             </div>
             
              <div class="form-group row">
                <div class="col-sm-10 offset-sm-2">
                  <button type="button" id="save" class="btn btn-primary"> Save </button>
                </div>
              </div>
            </form>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
    </div>
</div>

<div class="row" id="{{ $data['idBox'] }}">
  <div class="col-12">
    <!-- Default box -->
    <div class="card card-outline card-info">
      <div class="card-header">
        <h3 class="card-title">{{ ucwords(str_replace('_', ' ', 'Data_Master_Budgeting')) }}</h3>          
        <div class="card-tools">
          @if (auth()->user()->can('KATEGORI_CREATE')) 
          <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
            <i class="fas fa-plus-circle"></i></button>
          @endif
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <div class="card-body">
       
          <div id="{{ $data['idBoxContent'] }}"></div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->
    <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
      <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
    </div>
  </div>
</div>


<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

@section('scriptJS')
  @include('page/'.$class_link.'/index_js', $data)
@append

