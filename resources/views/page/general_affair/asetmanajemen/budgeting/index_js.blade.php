<script>
        open_table();
        render_bagianAset();
        vopen();
        var data_bagian = [];
        
        $('#save').click(function (e) { 
            e.preventDefault();
            var items = {bagian_id : $('#bagian_id').val(), th_anggaran : $('#th_anggaran').val(), keterangan : $('#keterangan').val(), kd_bgd : $('#kd_bgd').val()};
            submit(items);
        });

        function submit (form_id) {
            console.log(form_id)
            var url = "{{ url('general_affair/asetmanajemen/budgeting/add') }}?bagian_id="+form_id.bagian_id+"&th_anggaran="+form_id.th_anggaran+"&keterangan="+form_id.keterangan+"&kd_bgd="+form_id.kd_bgd;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        window.location.href = "{{ url('general_affair/asetmanajemen/budgeting/detail') }}?kode_budgeting="+data.datax.kode_budgeting;
                       
                        open_table();
                    }else if(data.code == 900){
                        $('#kd_bgd').val("");
                        $("#keterangan").val("");
                        sweetalert2 ('success', data.messages);
                        location.reload();
                        open_table();
                    }
                    else{
                        sweetalert2 ('error', data.messages);
                    }
                } 	        
            });
        }
        function vopen(){
            $.ajax({
                    method: "GET",
                    url: "{{ url('api/hrm/bagian/v1/asetSelect2real') }}",
                    headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
                    success: function (response) {
                        data_bagian = response;
                    }
            });

            var date = new Date(); 
            var yrz = date.getFullYear() + 1;

            if($('#kd_bgd').val() == ""){
                $('#th_anggaran').append(' <option value="'+yrz+'" selected>'+yrz+'</option>  ');
            }
            // alert(yrz)
            

        }
        function dulplicate_data(param) { 
            $.ajax({
                    method: "GET",
                    url: "{{ url('general_affair/asetmanajemen/budgeting/getall') }}?kode_budgeting="+param,
                    success: function (data) {
                    if(data.code == 200){
                        console.log(data)
                        sweetalert2 ('success', data.messages);
                        //location.reload();
                        open_table();
                    }
                    else{
                        sweetalert2 ('error', data.messages);
                    }
                    }
            });
         }
        function open_data(param) {     
            window.location.href = "{{ url('general_affair/asetmanajemen/budgeting/detail') }}?kode_budgeting="+param;
         }
        function moveTo(div_id) {
            $('html, body').animate({ scrollTop: $('#'+div_id).offset().top - $('header').height() }, 1000);
        }

        function open_table() {
            $('#<?php echo $idBoxContent; ?>').slideUp(function(){
                $.ajax({
                    type: 'GET',
                    url: '{{ url("general_affair/asetmanajemen/budgeting/partial_table_main") }}',
                    success: function(html) {
                        $('#{{ $idBoxContent }}').html(html);
                        $('#{{ $idBoxContent }}').slideDown();
                        moveTo('idMainContent');
                    }
                });
            });
        }

        function edit_data(kode, id, tahun, ket) { 
            var idxx = "";
            var val = "";
					var newArray = data_bagian.filter(function (el) {
						if (el.id == id){
							idxx = el.id;
                            val = el.text;
							return el.text;
						}
					});
            //$("#bagian_id").select2("val", id);

            $('#kd_bgd').val(kode);
            $("#bagian_id").append("<option value=" + id + " selected> " + val + "</option>" );
            $("#th_anggaran").append("<option value=" + tahun + " selected>" + tahun + "</option>" );
            $("#keterangan").val(ket);
         }

        function delete_data(items){
            var url = "{{ url('general_affair/asetmanajemen/lelang/tag/delete') }}?id_tag="+items;
            $.ajax({
                url: url ,
                type: "GET",
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data.code == 200){
                        // open_table();
                        sweetalert2 ('success', data.messages);
                        open_table();
                    }else{
                        sweetalert2 ('error', 'Unknown Error');
                    }
                } 	        
            });
        }

        function render_bagianAset() {
            $("#bagian_id").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0,
                ajax: {
                    url: "{{ url('api/hrm/bagian/v1/asetSelect2real') }}",
                    dataType: 'json',
                    headers: {'Authorization' : '{{ env("APP_API_TOKEN") }}'},
                    delay: 250,
                    data: function (params) {
                        console.log(params)
                        return {
                            paramSearch: params.term
                        };
                    },
                    processResults: function (response) {
                        console.log(response)
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#th_anggaran").select2({
                placeholder: '-- Cari Opsi --',
                minimumInputLength: 0
            });

           
	    }

        function sweetalert2 (type, title) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });

            // TYPE : success, info, error, warning
            Toast.fire({
                type: type,
                title: title
            })
        }
</script>