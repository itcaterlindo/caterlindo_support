@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.datatables_buttons')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php
    $master_var = 'suhu_kelembapan_report';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
@endphp

@section('content')
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
          <div class="card-tools">
            {{-- <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
              <i class="fas fa-plus-circle"></i></button> --}}
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-2">
                    <input type="number" class="form-control form-control-sm" id="idtahun" placeholder="Tahun" value="{{ date('Y') }}">
                </div>
                <div class="col-2">
                    <select class="form-control form-control-sm" id="idbulan">
                        @foreach ($opsiBulan as $bulan)
                            <option 
                            @if ($bulan['id'] == date('m'))
                                selected="selected"
                            @endif
                            value="{{ $bulan['id'] }}">{{ $bulan['text'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3">
                    <div class="btn-group">
                        <button  class="btn btn-info btn-sm" onclick="report_table()"> <i class="fas fa-search"></i> Tampilkan</button>
                        <button  class="btn btn-success btn-sm" onclick="temphumidityreport_object()"> <i class="fas fa-download"></i> Pdf</button>
                    </div>
                </div>
            </div>
            <hr>

            <div id="{{ $data['idBoxContent'] }}">              
          </div>    
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

@section('scriptJS')
<script type="text/javascript">

    function temphumidityreport_object () {
        event.preventDefault();
        var tahun = $('#idtahun').val();
        var bulan = $('#idbulan').val();
        $.ajax({
			type: "GET",
			url: "{{ url($class_link.'/temphumidityreport_object') }}",
            data: {tahun: tahun, bulan: bulan},
			success: function(html) {
                console.log(html);
				$('#{{ $data['idBoxContent'] }}').html(html);
			}
		});
    }
    function report_table() {
        event.preventDefault();
        var tahun = $('#idtahun').val();
        var bulan = $('#idbulan').val();
        $.ajax({
			type: "GET",
			url: "{{ url($class_link.'/temphumidityreport_table') }}",
            data: {tahun: tahun, bulan: bulan},
			success: function(html) {
				$('#{{ $data['idBoxContent'] }}').html(html);
			}
		});
    }

</script>
@append

