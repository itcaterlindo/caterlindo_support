@php
    $carbon = new \Carbon\Carbon;
    $dates = []; 
    $daysMonth = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

    for($i=1; $i < $daysMonth+1; $i++) {
        $dates[] = $carbon->createFromDate($tahun, $bulan, $i)->format('Y-m-d');
    }
@endphp

<table  id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 100%;" border="1" width="100%">
    <thead>
        <tr>
            <td width="5%" rowspan="2" style="text-align:center; font-weight:bold;">NO</td>
            <td width="12%" rowspan="2" style="text-align:center; font-weight:bold;">HARI</td>
            <td width="18%" rowspan="2" style="text-align:center; font-weight:bold;">TANGGAL</td>
            <td width="20%" colspan="2" style="text-align:center; font-weight:bold;">SUHU (&#8451;)</td>
            <td width="20%" colspan="2" style="text-align:center; font-weight:bold;">KELEMBAPAN (%)</td>
            <td width="25%" rowspan="2" style="text-align:center; font-weight:bold;">KETERANGAN</td>
        </tr>
        <tr>
            <td style="text-align:center; font-weight:bold; font-size: 70%;">Pagi <br> (07:00-09:00)</td>
            <td style="text-align:center; font-weight:bold; font-size: 70%;">Siang <br> (12:00-14:00)</td>
            <td style="text-align:center; font-weight:bold; font-size: 70%;">Pagi <br> (07:00-09:00)</td>
            <td style="text-align:center; font-weight:bold; font-size: 70%;">Siang <br> (12:00-14:00)</td>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($dates as $date) 
            @php 
            $avg_tempPagi = null; $avg_humidityPagi = null; 
            $avg_tempSiang = null; $avg_humiditySiang = null; 
            @endphp
            @foreach ($avgMonthlyPagi as $rPagi)
                @if ($rPagi->day == $date)
                    @php
                        $avg_tempPagi = $rPagi->avg_temp;
                        $avg_humidityPagi = $rPagi->avg_humidity;
                    @endphp
                @endif
            @endforeach
            @foreach ($avgMonthlySiang as $rSiang)
                @if ($rSiang->day == $date)
                    @php
                        $avg_tempSiang = $rSiang->avg_temp;
                        $avg_humiditySiang = $rSiang->avg_humidity;
                    @endphp
                @endif
            @endforeach
            <tr>
                <td width="5%" style="text-align:center; ">{{ $no }}</td>
                <td width="12%">
                    @switch(date('l', strtotime($date)))
                        @case('Sunday')
                            Minggu
                            @break
                        @case('Monday')
                            Senin
                            @break
                        @case('Tuesday')
                            Selasa
                            @break
                        @case('Wednesday')
                            Rabu
                            @break
                        @case('Thursday')
                            Kamis
                            @break
                        @case('Friday')
                            Jum`at
                            @break
                        @case('Saturday')
                            Sabtu
                            @break
                    @endswitch
                </td>
                <td width="18%">{{ date('d F Y', strtotime($date)) }}</td>
                <td width="10%" style="text-align:center; ">{!! !empty($avg_tempPagi) ? number_format($avg_tempPagi,2).' &#8451;' : null !!} </td>
                <td width="10%" style="text-align:center; ">{!! !empty($avg_tempSiang) ? number_format($avg_tempSiang,2).' &#8451;' : null !!} </td>
                <td width="10%" style="text-align:center; ">{!! !empty($avg_humidityPagi) ? number_format($avg_humidityPagi,2).' %' : null !!} </td>
                <td width="10%" style="text-align:center; ">{!! !empty($avg_humiditySiang) ? number_format($avg_humiditySiang,2).' %' : null !!} </td>
                <td width="25%" style="text-align:left; ">By system</td>
            </tr>
            @php
            $no++
            @endphp
            @php 
            $arrTempTotalAvgPagi[] = round($avg_tempPagi, 2); 
            $arrTempTotalAvgSiang[] = round($avg_tempSiang, 2); 
            $arrTempHumidityAvgPagi[] = round($avg_humidityPagi, 2); 
            $arrTempHumidityAvgSiang[] = round($avg_humiditySiang, 2); 
            @endphp
        @endforeach
        @php 
        $arrTempTotalAvgPagi = array_filter($arrTempTotalAvgPagi); 
        $arrTempTotalAvgSiang = array_filter($arrTempTotalAvgSiang); 
        $arrTempHumidityAvgPagi = array_filter($arrTempHumidityAvgPagi);
        $arrTempHumidityAvgSiang = array_filter($arrTempHumidityAvgSiang); 

        $avgTempPagiResult = array_sum($arrTempTotalAvgPagi) / count($arrTempTotalAvgPagi);
        $avgTempSiangResult = array_sum($arrTempTotalAvgSiang) / count($arrTempTotalAvgSiang);
        $avgHimidityPagiResult = array_sum($arrTempHumidityAvgPagi) / count($arrTempHumidityAvgPagi);
        $avgHimiditySiangResult = array_sum($arrTempHumidityAvgSiang) / count($arrTempHumidityAvgSiang);

        $resultBulanTemp = ( round($avgTempPagiResult, 2) + round($avgTempSiangResult, 2) ) / 2; 
        $resultBulanHumidity = (round($avgHimidityPagiResult, 2) + round($avgHimiditySiangResult, 2)) / 2;
        
        @endphp
        <tr>
            <td width="5%" style="text-align:center; "></td>
            <td width="12%"></td>
            <td width="18%" style="text-align:right; font-weight: bold;">Rata - rata :</td>
            <td width="10%" style="text-align:center; font-weight: bold;">{!! !empty($avgTempPagiResult) ? number_format($avgTempPagiResult,2).' &#8451;' : null !!} </td>
            <td width="10%" style="text-align:center; font-weight: bold;">{!! !empty($avgTempSiangResult) ? number_format($avgTempSiangResult,2).' &#8451;' : null !!} </td>
            <td width="10%" style="text-align:center; font-weight: bold;">{!! !empty($avgHimidityPagiResult) ? number_format($avgHimidityPagiResult,2).' %' : null !!} </td>
            <td width="10%" style="text-align:center; font-weight: bold;">{!! !empty($avgHimiditySiangResult) ? number_format($avgHimiditySiangResult,2).' %' : null !!} </td>
            <td width="25%" style="text-align:left; "></td>
        </tr>
        <tr>
            <td width="5%" style="text-align:center; "></td>
            <td width="12%"></td>
            <td width="18%" style="text-align:right; font-weight: bold;">Rata - rata bulan ini:</td>
            <td width="20%" colspan="2" style="text-align:center; font-weight: bold;">{!! !empty($resultBulanTemp) ? number_format($resultBulanTemp,2).' &#8451;' : null !!} </td>
            <td width="20%" colspan="2" style="text-align:center; font-weight: bold;">{!! !empty($resultBulanHumidity) ? number_format($resultBulanHumidity,2).' %' : null !!} </td>
            <td width="25%" style="text-align:left; "></td>
        </tr>
    </tbody>
</table>
<p>Keterangan : </p>
<p>1. Suhu rata-rata yang disarankan : 18-28  &#8451;</p>
<p>2. Kelembapan rata-rata yang disarankan :  40-60 %</p>
<br>
<table  id="idtablemain" cellspacing="0" cellpadding="1" style="font-size: 100%;" border="0" width="100%">
    <tr>
        <td style="text-align: center;">Dibuat oleh,</td>
        <td></td>
        <td style="text-align: center;"></td>
        <td></td>
        <td style="text-align: center;">Diketahui oleh,</td>
    </tr>
    <tr>
        <td height="50"></td>
        <td height="50"></td>
        <td height="50"></td>
        <td height="50"></td>
        <td height="50"></td>
    </tr>
    <tr>
        <td style="text-align: center;">______________</td>
        <td></td>
        <td style="text-align: center;"></td>
        <td></td>
        <td style="text-align: center;">______________</td>
    </tr>
    <tr>
        <td style="text-align: center;">IT Staff</td>
        <td></td>
        <td style="text-align: center;"></td>
        <td></td>
        <td style="text-align: center;">Legal & GA</td>
    </tr>
</table>