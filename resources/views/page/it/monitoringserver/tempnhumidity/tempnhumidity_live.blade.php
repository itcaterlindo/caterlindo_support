@extends('layouts.app')

@section('additionalStyle')
    @include('layouts.plugins.datatables')
    @include('layouts.plugins.datatables_buttons')
    @include('layouts.plugins.select2')
    @include('layouts.plugins.sweetalert2')
@endsection

@php
    $master_var = 'suhu_&_kelembapan_live';
    $data['idBox'] = 'idBox'.$master_var;
    $data['idBoxContent'] = 'idBoxContent'.$master_var;
    $data['idBoxLoader'] = 'idBoxLoader'.$master_var;
    $data['idBoxOverlay'] = 'idBoxOverlay'.$master_var;
    $data['idBtnAdd'] = 'idBtnAdd'.$master_var;

    $data['idModal'] = 'idModal'.$master_var;
    $data['idModalContent'] = 'idModalContent'.$master_var;

    /**Additional Variable */
@endphp

@section('content')
<div class="row" id="{{ $data['idBox'] }}">
    <div class="col-12">
      <!-- Default box -->
      <div class="card card-outline card-info">
        <div class="card-header">
          <h3 class="card-title">{{ ucwords(str_replace('_', ' ', $master_var)) }}</h3>          
          <div class="card-tools">
            {{-- <button type="button" class="btn btn-tool" id="{{ $data['idBtnAdd'] }}" data-toggle="tooltip" title="Tambah Data">
              <i class="fas fa-plus-circle"></i></button> --}}
            @if (auth()->user()->can('TEMPNHUMIDITY_VIEWREPORT')) 
            <button class="btn btn-tool btn-default btn-sm" onclick="window.location.replace('{{ url($class_link.'/report') }}')"> <i class="fas fa-book"></i> Report </button>
            @endif
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
            <div id="{{ $data['idBoxContent'] }}"></div>

            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
              <h3>Themperature & Humidity Server</h3>
              <p class="lead">Report Suhu dan Kelembapan Ruang Server</p>
            </div>

            <div class="row">
              <div class="col-12 col-sm-6 col-md-4">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-thermometer-half"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text"> <h4> Last Update </h4>  </span>
                    <span class="info-box-number">
                      {{ $maxid->tempserver_temp }} &#8451; <br>
                      {{ $maxid->tempserver_humidity }} % <br>
                      <small><?php echo date('d-M-Y H:i:s', strtotime($maxid->tempserver_created_at)) ?> </small>
                    </span>
                  </div>
                </div>
              </div>
  
              <div class="col-12 col-sm-6 col-md-4">
                <div class="info-box">
                  <span class="info-box-icon bg-default elevation-1"><i class="fas fa-thermometer-half"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text"> <h4> Pagi </h4> Rata-rata (07:00 - 09:00) </span>
                    <span class="info-box-number">
                      {{ number_format($avgMonthlyPagi->avg_temp,2) }} &#8451; <br>
                      {{ number_format($avgMonthlyPagi->avg_humidity, 2) }} % <br>
                      <small><?php echo date('F Y', strtotime($maxid->tempserver_created_at)) ?> </small>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-6 col-md-4">
                <div class="info-box">
                  <span class="info-box-icon bg-default elevation-1"><i class="fas fa-thermometer-half"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text"> <h4> Siang </h4> Rata-rata (12:00 - 14:00) </span>
                    <span class="info-box-number">
                      {{ number_format($avgMonthlySiang->avg_temp,2) }} &#8451; <br>
                      {{ number_format($avgMonthlySiang->avg_humidity, 2) }} % <br>
                      <small><?php echo date('F Y', strtotime($maxid->tempserver_created_at)) ?> </small>
                    </span>
                  </div>
                </div>
              </div>

            </div>
    
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
      <div class="overlay" id="{{ $data['idBoxOverlay'] }}" style="display: none;">
        <i class="fas fa-spinner fa-pulse" style="color:#31708f;"></i>
      </div>
    </div>
</div>

<div class="modal fade" id="{{ $data['idModal'] }}">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Extra Large Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="{{ $data['idModalContent'] }}"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

{{-- @section('scriptJS')
  @include('page/'.$class_link.'/hasilperiode_js', $data)
@append --}}

