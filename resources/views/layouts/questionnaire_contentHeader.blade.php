<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ ucwords(Request::segment(1)) }}</h1>
        </div>
        <div class="col-sm-6">
            <span class="badge bg-warning float-sm-right" id="idtimer" style="font-size: 120%;"> </span>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
