 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ route('home') }}" class="brand-link">
    <img src="{{ asset('dist/img/favicon.ico') }}"
         alt="AdminLTE Logo"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">{{ str_replace('_',' ',config('app.name', 'Laravel')) }}</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('dist/img/user.png') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="javascript:void(0);" class="d-block">{{ ucwords(Auth::user()->name) }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @if (auth()->user()->hasRole('ADMINISTRATOR')) 
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-cogs"></i>
            <p>
              Setting
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ url('setting/user') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>User</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('setting/role') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Role</p>
              </a>
            </li>
          </ul>
        </li>
        @endif

        @if (auth()->user()->can('SUPPORT_ROLE'))
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-ambulance"></i>
            <p>
              Support
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">

            @if (auth()->user()->can('KUESIONER_ROLE'))
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="fas fa-check-double nav-icon"></i>
                <p>
                  Kuesioner
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

                <li class="nav-item">
                  @if (auth()->user()->can('KATEGORI_VIEW')) 
                  <a href="#" class="nav-link">
                    <i class="fa fa-cog nav-icon"></i>
                    <p>Setting Kuesioner
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  @endif
                  <ul class="nav nav-treeview">
                    @if (auth()->user()->can('KATEGORI_VIEW')) 
                    <li class="nav-item">
                      <a href="{{ route('CategoryQuestion.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Kategori</p>
                      </a>
                    </li>
                    @endif
                  </ul>
                </li> 
                @if (auth()->user()->can('KUESIONER_VIEW')) 
                <li class="nav-item">
                  <a href="{{ route('ListQuestionnaire.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>List Kuesioner</p>
                  </a>
                </li>
                @endif
                @if (auth()->user()->can('PERIODE_VIEW')) 
                <li class="nav-item">
                  <a href="{{ route('PriodeQuestionnaire.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Periode Kuesioner</p>
                  </a>
                </li>
                @endif
                @if (auth()->user()->can('RESULTKUESIONER_VIEW'))
                <li class="nav-item">
                  <a href="{{ route('ResultQuestionnaire.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Result Kuesioner</p>
                  </a>
                </li>
                @endif

                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="fa fa-book nav-icon"></i>
                    <p>Report Kuesioner
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    @if (auth()->user()->can('REPORTKUESIONERKARYAWAN_VIEW')) 
                    <li class="nav-item">
                      <a href="{{ route('ReportKaryawankuesioner.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Karyawan Input</p>
                      </a>
                    </li>
                    @endif
                    @if (auth()->user()->can('REPORTKUESIONERPERIODE_VIEW')) 
                    <li class="nav-item">
                      <a href="{{ route('ReportHasilPeriode.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hasil Per-Periode</p>
                      </a>
                    </li>
                    @endif
                    @if (auth()->user()->can('REPORTKUESIONERPERKARYAWAN_VIEW')) 
                    <li class="nav-item">
                      <a href="{{ route('ReportHasilKaryawan.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hasil Per-Karyawan</p>
                      </a>
                    </li>
                    @endif
                  </ul>
                </li>            
    
              </ul>
            </li>
            @endif
            
          </ul>
        </li>
        @endif

        @if (auth()->user()->can('IT_ROLE')) 
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-microchip"></i>
            <p>
              IT
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-desktop"></i>
                <p>Monitoring Server
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
            
              <ul class="nav nav-treeview">
                @if (auth()->user()->can('TEMPNHUMIDITY_VIEW')) 
                <li class="nav-item">
                  <a href="{{ route('Tempserver.live') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Suhu & Humidity</p>
                  </a>
                </li>
                @endif
              </ul>
            </li>

          </ul>
        </li>
        @endif

        @if (auth()->user()->can('GENERALAFFAIR_ROLE'))
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-toolbox"></i>
            <p>
              General Affair
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            @if (auth()->user()->can('MANAJEMENASSET_ROLE'))
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-clipboard-list"></i>
                  <p>Manajemen Aset
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
              
                <ul class="nav nav-treeview">
                  @if (auth()->user()->can('SETTING_ASSET'))
                  <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fa-cog"></i>
                      <p>Setting Aset
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                  
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ route('LokasiAset.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Lokasi Aset</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('JenisAset.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Jenis Aset</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('MutasiJenisAset.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Jenis Mutasi Aset</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  @endif
                <li class="nav-item">
                  <a href="{{ route('AsetData.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Data Aset</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('AsetMutasi.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Mutasi Aset</p>
                  </a>
                </li>
                @if (auth()->user()->can('STOKTAKE_ASSET')) 

                <li class="nav-item">
                  <a href="{{ route('AsetStokOpname.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Stok Opname Aset</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="fa fa-book nav-icon"></i>
                    <p>Report Aset
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ route('ReportOpnameAset.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hasil Opname</p>
                      </a>
                    </li>
                  </ul>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ route('comparestokopname.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Compare Opname Per - Periode</p>
                      </a>
                    </li>
                  </ul>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="{{ route('reportmutasiaset.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Report Mutasi Aset</p>
                      </a>
                    </li>
                  </ul>
                </li>
                @endif
                @if (auth()->user()->can('TIKET_ASET')) 
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-ticket-alt"></i>
                    <p>
                      Tickets
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  
                  <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                      <a href="{{ route('alltickets.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-tasks"></i>
                        <p>All Tickets
                        </p>
                      </a>
                    </li>  
        
                  </ul>
                  <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                      <a href="{{ route('reportticket.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-address-book"></i>
                        <p>Report Tickets
                        </p>
                      </a>
                    </li>  
        
                  </ul>
                  
                </li>
                @endif
                @if (auth()->user()->can('IS_ADMIN_LELANG')) 
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-money-check-alt"></i>
                    <p>
                      Lelang
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                      <a href="{{ route('tag.lelang.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-ellipsis-h"></i>
                        <p>Kategori/Tag Lelang
                        </p>
                      </a>
                    </li>  
        
                  </ul>
                  
                  <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                      <a href="{{ route('lelang.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>Barang Lelang
                        </p>
                      </a>
                    </li>  
        
                  </ul>
                  <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                      <a href="{{ route('report.lelang.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-solar-panel"></i>
                        <p>Report Lelang
                        </p>
                      </a>
                    </li>  
        
                  </ul>
                  
                </li>
                @endif
                </ul>
              </li>
            @endif
          </ul>
        </li>    
        @endif

        
        @if (auth()->user()->can('BUDGETING_ROLE'))   
      <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-microchip"></i>
            <p>
              Budgeting
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
          @if (auth()->user()->can('CTG_BUDGETING'))   
            <li class="nav-item has-treeview">
              <a href="{{ route('budgeting.ctg.index') }}" class="nav-link">
                <i class="nav-icon fas fa-bars"></i>
                <p>Category Barang Budgeting
                </p>
              </a>
            </li>

            <li class="nav-item has-treeview">
              <a href="{{ route('reportbudgeting.index') }}" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>Report Budgeting by PR
                </p>
              </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="{{ route('reportbudgetingctg.index') }}" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>Report Budgeting by CTGRY
                </p>
              </a>
            </li>
            @endif
            <li class="nav-item has-treeview">
              <a href="{{ route('budgeting.index') }}" class="nav-link">
                <i class="nav-icon fas fa-calendar"></i>
                <p>Data Budgeting
                </p>
              </a>
            </li>

          </ul>
      </li>
        @endif
        <li class="nav-header">ADDITIONAL</li>
        <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link" onclick="
            var conf = confirm('Apakah anda yakin ?'); 
            if (conf) {
              event.preventDefault();
              document.getElementById('logout-form').submit();
            }">
            <i class="nav-icon far fa-circle text-danger"></i>
            <p class="text">Log Out</p>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>