<!-- daterange picker -->
<link href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@section('additionalScript')
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    
@append
