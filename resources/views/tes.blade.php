@extends('layouts.app')

@php
    // $master_var = 
    // $data[]    
@endphp

@section('content')
    <div class="row">
      <div class="col-12">
        <!-- Default box -->
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title">Selamat Datang, <strong> {{ auth()->user()->name }} </strong></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button> --}}
            </div>
          </div>
          <div class="card-body">
            Dashboard Antrian Pendaftaran RS KH Abdurrahman Syamsuri
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
      </div>
    </div>
@endsection