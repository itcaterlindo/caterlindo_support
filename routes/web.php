<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

#Support suhu server
Route::get('/tempserver', 'IT\MonitoringServer\TempserverController@index');

Route::prefix('support')->group(function() {
    Route::prefix('kuesioner')->group(function() {
        /** Questionnaire */
        Route::get('/portalkuesioner/encrPortalkuesioner/{kd_karyawan?}', 'Support\Questionnaire\PortalQuestionnaireController@encryptPortalQuestionnaire');
        Route::get('/portalkuesioner/{encryptKaryawan?}', 'Support\Questionnaire\PortalQuestionnaireController@index');
    
        /** Questionnaire */
        Route::get('/kuesioner/encryptKuesioner/{periode_id?}/{kd_karyawan?}', 'Support\Questionnaire\QuestionnaireController@encryptQuestionnaire');
        Route::get('/kuesioner/partial_form_main', 'Support\Questionnaire\QuestionnaireController@partial_form_main');
        Route::get('/kuesioner/{encryptPeriode?}/{encryptKaryawan?}', 'Support\Questionnaire\QuestionnaireController@index');
        Route::post('/kuesioner', 'Support\Questionnaire\QuestionnaireController@store')->name('Kuesioner.store');
    });
    Route::get('/getTicketActive', 'GeneralAffair\ManajemenAset\AllTicketController@allTicketActive');
});

Route::prefix('lelang')->group(function() {
    Route::prefix('penawaran')->group(function() {
        /** Questionnaire */
        Route::get('/{encryptKaryawan?}', 'GeneralAffair\ManajemenAset\FpenawaranController@index');
        Route::get('/encrLelang/{kd_karyawan?}', 'GeneralAffair\ManajemenAset\FpenawaranController@encryptLelang');
        Route::get('/{encryptKaryawan?}/search', 'GeneralAffair\ManajemenAset\FpenawaranController@search');
        Route::get('/{encryptKaryawan?}/partial_list_main', 'GeneralAffair\ManajemenAset\FpenawaranController@partial_list_main');
        Route::get('/{encryptKaryawan?}/open_view', 'GeneralAffair\ManajemenAset\FpenawaranController@open_view');
    
 
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {

    Route::prefix('setting')->group(function() {
        /** User */
        Route::get('/user', 'Setting\UserController@index')->name('User.index');
        Route::get('/user/partial_table_main', 'Setting\UserController@partial_table_main');
        Route::get('/user/table_data', 'Setting\UserController@table_data');
        Route::get('/user/partial_form_main', 'Setting\UserController@partial_form_main');
        Route::post('/user', 'Setting\UserController@store')->name('User.store');
        Route::post('/user/getlokasi', 'Setting\UserController@getLokasi')->name('User.Lokasi');
        Route::put('/user', 'Setting\UserController@destroy')->name('User.destroy');
        /** Role */
        Route::get('/role', 'Setting\RoleController@index')->name('Role.index');
        Route::get('/role/rolepermission', 'Setting\RoleController@rolepermission');
        Route::get('/role/rolepermission_tablemain', 'Setting\RoleController@rolepermission_tablemain');
        Route::post('/role/rolepermission_store', 'Setting\RoleController@rolepermission_store')->name('RolePermission.store');
        Route::put('/role/rolepermission_destroy', 'Setting\RoleController@rolepermission_destroy')->name('RolePermission.destroy');

        Route::get('/role/partial_table_main', 'Setting\RoleController@partial_table_main');
        Route::get('/role/table_data', 'Setting\RoleController@table_data');
        Route::get('/role/partial_form_main', 'Setting\RoleController@partial_form_main');
        Route::post('/role', 'Setting\RoleController@store')->name('Role.store');
        Route::put('/role', 'Setting\RoleController@destroy')->name('Role.destroy');
    });
    
    Route::prefix('support')->group(function() {
        # KUESIONER
        Route::prefix('kuesioner')->group(function() {

            Route::prefix('kategoripertanyaan')->group(function() {
                Route::get('/', 'Support\Questionnaire\CategoryQuestionController@index')->name('CategoryQuestion.index');
                Route::get('/partial_table_main', 'Support\Questionnaire\CategoryQuestionController@partial_table_main');
                Route::get('/table_data', 'Support\Questionnaire\CategoryQuestionController@table_data');
                Route::get('/partial_form_main', 'Support\Questionnaire\CategoryQuestionController@partial_form_main');
                Route::post('/', 'Support\Questionnaire\CategoryQuestionController@store')->name('CategoryQuestion.store');
                Route::delete('/', 'Support\Questionnaire\CategoryQuestionController@destroy')->name('CategoryQuestion.destroy');
            });

            Route::prefix('listkuesioner')->group(function() {
                Route::get('/', 'Support\Questionnaire\ListQuestionnaireController@index')->name('ListQuestionnaire.index');
                Route::get('/partial_table_main', 'Support\Questionnaire\ListQuestionnaireController@partial_table_main');
                Route::get('/table_data', 'Support\Questionnaire\ListQuestionnaireController@table_data');
                Route::get('/partial_form_main', 'Support\Questionnaire\ListQuestionnaireController@partial_form_main');
                Route::get('/preview', 'Support\Questionnaire\ListQuestionnaireController@preview');
                Route::get('/preview_main', 'Support\Questionnaire\ListQuestionnaireController@preview_main');
                Route::get('/preview_tablepoin', 'Support\Questionnaire\ListQuestionnaireController@preview_tablepoin');
                Route::post('/', 'Support\Questionnaire\ListQuestionnaireController@store')->name('ListQuestionnaire.store');
                Route::delete('/', 'Support\Questionnaire\ListQuestionnaireController@destroy')->name('ListQuestionnaire.destroy');
        
                Route::prefix('listpertanyaan')->group(function() {
                    Route::get('/partial_table_main', 'Support\Questionnaire\ListQuestionController@partial_table_main');
                    Route::get('/table_data', 'Support\Questionnaire\ListQuestionController@table_data');
                    Route::get('/partial_form_main', 'Support\Questionnaire\ListQuestionController@partial_form_main');
                    Route::get('/{q_id}', 'Support\Questionnaire\ListQuestionController@index');
                    Route::post('/', 'Support\Questionnaire\ListQuestionController@store')->name('ListQuestion.store');
                    Route::delete('/', 'Support\Questionnaire\ListQuestionController@destroy')->name('ListQuestion.destroy');
                });
        
                Route::prefix('listjawaban')->group(function() {
                    Route::get('/partial_table_main', 'Support\Questionnaire\ListAnswerController@partial_table_main');
                    Route::get('/table_data', 'Support\Questionnaire\ListAnswerController@table_data');
                    Route::get('/partial_form_main', 'Support\Questionnaire\ListAnswerController@partial_form_main');
                    Route::get('/{question_id}', 'Support\Questionnaire\ListAnswerController@index');
                    Route::post('/', 'Support\Questionnaire\ListAnswerController@store')->name('ListAnswer.store');
                    Route::delete('/', 'Support\Questionnaire\ListAnswerController@destroy')->name('ListAnswer.destroy');
                });
            });
            
            Route::prefix('periodekuesioner')->group(function() {
                Route::get('/', 'Support\Questionnaire\PeriodeQuestionnaireController@index')->name('PriodeQuestionnaire.index');
                Route::get('/partial_table_main', 'Support\Questionnaire\PeriodeQuestionnaireController@partial_table_main');
                Route::get('/table_data', 'Support\Questionnaire\PeriodeQuestionnaireController@table_data');
                Route::get('/partial_form_main', 'Support\Questionnaire\PeriodeQuestionnaireController@partial_form_main');
                Route::post('/', 'Support\Questionnaire\PeriodeQuestionnaireController@store')->name('PriodeQuestionnaire.store');
                Route::delete('/', 'Support\Questionnaire\PeriodeQuestionnaireController@destroy')->name('PriodeQuestionnaire.destroy');
            });
        
            Route::prefix('resultkuesioner')->group(function() {
                Route::get('/', 'Support\Questionnaire\ResultQuestionnaireController@index')->name('ResultQuestionnaire.index');
                Route::get('/partial_table_main', 'Support\Questionnaire\ResultQuestionnaireController@partial_table_main');
                Route::get('/table_data', 'Support\Questionnaire\ResultQuestionnaireController@table_data');
                Route::get('/partial_form_main', 'Support\Questionnaire\ResultQuestionnaireController@partial_form_main');
                Route::post('/', 'Support\Questionnaire\ResultQuestionnaireController@store')->name('ResultQuestionnaire.store');
                Route::delete('/', 'Support\Questionnaire\ResultQuestionnaireController@destroy')->name('ResultQuestionnaire.destroy');
            });

            Route::prefix('report')->group(function() {
                Route::get('/get_kuesioner', 'Support\Questionnaire\ReportQuestionnaireController@get_kuesioner');
                Route::get('/get_periode', 'Support\Questionnaire\ReportQuestionnaireController@get_periode');
                Route::get('/get_karyawan', 'Support\Questionnaire\ReportQuestionnaireController@get_karyawan');

                Route::prefix('karyawankuesioner')->group(function() {
                    Route::get('/', 'Support\Questionnaire\ReportQuestionnaireController@reportkaryawankuesioner_index')->name('ReportKaryawankuesioner.index');
                    Route::get('/reportkaryawankuesioner_table', 'Support\Questionnaire\ReportQuestionnaireController@reportkaryawankuesioner_table');
                });
                Route::prefix('reportperiodekuesioner')->group(function() {
                    Route::get('/', 'Support\Questionnaire\ReportQuestionnaireController@hasilperiode_index')->name('ReportHasilPeriode.index');
                    Route::get('/hasilperiode_table', 'Support\Questionnaire\ReportQuestionnaireController@hasilperiode_table');
                    
                });
                Route::prefix('reporthasilkaryawankuesioner')->group(function() {
                    Route::get('/', 'Support\Questionnaire\ReportQuestionnaireController@hasilkaryawan_index')->name('ReportHasilKaryawan.index');
                    Route::get('/hasilkaryawan_preview', 'Support\Questionnaire\ReportQuestionnaireController@hasilkaryawan_preview');
                });
            });
        });
    }); 
    
    Route::prefix('it')->group(function() {
        Route::prefix('monitoringserver')->group(function() {
            Route::get('/tempnhumidity', 'IT\MonitoringServer\TempserverController@temphumidity_live')->name('Tempserver.live');
            Route::get('/tempnhumidity/report', 'IT\MonitoringServer\TempserverController@temphumidityreport_box');
            Route::get('/tempnhumidity/temphumidityreport_table', 'IT\MonitoringServer\TempserverController@temphumidityreport_table');
            Route::get('/tempnhumidity/temphumidityreport_object', 'IT\MonitoringServer\TempserverController@temphumidityreport_object');
            Route::get('/tempnhumidity/temphumidityreport_pdf', 'IT\MonitoringServer\TempserverController@temphumidityreport_pdf');
        }); 
    }); 
    
    Route::prefix('general_affair')->group(function() {
        Route::prefix('asetmanajemen')->group(function() {
            Route::prefix('asetlokasi')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetLokasiController@index')->name('LokasiAset.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetLokasiController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetLokasiController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetLokasiController@partial_form_main');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetLokasiController@store')->name('LokasiAset.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetLokasiController@destroy')->name('LokasiAset.destroy');
            }); 

            Route::prefix('asetjenis')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetJenisController@index')->name('JenisAset.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetJenisController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetJenisController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetJenisController@partial_form_main');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetJenisController@store')->name('JenisAset.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetJenisController@destroy')->name('JenisAset.destroy');
            }); 

            Route::prefix('asetmutasijenis')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@index')->name('MutasiJenisAset.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@partial_form_main');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@store')->name('MutasiJenisAset.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetMutasiJenisController@destroy')->name('MutasiJenisAset.destroy');
            }); 

            Route::prefix('asetdata')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetDataController@index')->name('AsetData.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetDataController@partial_table_main');
                Route::get('/partial_report_main', 'GeneralAffair\ManajemenAset\AsetDataController@partial_report_main');
                Route::post('/table_data', 'GeneralAffair\ManajemenAset\AsetDataController@table_data');
                Route::get('/report_data', 'GeneralAffair\ManajemenAset\AsetDataController@report_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetDataController@partial_form_main');
                Route::get('/get_rmgr', 'GeneralAffair\ManajemenAset\AsetDataController@get_rmgr');
                Route::get('/view_aset', 'GeneralAffair\ManajemenAset\AsetDataController@view_aset');
                Route::get('/view_barcode_pdf', 'GeneralAffair\ManajemenAset\AsetDataController@view_barcode_pdf');
                Route::post('/call_barcode_pdf', 'GeneralAffair\ManajemenAset\AsetDataController@call_barcode_pdf');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetDataController@store')->name('AsetData.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetDataController@destroy')->name('AsetData.destroy');
                Route::post('/getasetkode', 'GeneralAffair\ManajemenAset\AsetDataController@getAsetKode')->name('AsetData.getAsetKode');
                Route::post('/regenerate_barcode', 'GeneralAffair\ManajemenAset\AsetDataController@action_regenerate_barcode')->name('AsetData.regenerate_barcode');
            }); 

            Route::prefix('asetmutasi')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetMutasiController@index')->name('AsetMutasi.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetMutasiController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetMutasiController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetMutasiController@partial_form_main');
                Route::post('/action_approve', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_approve');
                Route::post('/action_cancel', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_cancel');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetMutasiController@store')->name('AsetMutasi.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetMutasiController@destroy')->name('AsetMutasi.destroy');
            }); 
            Route::prefix('asetstokopname')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@index')->name('AsetStokOpname.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@partial_form_main');
                Route::get('/periode/active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@getActive');
                Route::get('/periode/partial_form_main', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@partial_form_main');
                Route::get('/periode/table_data', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@table_data');
                Route::get('/periode/partial_table_main', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@partial_table_main');
                Route::post('/store_periode', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@store')->name('addPeriode.store');
                Route::post('/action_non_active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionNonActive');
                Route::post('/action_active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionActive');
                Route::post('/finish', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionFinish');
                Route::post('/deleteperiode', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@destroy');
                
                Route::post('/action_approve', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_approve');
                Route::post('/action_cancel', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_cancel');
                Route::post('/', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@store')->name('AsetStokname.store');
                Route::delete('/', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@destroy')->name('AsetStokname.destroy');
                
            }); 

            Route::prefix('reportopnameaset')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@index')->name('ReportOpnameAset.index');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@partial_table_main');
                Route::get('/partial_table_report_main', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@partial_table_report_main');
                Route::get('/partial_table_report_final_main', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@partial_table_report_final_main');
                
                // Route::get('/table_data', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@table_data');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@partial_form_main');
                Route::get('/partial_form_final_report_main', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@partial_form_final_report_main');
                // Route::get('/periode/active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@getActive');
                // Route::get('/periode/partial_form_main', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@partial_form_main');
                // Route::get('/periode/table_data', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@table_data');
                // Route::get('/periode/partial_table_main', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@partial_table_main');
                // Route::post('/store_periode', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@store')->name('addPeriode.store');
                // Route::post('/action_non_active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionNonActive');
                // Route::post('/action_active', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionActive');
                // Route::post('/finish', 'GeneralAffair\ManajemenAset\AsetStokOpnamePeriodeController@actionFinish');
                Route::post('/set_nomor_seri', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@setNomorseri')->name('ReportOpname.setNomorseri');
                Route::post('/get_data', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@getData')->name('ReportOpname.getDataReport');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@tableData')->name('ReportOpname.tableData');
                Route::post('/test', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@test')->name('reportData.test');
                Route::post('/getDataReport', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@getDataReport')->name('ReportOpnamePerhitungan.tableData');
                Route::post('/get_lokasi', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@getLokasiInPeriode')->name('ReportOpname.setLokasi');
                Route::post('/get_data_final', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@finalReport')->name('ReportOpname.finalReport');
                
                Route::get('/sendmutasi', 'GeneralAffair\ManajemenAset\ReportOpnameAsetController@sendMutasi');

                // Route::post('/action_approve', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_approve');
                // // Route::post('/action_cancel', 'GeneralAffair\ManajemenAset\AsetMutasiController@action_cancel');
                // // Route::post('/', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@store')->name('AsetStokname.store');
                // Route::delete('/', 'GeneralAffair\ManajemenAset\AsetStokOpnameController@destroy')->name('AsetStokname.destroy');
                
            }); 

            
            Route::prefix('comparestokopname')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\CompareStokopnameController@index')->name('comparestokopname.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\CompareStokopnameController@partial_form_main');
                Route::post('/partial_table_main', 'GeneralAffair\ManajemenAset\CompareStokopnameController@partial_table_main');
                Route::post('/getData', 'GeneralAffair\ManajemenAset\CompareStokopnameController@getData')->name('CompareStokopname.getData');

            });

            Route::prefix('reportmutasiaset')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportMutasiAset@index')->name('reportmutasiaset.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\ReportMutasiAset@partial_form_main');
                Route::post('/partial_table_main', 'GeneralAffair\ManajemenAset\ReportMutasiAset@partial_table_main');
                Route::post('/getData', 'GeneralAffair\ManajemenAset\ReportMutasiAset@getData')->name('reportmutasiaset.getData');

            });

            

            Route::prefix('ticket/allticket')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\AllTicketController@index')->name('alltickets.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\AllTicketController@partial_form_main');
                Route::get('/getkode', 'GeneralAffair\ManajemenAset\AllTicketController@setKodeTicket');
                Route::post('/gettable', 'GeneralAffair\ManajemenAset\AllTicketController@getTable');
                Route::get('/detail/{id}', 'GeneralAffair\ManajemenAset\AllTicketController@detail');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\AllTicketController@partial_table_main');
                Route::post('/', 'GeneralAffair\ManajemenAset\AllTicketController@store')->name('ticket.store');
                Route::post('/data_detail_ticket', 'GeneralAffair\ManajemenAset\AllTicketController@getDataDetail')->name('ticket.detail');
                Route::post('/data_riwayat_ticket', 'GeneralAffair\ManajemenAset\AllTicketController@getRiwayatDetail')->name('ticket.riwayat');
                Route::post('/change_ticket', 'GeneralAffair\ManajemenAset\AllTicketController@statusTicket')->name('status.ticket');
                Route::post('/change', 'GeneralAffair\ManajemenAset\AllTicketController@dataSparepart')->name('sparepart.ticket');
                Route::post('/change_rawmat', 'GeneralAffair\ManajemenAset\AllTicketController@rawmat')->name('rawmat.ticket');
                Route::post('/delete_rm', 'GeneralAffair\ManajemenAset\AllTicketController@delete_rm')->name('rawmat.delete_rm');
                Route::get('/get_rm/{id}', 'GeneralAffair\ManajemenAset\AllTicketController@create_table_rm');
                Route::post('/kondisi_asset', 'GeneralAffair\ManajemenAset\AllTicketController@statusAsset')->name('kondisi.asset');


                //comment
                Route::post('/get_comment', 'GeneralAffair\ManajemenAset\AllTicketController@getComment');
                Route::post('/stored_comment', 'GeneralAffair\ManajemenAset\AllTicketController@storedComment');
                Route::post('/delete_comment', 'GeneralAffair\ManajemenAset\AllTicketController@destroy')->name('delete.coment');

            });

            Route::prefix('ticket/reportticket')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportTicketController@index')->name('reportticket.index');
                Route::post('/partial_table_main', 'GeneralAffair\ManajemenAset\ReportTicketController@partial_table_main');
                Route::post('/getData', 'GeneralAffair\ManajemenAset\ReportTicketController@getData');

            });

            Route::prefix('lelang')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\LelangController@index')->name('lelang.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\LelangController@partial_form_main');
                Route::get('/view_data', 'GeneralAffair\ManajemenAset\LelangController@view_data');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\LelangController@partial_table_main');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\LelangController@destroy')->name('lelang.del');
                Route::post('/', 'GeneralAffair\ManajemenAset\LelangController@store')->name('Lelang.store');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\LelangController@table_data');

            });
            Route::prefix('lelang/tag')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\TagController@index')->name('tag.lelang.index');
                Route::get('/add', 'GeneralAffair\ManajemenAset\TagController@store')->name('tag.lelang.store');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\TagController@destroy');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\TagController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\TagController@table_data');
                
            });

        Route::prefix('reportlelang')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportLelangController@index')->name('report.lelang.index');
                Route::get('/add', 'GeneralAffair\ManajemenAset\TagController@store')->name('tag.lelang.store');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\TagController@destroy');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\TagController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\TagController@table_data');
                
            });

            Route::prefix('budgeting')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\BudgetingController@index')->name('budgeting.index');
                Route::get('/add', 'GeneralAffair\ManajemenAset\BudgetingController@store')->name('budgeting.store');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\BudgetingController@destroy');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\BudgetingController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\BudgetingController@table_data');
                Route::get('/getCode', 'GeneralAffair\ManajemenAset\BudgetingController@getCode');
                Route::get('/getall', 'GeneralAffair\ManajemenAset\BudgetingController@getAll');
                
            });
            Route::prefix('budgeting/category')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\CategoryBudgetingController@index')->name('budgeting.ctg.index');
                Route::get('/add', 'GeneralAffair\ManajemenAset\CategoryBudgetingController@store');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\CategoryBudgetingController@destroy');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\CategoryBudgetingController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\CategoryBudgetingController@table_data');
            });

            Route::prefix('budgeting/detail')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\BarangBudgetingController@index');
                Route::post('/add', 'GeneralAffair\ManajemenAset\BarangBudgetingController@store');
                Route::post('/update', 'GeneralAffair\ManajemenAset\BarangBudgetingController@update');
                Route::get('/delete', 'GeneralAffair\ManajemenAset\BarangBudgetingController@destroy');
                Route::get('/partial_table_main', 'GeneralAffair\ManajemenAset\BarangBudgetingController@partial_table_main');
                Route::get('/table_data', 'GeneralAffair\ManajemenAset\BarangBudgetingController@table_data');
                Route::get('/approve', 'GeneralAffair\ManajemenAset\BarangBudgetingController@approve');
                Route::get('/reject', 'GeneralAffair\ManajemenAset\BarangBudgetingController@reject');
                Route::get('/edit', 'GeneralAffair\ManajemenAset\BarangBudgetingController@edit');
                Route::get('/check', 'GeneralAffair\ManajemenAset\BarangBudgetingController@check');
                Route::get('/getctg', 'GeneralAffair\ManajemenAset\BarangBudgetingController@getCtg');
                Route::get('/getPrdetail', 'GeneralAffair\ManajemenAset\BarangBudgetingController@getPrdetail');
                
            });

            Route::prefix('reportbudgeting')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportBudgetingController@index')->name('reportbudgeting.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\ReportBudgetingController@partial_form_main');
                Route::post('/partial_table_main', 'GeneralAffair\ManajemenAset\ReportBudgetingController@partial_table_main');
                Route::post('/getData', 'GeneralAffair\ManajemenAset\ReportBudgetingController@getData')->name('reportbudgeting.getData');

            });
            Route::prefix('reportbudgetingctg')->group(function() {
                Route::get('/', 'GeneralAffair\ManajemenAset\ReportBudgetingCtgController@index')->name('reportbudgetingctg.index');
                Route::get('/partial_form_main', 'GeneralAffair\ManajemenAset\ReportBudgetingCtgController@partial_form_main');
                Route::post('/partial_table_main', 'GeneralAffair\ManajemenAset\ReportBudgetingCtgController@partial_table_main');
                Route::post('/getData', 'GeneralAffair\ManajemenAset\ReportBudgetingCtgController@getData')->name('reportbudgetingctg.getData');

            });
        });
        
    });

    // Route::prefix('ticket')->group(function() {
    //         Route::prefix('allticket')->group(function() {
    //             Route::get('/', 'Ticket\AllTicket\AllTicketController@index')->name('alltickets.index');
    //             Route::get('/partial_form_main', 'Ticket\AllTicket\AllTicketController@partial_form_main');
    //             Route::get('/getkode', 'Ticket\AllTicket\AllTicketController@setKodeTicket');
    //             Route::post('/gettable', 'Ticket\AllTicket\AllTicketController@getTable');
    //             Route::get('/detail/{id}', 'Ticket\AllTicket\AllTicketController@detail');
    //             Route::get('/partial_table_main', 'Ticket\AllTicket\AllTicketController@partial_table_main');
    //             Route::post('/', 'Ticket\AllTicket\AllTicketController@store')->name('ticket.store');
    //             Route::post('/data_detail_ticket', 'Ticket\AllTicket\AllTicketController@getDataDetail')->name('ticket.detail');
    //             Route::post('/data_riwayat_ticket', 'Ticket\AllTicket\AllTicketController@getRiwayatDetail')->name('ticket.riwayat');
    //             Route::post('/change_ticket', 'Ticket\AllTicket\AllTicketController@statusTicket')->name('status.ticket');


    //             //comment
    //             Route::post('/get_comment', 'Ticket\AllTicket\AllTicketController@getComment');
    //             Route::post('/stored_comment', 'Ticket\AllTicket\AllTicketController@storedComment');
    //             Route::post('/delete_comment', 'Ticket\AllTicket\AllTicketController@destroy')->name('delete.coment');

    //         });
    // }); 

});
