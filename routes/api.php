<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Middleware\CheckTokenMiddleware;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// API APK Stok Take
Route::get('/api_apk_sttake/lokasi', 'API\SUPPORT\ApkStoktakeApiController@api_lokasi');
Route::get('/api_apk_sttake/periode', 'API\SUPPORT\ApkStoktakeApiController@api_stokopname_periode');
Route::post('/api_apk_sttake/save_data_stokopname', 'API\SUPPORT\ApkStoktakeApiController@api_stokopname_save');
Route::get('/api_apk_sttake/data_riwayat', 'API\SUPPORT\ApkStoktakeApiController@api_stokopname_riwayat');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

# Cek token on env
Route::middleware([CheckTokenMiddleware::class])->group(function () {
    Route::prefix('hrm')->group(function () {
        // Bagian
        Route::prefix('bagian')->group(function () {
            Route::get('/v1/asetSelect2', 'API\HRM\BagianApiController@asetSelect2');
            Route::get('/v1/asetSelect2real', 'API\HRM\BagianApiController@asetSelect2real');
        });
    });

    Route::prefix('support')->group(function () {
        // Aset
        Route::prefix('aset')->group(function () {
            Route::get('/v1/allSelect2', 'API\SUPPORT\AsetApiController@allSelect2');
            Route::get('/v1/allSelect2ntmutasi', 'API\SUPPORT\AsetApiController@allSelect2ntmutasi');
            Route::get('/v1/allAsetmutasijenisSelect2', 'API\SUPPORT\AsetApiController@allAsetmutasijenisSelect2');
            Route::get('/v1/allAsetstatusSelect2', 'API\SUPPORT\AsetApiController@allAsetstatusSelect2');
            Route::get('/v1/allAsetjenisSelect2', 'API\SUPPORT\AsetApiController@allAsetjenisSelect2');
            Route::get('/v1/allAsetOpnamejenisSelect2', 'API\SUPPORT\AsetApiController@allAsetOpnamejenisSelect2');
            Route::get('/v1/allAsetOpnamePeriodeSelect2', 'API\SUPPORT\AsetApiController@allAsetOpnamePeriodeSelect2');
            Route::get('/v1/allTag', 'API\SUPPORT\AsetApiController@allTag');
        });
        Route::prefix('lelang')->group(function () {
            Route::post('/v1/store_penawaran', 'API\SUPPORT\LelangApiController@StorePenawaran');
        });
        // lokasi
        Route::prefix('/lokasi')->group(function () {
            Route::get('/v1/allSelect2', 'API\SUPPORT\LokasiApiController@allSelect2');
        });

        Route::prefix('/getbudgeting')->group(function () {
            Route::get('/v1/getAllbudgeting', 'API\SUPPORT\LokasiApiController@allSelect2');
        });

        Route::prefix('/budgeting')->group(function () {
            Route::get('/v1/getAllDetailbudgeting', 'API\SUPPORT\BudgetingApiController@allDetailBudgeting');
        });
    });
    Route::prefix('ppic')->group(function () {
        
    });
});

Route::middleware(['cors'])->group(function () {
    Route::get('/getTicketActive', 'GeneralAffair\ManajemenAset\AllTicketController@allTicketActive');
});
